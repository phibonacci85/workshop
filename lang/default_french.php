<?php
/*
	created on 10/22/12 JK
	cory chambers 7-18-13 added nextreviewdate to medical direction section
*/
class Default_French
{	
//languages
	function lang_french() {return 'Français';}
	function lang_english() {return 'Anglais';}
   
	function locallang_french() {return 'Français';}
	function locallang_english() {return 'English';}
   
	function language() {return 'Language';}

	//documents
	function doc_usermanual() {return 'documents/R4R_User_Manual.pdf';}
	function doc_adminusermanual() {return 'documents/Rescue_One_Manual_Administration_Access_Details.pdf';}

    function doc_blankaedcheck()
    {
        return 'documents/Verification_DEA-Nouveau.pdf';
    }
	function doc_trainingtemplate() {return 'documents/Francais-Template-de-Formation.xls';}
	function doc_eventform() {return 'documents/AED-DEA-Post-Rescue -Medical-Report-2015.pdf';}
	function doc_recallhistory() {return 'documents/2014-01-01_industry-recall-history.doc';}

    function doc_blankservicecheck()
    {
        return 'documents/Verification_d_entretien_pour_DEA.pdf';
    }
	function doc_azstateform() {return 'documents/aed_use_form.doc';}
	
	//general headings
	function nav_home() {return 'Accueil';}
	function nav_profile() {return 'Profil';}
	function nav_message() {return 'Centre de messages';}
	function nav_aed() {return 'DEA';}
	function nav_masteraed(){return'Maître DEA';}
	function nav_equipment() {return 'Équipement';}
	function nav_training() {return 'Formation';}
	function nav_licenses() {return 'Licenses';}
	function nav_immunizations() {return 'Immunizations';}
	function nav_hazards() {return 'Dangers';}
	function nav_contact() {return 'Contacts';}
	function nav_firstvoice() {return 'First Voice';}
	function nav_persons() {return 'Personnes';}
	function nav_documents() {return 'Documents';}
	function nav_facility() {return 'Renseignements sites';}
	function nav_reports() {return 'Rapports';}
	function nav_help() {return 'Aide';}

    function nav_store()
    {
        return 'Boutique';
    }
	function nav_admin() {return 'Administrateur';}
	function nav_users() {return 'Utilisateurs';}
	function nav_tracing() {return 'Tracing';}
	function nav_setup() {return 'Installation';}
	function nav_organizations() {return 'Organisations';}
	function nav_keycodes() {return 'Codes-clés';}
	function nav_alerts() {return 'Alertes';}
	function nav_scheduler() {return 'Planificateur';}
	function nav_aedcheck() {return 'Vérification DEA';}
	function nav_aedservicing() {return 'Entretien de DEA';}
	function nav_erp() {return 'PRU';}
	function nav_medicaldirection() {return 'Directeur médical';}

    function nav_inactivemedicaldirection()
    {
        return 'Direction m�dicale inactif';
    }
	function nav_events() {return 'Evénements';}
	function nav_recalls() {return 'Rappels et entretien';}
	function nav_statelaws() {return 'State Laws';}
	function nav_login() {return 'Entrer';}
	function nav_logout() {return 'Sortie';}
	function nav_organization() {return 'Organisation';}
	function nav_location() {return 'Emplacement';}
	function nav_welcome() {return 'Bienvenue';}
	function sel_yes() {return 'Oui';}
	function sel_no() {return 'Non';}
	function sel_all() {return 'Tous';}
	function sel_one() {return 'Sélectionner une option';}
	function sel_aed() {return 'DEA';}
	function sel_pad() {return 'Coussinets électrode';}
	function sel_pak() {return 'Nécessaires de rechange et accessoires';}
	function sel_na() {return 'N/A';}
	function sel_battery() {return 'Piles';}
	function sel_none() {return 'Aucun';}
	function hom_version() {return 'Version';}

    function hom_nojs()
    {
        return 'Vous ne devez pas activer Javascript . S\'il vous pla�t activer Javascript pour la voir correctement le site .';
    }

    function nolocation()
    {
        return 'Il n\'y a aucun emplacement pour cette organisation .';
    }
   
	//buttons
	function but_new() {return 'Nouveau';}
	function but_edit() {return 'Modifier';}
	function but_delete() {return 'Effacer';}
	function but_submit() {return 'Envoyer';}
	function but_back() {return 'Retour';}
	function but_previous() {return 'Précédent';}
	function but_next() {return 'Prochaine';}
	function but_change() {return 'Modifier';}
	function but_newmessage() {return 'Nouveau message';}
	function but_send() {return 'Envoyer';}
	function but_fill() {return 'Remplir';}
	function but_newevent() {return 'Evenement-Nouveau';}
	function but_newaedcheck() {return 'Vérification DEA-Nouveau';}
	function but_clearthis() {return 'Desactivez Cette';}
	function but_clearall() {return 'Effacer Tout';}
   
	//hazards
	function haz_name() {return 'Type';}
	function haz_type() {return 'Catégorie';}
	function haz_location() {return 'Emplacement';}
	function haz_notes() {return 'Remarques';}
	function haz_heading() {return 'Dangers';}
	function haz_newhazard() {return 'Danger -Nouveau';}
	function haz_edithazard() {return 'Modifier Dangers';}
	function haz_deletehazard() {return 'Delete Hazard';}
	function haz_success_new() {return 'Votre danger a été ajouté.';}
	function haz_confirm_delete() {return 'Êtes-vous sûr de vouloir effacer ce danger?';}
	function haz_success_delete() {return 'Votre danger a été effacé.';}
	function haz_success_edit() {return 'Votre danger a été modifié.';}
	function haz_nohazard() {return 'Il n’y a aucun danger à cet emplacement à l’heure actuelle';}
	function haz_none() {return 'Il n’y a aucun danger à cet emplacement à l’heure actuelle';}
	
	//view
    function vie_set()
    {
        return 'd&#233;finir la vue';
    }

    function vie_remove()
    {
        return 'Retirer Voir';
    }

	//alerts
	function ale_invalidemail() {return 'Vous devez entrer une adresse électronique valide.';}
	function ale_notset() {return 'Non défini';}
	function ale_heading() {return 'Alertes';}
	function ale_newalert() {return 'Alertes-Nouveau';}
	function ale_editalert() {return 'Modifier Alertes';}

    function ale_deletealert()
    {
        return 'Supprimer Alerte';
    }
	function ale_delete() {return 'Effacer';}
	function ale_level() {return 'Niveau';}
	function ale_email() {return 'Courriel';}
	function ale_type() {return 'Type';}
	function ale_showtype($type) {
		switch($type){
			case 'all':
				return 'All';
			case 'aedservicing':
				return 'Entretien de l\'DEA';
			case 'aedcheck':
				return 'Vérification DEA';
			case 'aed':
				return 'DEA Pads et Batteries';
			case 'equipmentcheck':
				return 'Vérification de l\'équipement';
			case 'equipment':
				return 'équipement contenu';
			case 'firstvoicecheck':
				return 'Vérification First Voice';
			case 'firstvoice':
				return 'Contenu First Voice';
			case 'keycode':
				return 'Code Clé';
			case 'medicaldirection':
				return 'Direction médicale';
			case 'training':
				return 'Formation';
			default:
				return $type;
		}
	}
	function ale_success_new() {return 'Votre alerte a été ajoutée.';}
	function ale_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cet emplacement?';}
	function ale_success_delete($email, $level) {return $email.' a été effacé du niveau '.$level.'.';}
	function ale_success_edit() {return 'Vos alertes ont été modifiées.';}
	function ale_noalertsfound() {return 'Il n\'y a aucune alerte pour ce lieu.';}
   
	//keycodes
	function sel_pool() {return 'Piscine';}
	function key_type() {return 'Type';}
	function key_picktype($type) {
		switch($type) {
			case 'standard':
                return 'question de la norme';
			default:
				return $type;
		}		
	}
	function key_demo() {return 'Démo';}
	function key_length() {return 'Longueur (en jours)';}
	function key_amount() {return 'Montant';}
	function key_organization() {return 'Organisation';}
	function key_typeofkeycode() {return 'Type de mot de code';}
	function sel_typeofkeycode($count, $expiration) {return $count.' codes d\'activation qui ont une longueur de '.$expiration.' days.';}
	function key_keycode() {return 'Codes-clé';}
	function key_aedserialnum() {return 'Numéro série DEA';}
	function key_expiration() {return 'Expiration';}
	function key_insertedby() {return 'Inséré par';}
	function key_usedkeycodes() {return 'Codes-clés utilisés';}
	function key_unusedkeycodes() {return 'Codes-clés non-utilisés';}
	function key_heading() {return 'Codes-clés';}
	function key_newkeycode() {return 'Code-clé-Nouveau';}
	function key_generatekeycode() {return 'Générer Code Clé';}
	function key_assignkeycode() {return 'Assigner Code Clés';}
	function key_assignpool() {return 'Assigner Piscine';}
	function key_deletepool() {return 'Supprimer de Piscine';}
	function key_deletekeycode() {return 'Effacer Codes-clé';}
	function key_nounusedkeycodes() {return 'Il n’y a pas de codes-clés inutilisés dans cette organisation.';}
	function key_nousedkeycodes() {return 'Il n’y a pas de codes-clés utilisés dans cette organisation.';}
	function key_invalid_assignamount() {return 'Vous ne disposez pas que beaucoup de codes d\'activation à attribuer.';}
	function key_invalid_deleteamount() {return 'Vous ne disposez pas que beaucoup de codes d\'activation à supprimer.';}
	function key_success_new($keycode) {return 'Le code-clé '.$keycode.' a été entré dans le programme.';}
	function key_success_generate($num) {return 'Votre '.$num.' Codes-clé ont été générés avec succès.';}
	function key_success_assign($num, $organization) {return 'Vous avez affecté avec succès '.$num.' Codes-clé to '.$organization.'.';}
	function key_success_deletepool($num, $type) {return 'Vous avez supprimé avec succès '.$num.' Codes-clé with a length of '.$type.' days.';}
    function key_none() {return 'Il n\'y a pas de codes-clé pour cette organisation.';}
   
	//organizations
	function org_heading() {return 'Organisations';}
	function org_new() {return 'De nouvelles organisations';}
	function org_delete() {return 'Supprimer Organisation';}
	function org_edit() {return 'Modifier Organisation';}
	function org_orgname() {return 'Nom De L\'Organisation';}
	function org_orgtype() {return 'Type d\'organisation';}
	function org_internal() {return 'Interne';}
	function org_external() {return 'Externe';}
	function org_success_new() {return 'Votre organisation a été ajouté avec succès.';}
	function org_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cette organisation?';}
	function org_success_delete() {return 'Votre organisation a été supprimé avec succès.';}
	function org_success_edit() {return 'Votre organisation a été changé avec succès.';}
	function org_clientnumber() {return 'Numéro de client';}
	function org_streetaddress() {return 'Adresse De Rue';}
	function org_mailingaddress() {return 'Adresse';}
	function org_city() {return 'Ville';}
	function org_state() {return 'Province';}
	function org_zip() {return 'Code postal';}
	function org_phone() {return 'Téléphone';}
	function org_status() {return 'Status';}
	function org_pickstatus($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'active':
				return 'Actif';
			case 'closed':
				return 'Société fermée';
			case 'subdis':
				return 'Sous-distributeur';
			case 'noaeds':
				return 'Ne plus avoir DEA';
			default:
				return 'ERROR UNKNOWN STATUS:'.$type;
		}
	}
	function org_mainbillcontact() {return 'Facturation principal Contact';}
	function org_mainbillcontacttitle() {return 'Titre';}
	function org_mainbillcontactphone() {return 'Téléphone';}
	function org_mainbillcontactemail() {return 'Courriel';}

    function org_mainbillcontactfax()
    {
        return 'Telecopier';
    }
	function org_mainmailcontact() {return 'Livraison Contact';}
	function org_mainmailcontacttitle() {return 'Titre';}
	function org_mainmailcontactphone() {return 'Téléphone';}
	function org_mainmailcontactemail() {return 'Courriel';}

    function org_mainmailcontactfax()
    {
        return 'Telecopier';
    }
	function org_notes() {return 'Remarques';}
	function org_none() {return 'Il n\'y a pas les organisations sur ce filtre.';}
	
	//locations
	function loc_new() {return 'Nouvel emplacement';}
	function loc_delete() {return 'Supprimer Localisation';}
	function loc_edit() {return 'Modifier Situation';}
	function loc_success_new() {return 'Votre emplacement a été ajouté avec succès.';}
	function loc_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cet emplacement?';}
	function loc_success_delete() {return 'Votre emplacement a été supprimé avec succès.';}
	function loc_success_edit() {return 'Votre emplacement a été modifié avec succès.';}
	function loc_name() {return 'Lieu Nom';}
	function loc_address() {return 'Adresse civique';}
	function loc_city() {return 'Ville';}
	function loc_state() {return 'Province';}
	function loc_country() {return 'Pays';}
	function loc_phone() {return 'Téléphone';}
	function loc_status() {return 'Status';}
	function loc_pickstatus($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'active':
				return 'Actif';
			case 'closed':
				return 'Closed Endroit';
			case 'lost':
				return 'Perdu';
			case 'moved':
				return 'Déplacé';
			case 'noaeds':
				return 'Ne plus avoir DEA';
			case 'storage':
				return 'Stockage';
			case 'subdis':
				return 'Sous-distributeur';
			default:
				return 'ERROR UNKNOWN STATUS:'.$type;
		}
	}
	function loc_region() {return 'Région';}
	function loc_zip() {return 'Code postal';}

    function loc_fax()
    {
        return 'Telecopier';
    }
	function loc_website() {return 'Site Web';}
	function loc_mainlocation() {return 'Localisation principale';}
	function loc_shippingcontact() {return 'Livraison Contact';}
	function loc_shippingcontacttitle() {return 'Livraison Contactez-titre';}
	function loc_shippingcontactphone() {return 'Livraison Contact Téléphone';}
	function loc_shippingcontactemail() {return 'Frais de port Contact Courriel';}

    function loc_shippingcontactfax()
    {
        return 'Livraison Contact Telecopier';
    }
	function loc_billingcontact() {return 'Contact de facturation';}
	function loc_billingcontacttitle() {return 'Contact de facturation Titre';}
	function loc_billingcontactphone() {return 'Contact de facturation Téléphone';}
	function loc_billingcontactemail() {return 'Facturation Contact Courriel';}

    function loc_billingcontactfax()
    {
        return 'Contact de facturation Telecopier';
    }
	function loc_sitecontact() {return 'Site Contactez';}
	function loc_safetydirector() {return 'Directeur de la sécurité';}
	function loc_repname() {return 'Nom du représentant';}
	function loc_notes() {return 'Remarques';}
	function loc_none() {return 'Il n\'y a pas de lieux pour cette organisation.';}
   
		//tracing	
	function trc_heading() {return 'Traçant';}
	function trc_new_heading() {return 'Inbound Traçant';}
	function trc_new_success() {return 'Vous venez d\'ajouter nouveau tracé.';}
	function trc_edit_heading() {return 'Modifier Tracant';}
	function trc_edit_success() {return 'Vous avez édité avec succès ce tracé.';}
	function trc_edit_invalid() {return 'Vous devez remplir tous les champs nécessaires pour modifier ce tracé.';}
	function trc_delete_heading() {return 'Supprimer traçant';}
	function trc_delete_confirm() {return 'Êtes-vous sûr de vouloir supprimer ce tracé?';}
	function trc_delete_success() {return 'Vous avez supprimé avec succès ce tracé.';}
	function trc_delete_invalid() {return 'Vous ne pouvez pas supprimer ce tracé.';}
	function trc_assign_heading() {return 'Assigner';}
	function trc_assign_success() {return 'Vous avez ajouté que le traçage d\'un DEA.';}
	function trc_assign_invalid() {return 'Vous devez remplir tous les champs nécessaires pour ajouter cette traçage comme un DEA.';}
	function trc_assign_none() {return 'Il n\'y a pas tracés non affectés à ajouter.';}
	function trc_noadd() {return 'Ne pas ajouter ce traçage';}
	function trc_more() {return 'Plus';}
	function trc_inboundaed() {return 'Inbound DEA';}
	function trc_inboundaccessory() {return 'Inbound accessoire';}
	function trc_location() {return 'Emplacement';}
	function trc_assign() {return 'assigner';}
	function trc_brand() {return 'Marque';}
	function trc_model() {return 'Modèle';}
	function trc_serialnumber() {return 'Numéro De Série';}
	function trc_serialnumbers() {return 'Numéros de série';}
	function trc_serialnumbers_directions() {return '(S\'il vous plaît entrer tous les numéros de série avec soit un espace, retour, ou une virgule séparatrice)';}
	function trc_serial() {return 'en série';}
	function trc_invoicedate() {return 'Date de la facture';}
	function trc_invoicenumber() {return 'Numéro De Facture';}
	function trc_datereceived() {return 'Date de réception';}
	function trc_shipto() {return 'Envoyer À';}
	function trc_battery() {return 'Batterie';}
	function trc_pad() {return 'Pad';}
	function trc_accessory() {return 'Accessoire';}
	function trc_accessories() {return 'Accessoires';}
	function trc_accessoryname() {return 'Nom';}
	function trc_accessorytype() {return 'Type';}
	function trc_accessorylot() {return 'Numéro de lot';}
	function trc_accessoryexp() {return 'Expiration';}
	function trc_none() {return 'Il n\'y a pas encore tracés enregistrés.';}
	
	//setup
	function set_heading($type) {switch($type){
		case 'training': return 'Configuration de Formation'; break; 
		case 'hazards': return 'Configuration des Dangers'; break; 
		case 'documents': return 'Configuration des documents'; break; 
		case 'immunizations': return 'Configuration de Vaccinations'; break; 
		case 'licenses': return 'Configuration de licenses'; break;
        case 'equipment':
            return 'Configuration de l\'équipement';
            break;
        case 'firstvoice':
            return 'Configuration de First Voice';
            break;
        default:
            return 'error';
    }
    }

	//setup hazards
	function set_hazardtypes() {return 'Types de danger';}
	function set_addhazardtypes() {return 'Ajouter les types de danger';}
	function set_hazards() {return 'Dangers';}
	function set_hazard_invalid_newhazardtype($type, $language) {return 'Le type de danger \''.$type.'\' pour '.$language.' est déjà dans le programme.';}
	function set_hazard_invalid_newhazardtypelang($language) {return 'Vous devez remplir le '.$language.' lors d\'un nouveau type de danger.';}
	function set_hazard_success_newhazardtype($type, $language) {return 'Le type de danger \''.$type.'\' pour '.$language.' ont été ajoutées au programme.';}
	function set_hazard_success_deletehazardtype($type, $language) {return 'Votre type de danger, '.$type.', pour '.$language.'  a été supprimé.';}
	function set_hazard_success_edithazardtype($type, $language) {return 'Votre type de danger, '.$type.', pour '.$language.'  a été changé avec succès.';}
	
	//setup immunizations
	function set_immunizationtypes() {return 'Types de vaccination';}
	function set_addimmunizationtypes() {return 'Ajouter Type de vaccination';}
	function set_immunizations() {return 'Vaccinations';}
	function set_immunization_newheading() {return 'Nouveau vaccination';}
	function set_immunization_editheading() {return 'Modifier la vaccination';}
	function set_immunization_deleteheading() {return 'Supprimer la vaccination';}
	function set_immunization_name() {return 'Nom';}
	function set_immunization_nextimmunization() {return 'Suivant vaccination';}
	function set_immunization_alerts() {return 'Alertes';}
	function set_immunization_employee() {return 'Employé';}
	function set_immunization_supervisor() {return 'Superviseur';}
	function set_immunization_email() {return 'Courriel';}
	function set_immunization_alerton() {return 'Sur alerte';}
	function set_immunization_daysafter() {return 'Jours Après';}
	function set_immunization_pickstatus($status) {
		switch($status)
		{
			case 'completed':
				return 'Terminé';break;
			case 'declined':
				return 'Refusée';break;
			case 'negative':
				return 'Négatif';break;
			case 'offer':
				return 'Offre';break;
			case 'other':
				return 'Autre';break;
			default:
				return $status;break;
		}
	}
	function set_immunization_subject() {return 'Sujet';}
	function set_immunization_body() {return 'Body';}
	function set_immunization_attone() {return 'Annexe 1';}
	function set_immunization_atttwo() {return 'Annexe 2';}
	function set_immunization_attthree() {return 'Annexe 3';}
	function set_immunization_invalid_newimmunizationtype($type, $language) {return 'Le type de vaccination \''.$type.'\' pour '.$language.' est déjà dans le programme.';}
	function set_immunization_invalid_newimmunizationtypelang($language) {return 'Vous devez remplir le '.$language.' lors d\'un nouveau type de vaccination.';}
	function set_immunization_success_newimmunizationtype($type, $language) {return 'Le type de vaccination \''.$type.'\' pour '.$language.' ont été ajoutées au programme.';}
	function set_immunization_success_deleteimmunizationtype($type, $language) {return 'Votre type de vaccination, '.$type.', pour '.$language.'  a été supprimé.';}
	function set_immunization_success_editimmunizationtype($type, $language) {return 'Votre type de vaccination, '.$type.', pour '.$language.'  a été changé avec succès.';}
	
	//setup documents
	function set_documenttypes() {return 'Types de documents';}
	function set_adddocumenttypes() {return 'Ajouter Type de document';}
	function set_documents() {return 'Documents';}
	function set_document_newheading() {return 'Nouveau document';}
	function set_document_editheading() {return 'Modifier le document';}
	function set_document_deleteheading() {return 'Supprimer le document';}
	function set_document_delete_confirm() {return 'Êtes-vous sûr de vouloir supprimer cette licence?';}
	function set_document_name() {return 'Nom';}
	function set_document_category() {return 'Catégorie';}
	function set_document_compliance() {return 'Conformité';}
	function set_document_notes() {return 'Remarques';}
	function set_document_alerts() {return 'Alertes';}
	function set_document_employee() {return 'Employé';}
	function set_document_supervisor() {return 'Superviseur';}
	function set_document_email() {return 'Courriel';}
	function set_document_subject() {return 'Sujet';}
	function set_document_body() {return 'Body';}
	function set_document_attone() {return 'Annexe 1 ';}
	function set_document_atttwo() {return 'Annexe 2 ';}
	function set_document_attthree() {return 'Annexe 3 ';}
	function set_document_pickcategory($cat) {switch($cat){
		case 'Audit': return 'Audit'; break;
		case 'Audit/Inspection': return 'Audit/Inspection'; break;
		case 'Document': return 'Document'; break;
		case 'Document Review': return 'Examen de la documentation'; break;
		case 'Govt Report': return 'Rapport du gouvernement'; break;
		case 'Permits': return 'Permis'; break;
		case 'Procedures': return 'Procédures'; break;
		case 'Report': return 'Rapport'; break;
		case 'Safety Log': return 'Connexion sécurité'; break;
		case 'Site Visit': return 'Visite Du Site'; break;
		case 'Training Event': return 'Formation de l\'événement'; break;
		case 'Other': return 'Autre'; break; }}
	function set_document_alerton() {return 'Sur alerte';}
	function set_document_pickstatus($status) {
		switch($status)
		{
			case 'uncompleted':
				return 'Inachevé';
			case 'completed':
				return 'Terminé';
			default:
				return $status;
		}
	}
	function set_document_daysafter() {return 'Jours Après';}
	function set_document_invalid_newdocumenttype($type, $language) {return 'Le type de document \''.$type.'\' pour '.$language.' est déjà dans le programme.';}
	function set_document_invalid_newdocumenttypelang($language) {return 'Vous devez remplir le'.$language.' lors d\'un nouveau type de document.';}
	function set_document_success_newdocumenttype($type, $language) {return 'Le type de document \''.$type.'\' pour '.$language.' ont été ajoutées au programme.';}
	function set_document_success_deletedocumenttype($type, $language) {return 'Votre type de document, '.$type.', pour '.$language.'  a été supprimé.';}
	function set_document_success_editdocumenttype($type, $language) {return 'Votre type de document, '.$type.', pour '.$language.' a été changé avec succès.';}
	function set_document_noalerts() {return 'Il n\'y a aucune alerte pour ce type de document.';}
	
	//setup licenses
	function set_licensestypes() {return 'Types de licences';}
	function set_addlicensestypes() {return 'Ajouter Type de licence';}
	function set_licensess() {return 'Licenses';}
	function set_licenses_newheading() {return 'Nouvelles licences';}
	function set_licenses_editheading() {return 'Modifier Licence';}
	function set_licenses_deleteheading() {return 'Supprimer licence';}
	function set_licenses_delete_confirm() {return 'Êtes-vous sûr de vouloir supprimer cette licence?';}
	function set_licenses_name() {return 'Nom';}
	function set_licenses_ceu_ceusneeded() {return 'CEUs (Crédits) nécessaires';}
	function set_licenses_ceu_ceu() {return 'CEU';}
	function set_licenses_ceu_code() {return 'Code';}
	function set_licenses_ceu_safety() {return 'Sécurité';}
	function set_licenses_ceu_discipline() {return 'Discipline';}
	function set_licenses_notes() {return 'Remarques';}
	function set_licenses_alerts() {return 'Alertes';}
	function set_licenses_employee() {return 'Employé';}
	function set_licenses_supervisor() {return 'Superviseur';}
	function set_licenses_email() {return 'Courriel';}
	function set_licenses_alerton() {return 'Sur alerte';}
	function set_licenses_pickstatus($status) {
		switch($status)
		{
			case 'inactive':
				return 'Inactif';
			case 'active':
				return 'Actif';
			default:
				return $status;
		}
	}
	function set_licenses_daysafter() {return 'Jours Après';}
	function set_licenses_subject() {return 'Sujet';}
	function set_licenses_body() {return 'Body';}
	function set_licenses_attone() {return 'Annexe 1';}
	function set_licenses_atttwo() {return 'Annexe 2';}
	function set_licenses_attthree() {return 'Annexe 3';}
	function set_licenses_invalid_newlicensestype($type, $language) {return 'Le type de licences \''.$type.'\' pour '.$language.' est déjà dans le programme.';}
	function set_licenses_invalid_newlicensestypelang($language) {return 'Vous devez remplir le '.$language.' lors d\'un nouveau type de licences.';}
	function set_licenses_success_newlicensestype($type, $language) {return 'Le type de licences \''.$type.'\' pour '.$language.' ont été ajoutées au programme.';}
	function set_licenses_success_deletelicensestype($type, $language) {return 'Votre type de licences, '.$type.', pour '.$language.'  a été supprimé.';}
	function set_licenses_success_editlicensestype($type, $language) {return 'Votre type de licences, '.$type.', pour '.$language.' a été changé avec succès.';}
	function set_licenses_noalerts() {return 'Il n\'y a aucune alerte pour ce type de licences.';}
	
	//setup training
	function set_trainingcourses() {return 'Cours de formation';}
	function set_training_newheading() {return 'Nouveau Cours de formation';}
	function set_training_editheading() {return 'Modifier Cours de formation';}
	function set_training_deleteheading() {return 'Supprimer Cours de formation';}
	function set_training_delete_confirm() {return 'Êtes-vous sûr de vouloir supprimer ce cours de formation?';}
	function set_addtrainingcourses() {return 'Ajouter Cours de formation';}
	function set_training() {return 'Formation';}
	function set_trainingprints() {return 'Affiches de formation';}
	function set_training_name() {return 'Nom';}
	function set_training_cardname() {return 'Nom de la carte';}
	function set_training_notes() {return 'Remarques';}
	function set_training_printable() {return 'Imprimable';}
	function set_training_ceu_ceusgiven() {return 'CEUs (Crédits) Compte tenu';}
	function set_training_ceu_ceu() {return 'CEU';}
	function set_training_ceu_code() {return 'Code';}
	function set_training_ceu_safety() {return 'Sécurité';}
	function set_training_ceu_discipline() {return 'Discipline';}
	function set_training_course() {return 'Modifier Sous-catégorie';}
	function set_training_printsnumber() {return 'Nombre de tirages';}
	function set_training_printsnewtotal() {return 'Nouveaux total Prints';}
	function set_training_success_printcards() {return 'Le total de vos impressions ont été modifiés avec succès.';}
	function set_training_invalid_newtrainingcourse($type, $language) {return 'Le cours de formation \''.$type.'\' pour '.$language.' est déjà dans le programme.';}
	function set_training_invalid_newtrainingcourselang($language) {return 'Vous devez remplir le '.$language.' lors d\'un nouveau cours de formation.';}
	function set_training_success_newtrainingcourse($type, $language) {return 'Le cours de formation \''.$type.'\' pour '.$language.' ont été ajoutées au programme.';}
	function set_training_success_deletetrainingcourse($type, $language) {return 'Votre cours de formation, '.$type.', pour '.$language.'  a été supprimé.';}
	function set_training_success_edittrainingcourse($type, $language) {return 'Votre cours de formation, '.$type.', pour '.$language.'  a été changé avec succès.';}
	function set_training_success_edit() {return 'Votre cours de formation a été modifié avec succès.';}
	
	//setup equipment
	function set_equipment_addequipmentcontents() {return 'Ajouter Equipment Details';}
	function set_equipment_addequipmentquestions() {return 'Ajouter Équipement question';}
	function set_equipment() {return 'Equipment';}
	function set_equipment_newcat() {return 'Nouvelle catégorie';}
	function set_equipment_editcat() {return 'Modifier une catégorie';}
	function set_equipment_deletecat() {return 'Supprimer Catégorie';}
	function set_equipment_newsubcat() {return 'Nouvelle catégorie';}
	function set_equipment_editsubcat() {return 'Modifier Sous-catégorie';}
	function set_equipment_deletesubcat() {return 'Supprimer Sous-catégorie';}
	function set_equipment_type() {return 'Type';}
	function set_equipment_content() {return 'Content';}
	function sel_equipment_yesno() {return 'Oui/Non';}
	function sel_equipment_text() {return 'Texte';}
	function sel_equipment_picture() {return 'Image';}
	function sel_equipment_pdf() {return 'PDF';}
	function sel_equipment_expiration() {return 'Expiration';}
	function set_equipment_emailalerts() {return 'Courriel Alertes';}
	function set_equipment_name() {return 'Nom';}
	function set_equipment_checkfreq() {return 'Arrivée Fréquence (jours)';}
	function set_equipment_levelone() {return 'Niveau 1 (jours)';}
	function set_equipment_leveltwo() {return 'Niveau 2 (jours)';}
	function set_equipment_levelthree() {return 'Niveau 3 (jours)';}
	function set_equipment_alerts() {return 'Alertes';}
	function set_equipment_details() {return 'Détails';}
	function set_equipment_checkquestions() {return 'Vérifiez questions';}
	function set_equipment_confirm_deletecat() {return 'Êtes-vous sûr de vouloir supprimer cette catégorie de l\'équipement?';}
	function set_equipment_success_deletecat() {return 'Votre catégorie d\'équipement a été supprimé.';}
	function set_equipment_invalid_newsubcat() {return 'Cette sous-catégorie de l\'équipement est déjà dans le programme.';}
	function set_equipment_confirm_deletesubcat() {return 'Êtes-vous sûr de vouloir supprimer cet équipement sous-catégorie?';}
	function set_equipment_success_deletesubcat() {return 'Votre sous-catégorie de l\'équipement a été supprimé.';}
	function set_equipment_success_newdetails($name, $language) {return 'Vos détails d\'équipement de firstvoice, '.$name.', pour '.$language.' a été ajouté avec succès.';}
	function set_equipment_success_deletecheckquestion($name, $language) {return 'Votre vérification de l\'équipement question, '.$name.', pour '.$language.' a été supprimé.';}
	function set_equipment_success_editcheckquestion($name, $language) {return 'Votre vérification de l\'équipement question, '.$name.', pour '.$language.' a été changé avec succès.';}
	function set_equipment_success_deletedetails($name, $language) {return 'Vos détails d\'équipement, '.$name.', pour '.$language.' a été supprimé.';}
	function set_equipment_success_editdetails($name, $language) {return 'Vos détails l\'équipement, '.$name.', pour '.$language.' a été changé avec succès.';}
	function set_equipment_success_editsubcatname($type, $language) {return 'Le nom de la sous-catégorie de l\'équipement, '.$type.', pour '.$language.'  a été changé avec succès.';}
	function set_equipment_invalid_editsubcatname($type, $language) {return 'Le nom de la sous-catégorie de l\'équipement '.$type.', pour '.$language.'  ne peut toutefois pas être changé.';}
	function set_equipment_success_editcheckfreq() {return 'Votre fréquence de vérification personnalisée a été changé avec succès.';}
	function set_equipment_invalid_editcheckfreq() {return 'Votre fréquence de vérification personnalisée ne pouvait pas être changé. Vous devez entrer un numéro.';}
	function set_equipment_success_editlevelone() {return 'Votre niveau personnalisé une alerte a été changé avec succès.';}
	function set_equipment_invalid_editlevelone() {return 'Votre niveau personnalisé seule alerte ne pouvait pas être changé. Vous devez entrer un numéro.';}
	function set_equipment_success_editleveltwo() {return 'Votre niveau personnalisé de deux alerte a été changé avec succès.';}
	function set_equipment_invalid_editleveltwo() {return 'Votre niveau d\'alerte personnalisé deux ne pouvait pas être changé. Vous devez entrer un numéro.';}
	function set_equipment_success_editlevelthree() {return 'Votre niveau personnalisé de trois alerte a été changé avec succès.';}
	function set_equipment_invalid_editlevelthree() {return 'Votre niveau personnalisé de trois alerte ne pouvait pas être changé. Vous devez entrer un numéro.';}
	function set_equipment_success_newalerts() {return 'Vos alertes personnalisées ont été réalisés avec succès.';}
	function set_equipment_invalid_newalerts() {return 'Vous devez remplir la fréquence de contrôle, le niveau un, niveau deux et à trois niveaux lors de votre première édition.';}
	function set_equipment_success_newcat($name, $language) {return 'Votre nouvelle catégorie de l\'équipement, '.$name.', pour '.$language.' ont été ajoutées au programme.';}
	function set_equipment_success_editcat($name, $language) {return 'Votre catégorie d\'équipement, '.$name.', pour '.$language.' ont été ajoutées au programme.';}
	function set_equipment_success_newsubcat($name, $language) {return 'Votre nouvelle sous-catégorie de l\'équipement, '.$name.', pour '.$language.' has been added to the program.';}
	function set_equipment_success_editsubcat($name, $language) {return 'Votre sous-catégorie de l\'équipement, '.$name.', pour '.$language.' a été changé avec succès.';}
	function set_equipment_success_newcatandsubcat() {return 'Votre nouvelle catégorie et sous-catégorie de l\'équipement a été ajouté au programme.';}
	function set_equipment_contenttype($type) {
		switch($type)
		{
			case 'text':
				return $this->sel_equipment_text();
				break;
			case 'expiration':
				return $this->sel_equipment_expiration();
				break;
			case 'picture':
				return $this->sel_equipment_picture();
				break;
			case 'pdf':
				return $this->sel_equipment_pdf();
				break;
			default: return $type;
		}
	}
	function set_equipment_questiontype($type) {
		switch($type)
		{
			case 'text':
				return $this->sel_equipment_text();
				break;
			case 'expiration':
				return $this->sel_equipment_expiration();
				break;
			case 'picture':
				return $this->sel_equipment_picture();
				break;
			case 'pdf':
				return $this->sel_equipment_pdf();
				break;
			default: return $type;
		}
	}
	function set_equipment_success_newcontent($type, $language) {return 'Votre détail d\'équipement \''.$type.'\' pour '.$language.' a été ajouter au programme.';}
	function set_equipment_success_newquestion($type, $language) {return 'Votre question de l\'équipement \''.$type.'\' pour '.$language.' a été ajouter au programme.';}
	
	//setup firstvoice
	function set_firstvoice_addequipmentcontents() {return 'Ajouter Équipement détails';}
	function set_firstvoice_addequipmentquestions() {return 'Ajouter Équipement question';}
	function set_firstvoice() {return 'First Voice';}
	function set_firstvoice_newcat() {return 'Nouvelle catégorie';}
	function set_firstvoice_editcat() {return 'Modifier une catégorie';}
	function set_firstvoice_deletecat() {return 'Supprimer Catégorie';}
	function set_firstvoice_newsubcat() {return 'Nouvelle catégorie';}
	function set_firstvoice_editsubcat() {return 'Modifier Sous-catégorie';}
	function set_firstvoice_deletesubcat() {return 'Supprimer Sous-catégorie';}
	function set_firstvoice_type() {return 'Type';}
	function set_firstvoice_content() {return 'Content';}
	function sel_firstvoice_yesno() {return 'Oui/Non';}
	function sel_firstvoice_text() {return 'Text';}
	function sel_firstvoice_picture() {return 'Image';}
	function sel_firstvoice_pdf() {return 'PDF';}
	function sel_firstvoice_expiration() {return 'Expiration';}
	function set_firstvoice_emailalerts() {return 'Courriel Alertes';}
	function set_firstvoice_name() {return 'Nom';}
	function set_firstvoice_checkfreq() {return 'Arrivée Fréquence (jours)';}
	function set_firstvoice_levelone() {return 'Niveau 1 (jours)';}
	function set_firstvoice_leveltwo() {return 'Niveau 2 (jours)';}
	function set_firstvoice_levelthree() {return 'Niveau 3 (jours)';}
	function set_firstvoice_alerts() {return 'Alertse';}
	function set_firstvoice_details() {return 'Détails';}
	function set_firstvoice_checkquestions() {return 'Vérifiez questions';}
	function set_firstvoice_confirm_deletecat() {return 'Êtes-vous sûr de vouloir supprimer cette catégorie de l\'équipement?';}
	function set_firstvoice_success_deletecat() {return 'Votre catégorie d\'équipement a été supprimé.';}
	function set_firstvoice_invalid_newsubcat() {return 'Cette sous-catégorie de l\'équipement est déjà dans le programme.';}
	function set_firstvoice_confirm_deletesubcat() {return 'Êtes-vous sûr de vouloir supprimer cet équipement sous-catégorie?';}
	function set_firstvoice_success_deletesubcat() {return 'Votre sous-catégorie de l\'équipement a été supprimé.';}
	function set_firstvoice_success_newdetails($name, $language) {return 'Vos détails d\'équipement de firstvoice, '.$name.', pour '.$language.' a été ajouté avec succès.';}
	function set_firstvoice_success_deletecheckquestion($name, $language) {return 'Votre vérification de l\'équipement question, '.$name.', pour '.$language.' a été supprimé.';}
	function set_firstvoice_success_editcheckquestion($name, $language) {return 'Votre vérification de l\'équipement question, '.$name.', pour '.$language.' a été changé avec succès.';}
	function set_firstvoice_success_deletedetails($name, $language) {return 'Vos détailspour\'équipement, '.$name.', pour '.$language.' a été supprimé.';}
	function set_firstvoice_success_editdetails($name, $language) {return 'Vos détails d\'équipement, '.$name.', pour '.$language.' a été changé avec succès.';}
	function set_firstvoice_success_editsubcatname($type, $language) {return 'Le nom de la sous-catégorie de l\'équipement, '.$type.', pour '.$language.'  a été changé avec succès.';}
	function set_firstvoice_invalid_editsubcatname($type, $language) {return 'Le nom de la sous-catégorie de l\'équipement, '.$type.', pour '.$language.'  ne peut toutefois pas être changé.';}
	function set_firstvoice_success_editcheckfreq() {return 'Votre fréquence de vérification personnalisée a été changé avec succès.';}
	function set_firstvoice_invalid_editcheckfreq() {return 'Votre fréquence de vérification personnalisée ne pouvait pas être changé. Vous devez entrer un numéro.';}
	function set_firstvoice_success_editlevelone() {return 'Votre niveau personnalisé une alerte a été changé avec succès.';}
	function set_firstvoice_invalid_editlevelone() {return 'Votre niveau personnalisé seule alerte ne pouvait pas être changé. Vous devez entrer un numéro.';}
	function set_firstvoice_success_editleveltwo() {return 'Votre niveau personnalisé de deux alerte a été changé avec succès.';}
	function set_firstvoice_invalid_editleveltwo() {return 'Votre niveau d\'alerte personnalisé deux ne pouvait pas être changé. Vous devez entrer un numéro.';}
	function set_firstvoice_success_editlevelthree() {return 'Votre niveau personnalisé de trois alerte a été changé avec succès.';}
	function set_firstvoice_invalid_editlevelthree() {return 'Votre niveau personnalisé de trois alerte ne pouvait pas être changé. Vous devez entrer un numéro.';}
	function set_firstvoice_success_newalerts() {return 'Vos alertes personnalisées ont été réalisés avec succès.';}
	function set_firstvoice_invalid_newalerts() {return 'Vous devez remplir la fréquence de contrôle, le niveau un, niveau deux et à trois niveaux lors de votre première édition.';}
	function set_firstvoice_success_newcat($name, $language) {return 'Votre nouvelle catégorie de l\'équipement, '.$name.', pour '.$language.' ont été ajoutées au programme.';}
	function set_firstvoice_success_editcat($name, $language) {return 'Votre catégorie d\'équipement, '.$name.', pour '.$language.' a été changé avec succès.';}
	function set_firstvoice_success_newsubcat($name, $language) {return 'Votre nouvelle sous-catégorie de l\'équipement, '.$name.', pour '.$language.' ont été ajoutées au programme.';}
	function set_firstvoice_success_editsubcat($name, $language) {return 'Votre sous-catégorie de l\'équipement, '.$name.', pour '.$language.' a été changé avec succès.';}
	function set_firstvoice_success_newcatandsubcat() {return 'Votre nouvelle catégorie et sous-catégorie de l\'équipement a été ajouté au programme.';}
	function set_firstvoice_contenttype($type) {
		switch($type)
		{
			case 'text':
				return $this->sel_firstvoice_text();
				break;
			case 'expiration':
				return $this->sel_firstvoice_expiration();
				break;
			case 'picture':
				return $this->sel_firstvoice_picture();
				break;
			case 'pdf':
				return $this->sel_firstvoice_pdf();
				break;
			default: return $type;
		}
	}
	function set_firstvoice_questiontype($type) {
		switch($type)
		{
			case 'text':
				return $this->sel_firstvoice_text();
				break;
			case 'expiration':
				return $this->sel_firstvoice_expiration();
				break;
			case 'picture':
				return $this->sel_firstvoice_picture();
				break;
			case 'pdf':
				return $this->sel_firstvoice_pdf();
				break;
			default: return $type;
		}
	}
	function set_firstvoice_success_newcontent($type, $language) {return 'Your equipment detail \''.$type.'\' for '.$language.' has been add to the program.';}
	function set_firstvoice_success_newquestion($type, $language) {return 'Your equipment question \''.$type.'\' for '.$language.' has been add to the program.';}
	
	
	//facility
	function fac_heading() {return 'Renseignements sites';}
   
	//help
	function hel_usermanual() {return 'Manuel de l\'utilisateur';}
	function hel_adminusermanual() {return 'Manuel d\'administration de l\'utilisateur';}
	function hel_blankaedtemplate() {return 'Vide DEA Vérifier';}
	function hel_blankservicecheck() {return 'Vide entretien Vérifier';}
	function hel_trainingupload() {return 'Modèle de formation des étudiants';}
	function hel_eventform() {return 'Rapport Médical Aprés Sauvetage avec DEA 2015';}
	function hel_recallhistory() {return 'Rappel historique';}
	function hel_content() {
		$content = '';
		$content .= '
		<h1>Aide</h1>
		<table style="width:600px;">';
			if(file_exists($this->doc_usermanual())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_usermanual().':</td> 
					<td><a target="_blank" href="'.$this->doc_usermanual().'">PDF</a><td>
				</tr>';
			}
			if(file_exists($this->doc_adminusermanual())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_adminusermanual().':</td> 
					<td><a target="_blank" href="'.$this->doc_adminusermanual().'">PDF</a><td>
				</tr>';
			}
			
			if(file_exists($this->doc_blankaedcheck())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_blankaedtemplate().':</td> 
					<td><a target="_blank" href="'.$this->doc_blankaedcheck().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_blankservicecheck())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_blankservicecheck().':</td> 
					<td><a target="_blank" href="'.$this->doc_blankservicecheck().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_trainingtemplate())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_trainingupload().':</td> 
					<td><a target="_blank" href="'.$this->doc_trainingtemplate().'">XLS</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_eventform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_eventform().':</td> 
					<td><a target="_blank" href="'.$this->doc_eventform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_recallhistory())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_recallhistory().':</td> 
					<td><a target="_blank" href="'.$this->doc_recallhistory().'">DOC</a></td>
				</tr>';
			}
		$content .= '
		</table>';	
		return $content;
	}
   
	//profile
	function pro_heading() {return 'Profil';}
	function pro_username() {return 'Nom utilisateur';}
	function pro_password() {return 'Mot de passe';}
	function pro_currentpassword() {return 'Mot de passe actuel';}
	function pro_newpassword() {return 'Nouveau mot de passe';}
	function pro_retypepassword() {return 'Ressaisir le mot de passe';}
	function pro_clicktochange() {return '(click edit to change)';}
	function pro_firstname() {return 'Prénom';}
	function pro_lastname() {return 'Nom';}
	function pro_preferredlang() {return 'Langue Préférée';}
	function pro_phone() {return 'Téléphone';}
	function pro_company() {return 'Compagnie';}
	function pro_address() {return 'Adresse';}
	function pro_city() {return 'Ville';}
	function pro_state() {return 'Province';}
	function pro_country() {return 'Pays';}
	function pro_zip() {return 'Code postal';}
	function pro_invalid_validemail() {return 'Vous devez entrer une adresse électronique valide.';}
	function pro_invalid_uniqueemail() {return 'Votre adresse électronique n’est pas unique.';}
	function pro_emailsubject() {return 'Profile Update';}
	function pro_email() { return '<html><head></head><body style="color:#000001;">
										<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
											<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
											   
													<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
														<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
													</h1>
													<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
													<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
														Mise à jour de profil
													</h2>
													<br>
													<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
														Vous avez récemment mis à jour votre profil sur <a href="'.URL.'">'.PROGRAM.'</a>.  Si cela n\'a pas été vous, s\'il vous plaît
														contacter notre support technique au <b>'.SUPPORTEMAIL.'</b>.<br />
														<br />
														Pour toute question, s\'il vous plaît sentir libre à nous contacter au <b>'.SUPPORTEMAIL.'</b>.
													</div>
											</div>
										 </div>
										 <div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
											S\'il vous plaît ne pas répondre à cet e-mail. Nous sommes incapables de répondre aux demandes de renseignements envoyés à cette adresse.
										</div>
										<br />
										<div style="width:970px;margin:0 auto;color:#000001;">
											Copyright &copy;  2015 Think Safe Inc.
										</div>
									</body></html>';}
	function pro_addemail() {return 'Vous avez récemment mis à jour votre profil sur '.URL.'.  Si cela n\'a pas été vous, s\'il vous plaît contacter notre support technique au '.SUPPORTEMAIL.'

Pour toute question, s\'il vous plaît sentir libre à nous contacter au '.SUPPORTEMAIL.'.';}// uses a pre tag, so all spacing is recorded.
	function pro_success_edit() {return 'Votre profil a été mis à jour.';}
   
	//login
	function log_heading() {return 'Entrer';}
	function log_username() {return 'Nom utilisateurd';}
	function log_password() {return 'Mot de passe';}
	function but_login() {return 'Entrer';}
	function log_forgotpassword() {return 'Vous avez oublié votre mot de passe?';}
	function log_invalid_login() {return 'L\'utilisateur et mot de passe que vous avez saisi ne sont pas valides.';}
	function log_logout() {return '';}//removed for lack of french
   
	//forgot password
	function for_heading() {return 'Mot de passe oublié';}

    function for_invalid_forgot()
    {
        return 'Si vous avez oublié votre mot de passe, il suffit de taper dans votre courriel dans la bo�te et vous recevrez et envoyez avec votre nouveau mot de passe .';
    }
	function for_success_forgot() {return ' Vous devriez recevoir sous peu un courriel contenant votre nouveau mot de passe.';}
	function for_emailsubject() {return PROGRAM.' Mot De Passe Oublié';}
	function for_email($username, $password) {return '<html><head></head><body style="color:#000001;">
							<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
								<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
									<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
										<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
									</h1>
									<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
									<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
										Bienvenue!
									</h2>
									<br />
									<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
										Vous avez eu récemment changé votre profil à <a href="'.URL.'">'.PROGRAM.'</a>.  Ceci est un programme qui vous permet
										à votre gestionnaire des équipements tels que les DEA et les trousses de premiers soins.<br />
										<b>Votre nouvelles informations de connexion:</b>
											<div style="margin-left:10px;font-weight:bold;">
												Nom d\'utilisateur: '.$username.'<br />
												Mot de passe: '.$password.'
											</div>
										Si vous avez des questions anymore, contacter notre support technique au <b>'.SUPPORTEMAIL.'</b>.<br />
										Si vous pensez que vous avez obtenu ce message par accident, s\'il vous plaît ne pas tenir compte.
									</div>
								</div>
							</div>
							<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
									S\'il vous plaît ne pas répondre à cet e-mail. Nous sommes incapables de répondre aux demandes de renseignements envoyés à cette adresse.
							</div>
								<br />
							<div style="width:970px;margin:0 auto;color:#000001;">
								Copyright &copy;  2015 Think Safe Inc.
							</div>
						</body></html>';}
	function for_recordedemail($username) {return '<html><head></head><body style="color:#000001;">
						<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
							<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
								<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
									<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
								</h1>
								<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
								<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
									Welcome!
								</h2>
								<br />
								<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
									Vous avez eu récemment changé votre profil à <a href="'.URL.'">'.PROGRAM.'</a>.  Ceci est un programme qui vous permet
									à votre gestionnaire des équipements tels que les DEA et les trousses de premiers soins.<br />
									<b>Votre nouvelles informations de connexion:</b>
										<div style="margin-left:10px;font-weight:bold;">
										Nom d\'utilisateur: '.$username.'<br />
										Mot de passe: (non affiché pour des questions de confidentialité)
										</div>
									Si vous avez des questions anymore, contacter notre support technique au <b>'.SUPPORTEMAIL.'</b>.<br />
									Si vous pensez que vous avez obtenu ce message par accident, s\'il vous plaît ne pas tenir compte.
									<br />
									Si vous avez des questions, s\'il vous plaît sentir libre à nous contacter au <b>'.SUPPORTEMAIL.'</b>.
								</div>
							</div>
						</div>
						<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
								S\'il vous plaît ne pas répondre à cet e-mail. Nous sommes incapables de répondre aux demandes de renseignements envoyés à cette adresse.
						</div>
							<br />
						<div style="width:970px;margin:0 auto;color:#000001;">
							Copyright &copy;  2015 Think Safe Inc.
						</div>
					</body></html>';}
	function for_addemail($username, $password) {return 'Vous avez eu récemment changé votre profil à '.PROGRAM.' - '.URL.'.  
Ceci est un programme qui vous permet de manager, votre équipement tel que les DEA et les trousses de premiers soins.
Connexion à '.PROGRAM.' est simple!
Nom d\'utilisateur: '.$username.'
Mot de passe: '.$password.'
Si vous avez des questions anymore, contacter notre support technique au '.SUPPORTEMAIL.'.
Si vous pensez que vous avez obtenu ce message par accident, s\'il vous plaît ne pas tenir compte.';}// uses a pre tag, so all spacing is recorded.
   
	//visitor home
	function vis_home() {
		$home = '
		<h1 style="text-align:left;float:left;width:auto;">'.PROGRAM.':</h1>';
		
		$home .= '
		<br />
		<ul class="boldlist" style="clear:left;">
			<li>Programme de gestion '.TITLE.' et de DEA</li>
			<li>Gestion des dates d’expiration</li>
			<li>Suivi certification formation</li>
			<li>Surveillance des emplacements/des dangers</li>
			<li>Directeur médical</li>   
		</ul>   
		<br />
	   
		<p>
			Le programme de gestion Rescue Ready vous permet de gérer votre programme de réponse aux urgences en ligne, d’avoir des registres d’entretien à jour pour vos produits 
			Rescue Ready et vos DEA et de mettre à jour les dates de renouvellement de certification de vos employés.
		</p>
		<br />
		<p>
			Pour toute aide concernant votre programme de réponse aux urgences ou pour des questions sur la législation actuelle, merci de contacter un spécialiste de Rescue Ready.
		</p>
		<br />
		<p style="font-weight:bold;">Soutien technique: 1-866-764-8488 Telecopier: 1-866-252-3915</p>';
		return $home;
	}
	   
	//message center
	function mes_attachments() {return 'Attachments';}
	function mes_attachment() {return 'attachment';}
	function mes_invalid_emailrecord() {return 'ERREUR: Nous n’avons pas trouvé votre courriel. Veuillez contacter '.SUPPORTEMAIL.'.';}
	function mes_heading() {return 'Centre de messages';}
	function mes_newmessage() {return 'New Message';}
	function mes_howemail() {return 'Comment allez-vous envoyer votre courriel?';}
	function mes_byuser() {return 'Par utilisateur';}
	function mes_bymodel() {return 'Par modèle de DEA';}
	function mes_bybrand() {return 'Par marque de DEA';}
	function mes_byorg() {return 'Par organisation';}
	function mes_bylocation() {return 'Par emplacement';}
	function mes_to() {return 'To';}
	function mes_subject() {return 'Sujet';}
	function mes_body() {return 'Contenu';}
	function mes_success_send($to) {return 'Votre courriel a été envoyé à '.$to.'.';}
	function mes_ifquestions() {return 'Si vous avez des questions au sujet de cet e-mail, s\'il vous plaît sentir libre à nous contacter au <b>'.SUPPORTEMAIL.'</b>.';}
	function mes_pleasedonotreply() {return 'Merci de ne pas répondre à ce courriel. Nous ne sommes pas en mesure de répondre à cette adresse. ';}
	function mes_tscopyright() {return 'Copyright &copy; 2015 Think Safe Inc.';}
   
	//contacts
	function con_heading() {return 'Contacts';}
	function con_newcontact() {return 'Nouveaux contacts';}
	function con_success_new() {return 'Votre nouveau contact a été ajouté avec succès.';}
	function con_editcontact() {return 'Modifier les contacts';}
	function con_success_edit() {return 'Le contact a été modifié avec succès.';}
	function con_invalid_edit() {return 'Le contact n\'a pas pu être modifié avec succès.';}
	function con_deletecontact() {return 'Supprimer les contacts';}
	function con_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer ce contact?';}
	function con_success_delete() {return 'Votre contact a été supprimé.';}
	function con_invalid_delete() {return 'Votre contact n\'a pas pu être supprimé.';}
	function con_primarycontact() {return 'Premier Contact';}
	function con_secondarycontact() {return 'Contact secondaire';}
	function con_invalid_nocontacts() {return 'Il n\'y a pas de contact pour ce lieu.';}
	function con_preferredlang() {return 'Langue Préférée';}
	function con_name() {return 'Nom';}
	function con_secondarycontactname() {return 'Contact secondaire Nom';}
	function con_title() {return 'Titre';}
	function con_company() {return 'Société';}
	function con_department() {return 'Département';}
	function con_contacttype() {return 'Type de contact';}
	function con_pickcontacttype($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'aedcoordinator':
				return 'Coordonnateur DEA';
			case 'backupaedcoordinator':
				return 'Coordonnateur de sauvegarde DEA';
			case 'salesrep':
				return 'Représentant des ventes';
			case '':
				return 'None';
			default:
				return 'ERROR UNKNOWN TYPE:'.$type;
		}
	}
	function con_date() {return 'Date';}
	function con_phone() {return 'Téléphone';}

    function con_cell()
    {
        return 'Numero de portable';
    }

    function con_fax()
    {
        return 'Telecopier';
    }
	function con_email() {return 'Courriel';}
	function con_address() {return 'Adresse';}
	function con_city() {return 'Ville';}
	function con_state() {return 'Province';}
	function con_country() {return 'Pays';}
	function con_zip() {return 'Code postal';}
	function con_sendaedcheck() {return 'Copiez ce Contact dans tous les controles de DEA fait';}
	function con_primary() {return 'Primaire';}
	function con_secondary() {return 'Secondaire';}
	function con_preferredlanguage() {return 'Langue Préférée';}
	function con_notes() {return 'Remarques';}
	function con_none() {return 'Il n\'y a aucun contact pour ce lieu.';}
   
	//scheduler
	function sch_heading() {return 'Planificateur';}
	function sch_newevent() {return 'Nouveau Planificateur événement';}
	function sch_success_newevent() {return 'Votre réunion a été envoyée par courriel à votre logiciel Outlook.';}
	function sch_deleteevent() {return 'Delete Planificateur événement';}
	function sch_confirm_deleteevent() {return 'Êtes-vous sûr de vouloir effacer cette réunion?';}
	function sch_success_deleteevent() {return 'La réunion a été annulée.';}
	function sch_editevent() {return 'Edit Planificateur événement';}
	function sch_success_editevent() {return 'Cette réunion a été mise à jour.';}
	function sch_eventlisting() {return 'Liste des événements';}
	function sch_calendarlisting() {return 'Calendrier Annonce';}
	function sch_company() {return 'Compagnie';}
	function sch_subject() {return 'Sujet';}
	function sch_locationfill() {return 'Situation Fill';}
	function sch_location() {return 'Emplacement';}
	function sch_date() {return 'Date';}
	function sch_timezone() {return 'Fuseau Horaire';}
	function sch_time() {return 'Temps';}
	function sch_inviteemail() {return 'Inviter un autre courriel';}
	function sch_contact() {return 'Contact';}
	function sch_address() {return 'Adresse';}
	function sch_phone() {return 'Téléphone';}
	function sch_city() {return 'Ville';}
	function sch_state() {return 'Province';}
	function sch_country() {return 'Pays';}
	function sch_zip() {return 'Postal Code';}
	function sch_description() {return 'Description';}
	function sch_externalid() {return 'ID externe';}
	function sch_internalid() {return 'ID interne';}
	function sch_sendtomyoutlook() {return 'Envoyer à mon email';}
	function sch_updatedsubject($subject) {return 'Mise à jour '.$subject;}
	function sch_cancelledsubject($subject) {return 'Annulé '.$subject;}
	function sch_timezone_americalosangeles() {return '(GMT-08:00), heure du Pacifique (US & amp; Canada)';}
	function sch_timezone_americadenver() {return '(GMT-07:00) Montagne (US & amp; Canada)';}
	function sch_timezone_americachicago() {return '(GMT-06:00) Heure centrale (US &amp; Canada)';}
	function sch_timezone_americanewyork() {return '(GMT-05:00) Heure De L\'Est (US &amp; Canada)';}
	function sch_timezone_americaglacebay() {return '(GMT-04:00) Heure de l\'Atlantique (Canada)';}
	function sch_timezone_americastjohns() {return '(GMT-03:30) Terre-Neuve';}
	function sch_monthsarray() {return Array("Janvier", "F&#233;vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao&#251;t", "Septembre", "Octobre", "Novembre", "D&#233;cembre");}
   
	//recalls
	function rec_heading() {return 'Rappelle & services';}
	function rec_documentation() {return 'Documentation';}
	function rec_history() {return 'Rappel historique';}
	function rec_recalls() {return 'Rappels';}
	//function rec_newservice() {return 'New Servicing Documentation';}
	function rec_newrecall() {return 'Nouveau rappel';}
	function rec_newrecalldoc() {return 'Nouveau Documentation';}
	function rec_success_newrecall() {return 'Votre rappel a été ajouté avec succès.';}
	function rec_success_newrecalldoc() {return 'Votre documentation a été ajouté avec succès.';}
	function rec_invalid_newrecalldocfile() {return 'Vous devez télécharger un pdf, doc, xls ou.';}
	function rec_confirm_deleterecall() {return 'Êtes-vous sûr de vouloir supprimer ce rappel?';}
	function rec_success_deleterecall() {return 'Votre rappel a été supprimé.';}
	function rec_confirm_deleterecalldoc() {return 'Êtes-vous sûr de vouloir supprimer cette documentation?';}
	function rec_success_deleterecalldoc() {return 'Votre documentation a été supprimé.';}
	function rec_success_editrecall() {return 'Votre rappel a édité avec succès.';}
	function rec_invalid_editrecall() {return 'Votre rappel ne pouvait pas être modifié.';}
	function rec_success_editrecalldoc() {return 'Votre documentation a édité avec succès.';}
	function rec_date() {return 'Rappel date de';}
	function rec_recalltype() {return 'Type de rappel';}
	function rec_brandsandmodels() {return 'Marques et Modèles';}
	function rec_range() {return 'Gamme';}
	function rec_notes() {return 'Remarques';}
	function rec_searchhistory() {return '<p>Si vous souhaitez effectuer une recherche pour les rappels qui peuvent être liés à votre DEA au sein de la base de données US FDA pour "Medical Device rappelle".</p>
		<p>Please <a href="http://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfRES/res.cfm" target="blank">cliquez ici </a> et tapez votre nom de marque ou le modèle DEA dans les champs prévus.</p>';
		/* CANADA
		return '<p>If you would like to search for recalls that may be related to your AED within the Health Canada database for  “automated external defibrillator� recalls or your specific brand/make recall history, please click link below:</p>
		<p><a href="http://www.healthycanadians.gc.ca/recall-alert-rappel-avis/index-eng.php">http://www.healthycanadians.gc.ca/recall-alert-rappel-avis/index-eng.php</a></p>';
		*/
	}

    function rec_servicedate()
    {
        return 'Date de Service';
    }
	function rec_file() {return 'Fichier';}
	function rec_view() {return 'Vue';}
	function rec_type() {return 'Type';}
	function rec_picktype($type) {
		switch($type) {
			case 'other':
				return 'Autre';
			case 'event':
				return 'Evenements';
			default:
				return 'ERROR: Unknown type:'.$type;
		}
	}
	function rec_name() {return 'Nom';}
	function rec_brand() {return 'Marque';}
	function rec_model() {return 'Modèle';}
	function rec_none() {return 'Il n\'y a actuellement aucun rappel pour cet emplacement.';}
	function rec_docnone() {return 'Il n\'y a actuellement aucune documentation pour ce lieu.';}
   
	//medical direction
	function med_heading() {return 'Direction médicale';}
	function med_new() {return 'Nouveau Direction médicale';}
	function med_success_new() {return 'Votre nouvelle direction médicale a été ajouté avec succès.';}
	function med_edit() {return 'Modifier Direction médicale';}
	function med_success_edit() {return 'La direction médicale a été modifié avec succès.';}
	function med_invalid_edit() {return 'La direction médicale ne pouvait pas être modifié.';}
	function med_delete() {return 'Supprimer Direction médicale';}
	function med_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cette direction médicale?';}
	function med_success_delete() {return 'Votre direction médical a été retiré.';}
	function med_invalid_delete() {return 'Votre direction médicale ne peut être supprimée.';}
	function med_name() {return 'Nom';}
	function med_startdate() {return 'date De Début';}
	function med_enddate() {return 'date De Fin';}
	function med_reviewdate() {return 'Date de la dernière évaluation';}
	function med_nextreviewdate() {return 'Suivant Date de révision';}
	function med_license() {return 'Licence';}
	function med_company() {return 'Compagnie';}
	function med_address() {return 'Adresse';}
	function med_city() {return 'Ville';}
	function med_state() {return 'Province';}
	function med_country() {return 'Pays';}
	function med_zip() {return 'Code postal';}
	function med_phone() {return 'Numéro de téléphone';}
	function med_cell() {return 'Numéro de portable';}
	function med_fax() {return 'Télécopieur';}
	function med_email() {return 'Courriel';}
	function med_notes() {return 'Remarques';}
	function med_nodirector() {return 'Il n’y a à l’heure actuelle aucun directeur médical à cet emplacement';}
	function med_none() {return 'Il n’y a à l’heure actuelle aucun directeur médical à cet emplacement';}

    function med_status()
    {
        return 'Statut';
    }

    function med_pickstatus($status)
    {
        switch ($status) {
            case 'active':
                return 'Actif';
            case 'active-filepending':
                return 'Actif - En attente de fichier';
            case 'active-filesigned':
                return 'Actif - fichier sign�';
            case 'pending':
                return 'en attendant';
            case 'cancelled':
                return 'Annul�';
            case 'notrenewed':
                return 'pas renouvel�';
            default:
                return 'Actif';
        }
    }
   
	//erp
	function erp_heading() {return 'Programme d\'intervention d\'urgence';}
	function erp_new() {return 'Programme d\'intervention d\'urgence Nouveau';}
	function erp_success_new() {return 'Votre programme d\'intervention d\'urgence a été ajouté avec succès.';}
	function erp_delete() {return 'Supprimer Emergency Response Program';}
	function erp_confirm_delete() {return 'Etes-vous sûr de vouloir supprimer ce programme d\'intervention d\'urgence?';}
	function erp_success_delete() {return 'Votre programme d\'intervention d\'urgence a été supprimé avec succès.';}
	function erp_invalid_delete() {return 'Votre programme d\'intervention d\'urgence ne peut pas être supprimé.';}
	function erp_name() {return 'Nom';}
	function erp_type() {return 'Type';}
	function erp_picktype($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'aedpolicy':
				return 'Politique DEA';
			case 'aedprescriptioninfo':
				return 'DEA informations de prescription';
			case 'aedresponseprotocols':
				return 'Les protocoles d\'intervention de la DEA';
			case 'emsnotification':
				return 'notification EMS';
			case 'floorplans':
				return 'Les plans d\'étage';
			case 'other':
				return 'Autre';
			case 'siteassessment':
				return 'Évaluation du site';
			case 'technicalexceptions':
				return 'Exceptions techniques';
			case 'trainingdrills':
				return 'Exercices d\'entraînement';
			default: return 'ERROR UNKNOWN TYPE:'.$type;
		}
	}
	function erp_file() {return 'Fichier';}

    function erp_view()
    {
        return 'Vue';
    }
	function erp_registrationdate() {return 'Date d\'inscription';}
	function erp_registrationexpiration() {return 'Expiration d\'inscription';}
	function erp_registrationagency() {return 'Agence d\'enregistrement';}
	function erp_uploaddate() {return 'date De Dépôt';}
	function erp_invalid_file() {return 'Vous avez téléchargé un type de fichier non valide.';}
	function erp_none() {return 'Il n\'y a pas actuellement de programme d\'intervention d\'urgence pour ce lieu.';}
	
	
	function erp_aedprescriptioninfo_heading() {return 'DEA informations de prescription';}
	function erp_aedprescriptioninfo_new() {return 'Nouveau DEA informations de prescription';}
	function erp_aedprescriptioninfo_success_new() {return 'Votre DEA prescription de l\'information a été ajouté avec succès.';}
	function erp_aedprescriptioninfo_delete() {return 'Supprimer DEA prescription information';}
	function erp_aedprescriptioninfo_confirm_delete() {return 'Etes-vous sûr de vouloir supprimer cette DEA prescription de l\'information?';}
	function erp_aedprescriptioninfo_success_delete() {return 'Votre information prescription DEA a été supprimé avec succès.';}
	function erp_aedprescriptioninfo_invalid_file() {return 'Vous devez télécharger un pdf.';}
	function erp_aedprescriptioninfo_name() {return 'Nom';}
	function erp_aedprescriptioninfo_file() {return 'Fichier';}
	function erp_aedprescriptioninfo_uploaddate() {return 'date De Dépôt';}
	function erp_aedprescriptioninfo_none() {return 'Il n\'y a pas actuellement de DEA informations de prescription pour ce lieu.';}
	function erp_aedpolicy_heading() {return 'Politique ou lignes directrices DEA';}
	function erp_aedpolicy_new() {return 'Nouveau Politique ou lignes directrices DEA';}
	function erp_aedpolicy_success_new() {return 'Votre politique ou des directives DEA a été ajouté avec succès.';}
	function erp_aedpolicy_delete() {return 'Supprimer la stratégie ou les lignes directrices DEA';}
	function erp_aedpolicy_confirm_delete() {return 'Etes-vous sûr de vouloir supprimer cette politique ou de lignes directrices DEA?';}
	function erp_aedpolicy_success_delete() {return 'Votre politique ou des directives DEA a été supprimé avec succès.';}
	function erp_aedpolicy_invalid_file() {return 'Vous devez télécharger un pdf.';}
	function erp_aedpolicy_name() {return 'Nom';}
	function erp_aedpolicy_file() {return 'Fichier';}
	function erp_aedpolicy_uploaddate() {return 'date De Dépôt';}
	function erp_aedpolicy_none() {return 'Il n\'y a pas de politiques ou guidlines DEA pour cet emplacement.';}
	function erp_aedresponseprotocols_heading() {return 'Les protocoles d\'intervention de la DEA';}
	function erp_aedresponseprotocols_new() {return 'Nouveau Les protocoles d\'intervention de la DEA';}
	function erp_aedresponseprotocols_success_new() {return 'Votre Protocole de réponse a été ajouté avec succès.';}
	function erp_aedresponseprotocols_delete() {return 'Supprimer les protocoles d\'intervention DEA';}
	function erp_aedresponseprotocols_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer ce Protocole d\'intervention en DEA?';}
	function erp_aedresponseprotocols_success_delete() {return 'Votre protocole d\'intervention a été supprimé avec succès.';}
	function erp_aedresponseprotocols_invalid_file() {return 'Vous devez télécharger un pdf.';}
	function erp_aedresponseprotocols_name() {return 'Nom';}
	function erp_aedresponseprotocols_file() {return 'Fichier';}
	function erp_aedresponseprotocols_uploaddate() {return 'date De Dépôt';}
	function erp_aedresponseprotocols_none() {return 'Il n\'y a aucune protocoles d\'intervention pour ce lieu.';}
	function erp_emsnotification_heading() {return 'Inscription / Notification EMS';}
	function erp_emsnotification_new() {return 'Nouveau Inscription / Notification EMS';}
	function erp_emsnotification_success_new() {return 'Votre inscription / EMS notification a été ajouté avec succès.';}
	function erp_emsnotification_delete() {return 'Supprimer Enregistrement / Notification EMS';}
	function erp_emsnotification_confirm_delete() {return 'Etes-vous sûr de vouloir supprimer ce Inscription / Notification EMS?';}
	function erp_emsnotification_success_delete() {return 'Votre inscription / EMS notification a été supprimé avec succès.';}
	function erp_emsnotification_invalid_file() {return 'Vous devez télécharger un pdf.';}
	function erp_emsnotification_name() {return 'Nom';}
	function erp_emsnotification_file() {return 'Fichier';}
	function erp_emsnotification_uploaddate() {return 'date De Dépôt';}
	function erp_emsnotification_dateregistred() {return 'Date de Registred';}
	function erp_emsnotification_registrationexpiration() {return 'Expiration d\'inscription';}
	function erp_emsnotification_registrationagency() {return 'Agence d\'enregistrement';}
	function erp_emsnotification_none() {return 'Il n\'y a aucune inscription notifications / EMS pour cet emplacement.';}
	function erp_floorplans_heading() {return 'Plans & Site étage Photos';}
	function erp_floorplans_new() {return 'Plans et Photos de sol nouvelles';}
	function erp_floorplans_success_new() {return 'Votre plan d\'étage ou photo n\'a été ajoutée avec succès.';}
	function erp_floorplans_delete() {return 'Supprimer Plans d\'étage et Photos';}
	function erp_floorplans_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer ce plan d\'étage ou Photo?';}
	function erp_floorplans_success_delete() {return 'Votre plan d\'étage ou photo a été supprimé avec succès.';}
	function erp_floorplans_invalid_file() {return 'Vous devez télécharger un \'pdf\', \'png\', \'gif\', \'jpeg\' or \'jpg\'.';}
	function erp_floorplans_name() {return 'Nom';}
	function erp_floorplans_file() {return 'Fichier';}
	function erp_floorplans_uploaddate() {return 'date De Dépôt';}
	function erp_floorplans_none() {return 'Il n\'y a pas actuellement de plans de sol ou des photos du site pour ce lieu.';}
	function erp_siteassessment_heading() {return 'Évaluation et Documentation du site';}
	function erp_siteassessment_new() {return 'Nouveau Évaluation et Documentation du site';}
	function erp_siteassessment_success_new() {return 'Votre site d\'évaluation ou de documentation a été ajoutée avec succès.';}
	function erp_siteassessment_delete() {return 'Supprimer l\'évaluation et de la documentation du site';}
	function erp_siteassessment_confirm_delete() {return 'Etes-vous sûr de vouloir supprimer ce site d\'évaluation?';}
	function erp_siteassessment_success_delete() {return 'Votre évaluation du site ou de la Documentation a été supprimé avec succès.';}
	function erp_siteassessment_invalid_file() {return 'Vous devez télécharger un pdf.';}
	function erp_siteassessment_name() {return 'Nom';}
	function erp_siteassessment_file() {return 'Fichier';}
	function erp_siteassessment_uploaddate() {return 'date De Dépôt';}
	function erp_siteassessment_none() {return 'Il n\'y a actuellement aucune évaluation du site et de la documentation pour ce lieu.';}
	function erp_trainingdrills_heading() {return 'Exercices d\'entraînement ou des scénarios de formation';}
	function erp_trainingdrills_new() {return 'Nouveau Exercices d\'entraînement ou des scénarios de formation';}
	function erp_trainingdrills_success_new() {return 'Votre Drill de formation ou scénario de formation a été ajouté avec succès.';}
	function erp_trainingdrills_delete() {return 'Supprimer Drill de formation ou de formation Scénario';}
	function erp_trainingdrills_confirm_delete() {return 'Etes-vous sûr de vouloir supprimer cette Drill de formation ou de formation Scénario?';}
	function erp_trainingdrills_success_delete() {return 'Votre Drill de formation ou scénario de formation a été supprimé avec succès.';}
	function erp_trainingdrills_invalid_file() {return 'Vous devez télécharger un pdf.';}
	function erp_trainingdrills_name() {return 'Nom';}
	function erp_trainingdrills_file() {return 'Fichier';}
	function erp_trainingdrills_uploaddate() {return 'date De Dépôt';}
	function erp_trainingdrills_none() {return 'Il n\'y a aucun exercices d\'entraînement ou des scénarios de formation pour ce lieu.';}	
	function erp_technicalexceptions_heading() {return 'Exceptions techniques';}
	function erp_technicalexceptions_new() {return 'Nouveau Exceptions techniques';}
	function erp_technicalexceptions_success_new() {return 'Votre Exception technique a été ajouté avec succès.';}
	function erp_technicalexceptions_delete() {return 'Supprimer Exception Techinical';}
	function erp_technicalexceptions_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cette exception Techinical?';}
	function erp_technicalexceptions_success_delete() {return 'Votre Exception Techinical a été supprimé avec succès.';}
	function erp_technicalexceptions_invalid_file() {return 'Vous devez télécharger un pdf.';}
	function erp_technicalexceptions_name() {return 'Nom';}
	function erp_technicalexceptions_file() {return 'Fichier';}
	function erp_technicalexceptions_uploaddate() {return 'date De Dépôt';}
	function erp_technicalexceptions_none() {return 'Il n\'y a aucune expections techniques pour ce lieu.';}	
	function erp_othersupporting_heading() {return 'Autres documents à l\'appui';}
	function erp_othersupporting_new() {return 'Nouveau Autres documents à l\'appui';}
	function erp_othersupporting_success_new() {return 'Votre autre pièce justificative a été ajouté avec succès.';}
	function erp_othersupporting_delete() {return 'Supprimer autre pièce justificative';}
	function erp_othersupporting_confirm_delete() {return 'Etes-vous sûr de vouloir supprimer cette autre pièce justificative?';}
	function erp_othersupporting_success_delete() {return 'Votre autre document a été supprimé avec succès.';}
	function erp_othersupporting_invalid_file() {return 'Vous devez télécharger un pdf.';}
	function erp_othersupporting_name() {return 'Nom';}
	function erp_othersupporting_file() {return 'Fichier';}
	function erp_othersupporting_uploaddate() {return 'date De Dépôt';}
	function erp_othersupporting_none() {return 'Il n\'y a pas d\'autres documents à l\'appui de cet emplacement.';}

	//events
	function eve_heading() {return 'Evenements';}
	function eve_new() {return 'Nouveau Evenements';}
	function eve_success_new() {return 'Votre événement a été ajouté avec succès.';}
	function eve_delete() {return 'Supprimer l\'événement';}
	function eve_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cet événement?';}
	function eve_success_delete() {return 'Votre événement a été supprimé avec succès.';}
	function eve_name() {return 'Nom';}
	function eve_incident() {return 'DEA Formulaire d\'incident Poster';}
	function eve_ecg() {return 'Électrocardiographie (ECG)';}
	function eve_other() {return 'Autre';}
	function eve_incidentlink() {return 'Incident';}
	function eve_ecglink() {return 'ECG';}
	function eve_otherlink() {return 'Autre';}
	function eve_uploaddate() {return 'date De Dépôt';}
	function eve_invalid_pdf() {return 'Vous devez télécharger un pdf.';}
	function eve_none() {return 'Il n\'y a pas d\'événement pour ce lieu.';}
	
	//training
	function tra_heading() {return 'Formation';}
	function tra_newclass() {return 'Nouvelle classe de formation';}
	function tra_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cette personne??';}
	function nav_students() {return 'Personnes';}
	function tra_students() {return 'Personnes';}
	function nav_classes() {return 'Classes';}
	function tra_classes() {return 'Classes';}

    function tra_classtype()
    {
        return 'Class Type';
    }
	function tra_classnumber() {return 'Numero de la classe';}
	function tra_more() {return 'Plus';}
	function tra_status() {return 'Statut';}
	function tra_pickstatus($status) {
		switch($status)
		{
			case 'uncompleted':
				return 'Inachevé';
			case 'completed':
				return 'Terminé';
			case 'na':
				return 'Pas applicable';
			case 'past':
				return 'Passé Terminé';
			case 'other':
				return 'Autre';
			case 'neskts':
				return 'A besoin de compétences de test';
			default:
				return $status; //most likely a percentage!
		}
	}
	function tra_pickprefix($prefix) {
		switch($prefix)
		{
			case 'refresher':
				return 'Refresher: ';
			case 'demo':
				return 'Démo: ';
			case 'inperson':
				return 'En personne: ';
			case 'none':
				return '';
			case 'noneshow':
				return 'Aucun';
			default:
				return $status; //most likely a percentage!
		}
	}
	function tra_newstudent() {return 'Nouvelle étudiant';}
	function tra_searchnames() {return 'Recherche Noms';}
	function tra_prefix() {return 'Préfixe';}
	function but_newstudent() {return 'Nouvelle étudiant';}
	function but_printcards() {return 'Imprimer des cartes';}
	function tra_printcardsheading() {return 'Imprimer des cartes';}
	function tra_currenttrained() {return 'Formé actuel';}
	function tra_pasttrained() {return 'Passé Formé';}
	function tra_completiondate() {return 'Date D\'Achèvement';}
	function tra_expiration() {return 'Expiration';}
	function tra_keycode() {return 'Code Clé';}
	function tra_trainer_name() {return 'Nom formateur';}
	function tra_trainer_number() {return 'Nombre formateur';}
	function tra_invalid_prints() {return 'Vous ne pouvez pas imprimer ce nombre de cartes.';}
	function tra_success_prints() {return 'Vos cartes ont été envoyé par courriel à vous.';}
	function tra_uploaddirections() {return 'Si vous souhaitez télécharger personnes s\'il vous plaît suivre <a href="documents/Francais-Template-de-Formation.xls">ce modèle</a>.';}
	function tra_invalid_file_uploaddirections() {return 'Si vous souhaitez télécharger personnes s\'il vous plaît suivre <a href="documents/upload_training_template.xls">ce modèle</a>.	<span style="color:red;">You must upload either an .xls, .xlsx or .csv file.</span>';}
	function tra_invalid_student_uploaddirections() {return 'Si vous souhaitez télécharger personnes s\'il vous plaît suivre <a href="documents/upload_training_template.xls">ce modèle</a>.	<span style="color:red;">Atleast one of your employees does not have a name or employee id.</span>';}
	function tra_currentstudents() {return 'étudiant actuel';}
	function tra_newstudents() {return 'Nouvelle étudiant';}
	function tra_employeeid() {return 'Identification des employés';}
	function tra_name() {return 'Nom';}
	function tra_firstname() {return 'Prénom';}
	function tra_lastname() {return 'Nom De Famille';}
	function tra_first() {return 'Premier';}
	function tra_last() {return 'Dernier';}
	function tra_address() {return 'Adresse';}
	function tra_email() {return 'Courriel';}
	function tra_phone() {return 'Téléphone';}
	function tra_notes() {return 'Remarques';}
	function tra_notcurrent() {return 'Non actuel';}
	function tra_notonreportsheading() {return 'Non Sur Rapports';}
	function tra_onreports() {return 'Cette personne montre actuellement sur les rapports';}
	function tra_notonreports() {return 'Cette personne ne le fait actuellement<b><i> pas </i></b>apparaît sur les rapports';}
	function tra_roster() {return 'Liste';}
	function tra_success_new() {return 'Vous venez d\'ajouter une nouvelle classe.';}
	function tra_success_edit() {return 'Vous avez édité avec succès une personne.';}
	function tra_invalid_edit() {return 'Vous devez remplir tous les champs nécessaires pour modifier cette personne.';}
	function tra_success_delete() {return 'Vous avez supprimé avec succès une personne.';}
	function tra_invalid_print() {return 'Vous devez sélectionner au moins une certification.';}
	function tra_invalid_printnumber() {return 'Vous ne disposez pas de suffisamment de copies pour cette sélection de cartes.';}
	function tra_lastmodified() {return 'Dernière mise à jour';}
	function tra_printcards() {return 'Envoyez-moi cartes imprimables';}
	function tra_noavailableprints() {return 'Il n\'y a pas de cours disponibles à imprimer pour cette organisation.';}
	function tra_printcards_subject() {return 'Cartes Portefeuille de certification';}
	function tra_printcards_body() {
		return '
		<p> Le fichier de données électronique attachée contient les carte (s) de portefeuille pour les cours qui ont été complétés avec succès. </ p>
		<p> S\'il vous plaît contactez votre represenitive ou appelez notre bureau au 319-377-5125 si vous avez des questions ou besoin d\'informations supplémentaires. Vous pouvez également nous envoyer un courriel à support@firstvoicetraining.com. </ p>
		<p> Nous sommes impatients de travailler avec vous et votre personnel pour toute autre à venir en ligne ou dirigé par un instructeur secourisme, de RCR, ou d\'autres besoins en matière de sécurité et de formation de l\'entreprise. </ p>
		<p> Nous vous remercions de votre entreprise! </ p>
	';}
	function tra_printcards_addbody() {return 
'Le fichier de données électronique ci-joint contient les carte (s) de portefeuille pour les cours qui ont été complétés avec succès.

S\'il vous plaît contacter votre représentant des ventes ou appelez notre bureau au 319-377-5125 si vous avez des questions ou besoin d\'informations supplémentaires. Vous pouvez également nous envoyer un courriel à support@firstvoicetraining.com.

Nous sommes impatients de travailler avec vous et votre personnel pour toute autre autres besoins de sécurité et de formation des entreprises à venir en ligne ou les premiers secours dirigée par un instructeur, CPR, ou.

Nous vous remercions de votre entreprise!';}
	function tra_checkbox_noreports() {return 'Ne pas afficher cette personne sur les rapports.';}
	function tra_checkbox_responder() {return 'Cette personne est un répondeur formés.';}
	function tra_none() {return 'Il n\'y a pas de classes pour cet emplacement.';}
	
	//immunizations
	function imm_heading() {return 'Vaccinations';}
	function imm_newheading() {return 'Nouveau vaccinations';}
	function imm_success_new() {return 'Vous venez d\'ajouter une nouvelle vaccination.';}
	function imm_editheading() {return 'Modifier une personne';}
	function imm_success_edit() {return 'Cette personne a été mise à jour avec succès.';}
	function imm_invalid_edit() {return 'Cette personne n\'a pas pu être mis à jour, s\'il vous plaît assurez-vous que toutes les informations étaient correctes.';}
	function imm_deleteheading() {return 'Supprimer une personne';}
	function imm_success_delete() {return 'Cette personne a été supprimé avec succès.';}
	function imm_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cette personne?';}
	function imm_firstname() {return 'Prénom';}
	function imm_lastname() {return 'Nom De Famille';}
	function imm_first() {return 'Premier';}
	function imm_last() {return 'Dernier';}
	function imm_employeeid() {return 'Identification des employés';}
	function imm_email() {return 'Courriel';}
	function imm_address() {return 'Adresse';}
	function imm_phone() {return 'Téléphone';}
	function imm_notes() {return 'Remarques';}
	function imm_more() {return 'Plus';}
	function imm_date() {return 'Date';}
	function imm_employee() {return 'Employé';}
	function imm_supervisor() {return 'Superviseur';}
	function imm_expiration() {return 'Expiration';}
	function imm_ifapplicable() {return '(si applicable)';}
	function imm_status() {return 'Status';}
	function imm_addimmunization() {return 'Ajouter vaccination';}
	function imm_immunization() {return 'Vaccination';}
	function imm_persons() {return 'Personnes';}
	function imm_person() {return 'Personne';}
	function imm_newpersons() {return 'Nouveau personnes';}
	function imm_pickstatus($status) {
		switch($status)
		{
			case 'completed':
				return 'Terminé';break;
			case 'declined':
				return 'Refusée';break;
			case 'negative':
				return 'Négatif';break;
			case 'offer':
				return 'Offre';break;
			case 'other':
				return 'Autre';break;
			default:
				return $status;break;
		}
	}
	function imm_notonreportsheading() {return 'Non Sur Rapports';}
	function imm_onreports() {return 'Cette personne montre actuellement sur les rapports';}
	function imm_notonreports() {return 'Cette personne ne le fait actuellement<b><i> pas </i></b>apparaît sur les rapports';}
	function imm_none() {return 'Il n\'y a aucune vaccinations pour cet emplacement.';}
	
	//documents
	function doc_heading() {return 'Documents';}
	function doc_newheading() {return 'Nouveau document';}
	function doc_success_new() {return 'Vous venez d\'ajouter un nouveau document.';}
	function doc_editheading() {return 'Modifier le document';}
	function doc_success_edit() {return 'Ce document a été mis à jour avec succès.';}
	function doc_deleteheading() {return 'Supprimer le document';}
	function doc_success_delete() {return 'Ce document a été supprimé avec succès.';}
	function doc_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer ce document?';}
	function doc_firstname() {return 'Prénom';}
	function doc_lastname() {return 'Nom De Famille';}
	function doc_first() {return 'Premier';}
	function doc_last() {return 'Dernier';}
	function doc_employeeid() {return 'Identification des employés';}
	function doc_email() {return 'Courriel';}
	function doc_address() {return 'Adresse';}
	function doc_phone() {return 'Téléphone';}
	function doc_notes() {return 'Remarques';}
	function doc_more() {return 'Plus';}
	function doc_type() {return 'Type';}
	function doc_name() {return 'Nom';}
	function doc_date() {return 'Date';}
	function doc_nextdate() {return 'Prochaine date';}
	function doc_employee() {return 'Employé';}
	function doc_supervisor() {return 'Superviseur';}
	function doc_files() {return 'Fichiers';}
	function doc_status() {return 'Status';}
	function doc_adddocument() {return 'Ajouter Document';}
	function doc_document() {return 'Document';}
	function doc_persons() {return 'Personnes';}
	function doc_person() {return 'Personne';}
	function doc_newpersons() {return 'Nouveau Personnes';}
	function doc_peopleaffected() {return 'Les personnes touchées';}
	function doc_affected() {return 'Affecté';}
	function doc_nopeopleaffected() {return 'Il n\'y a pas attaché à ce document spécifique.';}
	function doc_pickstatus($status) {
		switch($status)
		{
			case 'uncompleted':
				return 'Inachevé';
			case 'completed':
				return 'Terminé';
			default:
				return $status;
		}
	}
	function doc_none() {return 'Il n\'y a actuellement pas de documents pour ce lieu.';}
	
	//persons
	function per_heading() {return 'Personnes';}
	function per_newheading() {return 'Nouvelle personne';}
	function per_moveheading() {return 'Transporter la personne';}
	function per_movepersons() {return 'Déplacer';}
	function per_success_new() {return 'Vous venez d\'ajouter une nouvelle personne.';}
	function per_success_move() {return 'Vous avez déménagé avec succès personne.';}
	function per_editheading() {return 'Modifier une personne';}
	function per_success_edit() {return 'Votre personne a été mise à jour avec succès.';}
	function per_invalid_edit() {return 'Vous devez utiliser des données valides à jour personnes.';}
	function per_deleteheading() {return 'Supprimer une personne';}
	function per_success_delete() {return 'Votre personne a été supprimé avec succès.';}
	function per_invalid_delete() {return 'Votre personne n\'a pas pu être supprimé.';}
	function per_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cette personne?';}
	function per_person() {return 'Personne';}
	function per_name() {return 'Nom';}
	function per_firstname() {return 'Prénom';}
	function per_lastname() {return 'Nom De Famille';}
	function per_first() {return 'Premier';}
	function per_last() {return 'Dernier';}
	function per_employeeid() {return 'Identification des employés';}
	function per_email() {return 'Courriel';}
	function per_phone() {return 'Téléphone';}
	function per_address() {return 'Adresse';}
	function per_city() {return 'Ville';}
	function per_state() {return 'Province';}
	function per_zip() {return 'Code postal';}
	function per_notes() {return 'Remarques';}
	function per_onreports() {return 'Cette personne montre actuellement sur les rapports';}
	function per_notonreports() {return 'Cette personne ne le fait actuellement <b><i>pas</i></b> montre sur rapports';}
	function per_notonreportsheading() {return 'Non Sur Rapports';}
	function per_nomove() {return 'Aucun deplacement';}
	function per_none() {return 'Il n\'y a pas de personnes pour cet endroit.';}
	
	//licenses
	function lic_heading() {return 'Licenses';}
	function lic_newheading() {return 'Nouvelle License';}
	function lic_success_new() {return 'Vous venez d\'ajouter une nouvelle licence.';}
	function lic_editheading() {return 'Modifier Licence';}
	function lic_success_edit() {return 'Votre licence a été mis à jour avec succès.';}
	function lic_invalid_edit() {return 'Vous devez utiliser des données valides de mettre à jour des licences.';}
	function lic_deleteheading() {return 'Supprimer licence';}
	function lic_success_delete() {return 'Votre licence a été supprimé avec succès.';}
	function lic_confirm_delete() {return 'Êtes-vous sûr de vouloir supprimer cette licence?';}
	function lic_person_firstname() {return 'Prénom';}
	function lic_person_lastname() {return 'Nom De Famille';}
	function lic_person_first() {return 'Premier';}
	function lic_person_last() {return 'Dernier';}
	function lic_person_employeeid() {return 'Identification des employés';}
	function lic_person_email() {return 'Courriel';}
	function lic_person_address() {return 'Adresse';}
	function lic_person_phone() {return 'Téléphone';}
	function lic_person_notes() {return 'Remarques';}
	function lic_license() {return 'License';}
	function lic_licensename() {return 'License Nom';}
	function lic_status() {return 'Status';}
	function lic_pickstatus($status) {
		switch($status)
		{
			case 'inactive':
				return 'Inactif';
			case 'active':
				return 'Actif';
			default:
				return $status;
		}
	}
	function lic_agency() {return 'Agence';}
	function lic_number() {return 'Numéro de licence';}
	function lic_dateissued() {return 'Orginal Date de publication';}
	function lic_renewaldate() {return 'Dernière date de renouvellement';}
	function lic_expiration() {return 'Expiration Suivant';}
	function lic_upload() {return 'Télécharger';}
	function lic_companyunitsneeded() {return 'Unités de la Société Nécessaires';}
	function lic_notes() {return 'Remarques';}
	function lic_header_ceus() {return 'Crédits (CEUs)';}
	function lic_ceu_classes() {return 'CEU Classes';}
	function lic_ceu_class() {return 'Class';}
	function lic_ceu_completiondate() {return 'date D\'Achèvement';}
	function lic_ceu_expiration() {return 'Expiration';}
	function lic_ceu_ceu() {return 'CEU';}
	function lic_ceu_code() {return 'Code';}
	function lic_ceu_safety() {return 'Sécurité';}
	function lic_ceu_discipline() {return 'Discipline';}
	function lic_ceu_picker($ceu) {
		switch($ceu) {
			case 'ceu':
				return 'CEU';
			case 'code':
				return 'Code';
			case 'safety':
				return 'Sécurité';
			case 'discipline':
				return 'Discipline';
			default:
				return $ceu;
		}
	}
	function lic_ceu_neededpicker($ceu) {
		switch($ceu) {
			case 'ceu':
				return 'CEU nécessaire';
			case 'code':
				return 'Code nécessaire';
			case 'safety':
				return 'Sécurité nécessaire';
			case 'discipline':
				return 'Discipline nécessaire';
			default:
				return $ceu;
		}
	}
	function lic_person() {return 'Personne';}
	function lic_notonreportsheading() {return 'Non Sur Rapports';}
	function lic_licenseonreports() {return 'Cette personne montre actuellement sur les rapports';}
	function lic_licensenotonreports() {return 'Cette personne ne le fait actuellement<b><i> pas </i></b>apparaît sur les rapports';}
	function lic_none() {return 'Il n\'y a pas de licences pour ce lieu.';}
	
	//aed
	function aed_heading() {return 'DEA';}
	function aed_newaedheading() {return 'Nouveau DEA';}
	function aed_newaed() {return 'Nouveau DEA';}
	function aed_success_newaed() {return 'Votre DEA a été ajouté avec succès.';}
	function aed_deleteaedheading() {return 'Supprimer DEA';}
	function aed_confirm_delete() {return 'Etes-vous sûr de vouloir supprimer cette DEA?';}
	function aed_success_deleteaedheading() {return 'Votre DEA a été supprimé.';}
	function aed_invalid_deleteaedheading() {return 'Votre DEA pas pu être supprimé.';}
	function aed_editaedheading() {return 'Modifier DEA';}
	function aed_success_editaedheading() {return 'Votre DEA a été modifié avec succès.';}	
	function aed_invalid_editaedheading() {return 'Votre DEA ne pouvait pas être modifié.';}	
	function aed_unlockaed_heading() {return 'Débloquez dea';}
	function aed_success_unlockaed() {return 'Votre DEA a été déverrouillé avec succès.';}	
	function aed_invalid_unlockaed() {return 'Votre DEA n\'a pas pu être déverrouillé, s\'il vous plaît essayer de nouveau ou contacter le support au '.SUPPORTEMAIL;}	
	function aed_aedbrand() {return 'DEA Marque';}
	function aed_aedmodel() {return 'DEA Modèle';}
	function aed_mustenterkeycode() {return 'Vous devez entrer un code d\'activation pour ajouter un DEA.';}
	function aed_brand() {return 'Marque';}
	function aed_model() {return 'Modèle';}
	function aed_serial() {return 'Numéro De Série';}
	function aed_purchasetype() {return 'Type d\'achat';}
	function aed_pickpurchasetype($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'own':
				return 'Propre';
			case 'lease':
				return 'Bail';
			case 'rent':
				return 'Louer';
			default: return 'ERROR UNKNOWN TYPE:'.$type;
		}
	}
	function aed_purchasedate() {return 'Date D\'Achat';}
	function aed_warranty() {return 'Garantie';}
	function aed_location() {return 'Emplacement';}
	function aed_installdate() {return 'Date d\'installation';}
	function aed_sitecoordinator() {return 'Coordonnateur du site';}
	function aed_plantype() {return 'Type de plan';}
	function aed_pickplantype($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'none':
				return 'None';
			case 'programmanagement':
				return 'Gestion du programme';
			case 'medicaldirection':
				return 'Direction médicale';
			case 'programmanagement1':
				return 'Gestion du programme-1 an';
			case 'programmanagement3':
				return 'Gestion du programme-3 an';
			case 'programmanagement4':
				return 'Gestion du programme-4 an';
			case 'programmanagement5':
				return 'Gestion du programme-5 an';
			case 'renta':
				return 'Location-annuel';
			case 'rents':
				return 'Location-Saisonnier';
			case 'rentm':
				return 'Location-mensuel';
			case 'rentq':
				return 'Location-trimestriel';
			case 'aedpmbasic1':
				return 'DEA de gestion du programme de base - 1';
			case 'aedpmstandard1':
				return 'DEA standard de gestion du programme - 1';
			case 'aedpmpremium1':
				return 'DEA Programme Prime gestion - 1';
			case 'aedcoord':
				return 'DEA coordonnateur des services';
			default: return 'ERROR UNKNOWN TYPE:'.$type;
		}
	}
	function aed_servicecheck() {return 'Entretien Vérifier';}
	function aed_pickservicechecklength($days) {
		if($days == '' || $days == null)
			return 'None';
		switch($days) {
			case '0':
				return 'Aucun';
			case '30':
				return 'Mensuel';
			case '91':
				return 'Trimestriel';
			case '182':
				return 'Semestriellement';
			case '365':
				return 'Annuel';
			default: return 'ERROR UNKNOWN DAYS:'.$days;
		}
	}
	function aed_plandate() {return 'Plan de date';}
	function aed_planexpiration() {return 'Plan Expiration';}
	function aed_hasannualcheck() {return 'A Vérifier annuel';}
	function aed_lastcheck() {return 'Dernière Vérifier';}
	function aed_lastservice() {return 'Dernier service';}
	function aed_notes() {return 'Remarques';}
	function aed_unlockaed_keycode() {return 'Code Clé';}
	function aed_unlockaed_days() {return 'Journées';}
	function aed_outofservice() {return 'Hors service';}
	function aed_inservice() {return 'En Service';}
	function aed_checkbox_outofservice() {return 'Ce DEA est actuellement hors service.';}
	function aed_nocheckinstructions() {return 'Ne plus afficher ce message à moi.';}
	function aed_unlockaed_none() {return 'Il n\'y a pas de codes d\'activation plus valides pour votre organisation, s\'il vous plaît contacter le support au '.SUPPORTEMAIL;}
	function aed_none() {return 'Il n\'y a aucune ASD pour cet emplacement.';}

    function aed_moveaeds()
    {
        return 'D&#233;placer DEA';
    }
	function aed_attachaeds() {return 'Fixez DEA de';}
	function aed_dontattach() {return 'Ne fixez pas';}
	function aed_success_move() {return 'DEA de succ�s d�plac�';}
	function aed_success_attach() {return 'attach�e avec succ�s DEA de';}
	function aed_fail_attach_pad($typename, $model) {return 'Impossible de joindre PAD: '.$typename.', le AED: '.$model.'. Pas de cr�neaux disponibles.';}
	function aed_fail_attach_pak($typename, $model) {return 'Impossible de joindre PAK: '.$typename.', le AED: '.$model.'. Pas de cr�neaux disponibles.';}
	function aed_fail_attach_battery($typename, $model) {return 'Impossible de joindre Batterie: '.$typename.', le AED: '.$model.'. Pas de cr�neaux disponibles.';}
	function aed_fail_attach_accessory($typename, $model) {return 'Impossible de joindre Accessoire: '.$typename.', le AED: '.$model.'. Pas de cr�neaux disponibles.';}
	function aed_invalid_attach_pad($typename, $model) {return 'Impossible de joindre PAD: '.$typename.', le AED: '.$model.'. Pas compatible.';}
	function aed_invalid_attach_pak($typename, $model) {return 'Impossible de joindre PAK: '.$typename.', le AED: '.$model.'. Pas compatible.';}
	function aed_invalid_attach_battery($typename, $model) {return 'Impossible de joindre Batterie: '.$typename.', le AED: '.$model.'. Pas compatible.';}
	function aed_invalid_attach_accessory($typename, $model) {return 'Impossible de joindre Accessoire: '.$typename.', le AED: '.$model.'. Pas compatible.';}
	
	//accessories
	function aed_accessories() {return 'Accessoires';}
	function aed_newaccessory() {return 'Nouvel accessoire';}
	function aed_success_newaccessory() {return 'Votre nouvel accessoire a été ajouté avec succès.';}
	function aed_confirm_deleteaccessory() {return 'Êtes-vous sûr de vouloir supprimer cet accessoire?';}
	function aed_success_deleteaccessory() {return 'Votre accessoire a été supprimé.';}
	function aed_invalid_deleteaccessory() {return 'Votre accessoire ne peut pas être supprimé.';}
	function aed_editaccessory() {return 'Modifier accessoire';}
	function aed_success_editaccessory() {return 'Votre accessoire a été changé avec succès.';}
	function aed_invalid_editaccessory() {return 'Votre accessoire ne pouvait pas être changé.';}
	function aed_spare() {return 'De rechange';}

    function aed_spares()
    {
        return 'Pièces de rechange';
    }
	function aed_pad() {return 'Pad';}

    function aed_spare_pad_1()
    {
        return 'Pad de rechange 1';
    }

    function aed_spare_pad_2()
    {
        return 'Pad de rechange 2';
    }

    function aed_pediatric_pad()
    {
        return 'Pad p�diatrique';
    }

    function aed_spare_pediatric_pad()
    {
        return 'Pad p�diatrique de rechange';
    }

    function aed_pak()
    {
        return 'Pak';
    }

    function aed_spare_pak_1()
    {
        return 'Pak de rechange 1';
    }

    function aed_spare_pak_2()
    {
        return 'Pak de rechange 1';
    }

    function aed_pediatric_pak()
    {
        return 'Pak p�diatrique';
    }

    function aed_spare_pediatric_pak()
    {
        return 'Pak p�diatrique de rechange';
    }
	function aed_battery() {return 'Batterie';}

    function aed_spare_battery()
    {
        return 'batterie de rechange';
    }

    function aed_accessory()
    {
        return 'Accessoire';
    }

    function aed_spare_accessory()
    {
        return 'Spare accessoire';
    }
	function aed_pads() {return 'Pads';}
	function aed_paks() {return 'Paks & Accessoires';}
	function aed_batteries() {return 'Batteries';}
	function aed_accessoryname() {return 'Nom';}
	function aed_accessorytype() {return 'Type';}

    function aed_part_number()
    {
        return 'R�f�rence';
    }
	function aed_accessorylot() {return 'Numéro de lot';}
	function aed_accessoryexp() {return 'Expiration';}
	function aed_accessorynone() {return 'Il n\'y a pas d\'accessoires pour ce DEA.';}
   
	//keycode input
	function rqk_heading() {return 'Code-clé d\'entrée';}
	function rqk_invalid_entry() {return 'Code-clé invalide. S\'il vous plaît entrer un nouveau mot de code ci-dessous.';}
	function rqk_aedlocked() {return 'Ce DEA a été bloqué en raison de l’expiration d’un code-clé. S\'il vous plaît entrer un nouveau mot de code ci-dessous.';}
   
	//users
	function use_heading() {return 'Gestion utilisateurs';}
	function use_newprofileheading() {return 'Profil nouvel utilisateur';}
	function use_newrightsheading() {return 'Droits nouvel utilisateur';}
	function use_newprivsheading() {return 'Privilèges nouvel utilisateur';}
	function use_editprofileheading() {return 'Modifier Profil utilisateur';}
	function use_editrightsheading() {return 'Modifier les droits utilisateur';}
	function use_editprivsheading() {return 'Modifier les privilèges de l\'utilisateur';}
	function use_success_edituser() {return 'L\'utilisateur a été changé avec succès.';}
	function use_success_deleteuser() {return 'L’utilisateur a été effacé.';}
	function but_newuser() {return 'Utilisateur-Nouveau';}
	function but_edituser() {return 'Modifier utilisateur';}
	function but_deleteuser() {return 'Effacer utilisateur';}
	function use_invalid_uniqueusername() {return 'Ce nom d’utilisateur est déjà utilisé.';}
	function use_invalid_emptyusername() {return 'Vous devez entrer une adresse électronique/un nom d’utilisateur valides.';}
	function use_profile() {return 'Profil';}
	function use_generalinfo() {return 'Renseignements généraux';}
	function use_contactinfo() {return 'Coordonnées';}
	function use_rights() {return 'Droits';}
	function use_privs() {return 'Privilèges';}
	function use_resetpassword() {return 'Réinitialiser Le Mot De Passe';}
	function use_usernameemail() {return 'Nom d’utilisateur/Courriel';}
	function use_checkall() {return 'Cochez Tous';}
	function use_uncheckall() {return 'Uncheck All';}
	function use_quickselector() {return 'Quid Sele-ctoc';}
	function use_qs_companyadmin() {return 'Administration compagnie';}
	function use_qs_user() {return 'Utilisateur';}
	function use_qs_viewuser() {return 'Voir utilisateur';}
	function use_creationemail_subject() {return 'Bienvenue à '.PROGRAM;}
	function use_creationemail_body($username, $password="(non représenté pour des questions de confidentialité)") {
		return '
		<html><head></head><body style="color:#000001;">
			<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
				<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
					<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
						<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
					</h1>
					<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
					<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
						Bienvenue
					</h2>
					<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
						Vous avez récemment été invité à rejoindre <a href="'.URL.'">'.PROGRAM.'</a>. Ceci est un programme qui vous permet
						à votre gestionnaire des équipements tels que les DEA et les trousses de premiers soins.<br />
						<b>Connexion à'.PROGRAM.' is simple!</b>
							<div style="margin-left:10px;font-weight:bold;">
							Nom d\'utilisateur: '.$username.'<br />
							Mot de passe: '.$password.'
							</div>
						Si vous pensez que vous avez obtenu ce message par accident, s\'il vous plaît ne pas tenir compte.
						<br />
						Si vous avez des questions, s\'il vous plaît sentir libre à nous contacter au<b>'.SUPPORTEMAIL.'</b>.
					</div>
				</div>
			</div>
			<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">'.$this->mes_pleasedonotreply().'</div>
			<br />
			<div style="width:970px;margin:0 auto;color:#000001;">'.$this->mes_tscopyright().'</div>
		</body></html>';
	}
	function use_creationemail_addbody($username, $password="(not shown for privacy issues)") {
		return 'You have recently been invited to join '.PROGRAM.' - '.URL.'.  
Ceci est un programme qui vous permet de manager, votre équipement tel que les DEA et les trousses de premiers soins.
Connexion à '.PROGRAM.' is simple!
	Nom d\'utilisateur: '.$username.'
	Mot de passe: '.$password.'
Si vous avez des questions, s\'il vous plaît sentir libre à nous contacter au '.SUPPORTEMAIL.'.
Si vous pensez que vous avez obtenu ce message par accident, s\'il vous plaît ne pas tenir compte.';
	}
	function use_profilechangeemail_subject() {return PROGRAM.' Profile Change';}
	function use_profilechangeemail_body($username, $password="(not shown for privacy issues)") {
		return '
		<html><head></head><body style="color:#000001;">
			<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
				<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
					<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
						<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
					</h1>
					<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
					<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
						Profile Change
					</h2>
					<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
						Vous avez eu récemment changé votre profil à <a href="'.URL.'">'.PROGRAM.'</a>.  Ceci est un programme qui vous permet
						à votre gestionnaire des équipements tels que les DEA et les trousses de premiers soins.<br />
						<b>Votre nouvelles informations de connexion:</b>
							<div style="margin-left:10px;font-weight:bold;">
							Nom d\'utilisateur: '.$username.'<br />
							Mot de passe: '.$password.'
							</div>
						Si vous pensez que vous avez obtenu ce message par accident, s\'il vous plaît ne pas tenir compte.
						<br />
						Si vous avez des questions, s\'il vous plaît sentir libre à nous contacter au <b>'.SUPPORTEMAIL.'</b>.
					</div>
				</div>
			</div>
			<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
				S\'il vous plaît ne pas répondre à cet e-mail. Nous sommes incapables de répondre aux demandes de renseignements envoyés à cette adresse.
			</div>
			<br />
			<div style="width:970px;margin:0 auto;color:#000001;">
				Copyright &copy;  2015 Think Safe Inc.
			</div>
		</body></html>';
	}
	function use_profilechangeemail_addbody($username, $password="(non affiché pour des questions de confidentialité)") {
		return 'Vous avez eu récemment changé votre profil à '.PROGRAM.' - '.URL.'.  
Ceci est un programme qui vous permet de manager, votre équipement tel que les DEA et les trousses de premiers soins.
Connexion à '.PROGRAM.' is simple!
	Nom d\'utilisateur: '.$username.'
	Mot de passe: '.$password.'
Si vous avez des questions anymore, contacter notre support technique au '.SUPPORTEMAIL.'.
Si vous pensez que vous avez obtenu ce message par accident, s\'il vous plaît ne pas tenir compte.';
	}   
	function use_getpriv() {
		return $privlist = array(
			'showaed' => 'Afficher DEA languette',
			'newaed' => 'Nouveau DEA',
			'editaed' => 'Modifier DEA',
			'deleteaed' => 'supprimer DEA',
			'newspare' => 'Nouveau Spare',
			'editspare' => 'Modifier Spare',
			'deletespare' => 'supprimer de rechange',
			'showerp' => 'Afficher ERP',
			'newerp' => 'Nouveau ERP',
			'deleteerp' => 'supprimer ERP',
            'showmasteraed' => 'Show Master AED',
			'showmedicaldirection' => 'Montrer Direction médicale',
			'newmedicaldirection' => 'Nouveau directeur médical',
			'editmedicaldirection' => 'Modifier directeur médical',
			'deletemedicaldirection' => 'Supprimer directeur médical',
			'showevents' => 'Afficher les événements',
			'newevents' => 'Nouvel événement',
			'deleteevents' => 'supprimer l\'événement',
			'showrecalls' => 'Afficher les rappels',
			'newrecalls' => 'Nouveau rappel',
            'editrecalls' => 'Modifier les rappels',
			'deleterecalls' => 'supprimer Recall',
			'showstatelaws' => 'Afficher lois de l\'Etat',
			'showequipment' => 'Afficher Équipement languette',
			'newequipment' => 'Nouvel équipement',
			'editequipment' => 'Modifier Equipement',
			'deleteequipment' => 'supprimer Equipement',
			'newequipmentcheck' => 'Nouvelle vérification de l\'équipement',
			'editequipmentcheck' => 'Modifier vérification de l\'équipement',
			'showpersons' => 'Montrer Personnes languette',
			'newpersons' => 'nouvelle personne',
			'editpersons' => 'Modifier une personne',
			'deletepersons' => 'supprimer une personne',
			'movepersons' => 'Transporter la personne',
			'showtraining' => 'Voir fiche formation',
			'newtraining' => 'nouvelle formation',
			'edittraining' => 'Modifier formation',
			'deletetraining' => 'supprimer formation',
			'printcards' => 'imprimer des cartes',
			'showdocuments' => 'Afficher les documents languette',
			'newdocuments' => 'Nouveau document',
			'editdocuments' => 'Modifier le document',
			'deletedocuments' => 'supprimer le document',
			'showlicenses' => 'Afficher Licences languette',
			'newlicenses' => 'Nouvelle licence',
			'editlicenses' => 'Modifier Licence',
			'deletelicenses' => 'supprimer licence',
			'showimmunizations' => 'Afficher Vaccinations languette',
			'newimmunizations' => 'Nouveau vaccination',
			'editimmunizations' => 'Modifier la vaccination',
			'deleteimmunizations' => 'supprimer la vaccination',
			'showhazard' => 'Afficher dangers languette',
			'newhazard' => 'Nouveau danger',
            'edithazard' => 'Modifier danger',
			'deletehazard' => 'supprimer danger',
			'showcontact' => 'Voir Onglet Contacts',
			'newcontact' => 'Nouveau contact',
			'editcontact' => 'Modifier le contact',
			'deletecontact' => 'supprimer le contact',
			'showaedcheck' => 'Afficher DEA Vérifier languette',
			'newaedcheck' => 'Nouveau DEA Vérifier',
			'editaedcheck' => 'Modifier DEA Voir les commentaires',
			'groupaedcheck' => 'Groupe DEA Vérifier',
			'showaedservicing' => 'Afficher DEA entretien languette',
			'newaedservicing' => 'Nouveau DEA entretien',
			'editaedservicing' => 'Modifier DEA entretien Commentaires',
			'showfirstvoice' => 'Montrer d\'abord Équipement Voix languette',
			'newfirstvoice' => 'Nouveau premier équipement Voix',
			'editfirstvoice' => 'Modifier premier équipement Voix',
			'deletefirstvoice' => 'Supprimer d\'abord Équipement Voix',
			'newfirstvoicecheck' => 'Nouveau premier équipement Voix Vérifier',
			'editfirstvoicecheck' => 'Modifier premier équipement Voix Vérifier',
			'showadmin' => 'Afficher la section admin',
			'showuser' => 'Voir Gérer les utilisateurs languette',
			'newuser' => 'Nouveau utilisateur',
			'edituser' => 'Modifier l\'utilisateur',
			'deleteuser' => 'supprimer l\'utilisateur',
			'showorganization' => 'Afficher Organisation languette',
			'neworganization' => 'Organisation Nouveau',
			'editorganization' => 'Modifier Organisation',
			'deleteorganization' => 'Supprimer Organisation',
			'newlocation' => 'Nouveau Lieu',
			'editlocation' => 'Modifier Situation',
			'deletelocation' => 'supprimer Localisation',
			'showkeycode' => 'Afficher Code Clé languette',
			'generatekeycode' => 'générer des codes d\'activation',
			'showkeycodepool' => 'Afficher Code Clé Piscine',
			'assignkeycodepool' => 'Attribuer Code Clé Piscine',
			'deletekeycodepool' => 'Supprimer de Code Clé Piscine',
			'newkeycode' => 'Nouveau code d\'activation',
			'showalert' => 'Montrer les alertes languette',
			'newalert' => 'Nouveau alerte',
			'editalert' => 'Modifier alerte',
			'deletealert' => 'supprimer alerte',
			'showtracing' => 'Afficher Tracing languette',
			'newtracing' => 'Nouveau Tracing',
			'edittracing' => 'Modifier Tracing',
			'deletetracing' => 'supprimer Tracing',
			'assigntracing' => 'Attribuer Tracing',
			'showsetup' => 'Afficher languette de configuration',
			'showsetuphazards' => 'Configuration Afficher les dangers',
			'newhazardtype' => 'Types Nouveau danger',
			'edithazardtype' => 'Modifier les types de danger',
			'deletehazardtype' => 'Supprimer Types danger',
			'newhazardalltype' => 'Types de danger Nouveau par défaut',
			'edithazardalltype' => 'Types de danger Edit Default',
			'deletehazardalltype' => 'Supprimer Types défaut de danger',
			'showsetupdocuments' => 'Voir Configuration des documents languette',
			'newdocumenttype' => 'Nouveaux types de documents par défaut',
			'editdocumenttype' => 'Modifier les types de documents',
			'deletedocumenttype' => 'Supprimer Types de documents',
			'newdocumentalltype' => 'Types de documents Nouveau par défaut',
			'editdocumentalltype' => 'Types de documents Edit Default',
			'deletedocumentalltype' => 'Supprimer Types défaut de documents',
			'showsetupimmunizations' => 'Voir Immmunizations système languette',
			'newimmunizationtype' => 'Types Nouveau vaccination',
			'editimmunizationtype' => 'Modifier les types de vaccination',
			'deleteimmunizationtype' => 'Supprimer Types vaccination',
			'newimmunizationalltype' => 'Nouveaux types défaut de vaccination',
			'editimmunizationalltype' => 'Types de vaccination modificateur par défaut',
			'deleteimmunizationalltype' => 'Supprimer Types défaut de vaccination',
			'showsetuplicenses' => 'Afficher Licences système languette',
			'newlicensetype' => 'Types Nouveau de licence',
			'editlicensetype' => 'Modifier les types de licence',
			'deletelicensetype' => 'Supprimer Types de licences',
			'newlicensealltype' => 'Nouveau Types défaut de licence',
			'editlicensealltype' => 'Types de licences Edit Default',
			'deletelicensealltype' => 'Supprimer Types défaut de licence',
			'showsetuptraining' => 'Afficher languette de configuration de formation',
			'newtrainingcourse' => 'Cours de formation de Nouveau',
			'edittrainingcourse' => 'Cours de formation Modifier',
			'deletetrainingcourse' => 'Supprimer Cours de formation',
			'newtrainingallcourse' => 'Cours de formation Nouveau par défaut',
			'edittrainingallcourse' => 'Cours de formation Edit Default',
			'deletetrainingallcourse' => 'Supprimer Cours de formation par défaut',
			'showtrainingprints' => 'Voir Prints de formation',
			'edittrainingprints' => 'Modifier Prints formation',
			'showsetupequipment' => 'Afficher languette d\'installation de l\'équipement',
			'newequipmenttypes' => 'Matériel Nouveau Catégories',
			'editequipmenttypes' => 'Modifier équipement Catégories',
			'deleteequipmenttypes' => 'Supprimer équipement Catégories',
			'newequipmentstyles' => 'Nouveau sous-catégories Matériel',
			'editequipmentstyles' => 'Modifier l\'équipement sous-catégories',
			'deleteequipmentstyles' => 'Supprimer l\'équipement sous-catégories',
			'newequipmentalltypes' => 'Nouveau défaut équipement Catégories',
			'editequipmentalltypes' => 'Modifier défaut équipement Catégories',
			'deleteequipmentalltypes' => 'Supprimer défaut équipement Catégories',
			'newequipmentallstyles' => 'Nouveau défaut équipement sous-catégories',
			'editequipmentallstyles' => 'Modifier par défaut de l\'équipement sous-catégories',
			'deleteequipmentallstyles' => 'Supprimer défaut équipement sous-catégories',
			'showsetupfirstvoice' => 'Languette de configuration Afficher Frist Voix',
			'newfirstvoicetypes' => 'Nouveau premier équipement Voix Catégories',
			'editfirstvoicetypes' => 'Modifier premier équipement Voix Catégories',
			'deletefirstvoicetypes' => 'Supprimer premier équipement Voix Catégories',
			'newfirstvoicestyles' => 'Nouveau première voix de l\'équipement sous-catégories',
			'editfirstvoicestyles' => 'Modifier première voix de l\'équipement sous-catégories',
			'deletefirstvoicestyles' => 'Supprimer Premières équipement vocal sous-catégories',
			'newfirstvoicealltypes' => 'Nouveau défaut premier équipement Voix Catégories',
			'editfirstvoicealltypes' => 'Modifier défaut premier équipement Voix Catégories',
			'deletefirstvoicealltypes' => 'Supprimer défaut premier équipement Voix Catégories',
			'newfirstvoiceallstyles' => 'Nouveau défaut équipement de premiers vocaux sous-catégories',
			'editfirstvoiceallstyles' => 'Modifier par défaut équipement de premiers vocaux sous-catégories',
			'deletefirstvoiceallstyles' => 'Supprimer défaut première voix de l\'équipement sous-catégories',
			'showscheduler' => 'Afficher Scheduler languette',
			'newscheduler' => 'Nouveau événements programmés',
			'editscheduler' => 'Modifier événements planifiés',
			'deletescheduler' => 'Supprimer événements planifiés'
		);
	}
	
	//aedcheck
	function ack_heading() {return 'Vérification DEA';}

    function ack_groupheading()
    {
        return 'groupe contr�le AED';
    }
	function ack_alertsubject() {return 'ALERTE! Vérifiez récente DEA Terminé - attention requise';}
	function ack_subject() {return 'DEA Vérifiez Terminé';}
	function ack_emailheading() {return 'DEA Vérifiez Confirmation';}
	function ack_emailtopbody($name, $serial, $location, $organization) {return $name.',<br />
										le suivant DEA '.$serial.' à '.$location.', '.$organization.' has been inspected.';}
	function ack_coordemailtopbody($name, $serial, $location, $organization) {return $name.',<br />
										le suivant DEA '.$serial.' à '.$location.', '.$organization.' has been recently inspected.';}
	function ack_newheading() {return 'Vérification DEA-Nouveau';}
	function ack_success_new() {return 'Votre vérification DEA a été enregistrée.';}
	function ack_editheading() {return 'Modifier DEA Vérifier';}
	function ack_success_edit() {return 'Votre DEA a été modifié.';}
	function ack_aed() {return 'DEA';}
	function ack_lastcheck() {return 'Dernière vérification';}
	function ack_pastcheck() {return 'Vérification passée';}
	function ack_annualcheck() {return 'Vérifiez annuel';}
	function ack_lastcheckers() {return 'dernière vérificateur(s)';}
	function ack_generalinfo() {return 'Renseignements généraux';}
	function ack_date() {return 'Date';}
	function ack_location() {return 'Emplacement';}
	function ack_serial() {return 'Numéro de série';}
	function ack_isaedlocation() {return 'Le DEA se trouve-t-il à l’endroit désigné?';}
	function ack_rep_aedlocation() {return 'Designated Area';}
	function ack_isaedclean() {return 'Le DEA a-t-il été nettoyé et est-il intact?';}
	function ack_rep_aedclean() {return 'Cleaned and Undamaged';}
	function ack_isaedinalarm() {return 'Le DEA est-il dans un placard avec alarme?';}
	function ack_rep_aedinalarm() {return 'In Alarmed Cabinet';}
	function ack_isalarmworking() {return 'L’alarme dans le placard où se trouve le DEA fonctionne-t-elle?';}
	function ack_rep_aedalarmworks() {return 'Alarm Works';}
	function ack_replacingalarmbattery() {return 'Avez-vous remplacé les piles de cette alarme?';}
	function ack_rep_aedreplacingalarmbatteries() {return 'Replacing Alarm Batteries';}
	function ack_alarmbatteryinsertiondate() {return 'Date insertion';}
	function ack_alarmbatterychangedate() {return 'Date modifié';}
	function ack_alarmbatteryexpirationdate() {return 'Expiration Date';}
	function ack_kitpresent() {return 'Y a-t-il une trousse de sauvetage DEA?';}
	function ack_rep_aedrescuekitpresent() {return 'Rescue Kit Present';}
	function ack_kitsealed() {return 'Cette trousse de sauvetage est-elle scellée et intacte?';}
	function ack_rep_aedrescuekitsealed() {return 'Rescue Kit Sealed';}
	function ack_kitinclude() {return 'Cette trousse de sauvetage inclut-elle ces articles?';}
	function ack_gloves() {return 'Gants';}
	function ack_razor() {return 'Rasoir preparation';}
	function ack_scissors() {return 'Ciseaux';}
	function ack_towel() {return 'Serviette';}
	function ack_towlette() {return 'Lingette humide';}
	function ack_mask() {return 'Masque RCR';}
	function ack_pads() {return 'Coussinets électrode';}
	function ack_paks() {return 'Nécessaires de rechange et accessoires';}
	function ack_batteries() {return 'Piles';}
	function ack_type() {return 'Type';}
	function ack_lot() {return 'Lot Number';}
	function ack_expiration() {return 'Expiration';}
	function ack_ispadattechedtoaed($exp, $lot) {return 'Ces électrodes sont-elles connectées à ľappareil? <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ack_isinstallingnewpads() {return 'Avez-vous installé de nouveaux coussinets?';}
	function ack_isinstallingnewpedpads() {return 'Are you installing new pediatric electrodes(pads)?';}
	function ack_isinstallingnewsparepads() {return 'Are you installing new spare electrodes(pads)?';}
	function ack_isinstallingnewsparepedpads() {return 'Are you installing new spare pediatric electrodes(pads)?';}
	function ack_whichpadsinstalling() {return 'Quelles électrodes installez-vous?';}
	function ack_ispadready() {return 'Après l\'installation de nouvelles électrodes, avez-vous mis en marche l\'appareil afin qu\'il puisse effectuer une vérification automatique et donner une indication visuelle ou auditive de son bon état de marche?';}
	function ack_belowpadspresent() {return 'Les coussinets électrodes suivants sont-ils présents?';}
	function ack_ispadsealed() {return 'Les ensembles d\'électrodes sont-ils fermés et la date d\'expiration est-elle échue?';}
	function ack_isbatteryattechedtoaed($exp, $lot) {return 'Cette pile est-elle branchée à l\'appareil? <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ack_isinstallingnewbattery() {return 'Avez-vous installé une nouvelle pile?';}
	function ack_isinstallingnewsparebattery() {return 'Are you installing a new spare battery?';}
	function ack_isinstallingnewaccessory() {return 'Are you installing a new accessory?';}
	function ack_isinstallingnewspareaccessory() {return 'Are you installing a new spare accessory?';}
	function ack_whichbatteryinstalling() {return 'Quelle pile installez-vous?';}
	function ack_isbatteryready() {return 'Après l\'installation de la nouvelle pile, avez-vous mis en marche l\'appareil afin qu\'il puisse effectuer une vérification automatique et donner une indication visuelle ou auditive de son bon état de marche?';}
	function ack_belowbatteriespresent() {return 'Les piles ci-dessous (si présentes) sont-elles installées?';}
	function ack_isbatterysealed() {return 'Le bloc-pile est-il fermé et la date d\'expiration est-elle échue?';}
	function ack_ispakattechedtoaed($exp, $lot) {return 'Ce nécessaire ou cet accessoire est-il branché à l\'appareil?  <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ack_isinstallingnewpaks() {return 'Installez-vous un nouveau nécessaire ou un nouvel accessoire?';}
	function ack_isinstallingnewsparepaks() {return 'Are you installing a new spare pak?';}
	function ack_isinstallingnewpedpaks() {return 'Are you installing a new pediatric pak?';}
	function ack_isinstallingnewsparepedpaks() {return 'Are you installing a new spare pediatric pak?';}
	function ack_whichpaksinstalling() {return 'Quel nécessaire/accessoire installez-vous?';}
	function ack_ispakready() {return 'Après l\'installation du nouveau nécessaire ou du nouvel accessoire, avez-vous mis en marche l\'appareil afin qu\'il puisse effectuer une vérification automatique et donner une indication visuelle ou auditive de son bon état de marche?';}
	function ack_belowpakspresent() {return 'Les nécessaires ou accessoires ci-dessous (si applicable) sont-ils présents?';}
	function ack_ispaksealed() {return 'L\'ensemble de nécessaire est-il fermé et la date d\'expiration est-elle échue?';}
	function ack_selectedtype($lot, $exp) {return 'Lot:'.$lot.' Exp:'.$exp;}
	function ack_hasindicator() {return 'L’indicateur d’état (Status) est-il illuminé en vert et le DEA affiche-t-il le statut Prêt?';}
	function ack_rep_aedreadtstatus() {return 'Ready Status';}
	function ack_checkerpromise() {return '';}//removed for lack of french
	function ack_aedready() {return 'DEA Prêt';}
	function ack_checkedby() {return 'Vérifié par';}
	function ack_fullname() {return 'Nom - Prénom';}
	function ack_email() {return 'Courriel';}
	function ack_confirm_comments() {return 'Vous ne pouvez qu’ajouter vos propres commentaires – Vous n’êtes pas autorisé à modifier les commentaires d’une autre personne. Tous les commentaires sont permanents; il est donc important de vérifier votre travail avant d’entrer vos commentaires.';}
	function ack_comments() {return 'Commentaires';}
	function ack_question() {return 'Question';}
	function ack_responses($resp) {
		switch($resp)
		{
			case 'Yes':
				return 'Oui';
				break;
			case 'yes':
				return 'oui';
				break;
			case 'No':
				return 'Non';
				break;
			case 'no':
				return 'non';
				break;
			case 'Other':
				return 'Other';
				break;
			case 'other':
				return 'other';
				break;
		}
		return $resp;
	}
	function ack_nocheck() {return 'Il n’y a pas encore eu de vérification pour ce DEA.';}
	function ack_nosecondcheck() {return 'Il n’y a pas encore de deuxième vérification pour ce DEA.';}
	function ack_ispadattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ack_issparepadattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ack_ispakattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ack_issparepakattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ack_isbatteryattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ack_issparebatteryattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ack_isaccessoryattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ack_isspareaccessoryattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	
	//aedservicing
	function ask_heading() {return 'DEA entretien';}
	function ask_alertsubject() {return 'ALERTE! Entretien DEA récente Terminé - attention requise';}
	function ask_subject() {return 'DEA entretien terminé';}
	function ask_emailheading() {return 'DEA entretien Confirmation';}
	function ask_emailtopbody($name, $serial, $location, $organization) {return $name.',<br />
										The following DEA '.$serial.' à '.$location.', '.$organization.' has been recently inspected.';}
	function ask_newheading() {return 'Nouveau DEA entretien';}
	function ask_success_new() {return 'Votre entretien DEA a été enregistrée avec succès.';}
	function ask_success_edit() {return 'Votre DEA entretien commentaire a été modifié avec succès.';}
	function ask_invalid_edit() {return 'Votre commentaire DEA entretien ne pouvait être modifié.';}
	function ask_aed() {return 'DEA';}
	function ask_lastservicing() {return 'Dernier entretien';}
	function ask_pastservicing() {return 'Passé entretien';}
	function ask_annualservicing() {return 'Annuel entretien';}
	function ask_lastservicingers() {return 'Dernier Servicinger(s)';}
	function ask_generalinfo() {return 'Informations Générales';}
	function ask_date() {return 'Date';}
	function ask_location() {return 'Emplacement';}
	function ask_serial() {return 'Numéro De Série';}
	function ask_milesdriven() {return 'Kilométrage parcouru pour la visite';}
	function ask_programmanagementexp() {return 'Expiration de gestion du programme';}
	function ask_isaedlocation() {return 'Est l\'unité AED situé dans la zone désignée?';}
	function ask_rep_aedlocation() {return 'Zone désignée';}
	function ask_isaedclean() {return 'Est l\'unité AED nettoyé et en bon état?';}
	function ask_rep_aedclean() {return 'Nettoyé et en bon état';}
	function ask_isaedinalarm() {return 'Est l\'AED dans une armoire alarmé?';}
	function ask_rep_aedinalarm() {return 'Dans Alarmé Cabinet';}
	function ask_isalarmworking() {return 'Est-ce que l\'alarme sur le Cabinet AED marche?';}
	function ask_rep_aedalarmworks() {return 'Travaux d\'alarme';}
	function ask_replacingalarmbattery() {return 'Avez-vous remplacez les piles d\'alarme?';}
	function ask_rep_aedreplacingalarmbatteries() {return 'Remplacement alarme Batteries';}
	function ask_alarmbatteryinsertiondate() {return 'date d\'insertion';}
	function ask_alarmbatterychangedate() {return 'Modification de la date';}
	function ask_alarmbatteryexpirationdate() {return 'Expiration Date';}
	function ask_kitpresent() {return 'Y at-il un DEA kit de secours présente?';}
	function ask_rep_aedrescuekitpresent() {return 'DEA Prep Présent';}
	function ask_kitsealed() {return 'Est la Prep Kit DEA scellé et intact?';}
	function ask_rep_aedrescuekitsealed() {return 'DEA Prep Kit Sealed';}
	function ask_kitinclude() {return 'Est-ce que votre trousse DEA Prep comprennent: gants, prep rasoir, des ciseaux, des serviettes, lingettes / CPR masque?';}
	function ask_kitincludereplace() {return 'Qu\'avez-vous de remplacer ou d\'ajouter à la trousse DEA Prep?';}
	function ask_pads() {return 'Pads';}
	function ask_paks() {return 'Paks & Accessoires';}
	function ask_batteries() {return 'Batteries';}
	function ask_type() {return 'Type';}
	function ask_lot() {return 'Numéro de lot';}
	function ask_expiration() {return 'Expiration';}
	function ask_ispadattechedtoaed($exp, $lot) {return 'Sont ces électrodes (raquettes), attachés à l\'unité? <b> Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ask_isinstallingnewpads() {return 'Vous installez de nouvelles électrodes (raquettes)?';}
	function ask_isinstallingnewpedpads() {return 'Are you installing new pediatric electrodes(pads)?';}
	function ask_isinstallingnewsparepads() {return 'Are you installing new spare electrodes(pads)?';}
	function ask_isinstallingnewsparepedpads() {return 'Are you installing new spare pediatric electrodes(pads)?';}
	function ask_whichpadsinstalling() {return 'Quels électrodes (raquettes) installez-vous?';}
	function ask_ispadready() {return 'Après l\'installation de nouvelles électrodes (raquettes), avez-vous allumez l\'appareil pour lui permettre de compléter un auto-test complet pour confirmer une lumière visibile ou indication sonore de l\'état prêt?';}
	function ask_belowpadspresent() {return 'Ils sont la ci-dessous (le cas échéant) électrodes (raquettes), présente?';}
	function ask_ispadsealed() {return 'Sont les électrodes (raquettes), les paquets scellés et la date de péremption?';}
	function ask_isbatteryattechedtoaed($exp, $lot) {return 'Est-ce la batterie attachée à l\'unité? <b> Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ask_isbatteryengerybars() {return 'Combien de barres sont montrant sur l\'indicateur d\'�tat?';}
	function ask_isinstallingnewbattery() {return 'Vous installez une nouvelle batterie?';}
	function ask_isinstallingnewsparebattery() {return 'Are you installing a new spare battery?';}
	function ask_isinstallingnewaccessory() {return 'Are you installing a new accessory?';}
	function ask_isinstallingnewspareaccessory() {return 'Are you installing a new spare accessory?';}
	function ask_whichbatteryinstalling() {return 'De quelle batterie installez-vous?';}
	function ask_isbatteryready() {return 'After installiAprès avoir installé une nouvelle batterie, avez-vous allumez l\'appareil pour lui permettre de compléter un auto-test complet pour confirmer une lumière visibile ou indication sonore de l\'état prêt?ng a new battery, did you turn on the unit to allow it to complete a full self-test to confirm a visibile light or audible indication of ready status?';}
	function ask_belowbatteriespresent() {return 'Batteries sont ci-dessous (le cas échéant) présente?';}
	function ask_isbatterysealed() {return 'Est le paquet de batterie scellée et la date de péremption?';}
	function ask_ispakattechedtoaed($exp, $lot) {return 'Est-ce pak / accessoire fixé à l\'unité?  <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ask_isinstallingnewpaks() {return 'Est-ce que vous installez un nouveau pak / accessoire?';}
	function ask_isinstallingnewsparepaks() {return 'Are you installing a new spare pak?';}
	function ask_isinstallingnewpedpaks() {return 'Are you installing a new pediatric pak?';}
	function ask_isinstallingnewsparepedpaks() {return 'Are you installing a new spare pediatric pak?';}
	function ask_whichpaksinstalling() {return 'Quels pak / accessoire installez-vous?';}
	function ask_ispakready() {return 'Après avoir installé le nouveau pak ou accessoire, avez-vous allumez \'appareil pour lui permettre de compléter un auto-test complet pour confirmer une lumière visibile ou indication sonore de l\'état prêt?';}
	function ask_belowpakspresent() {return 'Ils sont la ci-dessous (le cas échéant) pak ou accessoires présents?';}
	function ask_ispaksealed() {return 'Est le paquet de pak scellé et la date de péremption?';}
	function ask_selectedtype($lot, $exp) {return 'Lot:'.$lot.' Exp:'.$exp;}
	function ask_hasindicator() {return 'Est l\'indicateur d\'état montrant la DEA dans un état prêt?';}
	function ask_rep_aedreadtstatus() {return 'Statut prêt';}
	function ask_servicerpromise() {return 'En entrant votre nom, vous certifiez que l\'entretien ci-dessus a été fait et que vous avez déclaré l\'état réel de l\'unité AED et les remplacements ou d\'installer de nouvelles plaquettes ou des piles ou d\'autres accessoires comme indiqué ci-dessus.';}
	function ask_aedready() {return 'DEA Prêt';}
	function ask_servicingby() {return 'Desservi par';}
	function ask_digitalsignature() {return 'Signature numérique de Technicien';}
	function ask_dateverified() {return 'Date de Vérifié';}
	function ask_servicerepverified() {return 'Les personnes suivantes à l\'emplacement du client vérifié que l\'entretien a été effectué et fournitures remplacé si nécessaire conformément à la argeement au dossier. Toute information doit être rempli ci-dessous, afin d\'inclure email.';}
	function ask_clientrepname() {return 'Nom du représentant du client';}
	function ask_clientreptitle() {return 'Titre du représentant du client';}
	function ask_clientrepemail() {return 'Courriel de représentant du client';}
	function ask_clientrepphone() {return 'Téléphone du représentant du client';}
	function ask_dateverifiedclientlocation() {return 'Vérification de la date d\'entretien au client Localisation';}
	function ask_confirm_comments() {return 'Ajout de commentaires supplémentaires est tout ce que vous êtes autorisé à le faire. Vous ne pouvez pas modifier les commentaires d\'une autre personne. Tous les commentaires sont permament de sorte à double entretien de votre travail avant que vous postez.';}
	function ask_comments() {return 'Commentaires';}
	function ask_question() {return 'Question';}
	function ask_responses($resp) {
		switch($resp)
		{
			case 'Yes':
				return 'oui';
				break;
			case 'yes':
				return 'oui';
				break;
			case 'No':
				return 'Non';
				break;
			case 'no':
				return 'non';
				break;
			case 'Other':
				return 'Autre';
				break;
			case 'other':
				return 'Autre';
				break;
		}
		return $resp;
	}
	function ask_noservicing() {return 'Il n\'y a pas d\'entretien pour cette DEA encore.';}
	function ask_nosecondservicing() {return 'Il n\'y a aucune deuxième service pour cette DEA encore.';}
	function ask_ispadattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ask_issparepadattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ask_ispakattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ask_issparepakattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ask_isbatteryattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ask_issparebatteryattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ask_isaccessoryattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ask_isspareaccessoryattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}

	//equipment
	function equ_heading() {return 'Équipement';}
	function equ_checkheading() {return 'EquipChk';}
	function equ_pastcheck() {return 'Vérification passée';}
	function equ_lastcheck() {return 'Dernière vérificationk';}
	function equ_newequipment() {return 'Equipement-Nouveau';}
	function equ_invalid_newequipment() {return 'Votre équipement n’a pas pu être ajouté.  S\'il vous plaît contacter le support au '.SUPPORTEMAIL;}
	function equ_success_newequipment() {return 'Votre équipement a été ajouté.';}
	function equ_newequipmentcheck() {return 'New Equipment Check';}
	function equ_success_newequipmentcheck() {return 'Votre vérification a été ajoutée.';}
	function equ_deletequipment() {return 'Delete Equipment';}
	function equ_confirm_deletequipment() {return 'Êtes-vous sûr de vouloir effacer cet équipement ?';}
	function equ_success_deletequipment() {return 'Votre équipement a été effacé.';}
	function equ_editequipmentcheck() {return 'Edit Equipment Check';}
	function equ_success_editequipmentcheck() {return 'Votre vérification d’équipement a été modifiée.';}
	function equ_editequipment() {return 'Modifier équipement';}
	function equ_success_editequipment() {return 'Votre équipement a été modifié.';}
	function but_newequipment() {return 'Equipement-Nouveau';}
	function but_newequipmentcheck() {return 'Vérification -Nouveau';}
	function equ_cat() {return 'Catégorie';}
	function equ_subcat() {return 'Sous-catégorie';}
	function equ_generalinfo() {return 'Renseignements généraux';}
	function equ_location() {return 'Emplacement';}
	function equ_date() {return 'Date';}
	function equ_checkon() {return 'Check On';}
	function equ_questions() {return 'Questions';}
	function equ_no() {return 'Non';}
	function equ_yes() {return 'Oui';}
	function equ_compliance() {return 'Conformité';}
	function equ_complianceq1() {return 'Cet appareil doit-il être entretenu avant la prochaine inspection prévue?';}
	function equ_complianceq2() {return 'Cet appareil est-il conforme aux exigences établies par l’organisation?';}
	function equ_checkedby() {return 'Vérifié par';}
	function equ_checkedbypromise() {return 'En entrant votre nom, vous certifiez que le chèque ci-dessus a été fait et que vous a rapporté l\'état réel de cet équipement.';}
	function equ_checkedname() {return 'Nom Complet';}
	function equ_checkedemail() {return 'Courriel';}
	function equ_comments() {return 'Commentaires';}
	function equ_email_alertsubject() {return 'ALERTE! Vérification de l\'équipement Terminé - attention requise';}
	function equ_email_subject() {return 'Vérification de l\'équipement Terminé';}
	function equ_email_heading() {return 'Vérification de l\'équipement comfirmation';}
	function equ_email_bodyheading($name, $type, $style, $location, $organization) {
		return $name.',<br />
		Les suivants'.$type.':'.$style.' à '.$location.', '.$organization.' ont été inspectés.<br />
		<br />';
	}
	function equ_nocheck() {return 'Aucune vérification n’a été faite pour cet équipement ';}
	function equ_noequipment() {return 'Il n’y a actuellement aucun équipement à cet emplacement.';}
   
	//firstvoice
	function fir_heading() {return 'First Voice';}
	function fir_checkheading() {return 'First Voice Vérifier';}
	function fir_pastcheck() {return 'Vérifiez passées';}
	function fir_lastcheck() {return 'dernière Vérifier';}
	function fir_newequipment() {return 'Nouvel équipement First Voice';}
	function fir_invalid_newequipment() {return 'Votre équipement First Voice ne peut être ajouté. S\'il vous plaît contacter le support au '.SUPPORTEMAIL;}
	function fir_success_newequipment() {return 'Votre équipement First Voice a été ajouté avec succès.';}
	function fir_newfirstvoicecheck() {return 'Nouvel équipement First Voice Vérifier';}
	function fir_success_newfirstvoicecheck() {return 'Votre vérification a été ajoutée.';}
	function fir_deletequipment() {return 'Supprimer Equipement First Voice';}
	function fir_confirm_deletequipment() {return 'Êtes-vous sûr de vouloir effacer cet équipement?';}
	function fir_success_deletequipment() {return 'Votre équipement First Voice a été supprimé avec succès.';}
	function fir_editfirstvoicecheck() {return 'Modifier Équipement First Voice Vérifier';}
	function fir_success_editfirstvoicecheck() {return 'Votre chèque de matériel de First Voice a édité avec succès.';}
	function fir_editequipment() {return 'Modifier Équipement First Voice';}
	function fir_success_editequipment() {return 'Votre équipement First Voice a été mis à jour avec succès.';}
	function but_newfirstvoice() {return 'Nouvel équipement First Voice';}
	function but_newfirstvoicecheck() {return 'Nouveautés Cocher';}
	function fir_cat() {return 'Catégorie';}
	function fir_subcat() {return 'Sous-catégorie';}
	function fir_generalinfo() {return 'Renseignements généraux';}
	function fir_location() {return 'Emplacement';}
	function fir_date() {return 'Date';}
	function fir_checkon() {return 'Check On';}
	function fir_questions() {return 'Questions';}
	function fir_no() {return 'Non';}
	function fir_yes() {return 'Oui';}
	function fir_compliance() {return 'Compliance';}
	function fir_complianceq1() {return 'Est-ce que cet équipement First Voice il de l\'entretien avant la prochaine inspection planifiée?';}
	function fir_complianceq2() {return 'Est-ce l\'équipement First Voice en conformité avec les exigences de l\'organisation décrites?';}
	function fir_checkedby() {return 'Vérifié Par';}
	function fir_checkedbypromise() {return 'Coché Byby entrant votre nom, vous certifiez que le chèque ci-dessus a été fait et que vous avez déclaré l\'état réel de cet équipement First Voice.';}
	function fir_checkedname() {return 'Nom Complet';}
	function fir_checkedemail() {return 'Courriel';}
	function fir_comments() {return 'Commentaires';}
	function fir_email_alertsubject() {return 'ALERTE! Équipement First Voice Vérifier Terminé - attention requise';}
	function fir_email_subject() {return 'Équipement First Voice Vérifier Terminé';}
	function fir_email_heading() {return 'First Voice vérification de l\'équipement Confirmation ';}
	function fir_email_bodyheading($name, $type, $style, $location, $organization) {
		return $name.',<br />
		Les:'.$type.':'.style.'.'.$location.' à '.$organization.' suivants ont été inspectés.<br />
		<br />';
	}
	function fir_nocheck() {return 'Il n\'y a aucune vérification de cet équipement de First Voice encore.';}
	function fir_noequipment() {return 'Il n\'y a pas d\'équipement de First Voice pour ce lieu.';}
   
	//lock
	function loc_invalidkeycode() {return 'Input Code Clé';}
	function loc_tryagain() {return 'La Clé de code que vous avez entré est invalide, S\'il vous plaît essayer à nouveau.';}
	function loc_enterkeycode() {return 'S\'il vous plaît entrer un valide Code Cléci-dessous. Vérifiez vos dossiers de courriel pour un code d\'activation valide. Si vous êtes un utilisateur existant de votre compte a été bloqué en raison d\'un code d\'activation a expiré ou manquant. Contactez le support technique au '.PHONE.' '.CONTACTTIME.' or email '.SUPPORTEMAIL.' .';}
	
	//home
	function hom_firstlogin() {
	return '--S\'IL VOUS PLAÎT changer votre mot --<br />
			<br />
			S\'il vous plaît noter que pour augmenter la sécurité et la protection de votre compte, il est fortement recommandé
			que vous modifiez le nouveau mot de passe généré par le système vous a été assigné à votre propre mot de passe unique.<br />
			<br />
			<form action="'.URL.'index.php?content=profile&action=edit" method="POST" style="width:145px">
				<input type="submit" class="button" value="' . use_resetpassword() . '" />
			</form><br />
			<br />
			Questions? Écrivez-nous à <a href="mailto:'.SUPPORTEMAIL.'">'.SUPPORTEMAIL.'</a> or call:<br />
			'.PHONE.' M-F 8am to 5pm CST<br />';
	}
	function hom_badbrowser() {
	return '-- ATTENTION! S\'IL VOUS PLAÎT CHANGER VOTRE NAVIGATEUR --<br />
			<br />
			La version de votre navigateur Internet Explorer est ancienne. <br />  
			<br />  
			Veuillez soit obtenir une version plus récente de <a target="_blank" href="http://windows.microsoft.com/en-us/internet-explorer/products/ie/home">Internet Explorer 
			Version</a> ou changer votre navigateur internet à : <a target="_blank" href="https://www.google.com/intl/en/chrome/">Chrome</a>, <a target="_blank" href="http://www.mozilla.org/en-US/firefox/new/">Mozilla Firefox</a>, etc. pour vous assurer que le programme fonctionne parfaitement. Les versions plus anciennes d’Internet Explorer ne permettront pas au programme de fonctionner à son plein potentiel. <br />
			<br />
			Si vous ne pouvez pas télécharger une version plus récente ou un autre navigateur, contacter votre service IT ou de soutien technique pour qu’ils vous aident dans cette mise à jour. Veuillez spécifier qu’il vous faut une version 8 ou 
			supérieure d’Internet Explorer ou télécharger un autre navigateur.<br />
			<br />
			Pour toute question, envoyez-nous un courriel à '.SUPPORTEMAIL.' ou téléphonez <br />
			à:'.PHONE.' du lundi au vendredi de 8 h à 17 h, heure centrale. <br />
			<br />
			Merci de votre attention.<br />';
	}
	function hom_messagecenter() {return 'Centre messages';}
	function hom_act_training() {return 'Formation';}
	function hom_act_license() {return 'License';}
	function hom_act_immunization() {return 'Immunisation';}
	function hom_act_student() {return 'Etudiant';}
	function hom_act_class() {return 'Cours';}
	function hom_act_expiration() {return 'Expiration';}
	function hom_actionitems() {return 'Mesures à prendre';}

	//state laws
	function sta_heading() {return 'Lois de l\'État';}
	function sta_state() {return 'Province';}
	function sta_medicaldirection() {return 'Direction médicale';}
	function sta_training() {return 'Training Req.';}
	function sta_registration() {return 'Registration Req.';}
	function sta_moreinfo() {return 'Plus D\'Information';}
	function sta_aedowners() {return 'DEA Propri&#233;taires';}
	function sta_aedmandates() {return 'Mandats DEA';}

    function sta_legend_owners()
    {
        return 'DEA Propri&#233;taires: ';
    }

    function sta_legend_mandates()
    {
        return 'Mandats DEA: ';
    }

    function sta_legend_owners_details()
    {
        return 'R�sum� D�tails sur le droit pour l\'immunit� de responsabilit� civile pour les organisations qui poss�dent DEA';
    }

    function sta_legend_mandates_details()
    {
        return 'R�sum� D�tails de l\'endroit o� les DEA sont tenus par Etat / Lois locales par Industrie de l\'information est mis � jour p�riodiquement';
    }

    function sta_legend_footer()
    {
        return 'Si vous �tes au courant de toutes les lois qui ne sont pas en cours dans ce r�sum� de base de donn�es , s\'il vous pla�t contacter le support technique au 888-473-1777';
    }
	
	//nagger
	function nag_trainingexpired($traininginfo, $location, $org, $html = 0) {
		if($html) {
			//use html
			$traininghtml = '';
			foreach($traininginfo as $key => $val)
			{
				$traininghtml .= '
				<tr>
					<td>'.$traininginfo[$key]['student'].'</td>
					<td>'.$traininginfo[$key]['classname'].'</td>';
					
					if($traininginfo[$key]['expiration'] >= 0)
						$traininghtml .= '<td>'.$traininginfo[$key]['expiration'].' days left</td>';
					else
						$traininghtml .= '<td>'.($traininginfo[$key]['expiration']*-1).' days past</td>';
					
					$traininghtml .= '
					<td>'.$traininginfo[$key]['expirationdate'].'</td>
				</tr>';
			}
			
			return '<p>Nous avons détecté expirant cours de formation pour '.$location.', '.$org.'.</p>
			
			<table style="width:100%;">
				<tr>
					<td style="border-bottom:1px solid black;">Student</td>
					<td style="border-bottom:1px solid black;">Class</td>
					<td style="border-bottom:1px solid black;">Time</td>
					<td style="border-bottom:1px solid black;">Expiration Date</td>
				</tr>
				'.$traininghtml.'
			</table>
			
			<p>
				Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin pour planifier une formation bientôt. 
				Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a> 
				Pour toute question , s\'il vous plaît sentir libre à nous contacter à 
				'.SUPPORTEMAIL.'.
			</p>';
		} else {
			//use plain text
			$trainingbody = '';
			foreach($traininginfo as $key => $val)
			{
				$trainingbody .= $traininginfo[$key]['person'].'	|	'.$traininginfo[$key]['classname'].'	|	';
				
				if($traininginfo[$key]['expiration'] >= 0)
					$trainingbody .= $traininginfo[$key]['expiration'].' days left';
				else
					$trainingbody .= ($traininginfo[$key]['expiration']*-1).' days past';
				
				$trainingbody .= '	|	'.$traininginfo[$key]['expirationdate'].'
';
			}
			
			return '
Nous avons détecté expirant cours de formation pour '.$location.', '.$org.'.

étudiant|	classe	|	temps	|	date D\'Expiration
'.$trainingbody.'

Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin de planifier la formation bientôt.  
Pour plus d\'informations s\'il vous plaît visitez '.PROGRAM.' - '.URL.'
Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';
		}
		}
	
	function nag_trainingsubject() {return 'alerte formation';}
	function nag_trainingnotexpired($days, $student, $class) {return 'Nous avons détecté que l\'étudiant '.$student.' dispose de '.$days.' jours à gauche jusqu\'à ce que leur '.$class.' expire formation.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin de planifier la formation des '.$student.' bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a> Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_trainingexpiring($student, $class) {return 'Nous avons détecté que la formation de '.$class.' pour l\'étudiant '.$student.' expire aujourd\'hui.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin de planifier la formation des '.$student.' bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a> Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	//function nag_trainingexpired($days, $student, $class) {return 'Nous avons détecté que l\'étudiant '.$student.' est de '.$days.' jours expiration passé on '.$class.' formation.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin de planifier la formation des '.$student.' bientôt.  Pour plus d'informations s'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a> Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	
	function nag_pakheading() {return 'AED Pak/Accessory Alert';}
	function nag_paknotexpired($days, $pak, $lot, $location, $buylink, $org){
		$body = '';
		if($buylink == ''){
			$body.='Nous avons détecté que le DEA pak / accessoire '.$pak.' Lot:'.$lot.' à '.$location.', '.$org.' va expirer dans '.$days.' jours. Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau DEA pak/accessoire bientôt. Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.<br /><br />';
		} else {
			$body.= 'Nous avons détecté que le DEA pak / accessoire '.$pak.' Lot:'.$lot.' à '.$location.', '.$org.' va expirer dans '.$days.' jours. Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau DEA pak/accessoire bientôt. <br /><a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pak-fr.png" /></a><br />  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
		return $body;}
	function nag_pakexpiring($pak, $lot, $location, $buylink, $org){
		$body = '';
		if($buylink == ''){
			$body.='Nous avons détecté que le DEA pak / accessoire: '.$pak.' Lot:'.$lot.' à '.$location.', '.$org.' est expirant aujourd\'hui.   Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau DEA pak/accessoire bientôt.  <br /> Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.<br /><br />';
		} else {		
			$body.= 'Nous avons détecté que le DEA pak / accessoire: '.$pak.' Lot:'.$lot.' à '.$location.', '.$org.' est expirant aujourd\'hui. Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau AED pak/accessoire bientôt. <br /><a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pak-fr.png" /></a><br /> <br /> Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
		return $body;}
	function nag_pakexpired($days, $pak, $lot, $location, $buylink, $org){
		$body = '';
		if($buylink == ''){
			$body.='Nous avons détecté que le DEA pak / accessoire '.$pak.' Lot:'.$lot.' à '.$location.', '.$org.' est de '.$days.' jours expiration passé.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau AED pak/accessoire bientôt. Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.<br /><br />';
		}else {
			$body.= 'Nous avons détecté que le DEA pak / accessoire '.$pak.' Lot:'.$lot.' à '.$location.', '.$org.' est de '.$days.' jours expiration passé.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau AED pak/accessoire bientôt. <br /><a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pak-fr.png" /></a><br /> Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
		return $body;}
	
	function nag_padheading() {return 'AED Pad Alert';}
	function nag_padnotexpired($days, $pad, $lot, $location, $buylink, $org){
		$body = '';
		if($buylink == ''){
			$body.='Nous avons détecté que le pad DEA '.$pad.' Lot:'.$lot.' à '.$location.', '.$org.' est expirant en '.$days.' jours.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau pad bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';
		}else {
			$body.='Nous avons détecté que le pad DEA '.$pad.' Lot:'.$lot.' à '.$location.', '.$org.' est expirant en '.$days.' jours.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau pad bientôt. <br /><a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pad-fr.png" /></a><br /> Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
		return $body;}
		
	function nag_padexpiring($pad, $lot, $location, $buylink, $org){
		$body = '';
		if($buylink == ''){
			$body.='Nous avons détecté que le pad DEA '.$pad.' Lot:'.$lot.' à '.$location.', '.$org.' expire aujourd\'hui.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau pad bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';
		}else {
			$body.='Nous avons détecté que le pad DEA '.$pad.' Lot:'.$lot.' à '.$location.', '.$org.' expire aujourd\'hui.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau pad bientôt. <br /><a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pad-fr.png" /></a><br />  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	return $body;}
	function nag_padexpired($days, $pad, $lot, $location, $buylink, $org){
		$body = '';
		if($buylink == ''){
			$body.='Nous avons détecté que le pad DEA '.$pad.' Lot:'.$lot.' à '.$location.', '.$org.' est de '.$days.' jours expiration passé.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau pad bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';
		}else {
			$body.='Nous avons détecté que le pad DEA '.$pad.' Lot:'.$lot.' à '.$location.', '.$org.' est de '.$days.' jours expiration passé.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau pad bientôt. <br /><a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pad-fr.png" /></a><br /> Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	return $body;}
	function nag_batteryheading() {return 'AED Battery Alert';}
	function nag_batterynotexpired($days, $battery, $lot, $location, $buylink, $org){
		$body = '';
		if($buylink == ''){
			$body.='Nous avons detectex que la batterie DEA '.$battery.' Lot:'.$lot.' à '.$location.', '.$org.' va expirer dans '.$days.' jours. Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau batterie bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';
		}else {
			$body.='Nous avons detectex que la batterie DEA '.$battery.' Lot:'.$lot.' à '.$location.', '.$org.' va expirer dans '.$days.' jours. Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau batterie bientôt. <br /><a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-bat-fr.png" /></a><br /> Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	return $body;}
	function nag_batteryexpiring($battery, $lot, $location, $buylink, $org){
		$body = '';
		if($buylink == ''){
			$body.='Nous avons detectex que la batterie DEA '.$battery.' Lot:'.$lot.' à '.$location.', '.$org.' est expirant aujourd\'hui. Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau batterie bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';
		}else {
			$body.='Nous avons detectex que la batterie DEA '.$battery.' Lot:'.$lot.' à '.$location.', '.$org.' est expirant aujourd\'hui. Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau batterie bientôt. <br /><a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-bat-fr.png" /></a><br /> Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	return $body;}
	function nag_batteryexpired($days, $battery, $lot, $location, $buylink, $org){
		$body = '';
		if($buylink == ''){
			$body.='Nous avons detectex que la batterie DEA '.$battery.' Lot:'.$lot.' à '.$location.', '.$org.' est de '.$days.' jours expiration passé Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau batterie bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';
		}else {
			$body.='Nous avons detectex que la batterie DEA '.$battery.' Lot:'.$lot.' à '.$location.', '.$org.' est de '.$days.' jours expiration passé. Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau batterie bientôt. <br /><a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-bat-fr.png" /></a><br /> Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	return $body;}
	
	function nag_equipmentcontentheading(){return 'Equipement matières Alerte';}
	function nag_equipmentcontentnotexpired($days, $item, $cat, $subcat, $location, $org){return 'Nous avons détecté que le contenu '.$item.' de l\'équipement '.$cat.':'.$subcat.' à '.$location.', '.$org.' a laissé '.$days.' jours jusqu\'à ce qu\'il soit expiré.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau '.$item.' bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_equipmentcontentexpiring($item, $cat, $subcat, $location, $org){return 'Nous avons détecté que le contenu '.$item.' de l\'équipement '.$cat.':'.$subcat.' à '.$location.', '.$org.' expire aujourd\'hui.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau '.$item.' bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_equipmentcontentexpired($days, $item, $cat, $subcat, $location, $org){return 'Nous avons détecté que le contenu '.$item.' de l\'équipement '.$cat.':'.$subcat.' à '.$location.', '.$org.' est de '.$days.' jours expiration passé.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau '.$item.' bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
   
	function nag_firstvoicecontentheading(){return 'First Voice Equipement matières Alerte';}
	function nag_firstvoicecontentnotexpired($days, $item, $cat, $subcat, $location, $org){return 'Nous avons détecté que le contenu '.$item.' de l\'équipement First Voice '.$cat.':'.$subcat.' à '.$location.', '.$org.' a laissé '.$days.' jours jusqu\'à ce qu\'il soit expiré.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau '.$item.' bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_firstvoicecontentexpiring($item, $cat, $subcat, $location, $org){return 'Nous avons détecté que le contenu '.$item.' de l\'équipement First Voice '.$cat.':'.$subcat.' à '.$location.', '.$org.' expire aujourd\'hui.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau '.$item.' bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_firstvoicecontentexpired($days, $item, $cat, $subcat, $location, $org){return 'Nous avons détecté que le contenu '.$item.' de l\'équipement First Voice '.$cat.':'.$subcat.' à '.$location.', '.$org.' est de '.$days.' jours expiration passé.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouveau '.$item.' bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	
	function nag_equipmentcheckheading() {return 'Équipement Alerte de vérification';}
	function nag_equipmentcheckexpiring($cat, $subcat, $location, $org) {return 'Votre prochaine vérification de l\'équipement sur ​​l\'équipement '.$cat.':'.$subcat.' à '.$location.', '.$org.' exige une vérification de maintenance aujourd\'hui.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_equipmentcheckonedayafter($cat, $subcat, $location, $org) {return 'Votre prochaine vérification de l\'équipement sur ​​l\'équipement '.$cat.':'.$subcat.' à '.$location.', '.$org.' est un jour de retard.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_equipmentcheckexpired($days, $cat, $subcat, $location, $org) {return 'Votre prochaine vérification de l\'équipement sur ​​l\'équipement '.$cat.':'.$subcat.' à '.$location.', '.$org.' est de '.$days.' jour de retard.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	
	function nag_firstvoicecheckheading() {return 'First Voice Équipement Alerte de vérification';}
	function nag_firstvoicecheckexpiring($cat, $subcat, $location, $org) {return 'Votre prochaine vérification de l\'équipement sur ​​l\'équipement First Voice '.$cat.':'.$subcat.' à '.$location.', '.$org.' exige une vérification de maintenance aujourd\'hui.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_firstvoicecheckonedayafter($cat, $subcat, $location, $org) {return 'Votre prochaine vérification de l\'équipement sur ​​l\'équipement First Voice '.$cat.':'.$subcat.' à '.$location.', '.$org.' est un jour de retard.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_firstvoicecheckexpired($days, $cat, $subcat, $location, $org) {return 'Votre prochaine vérification de l\'équipement sur ​​l\'équipement First Voice '.$cat.':'.$subcat.' à '.$location.', '.$org.' est de '.$days.' jour de retard.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>.  Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	
	function nag_aedcheckheading() {return 'DEA Alerte de vérification';}
	function nag_aedexpiring($serial, $location, $org) {return 'Votre plus récente vérification DEA sur le DEA '.$serial.' à '.$location.', '.$org.' exige une vérification de maintenance aujourd\'hui.  Votre DEA peut ne pas être prêt pour l\'action , s\'il vous plaît vérifier votre DEA .  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_aedonedayafter($serial, $location, $org) {return 'Votre plus récente vérification DEA sur le DEA '.$serial.' à '.$location.', '.$org.' est un jour de retard sur son chèque de maintenance.  Votre DEA peut ne pas être prêt pour l\'action , s\'il vous plaît vérifier votre DEA .  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_aedexpired($days, $serial, $location, $org) {return 'Votre plus récente vérification DEA sur le DEA '.$serial.' à '.$location.', '.$org.' est de '.$days.' jours de retard sur son chèque de maintenance.  Votre DEA peut ne pas être prêt pour l\'action , s\'il vous plaît vérifier votre DEA .  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}

	function nag_aedservicing_heading() {return 'AED Servicing Alert';}

    function nag_aedservicing_expiringtoday($serial, $placement, $brand, $model, $location, $org)
    {
        return 'Your most recent AED servicing on the following AED requires a maintenance servicing today. <br /><br />Serial: ' . $serial . ' <br />Brand: ' . $brand . ' <br />Mod�le: ' . $model . ' <br />Placement: ' . $placement . ' <br />Location: ' . $location . ' <br />Organization: ' . $org . ' .<br /><br />  Your AED may not be ready for action, please servicing your AED.  For more information please visit <a href="' . URL . '">' . PROGRAM . '</a>. For any questions, please feel free to contact us at ' . SUPPORTEMAIL . '.';
    }

    function nag_aedservicing_expiring($lastdate, $checkdate, $checkfrequency, $serial, $placement, $brand, $model, $location, $org)
    {
        return 'Your last AED Servicing was ' . $lastdate . '<br />Your ' . $checkfrequency . ' Servicing is due on ' . $checkdate . ' <br /><br />Serial: ' . $serial . ' <br />Brand: ' . $brand . ' <br />Mod�le: ' . $model . ' <br />Placement: ' . $placement . ' <br />Location: ' . $location . ' <br />Organization: ' . $org . ' .<br /><br />Please service your AED.<br /><br />For more information please visit <a href="' . URL . '">' . PROGRAM . '</a>.<br />For any questions, please feel free to contact us at ' . SUPPORTEMAIL . '.';
    }

    function nag_aedservicing_expired($lastdate, $checkdate, $checkfrequency, $overdue, $serial, $placement, $brand, $model, $location, $org)
    {
        return 'Your last AED Servicing was ' . $lastdate . '<br />Your ' . $checkfrequency . ' AED Service check was due ' . $checkdate . '.<br />You are ' . $overdue . ' days overdue.<br /><br />Serial: ' . $serial . ' <br />Brand: ' . $brand . ' <br />Mod�le: ' . $model . ' <br />Placement: ' . $placement . ' <br />Location: ' . $location . ' <br />Organization: ' . $org . ' .<br /><br />Please service your AED.<br /><br />For more information please visit <a href="' . URL . '">' . PROGRAM . '</a>.<br />For any questions, please feel free to contact us at ' . SUPPORTEMAIL . '.';
    }
	
	function nag_keycodeheading() {return 'Inscription aux alertes';}
	function nag_keycodenotexpired($days, $aed, $location, $org){return 'Nous avons détecté que votre '.PROGRAM.' abonnement pour la DEA avec le numéro de série '.$aed.' à '.$location.', '.$org.' est expirant en '.$days.' jours.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouvel abonnement bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	function nag_keycodeexpiring($aed, $location, $org){return 'Nous avons détecté que DEA avec le numéro de série '.$aed.' à '.$location.', '.$org.' expire aujourd\'hui.  Ceci est une alerte automatique vous rappelle que vous pouvez avoir besoin d\'acheter un nouvel abonnement bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}

    //function nag_keycodeexpired($days, $aed, $location, $org){return 'Nous avons détecté que DEA avec le numéro de série '.$serial.' à '.$location.', '.$org.' est de '.$days.' jours expiration passé.  Ceci est une alerte automatisé pour vous rappeler que vous pouvez avoir besoin d\'acheter un nouvel abonnement bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL.'.';}
	
	function nag_medicaldirectionheading() {return 'Alerte Direction médicale';}
	function nag_medicaldirectionnotexpired($days, $location, $org){return 'Nous avons détecté que la direction médicale à '.$location.', '.$org.' est expirant en '.$days.' jours.  Ceci est une alerte automatisé pour vous rappeler que vous pouvez avoir besoin de mettre à jour bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter au '.SUPPORTEMAIL.'.';}
	function nag_medicaldirectionexpiring($location, $org){return 'Nous avons détecté que la direction médicale à  '.$location.', '.$org.' expire aujourd\'hui. Ceci est une alerte automatisé pour vous rappeler que vous pouvez avoir besoin de mettre à jour bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter au '.SUPPORTEMAIL.'.';}
	function nag_medicaldirectionexpired($days, $location, $org){return 'Nous avons détecté que la direction médicale à '.$location.' , '.$org.' est de '.$days.' jours expiration passé. Ceci est une alerte automatisé pour vous rappeler que vous pouvez avoir besoin de mettre à jour bientôt.  Pour plus d\'informations s\'il vous plaît visitez <a href="'.URL.'">'.PROGRAM.'</a>. Pour toute question , s\'il vous plaît sentir libre à nous contacter au '.SUPPORTEMAIL.'.';}

	//reports
	function rep_email(){return 
		'Rapport '.PROGRAM.'
		Vous avez demandé un rapport à '.URL.' - '.PROGRAMME.'. Votre rapport est joint en tant que feuille classeur multiple.
		S\'IL VOUS PLAÎT regardez au bas du classeur POUR VOIR FEUILLES à d\'autres sections du rapport.
		
		Si vous avez des questions au sujet de ce rapport , s\'il vous plaît sentir libre à nous contacter à '.SUPPORTEMAIL .'.
		
		S\'il vous plaît ne pas répondre à cet e-mail. Nous sommes incapables de répondre aux demandes de renseignements envoyés à cette adresse.
		Copyright &copy;  2015 Think Safe Inc.';
	}
	function rep_emailbody(){return
		'<html><head></head><body style="color:#000001;">
			<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
				<div style="padding:10px;width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
					<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
						<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
					</h1>
					<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
					<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
						Rapport
					</h2>
					<br />
					<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
						Vous avez demandé un rapport à <a href="'.URL.'">'.PROGRAM.'</a>.
						<br />
						<br />
						Si vous avez des questions au sujet de ce rapport , s\'il vous plaît sentir libre à nous contacter à <b>'.SUPPORTEMAIL.'</b>.<br><br>
					</div>
				</div>
			</div>
			<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
				S\'il vous plaît ne pas répondre à cet e-mail. Nous sommes incapables de répondre aux demandes de renseignements envoyés à cette adresse.
			</div>
			<br />
			<div style="width:970px;margin:0 auto;color:#000001;">
				Copyright &copy;  2015 Think Safe Inc.
			</div>
		</body></html>
	';}
	function rep_aedfilename(){return 'Rapport_DEA';}
	function rep_aedcheckfilename(){return 'Rapport_de_vérification_DEA';}
	function rep_aedservicingfilename(){return 'Rapport_de_lentretien_DEA';}
	function rep_contactsfilename(){return 'Contacts_Rapport';}
	function rep_documentsfilename(){return 'Documents_Rapport';}
	function rep_equipmentfilename(){return 'Rapport_sur_le_matériel';}
	function rep_erpfilename(){return 'ERP_Rapport';}
	function rep_facilityinfofilename(){return 'Facilité_Rapport';}
	function rep_firstvoicefilename(){return 'First_Voice_Rapport';}
	function rep_hazardsfilename(){return 'Rapport_sur_les_dangers';}
	function rep_immunizationsfilename(){return 'Rapport_Vaccinations';}
	function rep_keycodesfilename(){return 'Rapport_Keycodes';}
	function rep_licensesfilename(){return 'Licenses_Rapport';}
	function rep_medicaldirectionfilename(){return 'Rapport_de_la_direction_médicale';}

    function rep_inactivemedicaldirectionfilename()
    {
        return 'Direction_m�dicale_inactif';
    }
	function rep_masteraedfilename(){return 'Maître_rapport_DEA';}
	function rep_schedulerfilename(){return 'Planificateur_Rapport';}
	function rep_tracingfilename(){return 'Rapport_Tracing';}
	function rep_trainingfilename(){return 'Rapport_de_formation';}
	function rep_tracingbetween(){return 'entre';}
	function rep_tracingand(){return 'et';}
	function rep_emailsuccess(){return 'Votre rapport a été envoyé par courriel';}
	function rep_primarypad() {return 'Pad Primaire';}
	function rep_pediatricpad() {return 'Pad P�diatrique';}
	function rep_sparepad1() {return 'Pad de Rechange 1';}
	function rep_sparepad2() {return 'Pad de Rechange 2';}
	function rep_sparepediatricpad() {return 'Pad p�diatrique de rechange';}
	function rep_primarypak() {return 'Pak Primaire';}
	function rep_pediatricpak() {return 'Pak P�diatrique';}
	function rep_sparepak1() {return 'Pak de Rechange 1';}
	function rep_sparepak2() {return 'Pak de Rechange 1';}
	function rep_sparepediatricpak() {return 'Pak p�diatrique de rechange';}
	function rep_primarybattery() {return 'Batterie Primaire';}
	function rep_sparebattery() {return 'Batterie de Rechange';}
	function rep_primaryaccessory() {return 'Accessoire Primaire';}
	function rep_spareaccessory() {return 'Spare Accessoire';}
	function rep_expiration() {return 'expiration';}
	function rep_installation() {return 'installation';}
	function rep_kitincluded() {return 'Tout Inclus';}
	function rep_kitreplaced() {return 'Kit �l�ments remplac�s';}
	function rep_gloves() {return 'Gants';}
	function rep_razor() {return 'Rasoir';}
	function rep_scissors() {return 'Ciseaux';}
	function rep_towel() {return 'Serviette';}
	function rep_towlette() {return 'Lingette';}
	function rep_mask() {return 'Masquer';}
}
?>