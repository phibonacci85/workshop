﻿<?php
/*
	created on 10/22/12 JK
	cory chambers 7-18-13 added nextreviewdate to medical direction section                           
*/
require_once('default_french.php');
class French extends Default_French
{   
	function doc_usermanual() {return 'documents/R4R_User_Manual.pdf';}
	//visitor home
	function vis_home() {
		$home = '
		<h1 style="text-align:left;float:left;width:auto;">'.PROGRAM.':</h1>';
		
		if(file_exists($this->doc_blankaedcheck()))
		{
			$home .= '
			<div style="float:right;">
				<div class="button"><a style="color:white;" target="_blank" href="'.$this->doc_blankaedcheck().'">'.$this->hel_blankaedtemplate().'</a></div>
			</div>';
		}
		$home .= '
		<br />
		<ul class="boldlist" style="clear:left;">
			<li>Programme de gestion '.TITLE.' et de DEA</li>
			<li>Gestion des dates d’expiration</li>
			<li>Suivi certification formation</li>
			<li>Surveillance des emplacements/des dangers</li>
			<li>Directeur médical</li>   
		</ul>   
		<br />
	   
		<p>
			Le programme de gestion Rescue Ready vous permet de gérer votre programme de réponse aux urgences en ligne, d’avoir des registres d’entretien à jour pour vos produits 
			Rescue Ready et vos DEA et de mettre à jour les dates de renouvellement de certification de vos employés.
		</p>
		<br />
		<p>
			Pour toute aide concernant votre programme de réponse aux urgences ou pour des questions sur la législation actuelle, merci de contacter un spécialiste de Rescue Ready.
		</p>
		<br />
		<p style="font-weight:bold;">Soutien technique: 1-866-764-8488 Fax: 1-866-252-3915</p>';
		return $home;
	}
	
	//recalls
	function rec_searchhistory() {
		return '<p>Si vous souhaitez effectuer une recherche pour les rappels qui peuvent être liés à votre AED dans la base de Santé Canada pour « défibrillateur externe automatisé », se souvient ou votre marque spécifique / faire l\'histoire de rappel , s\'il vous plaît cliquer sur lien ci-dessous:</p>
		<p><a href="http://www.healthycanadians.gc.ca/recall-alert-rappel-avis/index-eng.php">http://www.healthycanadians.gc.ca/recall-alert-rappel-avis/index-eng.php</a></p>';
	}
	
	//aedcheck
	function ack_heading() {return 'Vérification DEA';}
	function ack_alertsubject() {return 'ALERT! Recent AED Check Completed - Attention Needed';}
	function ack_subject() {return 'AED Check Completed';}
	function ack_emailheading() {return 'AED Check Confirmation';}
	function ack_emailtopbody($name, $serial, $location, $organization) {return $name.',<br />
										The following AED '.$serial.' at '.$location.', '.$organization.' has been inspected.';}
	function ack_coordemailtopbody($name, $serial, $location, $organization) {return $name.',<br />
										The following AED '.$serial.' at '.$location.', '.$organization.' has been recently inspected.';}
	function ack_newheading() {return 'Vérification DEA-Nouveau';}
	function ack_success_new() {return 'Votre vérification DEA a été enregistrée.';}
	function ack_editheading() {return 'Edit AED Check';}
	function ack_success_edit() {return 'Votre DEA a été modifié.';}
	function ack_aed() {return 'DEA';}
	function ack_lastcheck() {return 'Dernière vérification';}
	function ack_pastcheck() {return 'Vérification passée';}
	function ack_annualcheck() {return 'Annual Check';}
	function ack_lastcheckers() {return 'Last Checker(s)';}
	function ack_generalinfo() {return 'Renseignements généraux';}
	function ack_date() {return 'Date';}
	function ack_location() {return 'Emplacement';}
	function ack_serial() {return 'Numéro de série';}
	function ack_isaedlocation() {return 'Le DEA se trouve-t-il à l’endroit désigné?';}
	function ack_rep_aedlocation() {return 'Designated Area';}
	function ack_isaedclean() {return 'Le DEA a-t-il été nettoyé et est-il intact?';}
	function ack_rep_aedclean() {return 'Cleaned and Undamaged';}
	function ack_isaedinalarm() {return 'Le DEA est-il dans un placard avec alarme?';}
	function ack_rep_aedinalarm() {return 'In Alarmed Cabinet';}
	function ack_isalarmworking() {return 'L’alarme dans le placard où se trouve le DEA fonctionne-t-elle?';}
	function ack_rep_aedalarmworks() {return 'Alarm Works';}
	function ack_replacingalarmbattery() {return 'Avez-vous remplacé les piles de cette alarme?';}
	function ack_rep_aedreplacingalarmbatteries() {return 'Replacing Alarm Batteries';}
	function ack_alarmbatteryinsertiondate() {return 'Date insertion';}
	function ack_alarmbatterychangedate() {return 'Date modifié';}
	function ack_alarmbatteryexpirationdate() {return 'Expiration Date';}
	function ack_kitpresent() {return 'Y a-t-il une trousse de sauvetage DEA? <a target="_blank" href="http://www.heartzap.ca/product/AED-Prep-Kit"><p class="button">Acheter Trousse de préparation pour DEA</p></a>';}
	function ack_rep_aedrescuekitpresent() {return 'Rescue Kit Present';}
	function ack_kitsealed() {return 'Cette trousse de sauvetage est-elle scellée et intacte?';}
	function ack_rep_aedrescuekitsealed() {return 'Rescue Kit Sealed';}
	function ack_kitinclude() {return 'Cette trousse de sauvetage inclut-elle ces articles?';}
	function ack_gloves() {return 'Gants';}
	function ack_razor() {return 'Rasoir preparation';}
	function ack_scissors() {return 'Ciseaux';}
	function ack_towel() {return 'Serviette';}
	function ack_towlette() {return 'Lingette humide';}
	function ack_mask() {return 'Masque RCR';}
	function ack_pads() {return 'Coussinets électrode';}
	function ack_paks() {return 'Nécessaires de rechange et accessoires';}
	function ack_batteries() {return 'Piles';}
	function ack_type() {return 'Type';}
	function ack_lot() {return 'Lot Number';}
	function ack_expiration() {return 'Expiration';}
	function ack_ispadattechedtoaed($exp, $lot) {return 'Ces électrodes sont-elles connectées à ľappareil? <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ack_isinstallingnewpads() {return 'Avez-vous installé de nouveaux coussinets?';}
	function ack_whichpadsinstalling() {return 'Quelles électrodes installez-vous?';}
	function ack_ispadready() {return 'Après l\'installation de nouvelles électrodes, avez-vous mis en marche l\'appareil afin qu\'il puisse effectuer une vérification automatique et donner une indication visuelle ou auditive de son bon état de marche?';}
	function ack_belowpadspresent() {return 'Les coussinets électrodes suivants sont-ils présents?';}
	function ack_ispadsealed() {return 'Les ensembles d\'électrodes sont-ils fermés et la date d\'expiration est-elle échue?';}
	function ack_isbatteryattechedtoaed($exp, $lot) {return 'Cette pile est-elle branchée à l\'appareil? <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ack_isinstallingnewbattery() {return 'Avez-vous installé une nouvelle pile?';}
	function ack_whichbatteryinstalling() {return 'Quelle pile installez-vous?';}
	function ack_isbatteryready() {return 'Après l\'installation de la nouvelle pile, avez-vous mis en marche l\'appareil afin qu\'il puisse effectuer une vérification automatique et donner une indication visuelle ou auditive de son bon état de marche?';}
	function ack_belowbatteriespresent() {return 'Les piles ci-dessous (si présentes) sont-elles installées?';}
	function ack_isbatterysealed() {return 'Le bloc-pile est-il fermé et la date d\'expiration est-elle échue?';}
	function ack_ispakattechedtoaed($exp, $lot) {return 'Ce nécessaire ou cet accessoire est-il branché à l\'appareil?  <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ack_isinstallingnewpaks() {return 'Installez-vous un nouveau nécessaire ou un nouvel accessoire?';}
	function ack_whichpaksinstalling() {return 'Quel nécessaire/accessoire installez-vous?';}
	function ack_ispakready() {return 'Après l\'installation du nouveau nécessaire ou du nouvel accessoire, avez-vous mis en marche l\'appareil afin qu\'il puisse effectuer une vérification automatique et donner une indication visuelle ou auditive de son bon état de marche?';}
	function ack_belowpakspresent() {return 'Les nécessaires ou accessoires ci-dessous (si applicable) sont-ils présents?';}
	function ack_ispaksealed() {return 'L\'ensemble de nécessaire est-il fermé et la date d\'expiration est-elle échue?';}
	function ack_selectedtype($lot, $exp) {return 'Lot:'.$lot.' Exp:'.$exp;}
	function ack_hasindicator() {return 'L’indicateur d’état (Status) est-il illuminé en vert et le DEA affiche-t-il le statut Prêt?';}
	function ack_rep_aedreadtstatus() {return 'Ready Status';}
	function ack_checkerpromise() {return '';}//removed for lack of french
	function ack_aedready() {return 'DEA Prêt';}
	function ack_checkedby() {return 'Vérifié par';}
	function ack_fullname() {return 'Nom - Prénom';}
	function ack_email() {return 'Courriel';}
	function ack_confirm_comments() {return 'Vous ne pouvez qu’ajouter vos propres commentaires – Vous n’êtes pas autorisé à modifier les commentaires d’une autre personne. Tous les commentaires sont permanents; il est donc important de vérifier votre travail avant d’entrer vos commentaires.';}
	function ack_comments() {return 'Commentaires';}
	function ack_question() {return 'Question';}
	function ack_responses($resp) {
		switch($resp)
		{
			case 'Yes':
				return 'Oui';
				break;
			case 'yes':
				return 'oui';
				break;
			case 'No':
				return 'Non';
				break;
			case 'no':
				return 'non';
				break;
			case 'Other':
				return 'Other';
				break;
			case 'other':
				return 'other';
				break;
		}
		return $resp;
	}
	function ack_nocheck() {return 'Il n’y a pas encore eu de vérification pour ce DEA.';}
	function ack_nosecondcheck() {return 'Il n’y a pas encore de deuxième vérification pour ce DEA.';}
	
	function nag_keycodeheading() {return 'Subscription Alert';}

	function nag_keycodenotexpired($days, $aed, $location, $org)
	{
		return 'We have detected your ' . PROGRAM . ' subscription for the AED with serial number ' . $aed . ' at ' . $location . ', ' . $org . ' is expiring in ' . $days . ' days. <br /><br /><a target="_blank" href="http://www.heartzap.ca/aed-catalog/aeds/aed-due-diligence-program"><img src="' . URL . '/images/renew-eng.png" /></a><br /><br /> This is an automated alert reminding you that you may need to purchase a new subscription soon.  For more information please visit <a href="' . URL . '">' . PROGRAM . '</a>. For any questions, please feel free to contact us at SUPPORTEMAIL.';
	}

	function nag_keycodeexpiring($aed, $location, $org)
	{
		return 'We have detected that the AED with serial number ' . $aed . ' at ' . $location . ', ' . $org . ' expires today.  This is an automated alert reminding you that you may need to purchase a new subscription soon. <br /><br /><a target="_blank" href="http://www.heartzap.ca/aed-catalog/aeds/aed-due-diligence-program"><img src="' . URL . '/images/renew-eng.png" /></a><br /><br /> For more information please visit <a href="' . URL . '">' . PROGRAM . '</a>. For any questions, please feel free to contact us at SUPPORTEMAIL.';
	}

	function nag_keycodeexpired($days, $aed, $location, $org)
	{
		return 'We have detected that the AED with serial number ' . $aed . ' at ' . $location . ', ' . $org . ' is ' . $days . ' days past expiration. <br /><br /><a target="_blank" href="http://www.heartzap.ca/aed-catalog/aeds/aed-due-diligence-program"><img src="' . URL . '/images/renew-eng.png" /></a><br /><br /> This is an automated alert reminding you that you may need to purchase a new subscription soon.  For more information please visit <a href="' . URL . '">' . PROGRAM . '</a>. For any questions, please feel free to contact us at SUPPORTEMAIL.';
	}
}
?>