<?php
/*
	created on 10/22/12 JK
	cory chambers 7-18-13 added nextreviewdate to medical direction section
*/
class Default_English
{	
	//languages
	function lang_french() {return 'French';}
	function lang_english() {return 'English';}
	
	function locallang_french() {return 'Français';}
	function locallang_english() {return 'English';}
	
	function language() {return 'Language';}

	//documents (no tab)
	function doc_usermanual() {return 'documents/Rescue_One_Manager_Manual.pdf';}
	function doc_adminusermanual() {return 'documents/Rescue_One_Manual_Administration_Access_Details.pdf';}
	function doc_blankaedcheck() {return 'documents/blank_aed_check_template.pdf';}
	function doc_trainingtemplate() {return 'documents/upload_training_template.xls';}
	function doc_eventform() {return 'documents/AED-Event-Report-Template-to-Fill-In.pdf';}
	function doc_recallhistory() {return 'documents/2014-01-01_industry-recall-history.doc';}
	function doc_blankservicecheck() {return 'documents/AED-Servicing-Form.pdf';}
	function doc_azstateform() {return 'documents/aed_use_form.doc';}
	function doc_ctstateform() {return 'documents/CT-HSinserts_jun8 - heartsafe_application.pdf';}
	function doc_dcstateform() {return 'documents/DC-padprogramapplication 2013.pdf';}
	function doc_destateform() {return 'documents/DE-Aedform1.pdf';}
	function doc_gastateform() {return 'documents/GA-Emergency_guidelines_for_Schools.pdf';}
	function doc_ilstateform() {return 'documents/IL-Registration-of-AEDs-with-ResourceHospitals.pdf';}
	function doc_mdstateform() {return 'documents/MD-Facility_AED_Report_Form_Cardiac.pdf';}
	function doc_nhstateform() {return 'documents/NH-aedregistryform.pdf';}
	function doc_nmstateform() {return 'documents/NEW-MEXICO-CARDIAC-ARREST-RESPONSE-AED-PROGRAM.pdf';}
	function doc_nystateform() {return 'documents/doh-4135.pdf';}
		
	//general headings
	function nav_home() {return 'Home';}
	function nav_profile() {return 'Profile';}
	function nav_message() {return 'Message Center';}
	function nav_aed() {return 'AED';}
	function nav_map() {return 'Map';}
	function nav_masteraed() {return 'Master AED';}
	function nav_equipment() {return 'Equipment';}
	function nav_training() {return 'Training';}
	function nav_licenses() {return 'Licenses';}
	function nav_immunizations() {return 'Immunizations';}
	function nav_hazards() {return 'Hazards';}
	function nav_contact() {return 'Contacts';}
	function nav_firstvoice() {return 'First Voice';}
	function nav_persons() {return 'Persons';}
	function nav_documents() {return 'Documents';}
	function nav_facility() {return 'Facility Info';}
	function nav_reports() {return 'Reports';}
	function nav_help() {return 'Help';}
	function nav_store() {return 'Store';}
	function nav_admin() {return 'Admin';}
	function nav_users() {return 'Users';}
	function nav_tracing() {return 'Tracing';}
	function nav_setup() {return 'Setup';}
	function nav_organizations() {return 'Organizations';}
	function nav_keycodes() {return 'Keycodes';}
	function nav_alerts() {return 'Alerts';}
	function nav_scheduler() {return 'Scheduler';}
	function nav_aedcheck() {return 'AED Check';}
	function nav_aedservicing() {return 'AED Servicing';}
	function nav_erp() {return 'ERP';}
	function nav_medicaldirection() {return 'Medical Direction / Program Management';}
	function nav_inactivemedicaldirection() {return 'Inactive Medical Direction';}
	function nav_events() {return 'Events';}
	function nav_recalls() {return 'Recalls & Servicing';}
	function nav_statelaws() {return 'State Laws';}
	function nav_login() {return 'Login';}
	function nav_logout() {return 'Logout';}
	function nav_organization() {return 'Organization';}
	function nav_location() {return 'Location';}
	function nav_welcome() {return 'Welcome';}
	function sel_yes() {return 'Yes';}
	function sel_no() {return 'No';}
	function sel_all() {return 'All';}
	function sel_one() {return 'Select One';}
	function sel_aed() {return 'AED';}
	function sel_pad() {return 'Pad';}
	function sel_pak() {return 'Paks & Accessories';}
	function sel_na() {return 'N/A';}
	function sel_battery() {return 'Battery';}
	function sel_accessory() {return 'Accessory';}
	function sel_none() {return 'None';}
	function hom_version() {return 'Version';}
	function hom_maintenence() {
		return '--ATTENTION!  Site Maintenance --<br />
			<br />
			This site will be down for Maintenence.<br />
			<br />
			Saturday October 3rd from 9am CST<br />
			<br />
			Until<br />
			<br />
			Saturday October 3rd from 4pm CST<br />
			<br />
			Questions? Email us at <a href="mailto:'.SUPPORTEMAIL.'">'.SUPPORTEMAIL.'</a> or call:<br />
			'.PHONE.' M-F 8am to 5pm CST<br />
			<br />
			Thank you for your attention to this matter.<br />';
	}
	function hom_nojs() {return 'You do not have JavaScript enabled.  Please enable Javascript to view the site properly.';}
	function nolocation() {return 'There are currently no locations for this organization.';}
	
	//buttons
	function but_new() {return 'New';}
	function but_edit() {return 'Edit';}
	function but_delete() {return 'Delete';}
	function but_submit() {return 'Submit';}
	function but_back() {return 'Back';}
	function but_previous() {return 'Previous';}
	function but_next() {return 'Next';}
	function but_change() {return 'Change';}
	function but_newmessage() {return 'New Message';}
	function but_send() {return 'Send';}
	function but_fill() {return 'Fill';}
	function but_newevent() {return 'New Event';}
	function but_newaedcheck() {return 'New AED Check';}
	function but_clearthis() {return 'Clear This';}
	function but_clearall() {return 'Clear All';}
	
	//hazards
	function haz_name() {return 'Type';}
	function haz_type() {return 'Category';}
	function haz_location() {return 'Location';}
	function haz_notes() {return 'Notes';}
	function haz_heading() {return 'Hazards';}
	function haz_newhazard() {return 'New Hazard';}
	function haz_edithazard() {return 'Edit Hazard';}
	function haz_deletehazard() {return 'Delete Hazard';}
	function haz_success_new() {return 'Your hazard has been successfully added.';}
	function haz_confirm_delete() {return 'Are you sure you want to delete this hazard?';}
	function haz_success_delete() {return 'Your hazard has been deleted.';}
	function haz_success_edit() {return 'Your hazard has been successfully edited.';}
	function haz_none() {return 'There are currently no hazards for this location.';}
	
	//view
	function vie_set() {return 'Set View';}
	function vie_remove() {return 'Remove View';}

	//alerts
	function ale_invalidemail() {return 'You must enter a valid email address.';}
	function ale_notset() {return 'Not set';}
	function ale_heading() {return 'Alerts';}
	function ale_newalert() {return 'New Alert';}
	function ale_editalert() {return 'Edit Alert';}
	function ale_deletealert() {return 'Delete Alert';}
	function ale_delete() {return 'Delete';}
	function ale_level() {return 'Level';}
	function ale_email() {return 'Email';}
	function ale_type() {return 'Type';}
	function ale_showtype($type) {
		switch($type){
			case 'all':
				return 'All';
			case 'aedservicing':
				return 'AED Servicing';
			case 'aedcheck':
				return 'AED Checks';
			case 'aed':
				return 'AED Pads and Batteries';
			case 'equipmentcheck':
				return 'Equipment Checks';
			case 'equipment':
				return 'Equipment Content';
			case 'firstvoicecheck':
				return 'First Voice Checks';
			case 'firstvoice':
				return 'First Voice Content';
			case 'keycode':
				return 'Keycode';
			case 'medicaldirection':
				return 'Medical Direction';
			case 'training':
				return 'Training';
			default:
				return $type;
		}
	}
	function ale_success_new() {return 'Your alert has been successfully added.';}
	function ale_confirm_delete() {return 'Select which alerts to delete.';}
	function ale_success_delete($email, $level, $type) {return $email.' has been deleted from level '.$level.' for '.$this->ale_showtype($type).'.';}
	function ale_success_edit() {return 'Your alerts has been successfully edited.';}
	function ale_noalertsfound() {return 'There are no alerts for this location.';}
	
	//keycodes
	function sel_pool() {return 'Pool';}
	function key_type() {return 'Type';}
	function key_picktype($type) {
		switch($type) {
			case 'standard':
				return 'Standard Issue';
			default:
				return $type;
		}		
	}
	function key_demo() {return 'Demo';}
	function key_length() {return 'Length(in days)';}
	function key_amount() {return 'Amount';}
	function key_organization() {return 'Organization';}
	function key_typeofkeycode() {return 'Type of Keycode';}
	function sel_typeofkeycode($count, $expiration) {return $count.' keycodes that have a length of '.$expiration.' days.';}
	function key_keycode() {return 'Keycode';}
	function key_aedserialnum() {return 'AED Serial Number';}
	function key_expiration() {return 'Expiration';}
	function key_insertedby() {return 'Inserted by';}
	function key_usedkeycodes() {return 'Used Keycodes';}
	function key_unusedkeycodes() {return 'Unused Keycodes';}
	function key_heading() {return 'Keycodes';}
	function key_newkeycode() {return 'New Keycode';}
	function key_generatekeycode() {return 'Generate Keycodes';}
	function key_assignkeycode() {return 'Assign Keycodes';}
	function key_assignpool() {return 'Assign Pool';}
	function key_deletepool() {return 'Delete from Pool';}
	function key_deletekeycode() {return 'Delete Keycodes';}
	function key_nounusedkeycodes() {return 'There are no unused keycodes for this organization.';}
	function key_nousedkeycodes() {return 'There are no used keycodes for this organization.';}
	function key_invalid_assignamount() {return 'You do not have that many keycodes to assign.';}
	function key_invalid_deleteamount() {return 'You do not have that many keycodes to delete.';}
	function key_success_new($keycode) {return 'Keycode '.$keycode.' was successfully entered into the program.';}
	function key_success_generate($num) {return 'Your '.$num.' keycodes have been successfully generated.';}
	function key_success_assign($num, $organization) {return 'You have successfully assigned '.$num.' keycodes to '.$organization.'.';}
	function key_success_deletepool($num, $type) {return 'You have successfully deleted '.$num.' keycodes with a length of '.$type.' days.';}
	function key_none() {return 'There are no keycodes for this organization.';}
	
	//organizations
	function org_heading() {return 'Organizations';}
	function org_new() {return 'New Organization';}
	function org_delete() {return 'Delete Organization';}
	function org_edit() {return 'Edit Organization';}
	function org_orgname() {return 'Organization Name';}
	function org_orgtype() {return 'Org Type';}
	function org_internal() {return 'Internal';}
	function org_external() {return 'External';}
	function org_success_new() {return 'Your organization has been successfully added.';}
	function org_confirm_delete() {return 'Are you sure you want to delete this organization?';}
	function org_success_delete() {return 'Your organization has been successfully removed.';}
	function org_success_edit() {return 'Your organization has been successfully changed.';}
	function org_clientnumber() {return 'Client Number';}
	function org_streetaddress() {return 'Street Address';}
	function org_mailingaddress() {return 'Mailing Address';}
	function org_city() {return 'City';}
	function org_state() {return 'State';}
	function org_zip() {return 'Zip';}
	function org_phone() {return 'Phone';}
	function org_status() {return 'Status';}
	function org_pickstatus($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'active':
				return 'Active';
			case 'closed':
				return 'Company Closed';
			case 'subdis':
				return 'Sub-distributor';
			case 'noaeds':
				return 'No longer have AEDs';
			default:
				return 'ERROR UNKNOWN STATUS:'.$type;
		}
	}
	function org_mainbillcontact() {return 'Main Billing Contact';}
	function org_mainbillcontacttitle() {return 'Title';}
	function org_mainbillcontactphone() {return 'Phone';}
	function org_mainbillcontactemail() {return 'Email';}
	function org_mainbillcontactfax() {return 'Fax';}
	function org_mainmailcontact() {return 'Main Mailing Contact';}
	function org_mainmailcontacttitle() {return 'Title';}
	function org_mainmailcontactphone() {return 'Phone';}
	function org_mainmailcontactemail() {return 'Email';}
	function org_mainmailcontactfax() {return 'Fax';}
	function org_notes() {return 'Notes';}
	function org_none() {return 'There are no organizations on this filter.';}
	
	//locations
	function loc_new() {return 'New Location';}
	function loc_delete() {return 'Delete Location';}
	function loc_edit() {return 'Edit Location';}
	function loc_success_new() {return 'Your location has been successfully added.';}
	function loc_confirm_delete() {return 'Are you sure you want to delete this location?';}
	function loc_success_delete() {return 'Your location has been successfully removed.';}
	function loc_success_edit() {return 'Your location has been successfully changed.';}
	function loc_name() {return 'Location Name';}
	function loc_address() {return 'Location Address';}
	function loc_city() {return 'City';}
	function loc_state() {return 'State';}
	function loc_country() {return 'Country';}
	function loc_phone() {return 'Phone';}
	function loc_status() {return 'Status';}
	function loc_pickstatus($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'active':
				return 'Active';
			case 'closed':
				return 'Location Closed';
			case 'lost':
				return 'Lost';
			case 'moved':
				return 'Moved';
			case 'noaeds':
				return 'No longer have AEDs';
			case 'storage':
				return 'Storage';
			case 'subdis':
				return 'Sub-Distributor';
			default:
				return 'ERROR UNKNOWN STATUS:'.$type;
		}
	}
	function loc_region() {return 'Region';}
	function loc_zip() {return 'Zip';}
	function loc_fax() {return 'Fax';}
	function loc_website() {return 'Website';}
	function loc_mainlocation() {return 'Main Location';}
	function loc_shippingcontact() {return 'Shipping Contact';}
	function loc_shippingcontacttitle() {return 'Shipping Contact Title';}
	function loc_shippingcontactphone() {return 'Shipping Contact Phone';}
	function loc_shippingcontactemail() {return 'Shipping Contact Email';}
	function loc_shippingcontactfax() {return 'Shipping Contact Fax';}
	function loc_billingcontact() {return 'Billing Contact';}
	function loc_billingcontacttitle() {return 'Billing Contact Title';}
	function loc_billingcontactphone() {return 'Billing Contact Phone';}
	function loc_billingcontactemail() {return 'Billing Contact Email';}
	function loc_billingcontactfax() {return 'Billing Contact Fax';}
	function loc_sitecontact() {return 'Site Contact';}
	function loc_safetydirector() {return 'Safety Director';}
	function loc_repname() {return 'Rep Name';}
	function loc_notes() {return 'Notes';}
	function loc_none() {return 'There are no locations for this organization.';}
	
	//tracing	
	function trc_heading() {return 'Tracing';}
	function trc_new_heading() {return 'Inbound Tracing';}
	function trc_new_success() {return 'You have successfully added new tracing.';}
	function trc_edit_heading() {return 'Edit Tracing';}
	function trc_edit_success() {return 'You have successfully edited this tracing.';}
	function trc_edit_invalid() {return 'You must fill out all required fields to edit this tracing.';}
	function trc_delete_heading() {return 'Delete Tracing';}
	function trc_delete_confirm() {return 'Are you sure you want to delete this tracing?';}
	function trc_delete_success() {return 'You have successfully deleted this tracing.';}
	function trc_delete_invalid() {return 'You could not delete this tracing.';}
	function trc_assign_heading() {return 'Assign';}
	function trc_assign_success() {return 'You have successfully added tracing as an AED.';}
	function trc_assign_invalid() {return 'You must fill out all required fields to add this tracing as an AED.';}
	function trc_assign_none() {return 'There are no unassigned tracings to add.';}
	function trc_noadd() {return 'Don\'t add this tracing';}
	function trc_more() {return 'More';}
	function trc_inboundaed() {return 'Inbound AED';}
	function trc_inboundaccessory() {return 'Inbound Accessory';}
	function trc_location() {return 'Location';}
	function trc_org() {return 'Organization';}
	function trc_assign() {return 'Assign';}
	function trc_brand() {return 'Brand';}
	function trc_model() {return 'Model';}
	function trc_serialnumber() {return 'Serial Number';}
	function trc_serialnumbers() {return 'Serial Numbers';}
	function trc_serialnumbers_directions() {return '(Please enter all serial numbers with either a space, return, or comma seperating)';}
	function trc_serial() {return 'Serial';}
	function trc_invoicedate() {return 'Invoice Date';}
	function trc_invoicenumber() {return 'Invoice Number';}
	function trc_datereceived() {return 'Date Received';}
	function trc_shipto() {return 'Ship To';}
	function trc_battery() {return 'Battery';}
	function trc_pad() {return 'Pad';}
	function trc_accessory() {return 'Accessory';}
	function trc_accessories() {return 'Accessories';}
	function trc_accessoryname() {return 'Name';}
	function trc_accessorytype() {return 'Type';}
	function trc_accessorylot() {return 'Lot Number / UDI';}
	function trc_accessoryexp() {return 'Expiration';}
	function trc_none() {return 'There are no tracings recorded yet.';}
	
	//setup
	function set_heading($type) {switch($type){
		case 'training': return 'Training Setup'; break; 
		case 'hazards': return 'Hazards Setup'; break; 
		case 'documents': return 'Documents Setup'; break; 
		case 'immunizations': return 'Immunizations Setup'; break; 
		case 'licenses': return 'Licenses Setup'; break; 
		case 'equipment': return 'Equipment Setup'; break; 
		case 'firstvoice': return 'First Voice Setup'; break; }}

	//setup hazards
	function set_hazardtypes() {return 'Hazard Types';}
	function set_addhazardtypes() {return 'Add Hazard Type';}
	function set_hazards() {return 'Hazards';}
	function set_hazard_invalid_newhazardtype($type, $language) {return 'The hazard type \''.$type.'\' for '.$language.' is already in the program.';}
	function set_hazard_invalid_newhazardtypelang($language) {return 'You must fill out the '.$language.' when making a new hazard type.';}
	function set_hazard_success_newhazardtype($type, $language) {return 'The hazard type \''.$type.'\' for '.$language.' has been added to the program.';}
	function set_hazard_success_deletehazardtype($type, $language) {return 'Your hazard type, '.$type.', for '.$language.'  has been deleted.';}
	function set_hazard_success_edithazardtype($type, $language) {return 'Your hazard type, '.$type.', for '.$language.'  has been successfully changed.';}
	
	//setup immunizations
	function set_immunizationtypes() {return 'Immunization Types';}
	function set_addimmunizationtypes() {return 'Add Immunization Type';}
	function set_immunizations() {return 'Immunizations';}
	function set_immunization_newheading() {return 'New Immunization';}
	function set_immunization_editheading() {return 'Edit Immunization';}
	function set_immunization_deleteheading() {return 'Delete Immunization';}
	function set_immunization_name() {return 'Name';}
	function set_immunization_nextimmunization() {return 'Next Immunization';}
	function set_immunization_alerts() {return 'Alerts';}
	function set_immunization_employee() {return 'Employee';}
	function set_immunization_supervisor() {return 'Supervisor';}
	function set_immunization_email() {return 'Email';}
	function set_immunization_alerton() {return 'Alert On';}
	function set_immunization_daysafter() {return 'Days After';}
	function set_immunization_pickstatus($status) {
		switch($status)
		{
			case 'completed':
				return 'Completed';break;
			case 'declined':
				return 'Declined';break;
			case 'negative':
				return 'Negative';break;
			case 'offer':
				return 'Offer';break;
			case 'other':
				return 'Other';break;
			default:
				return $status;break;
		}
	}
	function set_immunization_subject() {return 'Subject';}
	function set_immunization_body() {return 'Body';}
	function set_immunization_attone() {return 'Att 1 File';}
	function set_immunization_atttwo() {return 'Att 2 File';}
	function set_immunization_attthree() {return 'Att 3 File';}
	function set_immunization_invalid_newimmunizationtype($type, $language) {return 'The immunization type \''.$type.'\' for '.$language.' is already in the program.';}
	function set_immunization_invalid_newimmunizationtypelang($language) {return 'You must fill out the '.$language.' when making a new immunization type.';}
	function set_immunization_success_newimmunizationtype($type, $language) {return 'The immunization type \''.$type.'\' for '.$language.' has been added to the program.';}
	function set_immunization_success_deleteimmunizationtype($type, $language) {return 'Your immunization type, '.$type.', for '.$language.'  has been deleted.';}
	function set_immunization_success_editimmunizationtype($type, $language) {return 'Your immunization type, '.$type.', for '.$language.'  has been successfully changed.';}
	
	//setup documents
	function set_documenttypes() {return 'Document Types';}
	function set_adddocumenttypes() {return 'Add Document Type';}
	function set_documents() {return 'Documents';}
	function set_document_newheading() {return 'New Document';}
	function set_document_editheading() {return 'Edit Document';}
	function set_document_deleteheading() {return 'Delete Document';}
	function set_document_delete_confirm() {return 'Are you sure you want to delete this license?';}
	function set_document_name() {return 'Name';}
	function set_document_category() {return 'Category';}
	function set_document_compliance() {return 'Compliance';}
	function set_document_notes() {return 'Notes';}
	function set_document_alerts() {return 'Alerts';}
	function set_document_employee() {return 'Employee';}
	function set_document_supervisor() {return 'Supervisor';}
	function set_document_email() {return 'Email';}
	function set_document_subject() {return 'Subject';}
	function set_document_body() {return 'Body';}
	function set_document_attone() {return 'Att 1 File';}
	function set_document_atttwo() {return 'Att 2 File';}
	function set_document_attthree() {return 'Att 3 File';}
	function set_document_pickcategory($cat) {switch($cat){
		case 'Audit': return 'Audit'; break;
		case 'Audit/Inspection': return 'Audit/Inspection'; break;
		case 'Document': return 'Document'; break;
		case 'Document Review': return 'Document Review'; break;
		case 'Govt Report': return 'Govt Report'; break;
		case 'Permits': return 'Permits'; break;
		case 'Procedures': return 'Procedures'; break;
		case 'Report': return 'Report'; break;
		case 'Safety Log': return 'Safety Log'; break;
		case 'Site Visit': return 'Site Visit'; break;
		case 'Training Event': return 'Training Event'; break;
		case 'Other': return 'Other'; break; }}
	function set_document_alerton() {return 'Alert On';}
	function set_document_pickstatus($status) {
		switch($status)
		{
			case 'uncompleted':
				return 'Uncompleted';
			case 'completed':
				return 'Completed';
			default:
				return $status;
		}
	}
	function set_document_daysafter() {return 'Days After';}
	function set_document_invalid_newdocumenttype($type, $language) {return 'The document type \''.$type.'\' for '.$language.' is already in the program.';}
	function set_document_invalid_newdocumenttypelang($language) {return 'You must fill out the '.$language.' when making a new document type.';}
	function set_document_success_newdocumenttype($type, $language) {return 'The document type \''.$type.'\' for '.$language.' has been added to the program.';}
	function set_document_success_deletedocumenttype($type, $language) {return 'Your document type, '.$type.', for '.$language.'  has been deleted.';}
	function set_document_success_editdocumenttype($type, $language) {return 'Your document type, '.$type.', for '.$language.'  has been successfully changed.';}
	function set_document_noalerts() {return 'There are no alerts for this document type.';}
	
	//setup licenses
	function set_licensestypes() {return 'License Types';}
	function set_addlicensestypes() {return 'Add License Type';}
	function set_licensess() {return 'Licenses';}
	function set_licenses_newheading() {return 'New License';}
	function set_licenses_editheading() {return 'Edit License';}
	function set_licenses_deleteheading() {return 'Delete License';}
	function set_licenses_delete_confirm() {return 'Are you sure you want to delete this license?';}
	function set_licenses_name() {return 'Name';}
	function set_licenses_ceu_ceusneeded() {return 'CEUs (Credits) Needed';}
	function set_licenses_ceu_ceu() {return 'CEU';}
	function set_licenses_ceu_code() {return 'Code';}
	function set_licenses_ceu_safety() {return 'Safety';}
	function set_licenses_ceu_discipline() {return 'Discipline';}
	function set_licenses_notes() {return 'Notes';}
	function set_licenses_alerts() {return 'Alerts';}
	function set_licenses_employee() {return 'Employee';}
	function set_licenses_supervisor() {return 'Supervisor';}
	function set_licenses_email() {return 'Email';}
	function set_licenses_alerton() {return 'Alert On';}
	function set_licenses_pickstatus($status) {
		switch($status)
		{
			case 'inactive':
				return 'Inactive';
			case 'active':
				return 'Active';
			default:
				return $status;
		}
	}
	function set_licenses_daysafter() {return 'Days After';}
	function set_licenses_subject() {return 'Subject';}
	function set_licenses_body() {return 'Body';}
	function set_licenses_attone() {return 'Att 1 File';}
	function set_licenses_atttwo() {return 'Att 2 File';}
	function set_licenses_attthree() {return 'Att 3 File';}
	function set_licenses_invalid_newlicensestype($type, $language) {return 'The licenses type \''.$type.'\' for '.$language.' is already in the program.';}
	function set_licenses_invalid_newlicensestypelang($language) {return 'You must fill out the '.$language.' when making a new licenses type.';}
	function set_licenses_success_newlicensestype($type, $language) {return 'The licenses type \''.$type.'\' for '.$language.' has been added to the program.';}
	function set_licenses_success_deletelicensestype($type, $language) {return 'Your licenses type, '.$type.', for '.$language.'  has been deleted.';}
	function set_licenses_success_editlicensestype($type, $language) {return 'Your licenses type, '.$type.', for '.$language.'  has been successfully changed.';}
	function set_licenses_noalerts() {return 'There are no alerts for this licenses type.';}
	
	//setup training
	function set_trainingcourses() {return 'Training Courses';}
	function set_training_newheading() {return 'New Training Course';}
	function set_training_editheading() {return 'Edit Training Course';}
	function set_training_deleteheading() {return 'Delete Training Course';}
	function set_training_delete_confirm() {return 'Are you sure you want to delete this training course?';}
	function set_addtrainingcourses() {return 'Add Training Course';}
	function set_training() {return 'Training';}
	function set_trainingprints() {return 'Training Prints';}
	function set_training_name() {return 'Name';}
	function set_training_cardname() {return 'Card Name';}
	function set_training_notes() {return 'Notes';}
	function set_training_printable() {return 'Printable';}
	function set_training_ceu_ceusgiven() {return 'CEUs (Credits) Given';}
	function set_training_ceu_ceu() {return 'CEU';}
	function set_training_ceu_code() {return 'Code';}
	function set_training_ceu_safety() {return 'Safety';}
	function set_training_ceu_discipline() {return 'Discipline';}
	function set_training_course() {return 'Edit Subcategory';}
	function set_training_printsnumber() {return 'Number of Prints';}
	function set_training_printsnewtotal() {return 'New Total Prints';}
	function set_training_success_printcards() {return 'Your total prints have been successfully altered.';}
	function set_training_invalid_newtrainingcourse($type, $language) {return 'The training course \''.$type.'\' for '.$language.' is already in the program.';}
	function set_training_invalid_newtrainingcourselang($language) {return 'You must fill out the '.$language.' when making a new training course.';}
	function set_training_success_newtrainingcourse($type, $language) {return 'The training course \''.$type.'\' for '.$language.' has been added to the program.';}
	function set_training_success_deletetrainingcourse($type, $language) {return 'Your training course, '.$type.', for '.$language.'  has been deleted.';}
	function set_training_success_edittrainingcourse($type, $language) {return 'Your training course, '.$type.', for '.$language.'  has been successfully changed.';}
	function set_training_success_edit() {return 'Your training course has been successfully edited.';}
	
	//setup equipment
	function set_equipment_addequipmentcontents() {return 'Add Equipment Details';}
	function set_equipment_addequipmentquestions() {return 'Add Equipment Question';}
	function set_equipment() {return 'Equipment';}
	function set_equipment_newcat() {return 'New Category';}
	function set_equipment_editcat() {return 'Edit Category';}
	function set_equipment_deletecat() {return 'Delete Category';}
	function set_equipment_newsubcat() {return 'New Subcategory';}
	function set_equipment_editsubcat() {return 'Edit Subcategory';}
	function set_equipment_deletesubcat() {return 'Delete Subcategory';}
	function set_equipment_type() {return 'Type';}
	function set_equipment_content() {return 'Content';}
	function sel_equipment_yesno() {return 'Yes/No';}
	function sel_equipment_text() {return 'Text';}
	function sel_equipment_picture() {return 'Picture';}
	function sel_equipment_pdf() {return 'PDF';}
	function sel_equipment_expiration() {return 'Expiration';}
	function set_equipment_emailalerts() {return 'Email Alerts';}
	function set_equipment_name() {return 'Name';}
	function set_equipment_checkfreq() {return 'Check Frequency(days)';}
	function set_equipment_levelone() {return 'Level 1(days)';}
	function set_equipment_leveltwo() {return 'Level 2(days)';}
	function set_equipment_levelthree() {return 'Level 3(days)';}
	function set_equipment_alerts() {return 'Alerts';}
	function set_equipment_details() {return 'Details';}
	function set_equipment_checkquestions() {return 'Check Questions';}
	function set_equipment_confirm_deletecat() {return 'Are you sure you want to delete this equipment category?';}
	function set_equipment_success_deletecat() {return 'Your equipment category has been deleted.';}
	function set_equipment_invalid_newsubcat() {return 'This subcategory of equipment is already in the program.';}
	function set_equipment_confirm_deletesubcat() {return 'Are you sure you want to delete this equipment subcategory?';}
	function set_equipment_success_deletesubcat() {return 'Your equipment subcategory has been deleted.';}
	function set_equipment_success_newdetails($name, $language) {return 'Your firstvoice equipment details, '.$name.', for '.$language.' has been successfully added.';}
	function set_equipment_success_deletecheckquestion($name, $language) {return 'Your equipment check question, '.$name.', for '.$language.' has been deleted.';}
	function set_equipment_success_editcheckquestion($name, $language) {return 'Your equipment check question, '.$name.', for '.$language.' has been successfully changed.';}
	function set_equipment_success_deletedetails($name, $language) {return 'Your equipment details, '.$name.', for '.$language.' has been deleted.';}
	function set_equipment_success_editdetails($name, $language) {return 'Your equipment details, '.$name.', for '.$language.' has been successfully changed.';}
	function set_equipment_success_editsubcatname($type, $language) {return 'The equipment subcategory name, '.$type.', for '.$language.'  has been successfully changed.';}
	function set_equipment_invalid_editsubcatname($type, $language) {return 'The equipment subcategory name, '.$type.', for '.$language.'  could not be changed.';}
	function set_equipment_success_editcheckfreq() {return 'Your custom check frequency has been successfully changed.';}
	function set_equipment_invalid_editcheckfreq() {return 'Your custom check frequency could not be changed.  You must enter a number.';}
	function set_equipment_success_editlevelone() {return 'Your custom level one alert has been successfully changed.';}
	function set_equipment_invalid_editlevelone() {return 'Your custom level one alert could not be changed.  You must enter a number.';}
	function set_equipment_success_editleveltwo() {return 'Your custom level two alert has been successfully changed.';}
	function set_equipment_invalid_editleveltwo() {return 'Your custom level two alert could not be changed.  You must enter a number.';}
	function set_equipment_success_editlevelthree() {return 'Your custom level three alert has been successfully changed.';}
	function set_equipment_invalid_editlevelthree() {return 'Your custom level three alert could not be changed.  You must enter a number.';}
	function set_equipment_success_newalerts() {return 'Your custom alerts has been successfully made.';}
	function set_equipment_invalid_newalerts() {return 'You must fill out the check frequency, level one, level two and level three upon your first edit.';}
	function set_equipment_success_newcat($name, $language) {return 'Your new equipment category, '.$name.', for '.$language.' has been added to the program.';}
	function set_equipment_success_editcat($name, $language) {return 'Your equipment category, '.$name.', for '.$language.' has been successfully changed.';}
	function set_equipment_success_newsubcat($name, $language) {return 'Your new equipment subcategory, '.$name.', for '.$language.' has been added to the program.';}
	function set_equipment_success_editsubcat($name, $language) {return 'Your equipment subcategory, '.$name.', for '.$language.' has been successfully changed.';}
	function set_equipment_success_newcatandsubcat() {return 'Your new category and subcategory of equipment has been added to the program.';}
	function set_equipment_contenttype($type) {
		switch($type)
		{
			case 'text':
				return $this->sel_equipment_text();
				break;
			case 'expiration':
				return $this->sel_equipment_expiration();
				break;
			case 'picture':
				return $this->sel_equipment_picture();
				break;
			case 'pdf':
				return $this->sel_equipment_pdf();
				break;
			default: return $type;
		}
	}
	function set_equipment_questiontype($type) {
		switch($type)
		{
			case 'text':
				return $this->sel_equipment_text();
				break;
			case 'expiration':
				return $this->sel_equipment_expiration();
				break;
			case 'picture':
				return $this->sel_equipment_picture();
				break;
			case 'pdf':
				return $this->sel_equipment_pdf();
				break;
			default: return $type;
		}
	}
	function set_equipment_success_newcontent($type, $language) {return 'Your equipment detail \''.$type.'\' for '.$language.' has been add to the program.';}
	function set_equipment_success_newquestion($type, $language) {return 'Your equipment question \''.$type.'\' for '.$language.' has been add to the program.';}
	
	//setup firstvoice
	function set_firstvoice_addequipmentcontents() {return 'Add Equipment Details';}
	function set_firstvoice_addequipmentquestions() {return 'Add Equipment Question';}
	function set_firstvoice() {return 'First Voice';}
	function set_firstvoice_newcat() {return 'New Category';}
	function set_firstvoice_editcat() {return 'Edit Category';}
	function set_firstvoice_deletecat() {return 'Delete Category';}
	function set_firstvoice_newsubcat() {return 'New Subcategory';}
	function set_firstvoice_editsubcat() {return 'Edit Subcategory';}
	function set_firstvoice_deletesubcat() {return 'Delete Subcategory';}
	function set_firstvoice_type() {return 'Type';}
	function set_firstvoice_content() {return 'Content';}
	function sel_firstvoice_yesno() {return 'Yes/No';}
	function sel_firstvoice_text() {return 'Text';}
	function sel_firstvoice_picture() {return 'Picture';}
	function sel_firstvoice_pdf() {return 'PDF';}
	function sel_firstvoice_expiration() {return 'Expiration';}
	function set_firstvoice_emailalerts() {return 'Email Alerts';}
	function set_firstvoice_name() {return 'Name';}
	function set_firstvoice_checkfreq() {return 'Check Frequency(days)';}
	function set_firstvoice_levelone() {return 'Level 1(days)';}
	function set_firstvoice_leveltwo() {return 'Level 2(days)';}
	function set_firstvoice_levelthree() {return 'Level 3(days)';}
	function set_firstvoice_alerts() {return 'Alerts';}
	function set_firstvoice_details() {return 'Details';}
	function set_firstvoice_checkquestions() {return 'Check Questions';}
	function set_firstvoice_confirm_deletecat() {return 'Are you sure you want to delete this equipment category?';}
	function set_firstvoice_success_deletecat() {return 'Your equipment category has been deleted.';}
	function set_firstvoice_invalid_newsubcat() {return 'This subcategory of equipment is already in the program.';}
	function set_firstvoice_confirm_deletesubcat() {return 'Are you sure you want to delete this equipment subcategory?';}
	function set_firstvoice_success_deletesubcat() {return 'Your equipment subcategory has been deleted.';}
	function set_firstvoice_success_newdetails($name, $language) {return 'Your firstvoice equipment details, '.$name.', for '.$language.' has been successfully added.';}
	function set_firstvoice_success_deletecheckquestion($name, $language) {return 'Your equipment check question, '.$name.', for '.$language.' has been deleted.';}
	function set_firstvoice_success_editcheckquestion($name, $language) {return 'Your equipment check question, '.$name.', for '.$language.' has been successfully changed.';}
	function set_firstvoice_success_deletedetails($name, $language) {return 'Your equipment details, '.$name.', for '.$language.' has been deleted.';}
	function set_firstvoice_success_editdetails($name, $language) {return 'Your equipment details, '.$name.', for '.$language.' has been successfully changed.';}
	function set_firstvoice_success_editsubcatname($type, $language) {return 'The equipment subcategory name, '.$type.', for '.$language.'  has been successfully changed.';}
	function set_firstvoice_invalid_editsubcatname($type, $language) {return 'The equipment subcategory name, '.$type.', for '.$language.'  could not be changed.';}
	function set_firstvoice_success_editcheckfreq() {return 'Your custom check frequency has been successfully changed.';}
	function set_firstvoice_invalid_editcheckfreq() {return 'Your custom check frequency could not be changed.  You must enter a number.';}
	function set_firstvoice_success_editlevelone() {return 'Your custom level one alert has been successfully changed.';}
	function set_firstvoice_invalid_editlevelone() {return 'Your custom level one alert could not be changed.  You must enter a number.';}
	function set_firstvoice_success_editleveltwo() {return 'Your custom level two alert has been successfully changed.';}
	function set_firstvoice_invalid_editleveltwo() {return 'Your custom level two alert could not be changed.  You must enter a number.';}
	function set_firstvoice_success_editlevelthree() {return 'Your custom level three alert has been successfully changed.';}
	function set_firstvoice_invalid_editlevelthree() {return 'Your custom level three alert could not be changed.  You must enter a number.';}
	function set_firstvoice_success_newalerts() {return 'Your custom alerts has been successfully made.';}
	function set_firstvoice_invalid_newalerts() {return 'You must fill out the check frequency, level one, level two and level three upon your first edit.';}
	function set_firstvoice_success_newcat($name, $language) {return 'Your new equipment category, '.$name.', for '.$language.' has been added to the program.';}
	function set_firstvoice_success_editcat($name, $language) {return 'Your equipment category, '.$name.', for '.$language.' has been successfully changed.';}
	function set_firstvoice_success_newsubcat($name, $language) {return 'Your new equipment subcategory, '.$name.', for '.$language.' has been added to the program.';}
	function set_firstvoice_success_editsubcat($name, $language) {return 'Your equipment subcategory, '.$name.', for '.$language.' has been successfully changed.';}
	function set_firstvoice_success_newcatandsubcat() {return 'Your new category and subcategory of equipment has been added to the program.';}
	function set_firstvoice_contenttype($type) {
		switch($type)
		{
			case 'text':
				return $this->sel_firstvoice_text();
				break;
			case 'expiration':
				return $this->sel_firstvoice_expiration();
				break;
			case 'picture':
				return $this->sel_firstvoice_picture();
				break;
			case 'pdf':
				return $this->sel_firstvoice_pdf();
				break;
			default: return $type;
		}
	}
	function set_firstvoice_questiontype($type) {
		switch($type)
		{
			case 'text':
				return $this->sel_firstvoice_text();
				break;
			case 'expiration':
				return $this->sel_firstvoice_expiration();
				break;
			case 'picture':
				return $this->sel_firstvoice_picture();
				break;
			case 'pdf':
				return $this->sel_firstvoice_pdf();
				break;
			default: return $type;
		}
	}
	function set_firstvoice_success_newcontent($type, $language) {return 'Your equipment detail \''.$type.'\' for '.$language.' has been add to the program.';}
	function set_firstvoice_success_newquestion($type, $language) {return 'Your equipment question \''.$type.'\' for '.$language.' has been add to the program.';}
	
	
	//facility
	function fac_heading() {return 'Facility Information';}
	
	//help
	function hel_usermanual() {return 'User Manual';}
	function hel_adminusermanual() {return 'Admin User Manual';}
	function hel_blankaedtemplate() {return 'Blank AED Check';}
	function hel_blankservicecheck() {return 'Blank Servicing Check';}
	function hel_trainingupload() {return 'Student Training Upload';}
	function hel_eventform() {return 'Event Form';}
	function hel_recallhistory() {return 'Recall History';}
	function hel_aedcheckvideo() {return 'How to do a AED Check in First Voice Manager';}
	function hel_content() {
		$content = '';
		$content .= '
		<h1>Help</h1>
		<table style="width:600px;">';
			if(file_exists($this->doc_usermanual())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_usermanual().':</td> 
					<td><a target="_blank" href="'.$this->doc_usermanual().'">PDF</a><td>
				</tr>';
			}
			if(file_exists($this->doc_adminusermanual())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_adminusermanual().':</td> 
					<td><a target="_blank" href="'.$this->doc_adminusermanual().'">PDF</a><td>
				</tr>';
			}
			
			if(file_exists($this->doc_blankaedcheck())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_blankaedtemplate().':</td> 
					<td><a target="_blank" href="'.$this->doc_blankaedcheck().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_blankservicecheck())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_blankservicecheck().':</td> 
					<td><a target="_blank" href="'.$this->doc_blankservicecheck().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_trainingtemplate())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_trainingupload().':</td> 
					<td><a target="_blank" href="'.$this->doc_trainingtemplate().'">XLS</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_eventform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_eventform().':</td> 
					<td><a target="_blank" href="'.$this->doc_eventform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_recallhistory())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_recallhistory().':</td> 
					<td><a target="_blank" href="'.$this->doc_recallhistory().'">DOC</a></td>
				</tr>';
			}
		$content .= '
		</table>';	
		return $content;
	}
	
	//profile
	function pro_heading() {return 'Profile';}
	function pro_username() {return 'Username';}
	function pro_password() {return 'Password';}
	function pro_currentpassword() {return 'Current Password';}
	function pro_newpassword() {return 'New Password';}
	function pro_retypepassword() {return 'Retype Password';}
	function pro_clicktochange() {return '(click edit to change)';}
	function pro_firstname() {return 'First Name';}
	function pro_lastname() {return 'Last Name';}
	function pro_preferredlang() {return 'Preferred Language';}
	function pro_phone() {return 'Phone';}
	function pro_company() {return 'Company';}
	function pro_address() {return 'Address';}
	function pro_city() {return 'City';}
	function pro_state() {return 'State';}
	function pro_country() {return 'Country';}
	function pro_zip() {return 'Zip';}
	function pro_invalid_validemail() {return 'You must enter a valid email address.';}
	function pro_invalid_uniqueemail() {return 'Your email address in not unique.';}
	function pro_emailsubject() {return 'Profile Update';}
	function pro_email() { return '<html><head></head><body style="color:#000001;">
										<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
											<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
												
													<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
														<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
													</h1>
													<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
													<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
														Profile Update
													</h2>
													<br>
													<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
														You have recently updated your profile on <a href="'.URL.'">'.PROGRAM.'</a>.  If this was not you, please
														contact our technical support at <b>'.SUPPORTEMAIL.'</b>.<br />
														<br />
														For any questions, please feel free to contact us at <b>'.SUPPORTEMAIL.'</b>.
													</div>
											</div>
										 </div>
										 <div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
											Please do not reply to this email.  We are unable to respond to inquiries sent to this address.
										</div>
										<br />
										<div style="width:970px;margin:0 auto;color:#000001;">
											Copyright &copy;  2013 Think Safe Inc.
										</div>
									</body></html>';}
	function pro_addemail() {return 'You have recently updated your profile on '.URL.'.  If this was not you, please contact our technical support at '.SUPPORTEMAIL.'

For any questions, please feel free to contact us at '.SUPPORTEMAIL.'.';}// uses a pre tag, so all spacing is recorded.
	function pro_success_edit() {return 'Your profile has been successfully updated.';}
	
	//login
	function log_heading() {return 'Login';}
	function log_username() {return 'Username';}
	function log_password() {return 'Password';}
	function but_login() {return 'Login';}
	function log_forgotpassword() {return 'Forgot your password?';}
	function log_invalid_login() {return 'The user and password you have entered are invalid.';}
	function log_logout() {return 'You have logged out.';}
	
	//forgot password
	function for_heading() {return 'Forgot My Password';}
	function for_invalid_forgot() {return 'If you have forgot your password, just type in your email into the box and you will receive and email with your new password.';}
	function for_success_forgot() {return 'An email with your new password should arrive shortly.';}
	function for_emailsubject() {return PROGRAM.' Forgotten Password';}
	function for_email($username, $password) {return '<html><head></head><body style="color:#000001;">
							<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
								<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
									<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
										<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
									</h1>
									<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
									<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
										Welcome!
									</h2>
									<br />
									<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
										You have recently had your profile changed at <a href="'.URL.'">'.PROGRAM.'</a>.  This is a program that allows you 
										to manage your equipment such as AEDs and first aid kits.<br />
										<b>Your new login information:</b>
											<div style="margin-left:10px;font-weight:bold;">
												Username: '.$username.'<br />
												Password: '.$password.'
											</div>
										If you have anymore questions, contact our technical support at <b>'.SUPPORTEMAIL.'</b>.<br />
										If you think you got this email by accident, please disregard.
									</div>
								</div>
							</div>
							<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
									Please do not reply to this email.  We are unable to respond to inquiries sent to this address.
							</div>
								<br />
							<div style="width:970px;margin:0 auto;color:#000001;">
								Copyright &copy;  2013 Think Safe Inc.
							</div>
						</body></html>';}
	function for_recordedemail($username) {return '<html><head></head><body style="color:#000001;">
						<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
							<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
								<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
									<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
								</h1>
								<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
								<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
									Welcome!
								</h2>
								<br />
								<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
									You have recently had your profile changed at <a href="'.URL.'">'.PROGRAM.'</a>.  This is a program that allows you 
									to manage your equipment such as AEDs and first aid kits.<br />
									<b>Your new login information:</b>
										<div style="margin-left:10px;font-weight:bold;">
										Username: '.$username.'<br />
										Password: (not shown for privacy issues)
										</div>
									If you have anymore questions, contact our technical support at <b>'.SUPPORTEMAIL.'</b>.<br />
									If you think you got this email by accident, please disregard.
									<br />
									If you have any questions, please feel free to contact us at <b>'.SUPPORTEMAIL.'</b>.
								</div>
							</div>
						</div>
						<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
								Please do not reply to this email.  We are unable to respond to inquiries sent to this address.
						</div>
							<br />
						<div style="width:970px;margin:0 auto;color:#000001;">
							Copyright &copy;  2013 Think Safe Inc.
						</div>
					</body></html>';}
	function for_addemail($username, $password) {return 'You have recently had your profile changed at '.PROGRAM.' - '.URL.'.  
This is a program that allows you to manage your equipment such as AEDs and first aid kits.
Logging into '.PROGRAM.' is simple!
Username: '.$username.'
Password: '.$password.'
If you have anymore questions, contact our technical support at '.SUPPORTEMAIL.'.
If you think you got this email by accident, please disregard.';}// uses a pre tag, so all spacing is recorded.
	
	//visitor home
	function vis_home() {$home = '
		<h1 style="text-align:left;">'.PROGRAM.':</h1>';
		
		$home .= '
		<br />
		<ul class="boldlist">
			<li>'.TITLE.' & AED Management</li>
			<li>Expiration Date Management</li>
			<li>Training Certification Tracking</li>
			<li>Location & Hazard Tracking</li>
			<li>Medical Direction</li>	
		</ul>	
		<br />
		
		<p>
			'.PROGRAM.' allows you to manage your emergency response program<br />
			online, ensuring up-to-date maintenance records for your '.TITLE.' products and AEDS, and<br />employee training certification renewal dates.
		</p>
		<br />
		<p>
			For Assistance with your emergency response program, or answers to your questions on current<br />government regulations, contact a '.TITLE.' specialist.
		</p>
		<br />
		<p style="font-weight:bold;">Technical Support Phone Number: 1-888-473-1777 (United States) | 1-319-377-5125 (International)</p>';
		return $home;}
		
	//message center
	function mes_attachments() {return 'Attachments';}
	function mes_attachment() {return 'attachment';}
	function mes_invalid_emailrecord() {return 'ERROR: Your email could not be found.  Please contact '.SUPPORTEMAIL.'.';}
	function mes_heading() {return 'Message Center';}
	function mes_newmessage() {return 'New Message';}
	function mes_howemail() {return 'How are you going to email?';}
	function mes_byuser() {return 'By User';}
	function mes_bymodel() {return 'By AED Model';}
	function mes_bybrand() {return 'By AED Brand';}
	function mes_byorg() {return 'By Organization';}
	function mes_bylocation() {return 'By Location';}
	function mes_to() {return 'To';}
	function mes_subject() {return 'Subject';}
	function mes_body() {return 'Body';}
	function mes_success_send($to) {return 'Your email has successfully sent to '.$to.'.';}
	function mes_ifquestions() {return 'If you have any questions about this email, please feel free to contact us at <b>'.SUPPORTEMAIL.'</b>.';}
	function mes_pleasedonotreply() {return 'Please do not reply to this email. We are unable to respond to inquiries sent to this address.';}
	function mes_tscopyright() {return 'Copyright &copy; 2013 Think Safe Inc.';}
	
	//contacts
	function con_heading() {return 'Contacts';}
	function con_newcontact() {return 'New Contacts';}
	function con_success_new() {return 'Your new contact has been successfully added.';}
	function con_editcontact() {return 'Edit Contacts';}
	function con_success_edit() {return 'The contact has been successfully edited.';}
	function con_invalid_edit() {return 'The contact could not be successfully edited.';}
	function con_deletecontact() {return 'Delete Contacts';}
	function con_confirm_delete() {return 'Are you sure you want to delete this contact?';}
	function con_success_delete() {return 'Your contact has been deleted.';}
	function con_invalid_delete() {return 'Your contact could not be deleted.';}
	function con_primarycontact() {return 'Primary Contact';}
	function con_secondarycontact() {return 'Secondary Contact';}
	function con_invalid_nocontacts() {return 'There are currently no contact for this location.';}
	function con_preferredlang() {return 'Preferred Language';}
	function con_name() {return 'Name';}
	function con_secondarycontactname() {return 'Secondary Contact Name';}
	function con_title() {return 'Title';}
	function con_company() {return 'Company';}
	function con_department() {return 'Department';}
	function con_contacttype() {return 'Contact Type';}
	function con_pickcontacttype($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'aedcoordinator':
				return 'AED Coordinator';
			case 'backupaedcoordinator':
				return 'Backup AED Coordinator';
			case 'salesrep':
				return 'Sales Rep';
			case '':
				return 'None';
			default:
				return 'ERROR UNKNOWN TYPE:'.$type;
		}
	}
	function con_date() {return 'Date';}
	function con_phone() {return 'Phone';}
	function con_cell() {return 'Cell';}
	function con_fax() {return 'Fax';}
	function con_email() {return 'Email';}
	function con_address() {return 'Address';}
	function con_city() {return 'City';}
	function con_state() {return 'State';}
	function con_country() {return 'Country';}
	function con_zip() {return 'Zip';}
	function con_sendaedcheck() {return 'Copy this contact in on all AED checks done';}
	function con_primary() {return 'Primary';}
	function con_secondary() {return 'Secondary';}
	function con_preferredlanguage() {return 'Preferred Language';}
	function con_notes() {return 'Notes';}
	function con_none() {return 'There are no contacts for this location.';}
	
	//scheduler
	function sch_heading() {return 'Scheduler';}
	function sch_newevent() {return 'New Scheduler Event';}
	function sch_success_newevent() {return 'Your meeting has been emailed to your outlook.';}
	function sch_deleteevent() {return 'Delete Scheduler Event';}
	function sch_confirm_deleteevent() {return 'Are you sure you want to delete this meeting?';}
	function sch_success_deleteevent() {return 'The meeting has been deleted.';}
	function sch_editevent() {return 'Edit Scheduler Event';}
	function sch_success_editevent() {return 'This meeting has been successfully updated.';}
	function sch_eventlisting() {return 'Event Listing';}
	function sch_calendarlisting() {return 'Calendar Listing';}
	function sch_company() {return 'Company';}
	function sch_subject() {return 'Subject';}
	function sch_locationfill() {return 'Location Fill';}
	function sch_location() {return 'Location';}
	function sch_date() {return 'Date';}
	function sch_timezone() {return 'Time Zone';}
	function sch_time() {return 'Time';}
	function sch_inviteemail() {return 'Invite another email';}
	function sch_contact() {return 'Contact';}
	function sch_address() {return 'Address';}
	function sch_phone() {return 'Phone';}
	function sch_city() {return 'City';}
	function sch_state() {return 'State';}
	function sch_country() {return 'Country';}
	function sch_zip() {return 'Zip';}
	function sch_description() {return 'Description';}
	function sch_externalid() {return 'External ID';}
	function sch_internalid() {return 'Internal ID';}
	function sch_sendtomyemail() {return 'Send To My Email';}
	function sch_updatedsubject($subject) {return 'Updated '.$subject;}
	function sch_cancelledsubject($subject) {return 'Cancelled '.$subject;}
	function sch_timezone_americalosangeles() {return '(GMT-08:00) Pacific Time (US &amp; Canada)';}
	function sch_timezone_americadenver() {return '(GMT-07:00) Mountain Time (US &amp; Canada)';}
	function sch_timezone_americachicago() {return '(GMT-06:00) Central Time (US &amp; Canada)';}
	function sch_timezone_americanewyork() {return '(GMT-05:00) Eastern Time (US &amp; Canada)';}
	function sch_timezone_americaglacebay() {return '(GMT-04:00) Atlantic Time (Canada)';}
	function sch_timezone_americastjohns() {return '(GMT-03:30) Newfoundland';}
	function sch_monthsarray() {return Array("January", "February", "March", "April", "May", "June", "July", 
			"August", "September", "October", "November", "December");}
	
	//recalls
	function rec_heading() {return 'Recalls & Services';}
	function rec_documentation() {return 'Documentation';}
	function rec_history() {return 'Recall History';}
	function rec_recalls() {return 'Recalls';}
	//function rec_newservice() {return 'New Servicing Documentation';}
	function rec_newrecall() {return 'New Recall';}
	function rec_newrecalldoc() {return 'New Documentation';}
	function rec_success_newrecall() {return 'Your recall has been successfully added.';}
	function rec_success_newrecalldoc() {return 'Your documentation has been successfully added.';}
	function rec_invalid_newrecalldocfile() {return 'You must upload a pdf, doc, or xls.';}
	function rec_confirm_deleterecall() {return 'Are you sure you want to delete this recall?';}
	function rec_success_deleterecall() {return 'Your recall has been deleted.';}
	function rec_confirm_deleterecalldoc() {return 'Are you sure you want to delete this documentation?';}
	function rec_success_deleterecalldoc() {return 'Your documentation has been deleted.';}
	function rec_success_editrecall() {return 'Your recall has successfully edited.';}
	function rec_invalid_editrecall() {return 'Your recall could not be edited.';}
	function rec_success_editrecalldoc() {return 'Your documentation has successfully edited.';}
	function rec_date() {return 'Recall Date';}
	function rec_recalltype() {return 'Recall Type';}
	function rec_brandsandmodels() {return 'Brands and Models';}
	function rec_range() {return 'Range';}
	function rec_notes() {return 'Notes';}
	function rec_searchhistory() {return '<p>If you would like to search for recalls that may be related to your AED within the US FDA database for "Medical Device Recalls".</p>
		<p>Please <a href="http://www.accessdata.fda.gov/scripts/cdrh/cfdocs/cfRES/res.cfm" target="blank">click here</a> and type in  your AED brand name or model in the fields provided.</p>';
		/* CANADA
		return '<p>If you would like to search for recalls that may be related to your AED within the Health Canada database for  “automated external defibrillator?recalls or your specific brand/make recall history, please click link below:</p>
		<p><a href="http://www.healthycanadians.gc.ca/recall-alert-rappel-avis/index-eng.php">http://www.healthycanadians.gc.ca/recall-alert-rappel-avis/index-eng.php</a></p>';
		*/
	}
	function rec_servicedate() {return 'Service Date';}
	function rec_file() {return 'File';}
	function rec_view() {return 'View';}
	function rec_type() {return 'Type';}
	function rec_picktype($type) {
		switch($type) {
			case 'other':
				return 'Other';
			case 'event':
				return 'Event';
			default:
				return 'ERROR: Unknown type:'.$type;
		}
	}
	function rec_name() {return 'Name';}
	function rec_brand() {return 'Brand';}
	function rec_model() {return 'Model';}
	function rec_none() {return 'There are currently no recalls for this location.';}
	function rec_docnone() {return 'There are currently no documentation for this location.';}
	
	//medical direction
	function med_heading() {return 'Medical Direction';}
	function med_new() {return 'New Medical Direction';}
	function med_success_new() {return 'Your new medical direction has been successfully added.';}
	function med_edit() {return 'Edit Medical Direction';}
	function med_success_edit() {return 'The medical direction has been successfully edited.';}
	function med_invalid_edit() {return 'The medical direction could not be edited.';}
	function med_delete() {return 'Delete Medical Direction';}
	function med_confirm_delete() {return 'Are you sure you want to delete this medical direction?';}
	function med_success_delete() {return 'Your medical direction has been removed.';}
	function med_invalid_delete() {return 'Your medical direction could not be removed.';}
	function med_name() {return 'Name';}
	function med_startdate() {return 'Start Date';}
	function med_enddate() {return 'End Date';}
	function med_reviewdate() {return 'Last Review Date';}
	function med_nextreviewdate() {return 'Next Review Date';}
	function med_license() {return 'License';}
	function med_company() {return 'Company Name';}
	function med_address() {return 'Address';}
	function med_city() {return 'City';}
	function med_state() {return 'State';}
	function med_country() {return 'Country';}
	function med_zip() {return 'Zip';}
	function med_phone() {return 'Phone';}
	function med_cell() {return 'Cell';}
	function med_fax() {return 'Fax';}
	function med_email() {return 'Email';}
	function med_notes() {return 'Notes';}
	function med_none() {return 'There are currently no medical direction for this location.';}
	function med_status() {return 'Status';}
	function med_pickstatus($status){
		switch($status){
			case 'active':
				return 'Active';
			case 'active-filepending':
				return 'Active - File Pending';
			case 'active-filesigned':
				return 'Active - File Signed';
			case 'pending':
				return 'Pending';
			case 'cancelled':
				return 'Cancelled';
			case 'notrenewed':
				return 'Not Renewed';
			default:
				return 'Active';
		}
	}
	
	//erp
	function erp_heading() {return 'Emergency Response Program';}
	function erp_new() {return 'New Emergency Response Program';}
	function erp_success_new() {return 'Your Emergency Response Program has been successfully added.';}
	function erp_delete() {return 'Delete Emergency Response Program';}
	function erp_confirm_delete() {return 'Are you sure you want to delete this Emergency Response Program?';}
	function erp_success_delete() {return 'Your Emergency Response Program has been successfully deleted.';}
	function erp_invalid_delete() {return 'Your Emergency Response Program could not be successfully deleted.';}
	function erp_name() {return 'Name';}
	function erp_type() {return 'Type';}
	function erp_picktype($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'defaultfiles':
				return 'Pick Default Files';
			case 'aedpolicy':
				return 'AED Policy';
			case 'aedprescriptioninfo':
				return 'AED Prescription Info';
			case 'aedresponseprotocols':
				return 'AED Response Protocols';
			case 'emsnotification':
				return 'EMS Notification';
			case 'floorplans':
				return 'Floorplans';
			case 'other':
				return 'Other';
			case 'siteassessment':
				return 'Site Assessment';
			case 'technicalexceptions':
				return 'Technical Exceptions';
			case 'trainingdrills':
				return 'Training Drills';
			default: return 'ERROR UNKNOWN TYPE:'.$type;
		}
	}
	function erp_file() {return 'File';}
	function erp_view() {return 'View';}
	function erp_registrationdate() {return 'Registration Date';}
	function erp_registrationexpiration() {return 'Registration Expiration';}
	function erp_registrationagency() {return 'Registration Agency';}
	function erp_uploaddate() {return 'Upload Date';}
	function erp_invalid_file() {return 'You have uploaded an invalid file type.';}
	function erp_none() {return 'There are currently no Emergency Response Program for this location.';}
	
	
	function erp_aedprescriptioninfo_heading() {return 'AED Prescription Information';}
	function erp_aedprescriptioninfo_new() {return 'New AED prescription Information';}
	function erp_aedprescriptioninfo_success_new() {return 'Your AED prescription Information has been successfully added.';}
	function erp_aedprescriptioninfo_delete() {return 'Delete AED prescription Information';}
	function erp_aedprescriptioninfo_confirm_delete() {return 'Are you sure you want to delete this AED prescription Information?';}
	function erp_aedprescriptioninfo_success_delete() {return 'Your AED prescription Information has been successfully deleted.';}
	function erp_aedprescriptioninfo_invalid_file() {return 'You must upload a pdf.';}
	function erp_aedprescriptioninfo_name() {return 'Name';}
	function erp_aedprescriptioninfo_file() {return 'File';}
	function erp_aedprescriptioninfo_uploaddate() {return 'Upload Date';}
	function erp_aedprescriptioninfo_none() {return 'There are currently no AED prescription information for this location.';}
	function erp_aedpolicy_heading() {return 'AED Policy or Guidelines';}
	function erp_aedpolicy_new() {return 'New AED Policy or Guidelines';}
	function erp_aedpolicy_success_new() {return 'Your AED Policy or Guidelines has been successfully added.';}
	function erp_aedpolicy_delete() {return 'Delete AED Policy or Guidelines';}
	function erp_aedpolicy_confirm_delete() {return 'Are you sure you want to delete this AED Policy or Guidelines?';}
	function erp_aedpolicy_success_delete() {return 'Your AED Policy or Guidelines has been successfully deleted.';}
	function erp_aedpolicy_invalid_file() {return 'You must upload a pdf.';}
	function erp_aedpolicy_name() {return 'Name';}
	function erp_aedpolicy_file() {return 'File';}
	function erp_aedpolicy_uploaddate() {return 'Upload Date';}
	function erp_aedpolicy_none() {return 'There are currently no AED policies or guidlines for this location.';}
	function erp_aedresponseprotocols_heading() {return 'AED Response Protocols';}
	function erp_aedresponseprotocols_new() {return 'New AED Response Protocols';}
	function erp_aedresponseprotocols_success_new() {return 'Your Response Protocol has been successfully added.';}
	function erp_aedresponseprotocols_delete() {return 'Delete AED Response Protocols';}
	function erp_aedresponseprotocols_confirm_delete() {return 'Are you sure you want to delete this AED Response Protocol?';}
	function erp_aedresponseprotocols_success_delete() {return 'Your Response Protocol has been successfully deleted.';}
	function erp_aedresponseprotocols_invalid_file() {return 'You must upload a pdf.';}
	function erp_aedresponseprotocols_name() {return 'Name';}
	function erp_aedresponseprotocols_file() {return 'File';}
	function erp_aedresponseprotocols_uploaddate() {return 'Upload Date';}
	function erp_aedresponseprotocols_none() {return 'There are currently no response protocols for this location.';}
	function erp_emsnotification_heading() {return 'Registration/EMS Notification';}
	function erp_emsnotification_new() {return 'New Registration/EMS Notification';}
	function erp_emsnotification_success_new() {return 'Your Registration/EMS Notification has been successfully added.';}
	function erp_emsnotification_delete() {return 'Delete Registration/EMS Notification';}
	function erp_emsnotification_confirm_delete() {return 'Are you sure you want to delete this Registration/EMS Notification?';}
	function erp_emsnotification_success_delete() {return 'Your Registration/EMS Notification has been successfully deleted.';}
	function erp_emsnotification_invalid_file() {return 'You must upload a pdf.';}
	function erp_emsnotification_name() {return 'Name';}
	function erp_emsnotification_file() {return 'File';}
	function erp_emsnotification_uploaddate() {return 'Upload Date';}
	function erp_emsnotification_dateregistred() {return 'Date Registred';}
	function erp_emsnotification_registrationexpiration() {return 'Registration Expiration';}
	function erp_emsnotification_registrationagency() {return 'Registration Agency';}
	function erp_emsnotification_none() {return 'There are currently no registration/EMS notifications for this location.';}
	function erp_floorplans_heading() {return 'Floor Plans & Site Photos';}
	function erp_floorplans_new() {return 'New Floor Plans and Photos';}
	function erp_floorplans_success_new() {return 'Your Floor Plan or Photo has been successfully added.';}
	function erp_floorplans_delete() {return 'Delete Floor Plans and Photos';}
	function erp_floorplans_confirm_delete() {return 'Are you sure you want to delete this Floor Plan or Photo?';}
	function erp_floorplans_success_delete() {return 'Your Floor Plan or Photo has been successfully deleted.';}
	function erp_floorplans_invalid_file() {return 'You must upload a \'pdf\', \'png\', \'gif\', \'jpeg\' or \'jpg\'.';}
	function erp_floorplans_name() {return 'Name';}
	function erp_floorplans_file() {return 'File';}
	function erp_floorplans_uploaddate() {return 'Upload Date';}
	function erp_floorplans_none() {return 'There are currently no floor plans or site photos for this location.';}
	function erp_siteassessment_heading() {return 'Site Assessment & Documentation';}
	function erp_siteassessment_new() {return 'New Site Assessment and Documentation';}
	function erp_siteassessment_success_new() {return 'Your Site Assessment or Documentation has been successfully added.';}
	function erp_siteassessment_delete() {return 'Delete Site Assessment and Documentation';}
	function erp_siteassessment_confirm_delete() {return 'Are you sure you want to delete this Site Assessment?';}
	function erp_siteassessment_success_delete() {return 'Your Site Assessment or Documentation has been successfully deleted.';}
	function erp_siteassessment_invalid_file() {return 'You must upload a pdf.';}
	function erp_siteassessment_name() {return 'Name';}
	function erp_siteassessment_file() {return 'File';}
	function erp_siteassessment_uploaddate() {return 'Upload Date';}
	function erp_siteassessment_none() {return 'There are currently no site assessment and documentation for this location.';}
	function erp_trainingdrills_heading() {return 'Training Drills or Training Scenarios';}
	function erp_trainingdrills_new() {return 'New Training Drill or Training Scenario';}
	function erp_trainingdrills_success_new() {return 'Your Training Drill or Training Scenario has been successfully added.';}
	function erp_trainingdrills_delete() {return 'Delete Training Drill or Training Scenario';}
	function erp_trainingdrills_confirm_delete() {return 'Are you sure you want to delete this Training Drill or Training Scenario?';}
	function erp_trainingdrills_success_delete() {return 'Your Training Drill or Training Scenario has been successfully deleted.';}
	function erp_trainingdrills_invalid_file() {return 'You must upload a pdf.';}
	function erp_trainingdrills_name() {return 'Name';}
	function erp_trainingdrills_file() {return 'File';}
	function erp_trainingdrills_uploaddate() {return 'Upload Date';}
	function erp_trainingdrills_none() {return 'There are currently no training drills or training scenarios for this location.';}	
	function erp_technicalexceptions_heading() {return 'Technical Exceptions';}
	function erp_technicalexceptions_new() {return 'New Technical Exception';}
	function erp_technicalexceptions_success_new() {return 'Your Technical Exception has been successfully added.';}
	function erp_technicalexceptions_delete() {return 'Delete Techinical Exception';}
	function erp_technicalexceptions_confirm_delete() {return 'Are you sure you want to delete this Techinical Exception?';}
	function erp_technicalexceptions_success_delete() {return 'Your Techinical Exception has been successfully deleted.';}
	function erp_technicalexceptions_invalid_file() {return 'You must upload a pdf.';}
	function erp_technicalexceptions_name() {return 'Name';}
	function erp_technicalexceptions_file() {return 'File';}
	function erp_technicalexceptions_uploaddate() {return 'Upload Date';}
	function erp_technicalexceptions_none() {return 'There are currently no technical expections for this location.';}	
	function erp_othersupporting_heading() {return 'Other Supporting Documents';}
	function erp_othersupporting_new() {return 'New Other Supporting Document';}
	function erp_othersupporting_success_new() {return 'Your Other Supporting Document has been successfully added.';}
	function erp_othersupporting_delete() {return 'Delete Other Supporting Document';}
	function erp_othersupporting_confirm_delete() {return 'Are you sure you want to delete this Other Supporting Document?';}
	function erp_othersupporting_success_delete() {return 'Your Other Supporting Document has been successfully deleted.';}
	function erp_othersupporting_invalid_file() {return 'You must upload a pdf.';}
	function erp_othersupporting_name() {return 'Name';}
	function erp_othersupporting_file() {return 'File';}
	function erp_othersupporting_uploaddate() {return 'Upload Date';}
	function erp_othersupporting_none() {return 'There are currently no other supporting documents for this location.';}
	
	//events
	function eve_heading() {return 'Events';}
	function eve_new() {return 'New Event';}
	function eve_success_new() {return 'Your event has been successfully added.';}
	function eve_delete() {return 'Delete Event';}
	function eve_confirm_delete() {return 'Are you sure you want to delete this event?';}
	function eve_success_delete() {return 'Your event has been successfully deleted.';}
	function eve_name() {return 'Name';}
	function eve_incident() {return 'AED Post Incident Form';}
	function eve_ecg() {return 'Electrocardiography(ECG)';}
	function eve_other() {return 'Other';}
	function eve_incidentlink() {return 'Incident';}
	function eve_ecglink() {return 'ECG';}
	function eve_otherlink() {return 'Other';}
	function eve_uploaddate() {return 'Upload Date';}
	function eve_invalid_pdf() {return 'You must upload a pdf.';}
	function eve_none() {return 'There are currently no events for this location.';}
	
	//training
	function tra_heading() {return 'Training';}
	function tra_newclass() {return 'New Training Class';}
	function tra_confirm_delete() {return 'Are you sure you want to delete this person?';}
	function nav_students() {return 'Persons';}
	function tra_students() {return 'Persons';}
	function nav_classes() {return 'Classes';}
	function tra_classes() {return 'Classes';}
	function tra_classtype() {return 'Class Type';}
	function tra_classnumber() {return 'Class Number';}
	function tra_more() {return 'More';}
	function tra_status() {return 'Status';}
	function tra_pickstatus($status) {
		switch($status)
		{
			case 'uncompleted':
				return 'Uncompleted';
			case 'completed':
				return 'Completed';
			case 'na':
				return 'N/A';
			case 'past':
				return 'Past Completed';
			case 'other':
				return 'Other';
			case 'neskts':
				return 'Needs Skills Test';
			default:
				return $status; //most likely a percentage!
		}
	}
	function tra_pickprefix($prefix) {
		switch($prefix)
		{
			case 'refresher':
				return 'Refresher: ';
			case 'demo':
				return 'Demo: ';
			case 'inperson':
				return 'Inperson: ';
			case 'none':
				return '';
			case 'noneshow':
				return 'None';
			default:
				return $status; //most likely a percentage!
		}
	}
	function tra_newstudent() {return 'New Student';}
	function tra_searchnames() {return 'Search Names';}
	function tra_prefix() {return 'Prefix';}
	function but_newstudent() {return 'New Student';}
	function but_printcards() {return 'Print Cards';}
	function tra_printcardsheading() {return 'Print Cards';}
	function tra_currenttrained() {return 'Current Trained';}
	function tra_pasttrained() {return 'Past Trained';}
	function tra_completiondate() {return 'Completion Date';}
	function tra_expiration() {return 'Expiration';}
	function tra_keycode() {return 'Keycode';}
	function tra_trainer_name() {return 'Trainer Name';}
	function tra_trainer_number() {return 'Trainer Number';}
	function tra_invalid_prints() {return 'You cannot print this many cards.';}
	function tra_success_prints() {return 'Your cards have been emailed to you.';}
	function tra_uploaddirections() {return 'If you wish to upload persons please follow <a href="documents/upload_training_template.xls">this template</a>.';}
	function tra_invalid_file_uploaddirections() {return 'If you wish to upload persons please follow <a href="documents/upload_training_template.xls">this template</a>.	<span style="color:red;">You must upload either an .xls, .xlsx or .csv file.</span>';}
	function tra_invalid_student_uploaddirections() {return 'If you wish to upload persons please follow <a href="documents/upload_training_template.xls">this template</a>.	<span style="color:red;">Atleast one of your employees does not have a name or employee id.</span>';}
	function tra_currentstudents() {return 'Current Student';}
	function tra_newstudents() {return 'New Student';}
	function tra_employeeid() {return 'Employee ID';}
	function tra_name() {return 'Name';}
	function tra_firstname() {return 'First Name';}
	function tra_lastname() {return 'Last Name';}
	function tra_first() {return 'First';}
	function tra_last() {return 'Last';}
	function tra_address() {return 'Address';}
	function tra_email() {return 'Email';}
	function tra_phone() {return 'Phone';}
	function tra_notes() {return 'Notes';}
	function tra_notcurrent() {return 'Not Current';}
	function tra_notonreportsheading() {return 'Not On Reports';}
	function tra_onreports() {return 'This person currently shows up on reports';}
	function tra_notonreports() {return 'This person currently does <b><i>not</i></b> shows up on reports';}
	function tra_roster() {return 'Roster';}
	function tra_success_new() {return 'You have successfully added a new class.';}
	function tra_success_edit() {return 'You have successfully edited a person.';}
	function tra_invalid_edit() {return 'You must fill out all the required fields to edit this person.';}
	function tra_success_delete() {return 'You have successfully deleted a person.';}
	function tra_invalid_print() {return 'You must select at least one certification.';}
	function tra_invalid_printnumber() {return 'You do not have enough prints for this selection of cards.';}
	function tra_lastmodified() {return 'Last Modified';}
	function tra_printcards() {return 'Email me printable cards';}
	function tra_noavailableprints() {return 'There are no available courses to print for this organization.';}
	function tra_printcards_subject() {return 'Wallet Certification Cards';}
	function tra_printcards_body() {
		return '
		<p>The attached electronic data file contains wallet card(s) for courses that were successfully completed.</p>
		<p>Please contact your sales rep or call our office at 319-377-5125 if you have questions or need additional information.  You may also email us at support@firstvoicetraining.com. </p>
		<p>We look forward to working with you and your staff for any other upcoming online or instructor-led first aid, CPR, or other safety and business training needs.</p>
		<p>Thank you for your business!</p>
	';}
	function tra_printcards_addbody() {return 
'The attached electronic data file contains wallet card(s) for courses that were successfully completed.

Please contact your sales rep or call our office at 319-377-5125 if you have questions or need additional information.  You may also email us at support@firstvoicetraining.com.

We look forward to working with you and your staff for any other upcoming online or instructor-led first aid, CPR, or other safety and business training needs.

Thank you for your business!';}
	function tra_checkbox_noreports() {return 'Do not display this person on reports.';}
	function tra_checkbox_responder() {return 'This person is a trained responder.';}
	function tra_none() {return 'There are no classes for this location yet.';}
	
	//immunizations
	function imm_heading() {return 'Immunizations';}
	function imm_newheading() {return 'New Immunization';}
	function imm_success_new() {return 'You have successfully added a new immunization.';}
	function imm_editheading() {return 'Edit Person';}
	function imm_success_edit() {return 'This person has been successfully updated.';}
	function imm_invalid_edit() {return 'This person could not be updated, please make sure all information was correct.';}
	function imm_deleteheading() {return 'Delete Person';}
	function imm_success_delete() {return 'This person has been successfully deleted.';}
	function imm_confirm_delete() {return 'Are you sure you want to delete this person?';}
	function imm_firstname() {return 'First Name';}
	function imm_lastname() {return 'Last Name';}
	function imm_first() {return 'First';}
	function imm_last() {return 'Last';}
	function imm_employeeid() {return 'Employee ID';}
	function imm_email() {return 'Email';}
	function imm_address() {return 'Address';}
	function imm_phone() {return 'Phone';}
	function imm_notes() {return 'Notes';}
	function imm_more() {return 'More';}
	function imm_date() {return 'Date';}
	function imm_employee() {return 'Employee';}
	function imm_supervisor() {return 'Supervisor';}
	function imm_expiration() {return 'Expiration';}
	function imm_ifapplicable() {return '(if applicable)';}
	function imm_status() {return 'Status';}
	function imm_addimmunization() {return 'Add Immunization';}
	function imm_immunization() {return 'Immunization';}
	function imm_persons() {return 'Persons';}
	function imm_person() {return 'Person';}
	function imm_newpersons() {return 'New Persons';}
	function imm_pickstatus($status) {
		switch($status)
		{
			case 'completed':
				return 'Completed';break;
			case 'declined':
				return 'Declined';break;
			case 'negative':
				return 'Negative';break;
			case 'offer':
				return 'Offer';break;
			case 'other':
				return 'Other';break;
			default:
				return $status;break;
		}
	}
	function imm_notonreportsheading() {return 'Not On Reports';}
	function imm_onreports() {return 'This person currently shows up on reports';}
	function imm_notonreports() {return 'This person currently does <b><i>not</i></b> shows up on reports';}
	function imm_none() {return 'There are currently no immunizations for this location.';}
	
	//documents
	function doc_heading() {return 'Documents';}
	function doc_newheading() {return 'New Document';}
	function doc_success_new() {return 'You have successfully added a new document.';}
	function doc_editheading() {return 'Edit Document';}
	function doc_success_edit() {return 'This document has been successfully updated.';}
	function doc_deleteheading() {return 'Delete Document';}
	function doc_success_delete() {return 'This document has been successfully deleted.';}
	function doc_confirm_delete() {return 'Are you sure you want to delete this document?';}
	function doc_firstname() {return 'First Name';}
	function doc_lastname() {return 'Last Name';}
	function doc_first() {return 'First';}
	function doc_last() {return 'Last';}
	function doc_employeeid() {return 'Employee ID';}
	function doc_email() {return 'Email';}
	function doc_address() {return 'Address';}
	function doc_phone() {return 'Phone';}
	function doc_notes() {return 'Notes';}
	function doc_more() {return 'More';}
	function doc_type() {return 'Type';}
	function doc_name() {return 'Name';}
	function doc_date() {return 'Date';}
	function doc_nextdate() {return 'Next Date';}
	function doc_employee() {return 'Employee';}
	function doc_supervisor() {return 'Supervisor';}
	function doc_files() {return 'Files';}
	function doc_status() {return 'Status';}
	function doc_adddocument() {return 'Add Document';}
	function doc_document() {return 'Document';}
	function doc_persons() {return 'Persons';}
	function doc_person() {return 'Person';}
	function doc_newpersons() {return 'New Persons';}
	function doc_peopleaffected() {return 'People Affected';}
	function doc_affected() {return 'Affected';}
	function doc_nopeopleaffected() {return 'There is no one attached to this specific document.';}
	function doc_pickstatus($status) {
		switch($status)
		{
			case 'uncompleted':
				return 'Uncompleted';
			case 'completed':
				return 'Completed';
			default:
				return $status;
		}
	}
	function doc_none() {return 'There are currently no documents for this location.';}
	
	//persons
	function per_heading() {return 'Persons';}
	function per_newheading() {return 'New Person';}
	function per_moveheading() {return 'Move Person';}
	function per_movepersons() {return 'Move';}
	function per_success_new() {return 'You have successfully added a new person.';}
	function per_success_move() {return 'You have successfully moved person.';}
	function per_editheading() {return 'Edit Person';}
	function per_success_edit() {return 'Your person has been successfully updated.';}
	function per_invalid_edit() {return 'You must use valid data to update persons.';}
	function per_deleteheading() {return 'Delete Person';}
	function per_success_delete() {return 'Your person has been successfully deleted.';}
	function per_invalid_delete() {return 'Your person could not be deleted.';}
	function per_confirm_delete() {return 'Are you sure you want to delete this person?';}
	function per_person() {return 'Person';}
	function per_name() {return 'Name';}
	function per_firstname() {return 'First Name';}
	function per_lastname() {return 'Last Name';}
	function per_first() {return 'First';}
	function per_last() {return 'Last';}
	function per_employeeid() {return 'Employee ID';}
	function per_email() {return 'Email';}
	function per_phone() {return 'Phone';}
	function per_address() {return 'Address';}
	function per_city() {return 'City';}
	function per_state() {return 'State';}
	function per_zip() {return 'Zip';}
	function per_notes() {return 'Notes';}
	function per_onreports() {return 'This person currently shows up on reports';}
	function per_notonreports() {return 'This person currently does <b><i>not</i></b> shows up on reports';}
	function per_notonreportsheading() {return 'Not On Reports';}
	function per_nomove() {return 'No Move';}
	function per_none() {return 'There are no persons for this location.';}
	
	//licenses
	function lic_heading() {return 'Licenses';}
	function lic_newheading() {return 'New License';}
	function lic_success_new() {return 'You have successfully added a new license.';}
	function lic_editheading() {return 'Edit License';}
	function lic_success_edit() {return 'Your license has been successfully updated.';}
	function lic_invalid_edit() {return 'You must use valid data to update licenses.';}
	function lic_deleteheading() {return 'Delete License';}
	function lic_success_delete() {return 'Your license has been successfully deleted.';}
	function lic_confirm_delete() {return 'Are you sure you want to delete this license?';}
	function lic_person_firstname() {return 'First Name';}
	function lic_person_lastname() {return 'Last Name';}
	function lic_person_first() {return 'First';}
	function lic_person_last() {return 'Last';}
	function lic_person_employeeid() {return 'Employee ID';}
	function lic_person_email() {return 'Email';}
	function lic_person_address() {return 'Address';}
	function lic_person_phone() {return 'Phone';}
	function lic_person_notes() {return 'Notes';}
	function lic_license() {return 'License';}
	function lic_licensename() {return 'License Name';}
	function lic_status() {return 'Status';}
	function lic_pickstatus($status) {
		switch($status)
		{
			case 'inactive':
				return 'Inactive';
			case 'active':
				return 'Active';
			default:
				return $status;
		}
	}
	function lic_agency() {return 'Agency';}
	function lic_number() {return 'License Number';}
	function lic_dateissued() {return 'Orginal Date Issued';}
	function lic_renewaldate() {return 'Last Renewal Date';}
	function lic_expiration() {return 'Next Expiration';}
	function lic_upload() {return 'Upload';}
	function lic_companyunitsneeded() {return 'Company Units Needed';}
	function lic_notes() {return 'Notes';}
	function lic_header_ceus() {return 'Credits (CEUs)';}
	function lic_ceu_classes() {return 'CEU Classes';}
	function lic_ceu_class() {return 'Class';}
	function lic_ceu_completiondate() {return 'Completion Date';}
	function lic_ceu_expiration() {return 'Expiration';}
	function lic_ceu_ceu() {return 'CEU';}
	function lic_ceu_code() {return 'Code';}
	function lic_ceu_safety() {return 'Safety';}
	function lic_ceu_discipline() {return 'Discipline';}
	function lic_ceu_picker($ceu) {
		switch($ceu) {
			case 'ceu':
				return 'CEU';
			case 'code':
				return 'Code';
			case 'safety':
				return 'Safety';
			case 'discipline':
				return 'Discipline';
			default:
				return $ceu;
		}
	}
	function lic_ceu_neededpicker($ceu) {
		switch($ceu) {
			case 'ceu':
				return 'CEU Needed';
			case 'code':
				return 'Code Needed';
			case 'safety':
				return 'Safety Needed';
			case 'discipline':
				return 'Discipline Needed';
			default:
				return $ceu;
		}
	}
	function lic_person() {return 'Person';}
	function lic_notonreportsheading() {return 'Not On Reports';}
	function lic_licenseonreports() {return 'This person currently shows up on reports';}
	function lic_licensenotonreports() {return 'This person currently does <b><i>not</i></b> shows up on reports';}
	function lic_none() {return 'There are currently no licenses for this location.';}
	
	//aed
	function aed_heading() {return 'AED';}
	function aed_newaedheading() {return 'New AED';}
	function aed_newaed() {return 'New AED';}
	function aed_success_newaed() {return 'Your AED has been successfully added.';}
	function aed_deleteaedheading() {return 'Delete AED';}
	function aed_confirm_delete() {return 'Are you sure you want to delete this AED?';}
	function aed_success_deleteaedheading() {return 'Your AED has been deleted.';}
	function aed_invalid_deleteaedheading() {return 'Your AED could not be deleted.';}
	function aed_editaedheading() {return 'Edit AED';}
	function aed_success_editaedheading() {return 'Your AED has successfully been edited.';}	
	function aed_invalid_editaedheading() {return 'Your AED could not be edited.';}	
	function aed_unlockaed_heading() {return 'Unlock AED';}
	function aed_success_unlockaed() {return 'Your AED has successfully been unlocked.';}	
	function aed_invalid_unlockaed() {return 'Your AED could not be unlocked, please try again or contact support at '.SUPPORTEMAIL;}	
	function aed_aedbrand() {return 'AED Brand';}
	function aed_aedmodel() {return 'AED Model';}
	function aed_mustenterkeycode() {return 'You must enter a keycode to add an AED.';}
	function aed_brand() {return 'Brand';}
	function aed_model() {return 'Model';}
	function aed_serial() {return 'Serial Number';}
	function aed_purchasetype() {return 'Purchase Type';}
	function aed_pickpurchasetype($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'own':
				return 'Own';
			case 'lease':
				return 'Lease';
			case 'rent':
				return 'Rent';
			default: return 'ERROR UNKNOWN TYPE:'.$type;
		}
	}
	function aed_purchasedate() {return 'Purchase Date';}
	function aed_warranty() {return 'Warranty';}
	function aed_location() {return 'Location';}
	function aed_installdate() {return 'Install Date';}
	function aed_sitecoordinator() {return 'Site Coordinator';}
	function aed_plantype() {return 'Plan Type';}
	function aed_pickplantype($type) {
		if($type == '' || $type == null)
			return 'None';
		switch($type) {
			case 'none':
				return 'None';
			case 'programmanagement':
				return 'Program Management';
			case 'medicaldirection':
				return 'Medical Direction';
			case 'programmanagement1':
				return 'Program Management-1yr';
			case 'programmanagement3':
				return 'Program Management-3yr';
			case 'programmanagement4':
				return 'Program Management-4yr';
			case 'programmanagement5':
				return 'Program Management-5yr';
			case 'renta':
				return 'Rental-Annual';
			case 'rents':
				return 'Rental-Seasonal';
			case 'rentm':
				return 'Rental-Monthly';
			case 'rentq':
				return 'Rental-Quarterly';
			case 'aedpmbasic1':
				return 'AEDPM Basic - 1';
			case 'aedpmstandard1':
				return 'AEDPM Standard - 1';
			case 'aedpmpremium1':
				return 'AEDPM Premium - 1';
			case 'aedcoord':
				return 'AED Coordinator Services';
			default: return 'ERROR UNKNOWN TYPE:'.$type;
		}
	}
	function aed_servicecheck() {return 'Servicing Check';}
	function aed_pickservicechecklength($days) {
		if($days == '' || $days == null)
			return 'None';
		switch($days) {
			case '0':
				return 'None';
			case '30':
				return 'Monthly';
			case '91':
				return 'Quarterly';
			case '182':
				return 'Semi Annually';
			case '365':
				return 'Yearly';
			default: return 'ERROR UNKNOWN DAYS:'.$days;
		}
	}
	function aed_plandate() {return 'Plan Date';}
	function aed_planexpiration() {return 'Plan Expiration';}
	function aed_hasannualcheck() {return 'Has Annual Check';}
	function aed_lastcheck() {return 'Last Check';}
	function aed_lastservice() {return 'Last Service';}
	function aed_notes() {return 'Notes';}
	function aed_unlockaed_keycode() {return 'Keycode';}
	function aed_unlockaed_days() {return 'Days';}
	function aed_outofservice() {return 'Out of Service';}
	function aed_inservice() {return 'In Service';}
	function aed_checkbox_outofservice() {return 'This AED is currently out of service.';}
	function aed_nocheckinstructions() {return 'Do not show this message to me again.';}
	function aed_unlockaed_none() {return 'There are no more valid keycodes for your organization, please contact support at '.SUPPORTEMAIL;}
	function aed_none() {return 'There are currently no AEDs for this location.';}
	function aed_moveaeds() {return 'Move AEDs';}
	function aed_attachaeds() {return 'Attach AEDs';}
	function aed_dontattach() {return 'Don\'t Attach';}
	function aed_success_move() {return 'Successfully Moved AEDs!';}
	function aed_success_attach() {return 'Successfully Attached AEDs!';}
	function aed_fail_attach_pad($typename, $model) {return 'Can not attach PAD: '.$typename.', on AED: '.$model.'. No available slots.';}
	function aed_fail_attach_pak($typename, $model) {return 'Can not attach PAK: '.$typename.', on AED: '.$model.'. No available slots.';}
	function aed_fail_attach_battery($typename, $model) {return 'Can not attach Battery: '.$typename.', on AED: '.$model.'. No available slots.';}
	function aed_fail_attach_accessory($typename, $model) {return 'Can not attach Accessory: '.$typename.', on AED: '.$model.'. No available slots.';}
	function aed_invalid_attach_pad($typename, $model) {return 'Can not attach PAD: '.$typename.', on AED: '.$model.'. Not Compatible.';}
	function aed_invalid_attach_pak($typename, $model) {return 'Can not attach PAK: '.$typename.', on AED: '.$model.'. Not Compatible.';}
	function aed_invalid_attach_battery($typename, $model) {return 'Can not attach Battery: '.$typename.', on AED: '.$model.'. Not Compatible.';}
	function aed_invalid_attach_accessory($typename, $model) {return 'Can not attach Accessory: '.$typename.', on AED: '.$model.'. Not Compatible.';}
	
	//accessories
	function aed_accessories() {return 'Accessories';}
	function aed_newaccessory() {return 'New Accessory';}
	function aed_success_newaccessory() {return 'Your new accessory has successfully been added.';}
	function aed_confirm_deleteaccessory() {return 'Are you sure you want to delete this accessory?';}
	function aed_success_deleteaccessory() {return 'Your accessory has been deleted.';}
	function aed_invalid_deleteaccessory() {return 'Your accessory could not be deleted.';}
	function aed_editaccessory() {return 'Edit Accessory';}
	function aed_success_editaccessory() {return 'Your accessory has been successfully changed.';}
	function aed_invalid_editaccessory() {return 'Your accessory could not be changed.';}
	function aed_spare() {return 'Spare';}
	function aed_spares() {return 'Spares';}
	function aed_pad() {return 'Pad';}
	function aed_spare_pad_1() {return 'Spare Pad 1';}
	function aed_spare_pad_2() {return 'Spare Pad 2';}
	function aed_pediatric_pad() {return 'Pediatric Pad';}
	function aed_spare_pediatric_pad() {return 'Spare Pediatric Pad';}
	function aed_pak() {return 'Pak';}
	function aed_spare_pak_1() {return 'Spare Pak 1';}
	function aed_spare_pak_2() {return 'Spare Pak 2';}
	function aed_pediatric_pak() {return 'Pediatric Pak';}
	function aed_spare_pediatric_pak() {return 'Spare Pediatric Pak';}
	function aed_battery() {return 'Battery';}
	function aed_spare_battery() {return 'Spare Battery';}
	function aed_accessory() {return 'Accessory';}
	function aed_spare_accessory() {return 'Spare Accessory';}
	function aed_pads() {return 'Pads';}
	function aed_paks() {return 'Paks';}
	function aed_batteries() {return 'Batteries';}
	function aed_accessoryname() {return 'Name';}
	function aed_accessorytype() {return 'Type';}
	function aed_part_number() {return 'Part Number';}
	function aed_accessorylot() {return 'Lot Number / UDI';}
	function aed_accessoryexp() {return 'Expiration';}
	function aed_accessorynone() {return 'There are currently no accessories for this aed.';}
	
	//keycode input
	function rqk_heading() {return 'Input Keycode';}
	function rqk_invalid_entry() {return 'Invalid keycode. Please enter a new keycode below.';}
	function rqk_aedlocked() {return 'This AED has been locked down because of an expired keycode. Please enter a new keycode below.';}
	
	//users
	function use_heading() {return 'Manage Users';}
	function use_newprofileheading() {return 'New User Profile';}
	function use_newrightsheading() {return 'New User Rights';}
	function use_newprivsheading() {return 'New User Privileges';}
	function use_editprofileheading() {return 'Edit User Profile';}
	function use_editrightsheading() {return 'Edit User Rights';}
	function use_editprivsheading() {return 'Edit User Privileges';}
	function use_success_edituser() {return 'The user has been successfully changed.';}
	function use_success_deleteuser() {return 'The user has been successfully deleted.';}
	function but_newuser() {return 'New User';}
	function but_edituser() {return 'Edit User';}
	function but_deleteuser() {return 'Delete User';}
	function use_invalid_uniqueusername() {return 'That username has already been used.';}
	function use_invalid_emptyusername() {return 'You must enter a valid email/username for the new user.';}
	function use_profile() {return 'Profile';}
	function use_generalinfo() {return 'General Information';}
	function use_contactinfo() {return 'Contact Information';}
	function use_rights() {return 'Locations';}
	function use_privs() {return 'Privileges';}
	function use_resetpassword() {return 'Reset Password';}
	function use_usernameemail() {return 'Username/Email';}
	function use_checkall() {return 'Check All';}
	function use_uncheckall() {return 'Uncheck All';}
	function use_quickselector() {return 'Quick Selector';}
	function use_qs_companyadmin() {return 'Company Admin';}
	function use_qs_user() {return 'User';}
	function use_qs_viewuser() {return 'View User';}
	function use_creationemail_subject() {return 'Welcome to '.PROGRAM;}
	function use_creationemail_body($username, $password="(not shown for privacy issues)") {
		return '
		<html><head></head><body style="color:#000001;">
			<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
				<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
					<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
						<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
					</h1>
					<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
					<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;"> 
						Welcome
					</h2>
					<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
						You have recently been invited to join <a href="'.URL.'">'.PROGRAM.'</a>.  This is a program that allows you 
						to manage your equipment such as AEDs and first aid kits.<br />
						<b>Logging into '.PROGRAM.' is simple!</b>
							<div style="margin-left:10px;font-weight:bold;">
							Username: '.$username.'<br />
							Password: '.$password.'
							</div>
						If you think you got this email by accident, please disregard.
						<br />
						If you have any questions, please feel free to contact us at <b>'.SUPPORTEMAIL.'</b>.
					</div>
				</div>
			</div>
			<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">'.$this->mes_pleasedonotreply().'</div>
			<br />
			<div style="width:970px;margin:0 auto;color:#000001;">'.$this->mes_tscopyright().'</div>
		</body></html>';
	}
	function use_creationemail_addbody($username, $password="(not shown for privacy issues)") {
		return 'You have recently been invited to join '.PROGRAM.' - '.URL.'.  
This is a program that allows you to manage your equipment such as AEDs and first aid kits.
Logging into '.PROGRAM.' is simple!
	Username: '.$username.'
	Password: '.$password.'
If you have anymore questions, contact our technical support at '.SUPPORTEMAIL.'.
If you think you got this email by accident, please disregard.';
	}
	function use_profilechangeemail_subject() {return PROGRAM.' Profile Change';}
	function use_profilechangeemail_body($username, $password="(not shown for privacy issues)") {
		return '
		<html><head></head><body style="color:#000001;">
			<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
				<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
					<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
						<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
					</h1>
					<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
					<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;"> 
						Profile Change
					</h2>
					<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
						You have recently had your profile changed at <a href="'.URL.'">'.PROGRAM.'</a>.  This is a program that allows you 
						to manage your equipment such as AEDs and first aid kits.<br />
						<b>Your new login information:</b>
							<div style="margin-left:10px;font-weight:bold;">
							Username: '.$username.'<br />
							Password: '.$password.'
							</div>
						If you think you got this email by accident, please disregard.
						<br />
						If you have any questions, please feel free to contact us at <b>'.SUPPORTEMAIL.'</b>.
					</div>
				</div>
			</div>
			<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
				Please do not reply to this email.  We are unable to respond to inquiries sent to this address.
			</div>
			<br />
			<div style="width:970px;margin:0 auto;color:#000001;">
				Copyright &copy;  2013 Think Safe Inc.
			</div>
		</body></html>';
	}
	function use_profilechangeemail_addbody($username, $password="(not shown for privacy issues)") {
		return 'You have recently had your profile changed at '.PROGRAM.' - '.URL.'.  
This is a program that allows you to manage your equipment such as AEDs and first aid kits.
Logging into '.PROGRAM.' is simple!
	Username: '.$username.'
	Password: '.$password.'
If you have anymore questions, contact our technical support at '.SUPPORTEMAIL.'.
If you think you got this email by accident, please disregard.';
	}
	function use_getpriv() {
		return $privlist = array(
			'showaed' => 'Show AED Tab',
			'newaed' => 'New AED',
			'editaed' => 'Edit AED',
			'deleteaed' => 'Delete AED',
			'newspare' => 'New Spare',
			'editspare' => 'Edit Spare',
			'deletespare' => 'Delete Spare',
			'showerp' => 'Show ERP',
			'newerp' => 'New ERP',
			'deleteerp' => 'Delete ERP',
			'showmasteraed' => 'Show Master AED',
			'showmedicaldirection' => 'Show Medical Direction',
			'newmedicaldirection' => 'New Medical Director',
			'editmedicaldirection' => 'Edit Medical Director',
			'deletemedicaldirection' => 'Delete Medical Director',
			'showevents' => 'Show Events',
			'newevents' => 'New Event',
			'deleteevents' => 'Delete Event',
			'showrecalls' => 'Show Recalls',
			'newrecalls' => 'New Recall',
			'editrecalls' => 'Edit Recall',
			'deleterecalls' => 'Delete Recall',
			'showstatelaws' => 'Show State Laws',
			'showequipment' => 'Show Equipment Tab',
			'newequipment' => 'New Equipment',
			'editequipment' => 'Edit Equipment',
			'deleteequipment' => 'Delete Equipment',
			'newequipmentcheck' => 'New Equipment Check',
			'editequipmentcheck' => 'Edit Equipment Check',
			'showpersons' => 'Show Persons Tab',
			'newpersons' => 'New Person',
			'editpersons' => 'Edit Person',
			'deletepersons' => 'Delete Person',
			'movepersons' => 'Move Person',
			'showtraining' => 'Show Training Tab',
			'newtraining' => 'New Training',
			'edittraining' => 'Edit Training',
			'deletetraining' => 'Delete Training',
			'printcards' => 'Print Cards',
			'showdocuments' => 'Show Documents Tab',
			'newdocuments' => 'New Document',
			'editdocuments' => 'Edit Document',
			'deletedocuments' => 'Delete Document',
			'showlicenses' => 'Show Licenses Tab',
			'newlicenses' => 'New License',
			'editlicenses' => 'Edit License',
			'deletelicenses' => 'Delete License',
			'showimmunizations' => 'Show Immunizations Tab',
			'newimmunizations' => 'New Immunization',
			'editimmunizations' => 'Edit Immunization',
			'deleteimmunizations' => 'Delete Immunization',
			'showhazard' => 'Show Hazards Tab',
			'newhazard' => 'New Hazard',
			'edithazard' => 'Edit Hazard',
			'deletehazard' => 'Delete Hazard',
			'showcontact' => 'Show Contacts Tab',
			'newcontact' => 'New Contact',
			'editcontact' => 'Edit Contact',
			'deletecontact' => 'Delete Contact',
			'showaedcheck' => 'Show AED Check Tab',
			'newaedcheck' => 'New AED Check',
			'editaedcheck' => 'Edit AED Check Comments',
			'groupaedcheck' => 'Group AED Check',
			'showaedservicing' => 'Show AED Servicing Tab',
			'newaedservicing' => 'New AED Servicing',
			'editaedservicing' => 'Edit AED Servicing Comments',
			'showfirstvoice' => 'Show First Voice Equipment Tab',
			'newfirstvoice' => 'New First Voice Equipment',
			'editfirstvoice' => 'Edit First Voice Equipment',
			'deletefirstvoice' => 'Delete First Voice Equipment',
			'newfirstvoicecheck' => 'New First Voice Equipment Check',
			'editfirstvoicecheck' => 'Edit First Voice Equipment Check',
			'showadmin' => 'Show Admin Section',
			'showuser' => 'Show Manage Users Tab',
			'newuser' => 'New User',
			'edituser' => 'Edit User',
			'deleteuser' => 'Delete User',
			'showorganization' => 'Show Organization Tab',
			'neworganization' => 'New Organization',
			'editorganization' => 'Edit Organization',
			'deleteorganization' => 'Delete Organization',
			'newlocation' => 'New Location',
			'editlocation' => 'Edit Location',
			'deletelocation' => 'Delete Location',
			'showkeycode' => 'Show Keycode Tab',
			'generatekeycode' => 'Generate Keycodes',
			'showkeycodepool' => 'Show Keycode Pool',
			'assignkeycodepool' => 'Assign Keycode Pool',
			'deletekeycodepool' => 'Delete from Keycode Pool',
			'newkeycode' => 'New Keycode',
			'showalert' => 'Show Alerts Tab',
			'newalert' => 'New Alert',
			'editalert' => 'Edit Alert',
			'deletealert' => 'Delete Alert',
			'showtracing' => 'Show Tracing Tab',
			'newtracing' => 'New Tracing',
			'edittracing' => 'Edit Tracing',
			'deletetracing' => 'Delete Tracing',
			'assigntracing' => 'Assign Tracing',
			'showsetup' => 'Show Setup Tab',
			'showsetuphazards' => 'Show Hazards Setup Tab',
			'newhazardtype' => 'New Hazard Types',
			'edithazardtype' => 'Edit Hazard Types',
			'deletehazardtype' => 'Delete Hazard Types',
			'newhazardalltype' => 'New Default Hazard Types',
			'edithazardalltype' => 'Edit Default Hazard Types',
			'deletehazardalltype' => 'Delete Default Hazard Types',
			'showsetupdocuments' => 'Show Documents Setup Tab',
			'newdocumenttype' => 'New Document Types',
			'editdocumenttype' => 'Edit Document Types',
			'deletedocumenttype' => 'Delete Document Types',
			'newdocumentalltype' => 'New Default Document Types',
			'editdocumentalltype' => 'Edit Default Document Types',
			'deletedocumentalltype' => 'Delete Default Document Types',
			'showsetupimmunizations' => 'Show Immmunizations Setup Tab',
			'newimmunizationtype' => 'New Immunization Types',
			'editimmunizationtype' => 'Edit Immunization Types',
			'deleteimmunizationtype' => 'Delete Immunization Types',
			'newimmunizationalltype' => 'New Default Immunization Types',
			'editimmunizationalltype' => 'Edit Default Immunization Types',
			'deleteimmunizationalltype' => 'Delete Default Immunization Types',
			'showsetuplicenses' => 'Show Licenses Setup Tab',
			'newlicensetype' => 'New License Types',
			'editlicensetype' => 'Edit License Types',
			'deletelicensetype' => 'Delete License Types',
			'newlicensealltype' => 'New Default License Types',
			'editlicensealltype' => 'Edit Default License Types',
			'deletelicensealltype' => 'Delete Default License Types',
			'showsetuptraining' => 'Show Training Setup Tab',
			'newtrainingcourse' => 'New Training Courses',
			'edittrainingcourse' => 'Edit Training Courses',
			'deletetrainingcourse' => 'Delete Training Courses',
			'newtrainingallcourse' => 'New Default Training Courses',
			'edittrainingallcourse' => 'Edit Default Training Courses',
			'deletetrainingallcourse' => 'Delete Default Training Courses',
			'showtrainingprints' => 'Show Training Prints',
			'edittrainingprints' => 'Edit Training Prints',
			'showsetupequipment' => 'Show Equipment Setup Tab',
			'newequipmenttypes' => 'New Equipment Categories',
			'editequipmenttypes' => 'Edit Equipment Categories',
			'deleteequipmenttypes' => 'Delete Equipment Categories',
			'newequipmentstyles' => 'New Equipment Subcategories',
			'editequipmentstyles' => 'Edit Equipment Subcategories',
			'deleteequipmentstyles' => 'Delete Equipment Subcategories',
			'newequipmentalltypes' => 'New Default Equipment Categories',
			'editequipmentalltypes' => 'Edit Default Equipment Categories',
			'deleteequipmentalltypes' => 'Delete Default Equipment Categories',
			'newequipmentallstyles' => 'New Default Equipment Subcategories',
			'editequipmentallstyles' => 'Edit Default Equipment Subcategories',
			'deleteequipmentallstyles' => 'Delete Default Equipment Subcategories',
			'showsetupfirstvoice' => 'Show Frist Voice Setup Tab',
			'newfirstvoicetypes' => 'New First Voice Equipment Categories',
			'editfirstvoicetypes' => 'Edit First Voice Equipment Categories',
			'deletefirstvoicetypes' => 'Delete First Voice Equipment Categories',
			'newfirstvoicestyles' => 'New First Voice Equipment Subcategories',
			'editfirstvoicestyles' => 'Edit First Voice Equipment Subcategories',
			'deletefirstvoicestyles' => 'Delete First Voice Equipment Subcategories',
			'newfirstvoicealltypes' => 'New Default First Voice Equipment Categories',
			'editfirstvoicealltypes' => 'Edit Default First Voice Equipment Categories',
			'deletefirstvoicealltypes' => 'Delete Default First Voice Equipment Categories',
			'newfirstvoiceallstyles' => 'New Default First Voice Equipment Subcategories',
			'editfirstvoiceallstyles' => 'Edit Default First Voice Equipment Subcategories',
			'deletefirstvoiceallstyles' => 'Delete Default First Voice Equipment Subcategories',
			'showscheduler' => 'Show Scheduler Tab',
			'newscheduler' => 'New Scheduled Events',
			'editscheduler' => 'Edit Scheduled Events',
			'deletescheduler' => 'Delete Scheduled Events'
		);
	}

	//aedcheck
	function ack_heading() {return 'AED Check';}
	function ack_groupheading() {return 'Group AED Check';}
	function ack_alertsubject() {return 'ALERT! Recent AED Check Completed - Attention Needed';}
	function ack_subject() {return 'AED Check Completed';}
	function ack_emailheading() {return 'AED Check Confirmation';}
	function ack_emailtopbody($name, $serial, $location, $organization) {return $name.',<br />
										The following AED '.$serial.' at '.$location.', '.$organization.' has been inspected.';}
	function ack_coordemailtopbody($name, $serial, $location, $organization) {return $name.',<br />
										The following AED '.$serial.' at '.$location.', '.$organization.' has been recently inspected.';}
	function ack_newheading() {return 'New AED Check';}
	function ack_success_new() {return 'Your aed check has been successfully recorded.';}
	function ack_success_edit() {return 'Your AED check comment has been successfully edited.';}
	function ack_invalid_edit() {return 'Your AED check comment could not be edited.';}
	function ack_aed() {return 'AED';}
	function ack_lastcheck() {return 'Last Check';}
	function ack_pastcheck() {return 'Past Check';}
	function ack_annualcheck() {return 'Annual Check';}
	function ack_lastcheckers() {return 'Last Checker(s)';}
	function ack_generalinfo() {return 'General Information';}
	function ack_date() {return 'Date';}
	function ack_organization() {return 'Organization';}
	function ack_location() {return 'Location';}
	function ack_placement() {return 'Placement';}
	function ack_serial() {return 'Serial Number';}
	function ack_isaedlocation() {return 'Is the AED unit located in the designated area?';}
	function ack_rep_aedlocation() {return 'Designated Area';}
	function ack_isaedclean() {return 'Is the AED unit cleaned and undamaged?';}
	function ack_rep_aedclean() {return 'Cleaned and Undamaged';}
	function ack_isaedinalarm() {return 'Is the AED in an alarmed cabinet?';}
	function ack_rep_aedinalarm() {return 'In Alarmed Cabinet';}
	function ack_isalarmworking() {return 'Does the alarm on the AED Cabinet work?';}
	function ack_rep_aedalarmworks() {return 'Alarm Works';}
	function ack_replacingalarmbattery() {return 'Did you replace the alarm batteries?';}
	function ack_rep_aedreplacingalarmbatteries() {return 'Replacing Alarm Batteries';}
	function ack_alarmbatteryinsertiondate() {return 'Insertion Date';}
	function ack_alarmbatterychangedate() {return 'Change Date';}
	function ack_alarmbatteryexpirationdate() {return 'Expiration Date';}
	function ack_kitpresent() {return 'Is there an AED rescue kit (razors/scissors kit) present?';}
	function ack_rep_aedrescuekitpresent() {return 'Rescue Kit Present';}
	function ack_kitsealed() {return 'Is the rescue kit sealed and undamaged?';}
	function ack_rep_aedrescuekitsealed() {return 'Rescue Kit Sealed';}
	function ack_kitinclude() {return 'Does your rescue kit include these items?';}
	function ack_gloves() {return 'Gloves';}
	function ack_razor() {return 'Prep Razor';}
	function ack_scissors() {return 'Scissors';}
	function ack_towel() {return 'Towel';}
	function ack_towlette() {return 'Moist Towlette';}
	function ack_mask() {return 'CPR Mask';}
	function ack_pads() {return 'Pads';}
	function ack_paks() {return 'Paks';}
	function ack_accessories() {return 'Accessories';}
	function ack_batteries() {return 'Batteries';}
	function ack_type() {return 'Type';}
	function ack_lot() {return 'Lot Number / UDI';}
	function ack_expiration() {return 'Expiration';}
	function ack_ispadattechedtoaed($exp, $lot) {return 'Are these electrodes(pads) attached to the unit? <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ack_isinstallingnewpads() {return 'Are you installing new electrodes(pads)?';}
	function ack_isinstallingnewpedpads() {return 'Are you installing new pediatric electrodes(pads)?';}
	function ack_isinstallingnewsparepads() {return 'Are you installing new spare electrodes(pads)?';}
	function ack_isinstallingnewsparepedpads() {return 'Are you installing new spare pediatric electrodes(pads)?';}
	function ack_whichpadsinstalling() {return 'Which electrodes(pads) are you installing?';}
	function ack_ispadready() {return 'After installing new electrodes(pads), did you turn on the unit to allow it to complete a full self-test to confirm a visibile light or audible indication of ready status?';}
	function ack_belowpadspresent() {return 'Are the below(if any) electrodes(pads) present?';}
	function ack_ispadsealed() {return 'Are the electrode(pads) packages sealed and within the expiry date?';}
	function ack_isbatteryattechedtoaed($exp, $lot) {return 'Is this battery attached to the unit? <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ack_isinstallingnewbattery() {return 'Are you installing a new battery?';}
	function ack_isinstallingnewsparebattery() {return 'Are you installing a new spare battery?';}
	function ack_isinstallingnewaccessory() {return 'Are you installing a new accessory?';}
	function ack_isinstallingnewspareaccessory() {return 'Are you installing a new spare accessory?';}
	function ack_whichbatteryinstalling() {return 'Which battery are you installing?';}
	function ack_isbatteryready() {return 'After installing a new battery, did you turn on the unit to allow it to complete a full self-test to confirm a visibile light or audible indication of ready status?';}
	function ack_belowbatteriespresent() {return 'Are the below(if any) batteries present?';}
	function ack_isbatteriesealed() {return 'Is the battery package sealed and within the expiry date?';}
	function ack_ispakattechedtoaed($exp, $lot) {return 'Is this pak/accessory attached to the unit?  <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ack_isinstallingnewpaks() {return 'Are you installing a new pak/accessory?';}
	function ack_isinstallingnewsparepaks() {return 'Are you installing a new spare pak?';}
	function ack_isinstallingnewpedpaks() {return 'Are you installing a new pediatric pak?';}
	function ack_isinstallingnewsparepedpaks() {return 'Are you installing a new spare pediatric pak?';}
	function ack_whichpaksinstalling() {return 'Which pak/accessory are you installing?';}
	function ack_ispakready() {return 'After installing the new pak or accessory, did you turn on the unit to allow it to complete a full self-test to confirm a visibile light or audible indication of ready status?';}
	function ack_belowpakspresent() {return 'Are the below(if any) paks or accessories present?';}
	function ack_ispaksealed() {return 'Is the pak package sealed and within the expiry date?';}
	function ack_isbatterysealed() {return 'Is the battery sealed and within the expiry date?';}
	function ack_selectedtype($lot, $exp) {return 'Lot:'.$lot.' Exp:'.$exp;}
	function ack_hasindicator() {return 'Is the Status Indicator showing the AED in a ready status?';}
	function ack_rep_aedreadtstatus() {return 'Ready Status';}
	function ack_checkerpromise() {return 'By entering your name you are certifying that the above check was done and that you reported the actual condition of the AED unit.';}
	function ack_aedready() {return 'AED Ready';}
	function ack_checkedby() {return 'Checked By';}
	function ack_fullname() {return 'Full Name';}
	function ack_email() {return 'Email';}
	function ack_confirm_comments() {return 'Adding additional comments is all you are allowed to do. You cannot alter another person\'s comments.  All comments are permament so double check your work before you post.';}
	function ack_comments() {return 'Comments';}
	function ack_question() {return 'Question';}
	function ack_responses($resp) {
		switch($resp)
		{
			case 'Yes':
				return 'Yes';
				break;
			case 'yes':
				return 'yes';
				break;
			case 'No':
				return 'No';
				break;
			case 'no':
				return 'no';
				break;
			case 'Other':
				return 'Other';
				break;
			case 'other':
				return 'other';
				break;
		}
		return $resp;
	}
	function ack_nocheck() {return 'There is no check for this AED yet.';}
	function ack_nosecondcheck() {return 'There is no second check for this AED yet.';}
	function ack_ispadattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ack_issparepadattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ack_ispakattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ack_issparepakattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ack_isbatteryattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ack_issparebatteryattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ack_isaccessoryattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ack_isspareaccessoryattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	
	//aedservicing
	function ask_heading() {return 'AED Servicing';}
	function ask_alertsubject() {return 'Attention: Recent AED Servicing Completed';}
	function ask_subject() {return 'AED Servicing Completed';}
	function ask_emailheading() {return 'AED Servicing Confirmation';}
	function ask_emailtopbody($name, $serial, $location, $organization) {return $name.',<br />
										The following AED '.$serial.' at '.$location.', '.$organization.' has been recently inspected.';}
	function ask_newheading() {return 'New AED Servicing';}
	function ask_success_new() {return 'Your aed servicing has been successfully recorded.';}
	function ask_success_edit() {return 'Your AED servicing comment has been successfully edited.';}
	function ask_invalid_edit() {return 'Your AED servicing comment could not be edited.';}
	function ask_aed() {return 'AED';}
	function ask_lastservicing() {return 'Last Servicing';}
	function ask_pastservicing() {return 'Past Servicing';}
	function ask_annualservicing() {return 'Annual Servicing';}
	function ask_lastservicingers() {return 'Last Servicinger(s)';}
	function ask_generalinfo() {return 'General Information';}
	function ask_date() {return 'Date';}
	function ask_organization() {return 'Organization';}
	function ask_location() {return 'Location';}
	function ask_placement() {return 'Placement';}
	function ask_serial() {return 'Serial Number';}
	function ask_milesdriven() {return 'Mileage Driven for Visit';}
	function ask_programmanagementexp() {return 'Program Management Expiration';}
	function ask_isaedlocation() {return 'Is the AED unit located in the designated area?';}
	function ask_rep_aedlocation() {return 'Designated Area';}
	function ask_isaedclean() {return 'Is the AED unit cleaned and undamaged?';}
	function ask_rep_aedclean() {return 'Cleaned and Undamaged';}
	function ask_isaedinalarm() {return 'Is the AED in an alarmed cabinet?';}
	function ask_rep_aedinalarm() {return 'In Alarmed Cabinet';}
	function ask_isalarmworking() {return 'Does the alarm on the AED Cabinet work?';}
	function ask_rep_aedalarmworks() {return 'Alarm Works';}
	function ask_replacingalarmbattery() {return 'Did you replace the alarm batteries?';}
	function ask_rep_aedreplacingalarmbatteries() {return 'Replacing Alarm Batteries';}
	function ask_alarmbatteryinsertiondate() {return 'Insertion Date';}
	function ask_alarmbatterychangedate() {return 'Change Date';}
	function ask_alarmbatteryexpirationdate() {return 'Expiration Date';}
	function ask_kitpresent() {return 'Is there an AED rescue kit (razors / scissors) present?';}
	function ask_rep_aedrescuekitpresent() {return 'Rescue Kit Present';}
	function ask_kitsealed() {return 'Is the rescue kit sealed and undamaged?';}
	function ask_rep_aedrescuekitsealed() {return 'Rescue Kit Sealed';}
	function ask_kitinclude() {return 'Does your rescue kit include: gloves, prep razor, scissors, towels/towelettes, CPR Mask?';}
	function ask_kitincludereplace() {return 'What did you replace or add to the rescue kit?';}
	function ask_pads() {return 'Pads';}
	function ask_paks() {return 'Paks & Accessories';}
	function ask_batteries() {return 'Batteries';}
	function ask_type() {return 'Type';}
	function ask_lot() {return 'Lot Number / UDI';}
	function ask_expiration() {return 'Expiration';}
	function ask_ispadattechedtoaed($exp, $lot) {return 'Are these electrodes(pads) attached to the unit? <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ask_isinstallingnewpads() {return 'Are you installing new electrodes(pads)?';}
	function ask_isinstallingnewpedpads() {return 'Are you installing new pediatric electrodes(pads)?';}
	function ask_isinstallingnewsparepads() {return 'Are you installing new spare electrodes(pads)?';}
	function ask_isinstallingnewsparepedpads() {return 'Are you installing new spare pediatric electrodes(pads)?';}
	function ask_whichpadsinstalling() {return 'Which electrodes(pads) are you installing?';}
	function ask_ispadready() {return 'After installing new electrodes(pads), did you turn on the unit to allow it to complete a full self-test to confirm a visibile light or audible indication of ready status?';}
	function ask_belowpadspresent() {return 'Are the below(if any) electrodes(pads) present?';}
	function ask_ispadsealed() {return 'Are the electrode(pads) packages sealed and within the expiry date?';}
	function ask_isbatteryattechedtoaed($exp, $lot) {return 'Is this battery attached to the unit? <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ask_isbatteryengerybars() {return 'How many bars are showing on the status indicator?';}
	function ask_isinstallingnewbattery() {return 'Are you installing a new battery?';}
	function ask_isinstallingnewsparebattery() {return 'Are you installing a new spare battery?';}
	function ask_isinstallingnewaccessory() {return 'Are you installing a new accessory?';}
	function ask_isinstallingnewspareaccessory() {return 'Are you installing a new spare accessory?';}
	function ask_whichbatteryinstalling() {return 'Which battery are you installing?';}
	function ask_isbatteryready() {return 'After installing a new battery, did you turn on the unit to allow it to complete a full self-test to confirm a visibile light or audible indication of ready status?';}
	function ask_belowbatteriespresent() {return 'Are the below(if any) batteries present?';}
	function ask_isbatteriesealed() {return 'Is the battery package sealed and within the expiry date?';}
	function ask_isbatterysealed() {return 'Is the battery sealed and within the expiry date?';}
	function ask_ispakattechedtoaed($exp, $lot) {return 'Is this pak/accessory attached to the unit?  <b>Expiration:</b>'.$exp.' <b>Lot:</b>'.$lot;}
	function ask_isinstallingnewpaks() {return 'Are you installing a new pak?';}
	function ask_isinstallingnewsparepaks() {return 'Are you installing a new spare pak?';}
	function ask_isinstallingnewpedpaks() {return 'Are you installing a new pediatric pak?';}
	function ask_isinstallingnewsparepedpaks() {return 'Are you installing a new spare pediatric pak?';}
	function ask_whichpaksinstalling() {return 'Which pak are you installing?';}
	function ask_ispakready() {return 'After installing the new pak or accessory, did you turn on the unit to allow it to complete a full self-test to confirm a visibile light or audible indication of ready status?';}
	function ask_belowpakspresent() {return 'Are the below(if any) paks or accessories present?';}
	function ask_ispaksealed() {return 'Is the pak package sealed and within the expiry date?';}
	function ask_selectedtype($lot, $exp) {return 'Lot:'.$lot.' Exp:'.$exp;}
	function ask_hasindicator() {return 'Is the Status Indicator showing the AED in a ready status?';}
	function ask_rep_aedreadtstatus() {return 'Ready Status';}
	function ask_servicerpromise() {return 'By entering your name you are certifying that the above servicing was done and that you reported the actual condition of the AED unit and any replacements or install of new pads or batteries or other accessories as noted above.';}
	function ask_aedready() {return 'AED Ready';}
	function ask_servicingby() {return 'Serviced By';}
	function ask_digitalsignature() {return 'Digital Signature of Technician';}
	function ask_dateverified() {return 'Date Verified';}
	function ask_servicerepverified() {return 'The following personnel at the client location verified that maintenance has been performed and supplies replaced as necessary in accordance with the argeement on file.  All information must be filled out below, to include email.';}
	function ask_clientrepname() {return 'Name of Client Representative';}
	function ask_clientreptitle() {return 'Title of Client Representative';}
	function ask_clientrepemail() {return 'Email of Client Representative';}
	function ask_clientrepphone() {return 'Phone of Client Representative';}
	function ask_dateverifiedclientlocation() {return 'Verification of Servicing Date at Client Location';}
	function ask_confirm_comments() {return 'Adding additional comments is all you are allowed to do. You cannot alter another person\'s comments.  All comments are permament so double servicing your work before you post.';}
	function ask_comments() {return 'Comments';}
	function ask_question() {return 'Question';}
	function ask_responses($resp) {
		switch($resp)
		{
			case 'Yes':
				return 'Yes';
				break;
			case 'yes':
				return 'yes';
				break;
			case 'No':
				return 'No';
				break;
			case 'no':
				return 'no';
				break;
			case 'Other':
				return 'Other';
				break;
			case 'other':
				return 'other';
				break;
		}
		return $resp;
	}
	function ask_noservicing() {return 'There is no servicing for this AED yet.';}
	function ask_nosecondservicing() {return 'There is no second servicing for this AED yet.';}
	function ask_ispadattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ask_issparepadattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ask_ispakattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ask_issparepakattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ask_isbatteryattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ask_issparebatteryattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}
	function ask_isaccessoryattached() {return 'Note the expiration date. Are they going to remain attached?';}
	function ask_isspareaccessoryattached() {return 'Note the expiration date on spares. Are they going to remain attached?';}

	//equipment
	function equ_heading() {return 'Equipment';}
	function equ_checkheading() {return 'EquipChk';}
	function equ_pastcheck() {return 'Past Check';}
	function equ_lastcheck() {return 'Last Check';}
	function equ_newequipment() {return 'New Equipment';}
	function equ_invalid_newequipment() {return 'Your equipment could not be added.  Please contact support at '.SUPPORTEMAIL;}
	function equ_success_newequipment() {return 'Your equipment was successfully added.';}
	function equ_newequipmentcheck() {return 'New Equipment Check';}
	function equ_success_newequipmentcheck() {return 'Your check was successfully added.';}
	function equ_deletequipment() {return 'Delete Equipment';}
	function equ_confirm_deletequipment() {return 'Are you sure you want to delete this equipment?';}
	function equ_success_deletequipment() {return 'Your equipment has been successfully deleted.';}
	function equ_editequipmentcheck() {return 'Edit Equipment Check';}
	function equ_success_editequipmentcheck() {return 'Your equipment check has successfully edited.';}
	function equ_editequipment() {return 'Edit Equipment';}
	function equ_success_editequipment() {return 'Your equipment was successfully updated.';}
	function but_newequipment() {return 'New Equipment';}
	function but_newequipmentcheck() {return 'New Check';}
	function equ_cat() {return 'Category';}
	function equ_subcat() {return 'Subcategory';}
	function equ_generalinfo() {return 'General Information';}
	function equ_location() {return 'Location';}
	function equ_placement() {return 'Placement';}
	function equ_brand() {return 'Brand';}
	function equ_model() {return 'Model';}
	function equ_modelyear() {return 'Model Year';}
	function equ_serialnumber() {return 'Serial Number';}
	function equ_lot() {return 'Lot Number';}
	function equ_date() {return 'Date';}
	function equ_checkon() {return 'Check On';}
	function equ_questions() {return 'Questions';}
	function equ_no() {return 'No';}
	function equ_yes() {return 'Yes';}
	function equ_compliance() {return 'Compliance';}
	function equ_complianceq1() {return 'Does this equipment need servicing prior to the next scheduled inspection?';}
	function equ_complianceq2() {return 'Is this equipment in compliance with outlined organization requirements?';}
	function equ_checkedby() {return 'Checked By';}
	function equ_checkedbypromise() {return 'By entering your name you are certifying that the above check was done and that you reported the actual condition of this equipment.';}
	function equ_checkedname() {return 'Full Name';}
	function equ_checkedemail() {return 'Email';}
	function equ_comments() {return 'Comments';}
	function equ_email_alertsubject() {return 'ALERT! Equipment Check Completed - Attention Needed';}
	function equ_email_subject() {return 'Equipment Check Completed';}
	function equ_email_heading() {return 'Equipment Check Confirmation';}
	function equ_email_bodyheading($name, $type, $style, $location, $organization) {
		return $name.',<br />
		The following '.$type.':'.$style.' at '.$location.', '.$organization.' has been inspected.<br />
		<br />';
	}
	function equ_nocheck() {return 'There is no check for this equipment yet.';}
	function equ_noequipment() {return 'There are currently no equipment for this location.';}
	
	//firstvoice
	function fir_heading() {return 'First Voice';}
	function fir_checkheading() {return 'FV Chk';}
	function fir_pastcheck() {return 'Past Check';}
	function fir_lastcheck() {return 'Last Check';}
	function fir_newequipment() {return 'New First Voice Equipment';}
	function fir_invalid_newequipment() {return 'Your First Voice equipment could not be added.  Please contact support at '.SUPPORTEMAIL;}
	function fir_success_newequipment() {return 'Your First Voice equipment was successfully added.';}
	function fir_newfirstvoicecheck() {return 'New First Voice Equipment Check';}
	function fir_success_newfirstvoicecheck() {return 'Your check was successfully added.';}
	function fir_deletequipment() {return 'Delete First Voice Equipment';}
	function fir_confirm_deletequipment() {return 'Are you sure you want to delete this First Voice equipment?';}
	function fir_success_deletequipment() {return 'Your First Voice equipment has been successfully deleted.';}
	function fir_editfirstvoicecheck() {return 'Edit First Voice Equipment Check';}
	function fir_success_editfirstvoicecheck() {return 'Your First Voice equipment check has successfully edited.';}
	function fir_editequipment() {return 'Edit First Voice Equipment';}
	function fir_success_editequipment() {return 'Your First Voice equipment was successfully updated.';}
	function but_newfirstvoice() {return 'New First Voice Equipment';}
	function but_newfirstvoicecheck() {return 'New Check';}
	function fir_cat() {return 'Category';}
	function fir_subcat() {return 'Subcategory';}
	function fir_generalinfo() {return 'General Information';}
	function fir_location() {return 'Location';}
	function fir_date() {return 'Date';}
	function fir_checkon() {return 'Check On';}
	function fir_questions() {return 'Questions';}
	function fir_no() {return 'No';}
	function fir_yes() {return 'Yes';}
	function fir_compliance() {return 'Compliance';}
	function fir_complianceq1() {return 'Does this First Voice equipment need servicing prior to the next scheduled inspection?';}
	function fir_complianceq2() {return 'Is this First Voice equipment in compliance with outlined organization requirements?';}
	function fir_checkedby() {return 'Checked By';}
	function fir_checkedbypromise() {return 'By entering your name you are certifying that the above check was done and that you reported the actual condition of this First Voice equipment.';}
	function fir_checkedname() {return 'Full Name';}
	function fir_checkedemail() {return 'Email';}
	function fir_comments() {return 'Comments';}
	function fir_email_alertsubject() {return 'ALERT! First Voice Equipment Check Completed - Attention Needed';}
	function fir_email_subject() {return 'First Voice Equipment Check Completed';}
	function fir_email_heading() {return 'First Voice Equipment Check Confirmation';}
	function fir_email_bodyheading($name, $type, $style, $location, $organization) {
		return $name.',<br />
		The following '.$type.':'.$style.' at '.$location.', '.$organization.' has been inspected.<br />
		<br />';
	}
	function fir_nocheck() {return 'There is no check for this First Voice equipment yet.';}
	function fir_noequipment() {return 'There are currently no First Voice equipment for this location.';}
	
	//lock
	function loc_invalidkeycode() {return 'Input Keycode';}
	function loc_tryagain() {return 'The keycode you entered was invalid, please try again.';}
	function loc_enterkeycode() {return 'Please enter a valid keycode below. Check your email records for a valid keycode. If you are an existing user your account has been locked due to an expired or missing keycode. Contact Technical Support at '.PHONE.' '.CONTACTTIME.' or email '.SUPPORTEMAIL.' .';}
	
	//home
	function hom_firstlogin() {
		return '--PLEASE CHANGE YOUR PASSWORD --<br />
			<br />
			Please be advised that for increased security and your account protection, it is highly recommended
			that you change the new system-generated password you were assigned to your own unique password.<br />
			<br />
			<form action="'.URL.'index.php?content=profile&action=edit" method="POST" style="width:145px">
				<input type="submit" class="button" value="RESET PASSWORD" />
			</form><br />
			<br />
			Questions? Email us at <a href="mailto:'.SUPPORTEMAIL.'">'.SUPPORTEMAIL.'</a> or call:<br />
			'.PHONE.' M-F 8am to 5pm CST<br />';
	}
	function hom_badbrowser() {
		return '--ATTENTION!  PLEASE CHANGE YOUR BROWSER --<br />
			<br />
			The program works best on Internet Explorer versions that are Version 9 or more recent.<br />  
			<br />
			Your browser has been identified as an older version of Internet Explorer.<br />
			<br />
			Please either upgrade your <a target="_blank" href="http://windows.microsoft.com/en-us/internet-explorer/products/ie/home">Internet Explorer Version</a> or switch internet browsers (<a target="_blank" href="https://www.google.com/intl/en/chrome/">Chrome</a>, <a target="_blank" href="http://www.mozilla.org/en-US/firefox/new/">Mozilla Firefox</a>, etc.) to ensure the program works properly.  Older versions of Internet Explorer will not allow full functionality of the program.<br />
			<br />
			If you can not download an updated Version or a different browser, contact your in-house IT or Technical Support Department for their assistance in updating your computer.  Please specify when talking with them the need for Version 8 or higher Internet Explorer or the downloading of any other browser as an alternative.<br />
			<br />
			Questions? Email us at <a href="mailto:'.SUPPORTEMAIL.'">'.SUPPORTEMAIL.'</a> or call:<br />
			'.PHONE.' M-F 8am to 5pm CST<br />
			<br />
			Thank you for your attention to this matter.<br />';
	}
	function hom_messagecenter() {return 'Message Center';}
	function hom_act_training() {return 'Training';}
	function hom_act_license() {return 'License';}
	function hom_act_immunization() {return 'Immunization';}
	function hom_act_student() {return 'Student';}
	function hom_act_class() {return 'Class';}
	function hom_act_expiration() {return 'Expiration';}
	function hom_actionitems() {return 'Action Items';}
	
	//state laws
	function sta_heading() {return 'State Laws';}
	function sta_state() {return 'State';}
	function sta_medicaldirection() {return 'Medical Direction';}
	function sta_training() {return 'Training Req.';}
	function sta_registration() {return 'Registration Req.';}
	function sta_moreinfo() {return 'More Information';}
	function sta_aedowners() {return 'AED Owners';}
	function sta_aedmandates() {return 'AED Mandates';}
	function sta_legend_owners() {return 'AED Owners: ';}
	function sta_legend_mandates() {return 'AED Mandates: ';}
	function sta_legend_owners_details() {return 'Summary Details on Law for Civil Liability Immunity for Organizations that Own AED\'s';}
	function sta_legend_mandates_details() {return 'Summary Details of where AEDs are Required by State/Local Laws by Industry Information is Updated Periodically';}
	function sta_legend_footer() {return 'If you are aware of any laws that are not current in this database summary, please contact technical support at 888-473-1777';}
	
	//nagger
	function nag_trainingexpired($traininginfo, $location, $org, $html = 0) {
		if($html) {
			//use html
			$traininghtml = '';
			foreach($traininginfo as $key => $val)
			{
				$traininghtml .= '
				<tr>
					<td>'.$traininginfo[$key]['student'].'</td>
					<td>'.$traininginfo[$key]['classname'].'</td>';
					
					if($traininginfo[$key]['expiration'] >= 0)
						$traininghtml .= '<td>'.$traininginfo[$key]['expiration'].' days left</td>';
					else
						$traininghtml .= '<td>'.($traininginfo[$key]['expiration']*-1).' days past</td>';
					
					$traininghtml .= '
					<td>'.$traininginfo[$key]['expirationdate'].'</td>
				</tr>';
			}
			
			return '<p>We have detected expiring training courses for '.$location.', '.$org.'.</p>
			
			<table style="width:100%;">
				<tr>
					<td style="border-bottom:1px solid black;">Student</td>
					<td style="border-bottom:1px solid black;">Class</td>
					<td style="border-bottom:1px solid black;">Time</td>
					<td style="border-bottom:1px solid black;">Expiration Date</td>
				</tr>
				'.$traininghtml.'
			</table>
			
			<p>
				This is an automated alert reminding you that you may need 
				to schedule training soon.  For more information please visit <a href="'.URL.'">'.PROGRAM.'</a> 
				For any questions, please feel free to contact us at 
				'.SUPPORTEMAIL.'.
			</p>';
		} else {
			//use plain text
			$trainingbody = '';
			foreach($traininginfo as $key => $val)
			{
				$trainingbody .= $traininginfo[$key]['student'].'	|	'.$traininginfo[$key]['classname'].'	|	';
				
				if($traininginfo[$key]['expiration'] >= 0)
					$trainingbody .= $traininginfo[$key]['expiration'].' days left';
				else
					$trainingbody .= ($traininginfo[$key]['expiration']*-1).' days past';
				
				$trainingbody .= '	|	'.$traininginfo[$key]['expirationdate'].'
';
			}
			
			return '
We have detected expiring training courses for '.$location.', '.$org.'.

Student	|	Class	|	Time	|	Expiration Date
'.$trainingbody.'

This is an automated alert reminding you that you may need to schedule training soon.  
For more information please visit '.PROGRAM.' - '.URL.'
For any questions, please feel free to contact us at '.SUPPORTEMAIL.'.';
		}
	}
	
	function nag_trainingsubject() {return 'Training Alert';}
	function nag_trainingnotexpired($days, $student, $class) {$body = 'We have detected that the student '.$student.' has '.$days.' days left until their '.$class.' training expires.<br /><br />This is an automated alert reminding you that you may need to schedule training for '.$student.' soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a> For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_trainingexpiring($student, $class) {$body = 'We have detected that the '.$class.' training for the student '.$student.' expires today.<br /><br />This is an automated alert reminding you that you may need to schedule training for '.$student.' soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a> For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	//function nag_trainingexpired($days, $student, $class) {return 'We have detected that the student '.$student.' is '.$days.' days past expiration on '.$class.' training.  This is an automated alert reminding you that you may need to schedule training for '.$student.' soon.  For more information please visit <a href="'.URL.'">'.PROGRAM.'</a> For any questions, please feel free to contact us at '.SUPPORTEMAIL.'.'; return $body;}
	
	function nag_pakheading() {return 'AED Pak/Accessory Alert';}
	function nag_paknotexpired($days, $pak, $lot, $location, $org, $placement, $serialnumber, $buylink) {$body = 'Your AED Pak/Accessory expires in '.$days.' days.<br /><br />Serial Number: '.$serialnumber.'<br />Pak/Accessory: '.$pak.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />This is an automated alert reminding you that you may need to purchase a new AED pak/accessory soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pak-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_pakexpiring($pak, $lot, $location, $org, $placement, $serialnumber, $buylink) {$body = 'Your AED Pak/Accessory expires today.<br /><br />Serial Number: '.$serialnumber.'<br />Pak/Accessory: '.$pak.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />This is an automated alert reminding you that you may need to purchase a new AED pak/accessory soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pak-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_pakexpired($days, $pak, $lot, $location, $org, $placement, $serialnumber, $buylink) {$body = 'Your AED Pak/Accessory is '.$days.' days expired.<br /><br />Serial Number: '.$serialnumber.'<br />Pak/Accessory: '.$pak.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />This is an automated alert reminding you that you may need to purchase a new AED pak/accessory soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pak-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	
	function nag_padheading() {return 'AED Pad Alert';}
	function nag_padnotexpired($days, $pad, $lot, $location, $org, $placement, $serialnumber, $buylink){$body = 'Your AED pad expires in '.$days.' days<br /><br />Serial Number: '.$serialnumber.'<br />Pad: '.$pad.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />This is an automated alert reminding you that you may need to purchase a new pad soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pad-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_padexpiring($pad, $lot, $location, $org, $placement, $serialnumber, $buylink){$body = 'Your AED pad expires today<br /><br />Serial Number: '.$serialnumber.'<br />Pad: '.$pad.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />This is an automated alert reminding you that you may need to purchase a new pad soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pad-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_padexpired($days, $pad, $lot, $location, $org, $placement, $serialnumber, $buylink){$body = 'Your AED pad is '.$days.' days expired<br /><br />Serial Number: '.$serialnumber.'<br />Pad: '.$pad.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />This is an automated alert reminding you that you may need to purchase a new pad soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-pad-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}

	function nag_accessoryheading() {return 'AED Accessory Alert';}
	function nag_accessorynotexpired($days, $accessory, $lot, $location, $org, $placement, $serialnumber, $buylink){$body = 'Your AED accessory expires in '.$days.' days<br /><br />Serial Number: '.$serialnumber.'<br />Accessory: '.$accessory.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />This is an automated alert reminding you that you may need to purchase a new accessory soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-acc-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_accessoryexpiring($accessory, $lot, $location, $org, $placement, $serialnumber, $buylink){$body = 'Your AED accessory expires today<br /><br />Serial Number: '.$serialnumber.'<br />Accessory: '.$accessory.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />This is an automated alert reminding you that you may need to purchase a new accessory soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-acc-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_accessoryexpired($days, $accessory, $lot, $location, $org, $placement, $serialnumber, $buylink){$body = 'Your AED accessory is '.$days.' days expired<br /><br />Serial Number: '.$serialnumber.'<br />Accessory: '.$accessory.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />This is an automated alert reminding you that you may need to purchase a new accessory soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-acc-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	
	function nag_batteryheading() {return 'AED Battery Alert';}
	function nag_batterynotexpired($days, $battery, $lot, $location, $org, $placement, $serialnumber, $buylink){$body = 'Your AED battery expires in '.$days.' days.<br /><br />Serial Number: '.$serialnumber.'<br />Battery: '.$battery.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />You may need to purchase a new battery soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-bat-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_batteryexpiring($battery, $lot, $location, $org, $placement, $serialnumber, $buylink){$body = 'Your AED battery expires today.<br /><br />Serial Number: '.$serialnumber.'<br />Battery: '.$battery.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />You may need to purchase a new battery soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-bat-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_batteryexpired($days, $battery, $lot, $location, $org, $placement, $serialnumber, $buylink){$body = 'Your AED battery is '.$days.' days expired.<br /><br />Serial Number: '.$serialnumber.'<br />Battery: '.$battery.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />You may need to purchase a new battery soon.<br /><br />';if($buylink != '') $body .= '<a target="_blank" href="'.$buylink.'"><img src="'.URL.'/images/buy-bat-eng.png" /></a>'; $body .= '<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	
	function nag_equipmentcontentheading(){return 'Equipment Contents Alert';}
	function nag_equipmentcontentnotexpired($days, $item, $cat, $subcat, $location, $org){$body = 'Your equipment content expires in '.$days.' days<br /><br />Item: '.$item.'<br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to purchase a new '.$item.' soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_equipmentcontentexpiring($item, $cat, $subcat, $location, $org){$body = 'Your equipment content expires today<br /><br />Item: '.$item.'<br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to purchase a new '.$item.' soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_equipmentcontentexpired($days, $item, $cat, $subcat, $location, $org){$body = 'Your equipment content is '.$days.' days expired.<br /><br />Item: '.$item.'<br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to purchase a new '.$item.' soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
   
	function nag_firstvoicecontentheading(){return 'First Voice Equipment Contents Alert';}
	function nag_firstvoicecontentnotexpired($days, $item, $cat, $subcat, $location, $org){$body = 'Your First Voice equipment content expires in '.$days.' days.<br /><br />Item: '.$item.'<br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to purchase a new '.$item.' soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_firstvoicecontentexpiring($item, $cat, $subcat, $location, $org){$body = 'Your First Voice equipment content expires today.<br /><br />Item: '.$item.'<br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to purchase a new '.$item.' soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_firstvoicecontentexpired($days, $item, $cat, $subcat, $location, $org){$body = 'Your First Voice equipment content is '.$days.' days expired.<br /><br />Item: '.$item.'<br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to purchase a new '.$item.' soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	
	function nag_equipmentcheckheading() {return 'Equipment Check Alert';}
	function nag_equipmentcheckexpiring($cat, $subcat, $location, $org) {$body = 'Your equipment requires a maintenance check today<br /><br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_equipmentcheckonedayafter($cat, $subcat, $location, $org) {$body = 'Your equipment check is 1 day overdue<br /><br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_equipmentcheckexpired($days, $cat, $subcat, $location, $org) {$body = 'Your equipment check is '.$days.' days overdue.<br /><br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	
	function nag_firstvoicecheckheading() {return 'First Voice Equipment Check Alert';}
	function nag_firstvoicecheckexpiring($cat, $subcat, $location, $org) {$body = 'Your First Voice equipment check expires today<br /><br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_firstvoicecheckonedayafter($cat, $subcat, $location, $org) {$body = 'Your First Voice equipment check is 1 day overdue<br /><br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_firstvoicecheckexpired($days, $cat, $subcat, $location, $org) {$body = 'Your First Voice equipment check is '.$days.' days overdue<br /><br />Category: '.$cat.'<br />Subcategory: '.$subcat.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
		
	function nag_aedcheckheading() {return 'AED Check Alert';}
	function nag_aedexpiring($serial, $location, $org, $placement) {$body = 'Your AED requires a maintenance check today<br /><br />Serial Number: '.$serial.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />Please check your AED<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a><br />For any questions, please feel free to contact us at '.AEDPHONE.' or email: '.AEDSUPPORTEMAIL.'.'; return $body;}
	function nag_aedonedayafter($serial, $location, $org, $placement) {$body = 'Your AED maintenance check is 1 day overdue<br /><br />Serial Number: '.$serial.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />Please check your AED.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a><br />For any questions, please feel free to contact us at '.AEDPHONE.' or email: '.AEDSUPPORTEMAIL.'.'; return $body;}
	function nag_aedexpired($days, $serial, $location, $org, $placement) {$body = 'Your AED maintenance check is '.$days.' days overdue<br /><br />Serial Number: '.$serial.'<br />Location: '.$location.'<br />Organization: '.$org.'<br />Placement: '.$placement.'<br /><br />Please check your AED.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a><br />For any questions, please feel free to contact us at '.AEDPHONE.' or email: '.AEDSUPPORTEMAIL.'.'; return $body;}
	
	function nag_aedservicing_heading() {return 'AED Servicing Alert';}
	function nag_aedservicing_expiringtoday($serial, $placement, $brand, $model, $location, $org) {$body = 'Your AED requires servicing today<br /><br />Serial: '.$serial.'<br />Brand: '.$brand.' <br />Model: '.$model.' <br />Placement: '.$placement.' <br />Location: '.$location.'<br />Organization: '.$org.' .<br /><br />Please service your AED.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.AEDPHONE.' or email:'.AEDSUPPORTEMAIL.'.'; return $body;}
	function nag_aedservicing_expiring($lastdate, $checkdate, $checkfrequency, $serial, $placement, $brand, $model, $location, $org) {$body = 'Your last AED Servicing was '.$lastdate.'<br />Your '.$checkfrequency.' Servicing is due on '.$checkdate.'<br /><br />Serial: '.$serial.'<br />Brand: '.$brand.' <br />Model: '.$model.' <br />Placement: '.$placement.' <br />Location: '.$location.' <br />Organization: '.$org.' .<br /><br />Please service your AED.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.AEDPHONE.' or email:'.AEDSUPPORTEMAIL.'.'; return $body;}
	function nag_aedservicing_expired($lastdate, $checkdate, $checkfrequency, $overdue, $serial, $placement, $brand, $model, $location, $org) {$body = 'Your last AED Servicing was '.$lastdate.'<br />Your '.$checkfrequency.' AED Service check was due '.$checkdate.'.<br />You are '.$overdue.' days overdue.<br /><br />Serial: '.$serial.'<br />Brand: '.$brand.' <br />Model: '.$model.' <br />Placement: '.$placement.' <br />Location: '.$location.' <br />Organization: '.$org.' .<br /><br />Please service your AED.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.AEDPHONE.' or email:'.AEDSUPPORTEMAIL.'.'; return $body;}
	
	function nag_keycodeheading() {return 'Subscription Alert';}
	function nag_keycodenotexpired($days, $aed, $location, $org){$body = 'Your AED subscription for '.PROGRAM.' expires in '.$days.' days<br /><br />AED: '.$aed.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to purchase a new subscription soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_keycodeexpiring($aed, $location, $org){$body = 'Your AED subscription for '.PROGRAM.' expires today<br /><br />AED: '.$aed.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to purchase a new subscription soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_keycodeexpired($days, $aed, $location, $org){$body = 'Your AED subscription for '.PROGRAM.' is '.$days.' days expired<br /><br />AED: '.$aed.'<br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to purchase a new subscription soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	
	function nag_medicaldirectionheading() {return 'Subscription Alert';}
	function nag_medicaldirectionnotexpired($days, $location, $org){$body = 'Your medical direction expires in '.$days.' days.<br /><br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to update soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_medicaldirectionexpiring($location, $org){$body = 'Your medical direction expires today.<br /><br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to update soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}
	function nag_medicaldirectionexpired($days, $location, $org){$body = 'Your medical direction is '.$days.' days expired.<br /><br />Location: '.$location.'<br />Organization: '.$org.'<br /><br />You may need to update soon.<br /><br />For more information please visit <a href="'.WEBSITELINK.'">'.WEBSITE.'</a>.<br />For any questions, please feel free to contact us at '.SUPPLYPHONE.' or email:'.SUPPLYSUPPORTEMAIL.'.'; return $body;}

	//reports
	function rep_email(){return 
		PROGRAM.' Report
		You have requested a report at '.URL.' - '.PROGRAM.'.  Your report is attached as a multiple sheet workbook.  
		PLEASE LOOK AT THE BOTTOM OF THE WORKBOOK TO SEE SHEETS TO OTHER SECTIONS OF THE REPORT.
		
		If you have any questions about this report, please feel free to contact us at '.SUPPORTEMAIL.'.
		
		Please do not reply to this email.  We are unable to respond to inquiries sent to this address.
		Copyright &copy;  2015 Think Safe Inc.';
	}
	function rep_emailbody(){return
		'<html><head></head><body style="color:#000001;">
			<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
				<div style="padding:10px;width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
					<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
						<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
					</h1>
					<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
					<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
						Report
					</h2>
					<br />
					<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
						You have requested a report at <a href="'.URL.'">'.PROGRAM.'</a>.  
						<br />
						<br />
						If you have any questions about this report, please feel free to contact us at <b>'.SUPPORTEMAIL.'</b>.
					</div>
				</div>
			</div>
			<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
				Please do not reply to this email.  We are unable to respond to inquiries sent to this address.
			</div>
			<br />
			<div style="width:970px;margin:0 auto;color:#000001;">
				Copyright &copy;  2015 Think Safe Inc.
			</div>
		</body></html>
	';}
	function rep_aedfilename(){return 'AED_Report';}
	function rep_aedcheckfilename(){return 'AED_Check_Report';}
	function rep_aedservicingfilename(){return 'AED_Servicing_Report';}
	function rep_contactsfilename(){return 'Contacts_Report';}
	function rep_documentsfilename(){return 'Documents_Report';}
	function rep_equipmentfilename(){return 'Equipment_Report';}
	function rep_erpfilename(){return 'ERP_Report';}
	function rep_facilityinfofilename(){return 'Facility_Information_Report';}
	function rep_firstvoicefilename(){return 'First_Voice_Report';}
	function rep_hazardsfilename(){return 'Hazards_Report';}
	function rep_immunizationsfilename(){return 'Immunizations_Report';}
	function rep_keycodesfilename(){return 'Keycodes_Report';}
	function rep_licensesfilename(){return 'Licenses_Report';}
	function rep_medicaldirectionfilename(){return 'Medical_Direction_Report';}
	function rep_inactivemedicaldirectionfilename(){return 'Inactive_Medical_Direction_Report';}
	function rep_masteraedfilename(){return 'Master_AED_Report';}
	function rep_schedulerfilename(){return 'Scheduler_Report';}
	function rep_tracingfilename(){return 'Tracing_Report';}
	function rep_trainingfilename(){return 'Training_Report';}
	function rep_tracingbetween(){return 'Between';}
	function rep_tracingand(){return 'and';}
	function rep_emailsuccess(){return 'Your report has been emailed to you';}
	function rep_primarypad() {return 'Primary Pad';}
	function rep_pediatricpad() {return 'Pediatric Pad';}
	function rep_sparepad1() {return 'Spare Pad 1';}
	function rep_sparepad2() {return 'Spare Pad 2';}
	function rep_sparepediatricpad() {return 'Spare Pediatric Pad';}
	function rep_primarypak() {return 'Primary Pak';}
	function rep_pediatricpak() {return 'Pediatric Pak';}
	function rep_sparepak1() {return 'Spare Pak 1';}
	function rep_sparepak2() {return 'Spare Pak 2';}
	function rep_sparepediatricpak() {return 'Spare Pediatric Pak';}
	function rep_primarybattery() {return 'Primary Battery';}
	function rep_sparebattery() {return 'Spare Battery';}
	function rep_primaryaccessory() {return 'Primary Accessory';}
	function rep_spareaccessory() {return 'Spare Accessory';}
	function rep_expiration() {return 'expiration';}
	function rep_installation() {return 'installation';}
	function rep_kitincluded() {return 'Everything included';}
	function rep_kitreplaced() {return 'Kit Items Replaced';}
	function rep_gloves() {return 'Gloves';}
	function rep_razor() {return 'Razor';}
	function rep_scissors() {return 'Scissors';}
	function rep_towel() {return 'Towel';}
	function rep_towlette() {return 'Towlette';}
	function rep_mask() {return 'Mask';}
}
?>
