<?php
@ $webdb = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_websites_live');
$repId = $webdb->real_escape_string($_GET['rep']);
$reps = $webdb->query("
    select *
    from users
    where id = '".$repId."'
");
$rep = $reps->fetch_assoc();
?>

<!DOCTYPE html>

<html class="no-js">
<head>
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div id="heading-logo">
    <img src="img/TSIsiteLogo.jpg" alt="Think Safe Logo"/>
</div>
<div class="top-bar">
    <div class="top-bar-title">
    <span data-responsive-toggle="responsive-menu" data-hide-for="medium">
      <button class="menu-icon dark" type="button" data-toggle></button>
    </span>
        <strong>More Info</strong>
    </div>
    <div id="responsive-menu">
        <div class="top-bar-left">
        </div>
        <div class="top-bar-right">
        </div>
    </div>
</div>
<div id="sub-heading">
    For assistance call <?php echo $rep['firstname'].' '.$rep['lastname']; ?>: <?php echo $rep['phone']; ?> or email: <?php echo $rep['username']; ?>
</div>
<div class="row">
    <div class="large-12 column">
        <h1 class="text-center title">More Info Request</h1>
    </div>
</div>
<form id="more-info-form" action="success.php" method="post">
    <input name="rep" type="hidden" value="<?php echo $rep['id']; ?>" />
    <div class="row">
        <div class="large-12 column content">
            <div class="row">
                <div class="large-6 column form-field">
                    <label>Name</label>
                    <input name="name" type="text"/>
                </div>
                <div class="large-6 column form-field">
                    <label>Phone</label>
                    <input name="phone" type="text"/>
                </div>
            </div>
            <div class="row">
                <div class="large-12 column form-field">
                    <label>Email</label>
                    <input name="email" type="text"/>
                </div>
            </div>
            <div class="row">
                <div class="large-12 column form-field">
                    <label>Topics</label>
                    <select name="topics[]" style="height: 6rem;" multiple>
                        <option>AED Law Compliance</option>
                        <option>AED State Laws</option>
                        <option>AED Brand & Comparisons</option>
                        <option>AED Specials</option>
                        <option>AED Servicing</option>
                        <option>AED Programs</option>
                        <option>AED Registration Tool</option>
                        <option>Bulk Buy Programs</option>
                        <option>Compliance Management Systems</option>
                        <option>DEMO - First Voice Manager</option>
                        <option>DEMO - First Voice Training</option>
                        <option>Epinephrine Laws & Help</option>
                        <option>First Aid Kits: ANSI Z308.1</option>
                        <option>First Aid Requirements</option>
                        <option>First Aid & CPR Science 2015 Updates</option>
                        <option>Grant Assistance</option>
                        <option>Learning Management Systems</option>
                        <option>Mandatory Reporter Help</option>
                        <option>Medical Oversight</option>
                        <option>Online Training</option>
                        <option>OSHA First Aid Help</option>
                        <optino>OSHA BBP Help</optino>
                        <option>OSHA CPR Help</option>
                        <option>OSHA Compliance Help</option>
                        <option>OSHA Emergency Action Plan (EAP) Help</option>
                        <option>OSHA Hazcom Help (Right to Know)</option>
                        <option>OSHA Training Programs</option>
                        <option>Preferred Vendor Programs</option>
                        <option>Private Label License Options</option>
                        <option>Technical Assistance</option>
                        <option>NONE of above - Other</option>
                        <option>ALL of above</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="large-12 column form-field">
                    <label>Comments</label>
                    <textarea name="comments" style="height:6rem;"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="row collapse" style="margin-top: 1rem;">
        <div class="large-2 large-offset-10 column text-right">
            <input type="submit" class="button expanded" value="Send"/>
        </div>
    </div>
</form>
<script src="js/jquery.js"></script>
<script src="js/foundation.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>