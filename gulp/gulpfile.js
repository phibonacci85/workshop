var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var rimraf = require('rimraf');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('default', ['serve']);

//Clean out the distribution folder
gulp.task('clean', function (callback) {
    rimraf('./dist', callback);
});

//Copy everything from source folder into distribution folder (except .js and .scss)
gulp.task('copy', function () {
    return gulp
        .src(['./src/**/*.*', '!./src/{js,scss}/*.*'])
        .pipe(gulp.dest('./dist'));
});

gulp.task('scripts', function () {
    gulp.src('js/*.js')
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('dist'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

gulp.task('styles', function () {
    gulp.src('scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist'));
});

gulp.task('automate', function () {
    gulp.watch(['scss/**/*.scss'], ['styles', reload]);
    gulp.watch(['js/**/*.js'], ['scripts', reload]);
});

gulp.task('serve', function () {
    browserSync({
        server: {
            baseDir: './'
        }
    });

    gulp.watch(['*.html'], {cwd: './'}, reload);
    gulp.watch(['scss/**/*.scss'], ['styles', reload]);
    gulp.watch(['js/**/*.js'], ['scripts', reload]);
});