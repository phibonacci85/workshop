<?php
/*
	created on 10/22/12 JK
	cory chambers 7-18-13 added nextreviewdate to medical direction section
*/
require_once('default_english.php');
class English extends Default_English
{
	function hel_content() {
		$content = '';
		$content .= '
		<h1>Help</h1>
		<table style="width:600px;">';
			if(file_exists($this->doc_usermanual())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_usermanual().':</td> 
					<td><a target="_blank" href="'.$this->doc_usermanual().'">PDF</a><td>
				</tr>';
			}
			if(file_exists($this->doc_adminusermanual())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_adminusermanual().':</td> 
					<td><a target="_blank" href="'.$this->doc_adminusermanual().'">PDF</a><td>
				</tr>';
			}
			
			if(file_exists($this->doc_blankaedcheck())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_blankaedtemplate().':</td> 
					<td><a target="_blank" href="'.$this->doc_blankaedcheck().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_blankservicecheck())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_blankservicecheck().':</td> 
					<td><a target="_blank" href="'.$this->doc_blankservicecheck().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_trainingtemplate())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_trainingupload().':</td> 
					<td><a target="_blank" href="'.$this->doc_trainingtemplate().'">XLS</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_eventform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_eventform().':</td> 
					<td><a target="_blank" href="'.$this->doc_eventform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_recallhistory())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_recallhistory().':</td> 
					<td><a target="_blank" href="'.$this->doc_recallhistory().'">DOC</a></td>
				</tr>';
			}
		$content .= '
		</table>';
		
		$content .= '
		<h1>State Forms</h1>
		<table style="width:600px;">';
			if(file_exists($this->doc_azstateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_azstateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_azstateform().'">DOC</a><td>
				</tr>';
			}
			
			if(file_exists($this->doc_ctstateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_ctstateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_ctstateform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_dcstateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_dcstateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_dcstateform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_destateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_destateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_destateform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_gastateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_gastateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_gastateform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_ilstateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_ilstateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_ilstateform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_mdstateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_mdstateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_mdstateform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_nhstateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_nhstateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_nhstateform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_nmstateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_nmstateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_nmstateform().'">PDF</a></td>
				</tr>';
			}
			
			if(file_exists($this->doc_nystateform())) {
				$content .= '
				<tr>
					<td style="width:550px;">'.$this->hel_nystateform().':</td> 
					<td><a target="_blank" href="'.$this->doc_nystateform().'">PDF</a></td>
				</tr>';
			}
		$content .= '
		</table>';
		
		$content .= '
		<h1>Videos</h1>
		<table style="width:600px;">
			<tr>
				<td style="width:550px;">'.$this->hel_aedcheckvideo().':</td> 
				<td><a target="_blank" href="http://rescueoneaedmanageronline.com/index.php?content=video&r=527">View</a><td>
			</tr>
		</table>';		
		return $content;
	}

	function vis_home() {$home = '
		<h1 style="text-align:left;">'.PROGRAM.':</h1>';
		
		$home .= '
		<br />
		<ul class="boldlist">
			<li>'.TITLE.' & AED Management</li>
			<li>Expiration Date Management</li>
			<li>Training Certification Tracking</li>
			<li>Location & Hazard Tracking</li>
			<li>Medical Direction</li>	
		</ul>	
		<br />
		
		<p>
			'.PROGRAM.' allows you to manage your emergency response program<br />
			online, ensuring up-to-date maintenance records for your '.TITLE.' products and AEDS, and<br />employee training certification renewal dates.
		</p>
		<br />
		<p>
			For Assistance with your emergency response program, or answers to your questions on current<br />government regulations, contact a '.TITLE.' specialist.
		</p>
		<br />
		<p style="font-weight:bold;">Technical Support Phone Number: '.PHONE.' </p>';
		return $home;}

	//reports
	function rep_email(){return 
		PROGRAM.' Report
		You have requested a report at '.URL.' - '.PROGRAM.'.  Your report is attached as a multiple sheet workbook.  
		PLEASE LOOK AT THE BOTTOM OF THE WORKBOOK TO SEE SHEETS TO OTHER SECTIONS OF THE REPORT.
		
		If you have any questions about this report, please feel free to contact us at '.SUPPORTEMAIL.'.
		
		Please do not reply to this email.  We are unable to respond to inquiries sent to this address.
		Copyright &copy;  2015 Think Safe Inc.';
	}
	function rep_emailbody(){return
		'<html><head></head><body style="color:#000001;">
			<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
				<div style="padding:10px;width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
					<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
						<img alt="'.PROGRAM.'" src="'.URL.LOGO.'">
					</h1>
					<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
					<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
						Report
					</h2>
					<br />
					<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
						You have requested a report at <a href="'.URL.'">'.PROGRAM.'</a>.  
						<br />
						<br />
						If you have any questions about this report, please feel free to contact us at <b>'.SUPPORTEMAIL.'</b>.
					</div>
				</div>
			</div>
			<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
				Please do not reply to this email.  We are unable to respond to inquiries sent to this address.
			</div>
			<br />
			<div style="width:970px;margin:0 auto;color:#000001;">
				Copyright &copy;  2015 Think Safe Inc.
			</div>
		</body></html>
	';}
}
?>