$(document).ready(function () {
    $('#statepicker').hide();
    $('#singlestatepicker').hide();
           $('.statecheck').prop('checked', true);
    $('input[type="radio"]').change(function () {
       if( $(this).val() ==  "multiple"){
           $('#singlestatepicker').hide();
           $('.statecheck').prop('checked', false);
           $('#statepicker').slideDown();
       } else if( $(this).val() == "all") {
           $('#singlestatepicker').hide();
           $('.statecheck').prop('checked', true);
           $('#statepicker').slideUp();
       } else {
           $('#singlestatepicker').show();
           $('.statecheck').prop('checked', false);
           $('#statepicker').slideUp();
       }
    });
});