<?php
	define("FVMDBHOST", "localhost");
	define("FVMDBUSER", "root");
	define("FVMDBPASS", "Jbake1234");
define("FVMDB", "firstvoicemanager");
	@ $fvmdb = new mysqli( FVMDBHOST, FVMDBUSER, FVMDBPASS, FVMDB);


/************************************************************************************************************************************************
 * 																																				*
 *															 AED PADS																			*
 * 																																				*
 ************************************************************************************************************************************************/
	echo 'working on distinct pads<br />';
	$fvmdb->query("
		drop table updated_aed_pads;
	");
	$fvmdb->query("
		create table updated_aed_pads
		(
			aed_pad_id int(11) primary key auto_increment,
			location_id int(11),
			aed_id int(11),
			aed_pad_type_id int(11),
			expiration varchar(30),
			lot varchar(50),
			spare int(1),
			display int(1)
		);
	");
	$fvmdb->query("	
		drop table updated_aed_pad_types;
	");
	$fvmdb->query("
		create table updated_aed_pad_types
		(
			aed_pad_type_id int(11) primary key auto_increment,
			type varchar(100),
			pediatric int(1),
			partnum varchar(30),
			length varchar(2)
		);
	");
	$fvmdb->query("	
		drop table updated_aed_pad_types_on_models;
	");
	$fvmdb->query("
		create table updated_aed_pad_types_on_models
		(
			aed_pad_types_on_model_id int(11) primary key auto_increment,
			aed_pad_type_id int(11),
			aed_model_id int(11),
			creation timestamp
		);
	");
$fvmdb->query("	
		DROP TABLE updated_aedcheck_pads;
	");
$fvmdb->query("
		CREATE TABLE updated_aedcheck_pads LIKE aedcheck_pads;
	");
$fvmdb->query("
		INSERT INTO updated_aedcheck_pads SELECT * FROM aedcheck_pads;
	");
$fvmdb->query("	
		DROP TABLE updated_aedservicing_pads;
	");
$fvmdb->query("
		CREATE TABLE updated_aedservicing_pads LIKE aedservicing_pads;
	");
$fvmdb->query("
		INSERT INTO updated_aedservicing_pads SELECT * FROM aedservicing_pads;
	");

	$distincts = $fvmdb->query("
		select distinct partnum, type, pediatric, length
		from aed_pad_types
	");

	while($distinct = $distincts->fetch_assoc()){
		//add distinct partnum
		$fvmdb->query("
			insert into updated_aed_pad_types
			(type, pediatric, partnum, length)
			values
			('".$distinct['type']."', '".$distinct['pediatric']."', '".$distinct['partnum']."', '".$distinct['length']."')
		");
		$newTypeId = $fvmdb->insert_id;
		//get duplicates
		$duplicates = $fvmdb->query("
			select *
			from aed_pad_types
			where partnum = '".$distinct['partnum']."'
		");
		
		// the rest can be done with sql updates instead of insertions to preserve original id's 
		
		//find old refrences to duplicates, add them to new database with new reference id
		while($duplicate = $duplicates->fetch_assoc()){
			//add aed_pad refrences to updated database
			$aedpads = $fvmdb->query("
				select *
				from aed_pads
				where aed_pad_type_id = '".$duplicate['aed_pad_type_id']."'
			");
			while($aedpad = $aedpads->fetch_assoc()){
				$fvmdb->query("
					insert into updated_aed_pads
					(location_id, aed_id, aed_pad_type_id, expiration, lot, spare, display)
					values
					('".$aedpad['location_id']."', '".$aedpad['aed_id']."', '".$newTypeId."', '".$aedpad['expiration']."', '".$aedpad['lot']."', '".$aedpad['spare']."', '".$aedpad['display']."')
				");
			}
			
			//add aed_pad_types_on_models to updated database
			$padTypesOnModels = $fvmdb->query("
				select *
				from aed_pad_types_on_models
				where aed_pad_type_id = '".$duplicate['aed_pad_type_id']."'
			");
			while($padTypesOnModel = $padTypesOnModels->fetch_assoc()){
				$fvmdb->query("
					insert into updated_aed_pad_types_on_models
					(aed_pad_type_id, aed_model_id, creation)
					values
					('".$newTypeId."', '".$padTypesOnModel['aed_model_id']."', '".$padTypesOnModel['creation']."')
				");
			}
			
			//update aedservicing_pads to new type id
			$updateOldServicingPads = $fvmdb->query("
				update updated_aedservicing_pads
				set aed_pad_type_id = '".$newTypeId."'
				where aed_pad_type_id = '".$duplicate['aed_pad_type_id']."'
			");
			$updateNewServicingPads = $fvmdb->query("
				update updated_aedservicing_pads
				set new_aed_pad_type_id = '".$newTypeId."'
				where aed_pad_type_id = '".$duplicate['aed_pad_type_id']."'
			");
			
			//update aedcheck_pads to new type id
			$updateOldCheckPads = $fvmdb->query("
				update updated_aedcheck_pads
				set aed_pad_type_id = '".$newTypeId."'
				where aed_pad_type_id = '".$duplicate['aed_pad_type_id']."'
			");
			$updateNewCheckPads = $fvmdb->query("
				update updated_aedcheck_pads
				set new_aed_pad_type_id = '".$newTypeId."'
				where aed_pad_type_id = '".$duplicate['aed_pad_type_id']."'
			");
		}
	}
	
/************************************************************************************************************************************************
 * 																																				*
 *															 AED PAKS																			*
 * 																																				*
 ************************************************************************************************************************************************/
	echo 'working on distinct paks<br />';
	$fvmdb->query("
		drop table updated_aed_paks;
	");
	$fvmdb->query("
		create table updated_aed_paks
		(
			aed_pak_id int(11) primary key auto_increment,
			location_id int(11),
			aed_id int(11),
			aed_pak_type_id int(11),
			expiration varchar(30),
			lot varchar(50),
			spare int(1),
			display int(1)
		);
	");
	$fvmdb->query("	
		drop table updated_aed_pak_types;
	");
	$fvmdb->query("
		create table updated_aed_pak_types
		(
			aed_pak_type_id int(11) primary key auto_increment,
			type varchar(100),
			pediatric int(1),
			partnum varchar(30),
			length varchar(2),
			buylink varchar(150)
		);
	");
	$fvmdb->query("	
		drop table updated_aed_pak_types_on_models;
	");
	$fvmdb->query("
		create table updated_aed_pak_types_on_models
		(
			aed_pak_types_on_model_id int(11) primary key auto_increment,
			aed_pak_type_id int(11),
			aed_model_id int(11),
			creation timestamp
		);
	");
$fvmdb->query("	
		drop table updated_aedcheck_paks;
	");
$fvmdb->query("
		create table updated_aedcheck_paks like aedcheck_paks;
	");
$fvmdb->query("
		insert into updated_aedcheck_paks select * from aedcheck_paks;
	");
$fvmdb->query("	
		drop table updated_aedservicing_paks;
	");
$fvmdb->query("
		create table updated_aedservicing_paks like aedservicing_paks;
	");
$fvmdb->query("
		insert into updated_aedservicing_paks select * from aedservicing_paks;
	");

	$distincts = $fvmdb->query("
		select distinct partnum, type, pediatric, length, buylink
		from aed_pak_types
	");
	
	while($distinct = $distincts->fetch_assoc()){
		//add distinct partnum
		$fvmdb->query("
			insert into updated_aed_pak_types
			(type, pediatric, partnum, length, buylink)
			values
			('".$distinct['type']."', '".$distinct['pediatric']."', '".$distinct['partnum']."', '".$distinct['length']."', '".$distinct['buylink']."')
		");
		$newTypeId = $fvmdb->insert_id;
		//get duplicates
		$duplicates = $fvmdb->query("
			select *
			from aed_pak_types
			where partnum = '".$distinct['partnum']."'
		");
	
		// the rest can be done with sql updates instead of insertions to preserve original id's 
	
		//find old refrences to duplicates, add them to new database with new reference id
		while($duplicate = $duplicates->fetch_assoc()){
			//add aed_pak refrences to updated database
			$aedpaks = $fvmdb->query("
				select *
				from aed_paks
				where aed_pak_type_id = '".$duplicate['aed_pak_type_id']."'
			");
			while($aedpak = $aedpaks->fetch_assoc()){
				$fvmdb->query("
					insert into updated_aed_paks
					(location_id, aed_id, aed_pak_type_id, expiration, lot, spare, display)
					values
					('".$aedpak['location_id']."', '".$aedpak['aed_id']."', '".$newTypeId."', '".$aedpak['expiration']."', '".$aedpak['lot']."', '".$aedpak['spare']."', '".$aedpak['display']."')
				");
			}
				
			//add aed_pak_types_on_models to updated database
			$pakTypesOnModels = $fvmdb->query("
				select *
				from aed_pak_types_on_models
				where aed_pak_type_id = '".$duplicate['aed_pak_type_id']."'
			");
			while($pakTypesOnModel = $pakTypesOnModels->fetch_assoc()){
				$fvmdb->query("
					insert into updated_aed_pak_types_on_models
					(aed_pak_type_id, aed_model_id, creation)
					values
					('".$newTypeId."', '".$pakTypesOnModel['aed_model_id']."', '".$pakTypesOnModel['creation']."')
				");
			}
			
			//update aedservicing_paks to new type id
			$updateOldServicingPaks = $fvmdb->query("
				update updated_aedservicing_paks
				set aed_pak_type_id = '".$newTypeId."'
				where aed_pak_type_id = '".$duplicate['aed_pak_type_id']."'
			");
			$updateNewServicingPaks = $fvmdb->query("
				update updated_aedservicing_paks
				set new_aed_pak_type_id = '".$newTypeId."'
				where aed_pak_type_id = '".$duplicate['aed_pak_type_id']."'
			");
			
			//update aedcheck_paks to new type id
			$updateOldCheckPaks = $fvmdb->query("
				update updated_aedcheck_paks
				set aed_pak_type_id = '".$newTypeId."'
				where aed_pak_type_id = '".$duplicate['aed_pak_type_id']."'
			");
			$updateNewCheckPaks = $fvmdb->query("
				update updated_aedcheck_paks
				set new_aed_pak_type_id = '".$newTypeId."'
				where aed_pak_type_id = '".$duplicate['aed_pak_type_id']."'
			");
		}
	}
	

/************************************************************************************************************************************************
 * 																																				*
 *															 AED BATTERIES																		*
 * 																																				*
 ************************************************************************************************************************************************/
	echo 'working on distinct batteries<br />';
	$fvmdb->query("
		drop table updated_aed_batteries;
	");
	$fvmdb->query("
		create table updated_aed_batteries
		(
			aed_battery_id int(11) primary key auto_increment,
			location_id int(11),
			aed_id int(11),
			aed_battery_type_id int(11),
			expiration varchar(30),
			lot varchar(50),
			spare int(1),
			display int(1)
		);
	");
	$fvmdb->query("
		drop table updated_aed_battery_types;
	");
	$fvmdb->query("
		create table updated_aed_battery_types
		(
			aed_battery_type_id int(11) primary key auto_increment,
			type varchar(100),
			partnum varchar(30),
			length varchar(2)
		);
	");
	$fvmdb->query("
		drop table updated_aed_battery_types_on_models;
	");
	$fvmdb->query("
		create table updated_aed_battery_types_on_models
		(
			aed_battery_types_on_model_id int(11) primary key auto_increment,
			aed_battery_type_id int(11),
			aed_model_id int(11),
			creation timestamp
		);
	");
$fvmdb->query("	
		drop table updated_aedcheck_batteries;
	");
$fvmdb->query("
		create table updated_aedcheck_batteries like aedcheck_batteries;
	");
$fvmdb->query("
		insert into updated_aedcheck_batteries select * from aedcheck_batteries;
	");
$fvmdb->query("	
		drop table updated_aedservicing_batteries;
	");
$fvmdb->query("
		create table updated_aedservicing_batteries like aedservicing_batteries;
	");
$fvmdb->query("
		insert into updated_aedservicing_batteries select * from aedservicing_batteries;
	");
	
	$distincts = $fvmdb->query("
		select distinct partnum, type, length
		from aed_battery_types
	");
	
	while($distinct = $distincts->fetch_assoc()){
		//add distinct partnum
		$fvmdb->query("
			insert into updated_aed_battery_types
			(type, partnum, length)
			values
			('".$distinct['type']."', '".$distinct['partnum']."', '".$distinct['length']."')
		");
		$newTypeId = $fvmdb->insert_id;
		//get duplicates
		$duplicates = $fvmdb->query("
			select *
			from aed_battery_types
			where partnum = '".$distinct['partnum']."'
		");
	
		// the rest can be done with sql updates instead of insertions to preserve original id's 
	
		//find old refrences to duplicates, add them to new database with new reference id
		while($duplicate = $duplicates->fetch_assoc()){
			//add aed_battery refrences to updated database
			$aedbatteries = $fvmdb->query("
				select *
				from aed_batteries
				where aed_battery_type_id = '".$duplicate['aed_battery_type_id']."'
			");
			while($aedbattery = $aedbatteries->fetch_assoc()){
				$fvmdb->query("
					insert into updated_aed_batteries
					(location_id, aed_id, aed_battery_type_id, expiration, lot, spare, display)
					values
					('".$aedbattery['location_id']."', '".$aedbattery['aed_id']."', '".$newTypeId."', '".$aedbattery['expiration']."', '".$aedbattery['lot']."', '".$aedbattery['spare']."', '".$aedbattery['display']."')
				");
			}
	
			//add aed_battery_types_on_models to updated database
			$batteryTypesOnModels = $fvmdb->query("
				select *
				from aed_battery_types_on_models
				where aed_battery_type_id = '".$duplicate['aed_battery_type_id']."'
			");
			while($batteryTypesOnModel = $batteryTypesOnModels->fetch_assoc()){
				$fvmdb->query("
					insert into updated_aed_battery_types_on_models
					(aed_battery_type_id, aed_model_id, creation)
					values
					('".$newTypeId."', '".$batteryTypesOnModel['aed_model_id']."', '".$batteryTypesOnModel['creation']."')
				");
			}
			
			//update aedservicing_batteries to new type id
			$updateOldServicingBatteries = $fvmdb->query("
				update updated_aedservicing_batteries
				set aed_battery_type_id = '".$newTypeId."'
				where aed_battery_type_id = '".$duplicate['aed_battery_type_id']."'
			");
			$updateNewServicingBatteries = $fvmdb->query("
				update updated_aedservicing_batteries
				set new_aed_battery_type_id = '".$newTypeId."'
				where aed_battery_type_id = '".$duplicate['aed_battery_type_id']."'
			");
			
			//update aedcheck_batteries to new type id
			$updateOldCheckBatteries = $fvmdb->query("
				update updated_aedcheck_batteries
				set aed_battery_type_id = '".$newTypeId."'
				where aed_battery_type_id = '".$duplicate['aed_battery_type_id']."'
			");
			$updateNewCheckBatteries = $fvmdb->query("
				update updated_aedcheck_batteries
				set new_aed_battery_type_id = '".$newTypeId."'
				where aed_battery_type_id = '".$duplicate['aed_battery_type_id']."'
			");
		}
	}


/************************************************************************************************************************************************
 * 																																				*
 *															 AED ACCESSORIES																	*
 * 																																				*
 ************************************************************************************************************************************************/
	echo 'working on distinct accessories<br />';
	$fvmdb->query("
		drop table updated_aed_accessories;
	");
	$fvmdb->query("
		create table updated_aed_accessories
		(
			aed_accessory_id int(11) primary key auto_increment,
			location_id int(11),
			aed_id int(11),
			aed_accessory_type_id int(11),
			expiration varchar(30),
			lot varchar(50),
			spare int(1),
			display int(1)
		);
	");
	$fvmdb->query("
		drop table updated_aed_accessory_types;
	");
	$fvmdb->query("
		create table updated_aed_accessory_types
		(
			aed_accessory_type_id int(11) primary key auto_increment,
			type varchar(100),
			partnum varchar(30),
			length varchar(2),
			buylink varchar(150)
		);
	");
	$fvmdb->query("
		drop table updated_aed_accessory_types_on_models;
	");
	$fvmdb->query("
		create table updated_aed_accessory_types_on_models
		(
			aed_accessory_types_on_model_id int(11) primary key auto_increment,
			aed_accessory_type_id int(11),
			aed_model_id int(11),
			creation timestamp
		);
	");
$fvmdb->query("	
		drop table updated_aedcheck_accessories;
	");
$fvmdb->query("
		create table updated_aedcheck_accessories like aedcheck_accessories;
	");
$fvmdb->query("
		insert into updated_aedcheck_accessories select * from aedcheck_accessories;
	");
$fvmdb->query("	
		drop table updated_aedservicing_accessories;
	");
$fvmdb->query("
		create table updated_aedservicing_accessories like aedservicing_accessories;
	");
$fvmdb->query("
		insert into updated_aedservicing_accessories select * from aedservicing_accessories;
	");
	
	$distincts = $fvmdb->query("
		select distinct partnum, type, length, buylink
		from aed_accessory_types
	");
	
	while($distinct = $distincts->fetch_assoc()){
		//add distinct partnum
		$fvmdb->query("
			insert into updated_aed_accessory_types
			(type, partnum, length, buylink)
			values
			('".$distinct['type']."', '".$distinct['partnum']."', '".$distinct['length']."', '".$distinct['buylink']."')
		");
		$newTypeId = $fvmdb->insert_id;
		//get duplicates
		$duplicates = $fvmdb->query("
			select *
			from aed_accessory_types
			where partnum = '".$distinct['partnum']."'
		");
	
		// the rest can be done with sql updates instead of insertions to preserve original id's 
	
		//find old refrences to duplicates, add them to new database with new reference id
		while($duplicate = $duplicates->fetch_assoc()){
			//add aed_accessory refrences to updated database
			$aedaccessories = $fvmdb->query("
				select *
				from aed_accessories
				where aed_accessory_type_id = '".$duplicate['aed_accessory_type_id']."'
			");
			while($aedaccessory = $aedaccessories->fetch_assoc()){
				$fvmdb->query("
					insert into updated_aed_accessories
					(location_id, aed_id, aed_accessory_type_id, expiration, lot, spare, display)
					values
					('".$aedaccessory['location_id']."', '".$aedaccessory['aed_id']."', '".$newTypeId."', '".$aedaccessory['expiration']."', '".$aedaccessory['lot']."', '".$aedaccessory['spare']."', '".$aedaccessory['display']."')
				");
			}
	
			//add aed_accessory_types_on_models to updated database
			$accessoryTypesOnModels = $fvmdb->query("
				select *
				from aed_accessory_types_on_models
				where aed_accessory_type_id = '".$duplicate['aed_accessory_type_id']."'
			");
			while($accessoryTypesOnModel = $accessoryTypesOnModels->fetch_assoc()){
				$fvmdb->query("
					insert into updated_aed_accessory_types_on_models
					(aed_accessory_type_id, aed_model_id, creation)
					values
					('".$newTypeId."', '".$accessoryTypesOnModel['aed_model_id']."', '".$accessoryTypesOnModel['creation']."')
				");
			}
			
			//update aedservicing_accessories to new type id
			$updateOldServicingAccessories = $fvmdb->query("
				update updated_aedservicing_accessories
				set aed_accessory_type_id = '".$newTypeId."'
				where aed_accessory_type_id = '".$duplicate['aed_accessory_type_id']."'
			");
			$updateNewServicingAccessories = $fvmdb->query("
				update updated_aedservicing_accessories
				set new_aed_accessory_type_id = '".$newTypeId."'
				where aed_accessory_type_id = '".$duplicate['aed_accessory_type_id']."'
			");
			
			//update aedcheck_accessories to new type id
			$updateOldCheckaccessories = $fvmdb->query("
				update updated_aedcheck_accessories
				set aed_accessory_type_id = '".$newTypeId."'
				where aed_accessory_type_id = '".$duplicate['aed_accessory_type_id']."'
			");
			$updateNewCheckAccessories = $fvmdb->query("
				update updated_aedcheck_accessories
				set new_aed_accessory_type_id = '".$newTypeId."'
				where aed_accessory_type_id = '".$duplicate['aed_accessory_type_id']."'
			");
		}
	}
?>