<?php 
/* 
Updated jk_displayusers() on 9/17/12 
Added gethost() on 10/11/12zz
*/

/*
	better print_r function
*/
function print_re($toprint)
{
	echo '<pre>'.print_r($toprint, 1).'</pre>';
}
/*
	removes everything in array list less than given min
*/
function removeminfromarray($input_var_outer,$param) { 

    global $var_to_pass; 
    $var_to_pass = $param; 

    $return_arr = array_filter($input_var_outer, 
		function($input_var_inner) { 
			global $var_to_pass; 
			return ($input_var_inner>$var_to_pass) ? true : false; 
		} 
	); 
	
    //re-key if you want 
    $return_arr = array_merge(array(),$return_arr); 
    return $return_arr; 

} 

/*
	converts boolean to yes/no
*/
function readableboolean($bool) {
	if($bool)
		return 'yes';
	else
		return 'no';
}

/*
	sort arrays
*/
function array_msort($array, $cols)
{
	$colarr = array();
	foreach ($cols as $col => $order) {
		$colarr[$col] = array();
		foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
	}
	$eval = 'array_multisort(';
	foreach ($cols as $col => $order) {
		$eval .= '$colarr[\''.$col.'\'],'.$order.',';
	}
	$eval = substr($eval,0,-1).');';
	eval($eval);
	$ret = array();
	foreach ($colarr as $col => $arr) {
		foreach ($arr as $k => $v) {
			$k = substr($k,1);
			if (!isset($ret[$k])) $ret[$k] = $array[$k];
			$ret[$k][$col] = $array[$k][$col];
		}
	}
	return $ret;

}

/*
	gets the domain and subdomain of a url
*/
function gethost() 
{ 
	$issub = explode(".",$_SERVER['HTTP_HOST']);
	if(count($issub) == 2)
		return 'en';
	else
		return $issub[0];
}

/*
	checks to see if the parameter is empty, empty() does not work as '0' returns false
*/
function isempty($var)
{
	if($var == "")
		return true;
	else
		return false;
}
/*
	checks to see if token is in token database
*/
function isvalidtoken($token)
{
	global $db;
	$query = $db->query("
		select token
		from tokens
		where token = '".$db->real_escape_string($token)."'
	");
	if($row = $query->fetch_assoc())
		return false;
	else
		return true;
}

/*
	checks to see if the userid is in the config_admins table
*/
function isadmin($userid) {
	global $db;
	$query = $db->query("
		select id
		from config_admins
		where userid = '$userid'
	");
	if($row = $query->fetch_assoc())
		return true;
	else
		return false;
}

/*
	function that gathers all the emails from internalemails and spits a sql statment for finding external users
*/
function displayexternalemails() {
	global $db;
	
	$sql = '';	
	$query = $db->query("
		select email
		from config_internalemails
	");
	while($row = $query->fetch_assoc()) {
		$sql .= 'and users.username not like \''.$row['email'].'\'';
	}
	return $sql;
}

/*
	function that gathers all the emails from internalemails and spits out an array
*/
function jk_getinternalemails()
{
	global $db;
	$interalemails = array();
	$query = $db->query("
		select email
		from config_internalemails
	");
	while($row = $query->fetch_assoc()) {
		$internalemails[] = $row['email'];
	}
	return $internalemails;
}

/*
	function that will accept an array as a parameter for strpos
*/
function my_strpos($haystack, $needle) 
{
	if (is_array($needle))  {
		foreach ($needle as $need)  {
			if (strpos($haystack, $need) !== false)  {
				return true;
			}
		}
	} else {
		if (strpos($haystack, $need) !== false)  {
			return true;
		}
	}
	return false;
}

/*
	checks to see if the given date is a valid date, returns true or false, not the actual date.
*/
function isvaliddate($date)
{
	if(!is_numeric(str_replace('/', '', $date)))
		return false;
		
	$pieces = explode("/", $date);
	
	//check to see if month/date are two digits long
	if(strlen($pieces[0]) != 2 || strlen($pieces[1]) != 2)
		return false;
		
	//check to see if the year is four digits long
	if(strlen($pieces[2]) != 4)
		return false;
		
	return checkdate($pieces[0], $pieces[1], $pieces[2]);
}
/*
	this will count all the duplicate values in an array given
*/
function findduplicates($data,$dupval) 
{
	$nb= 0;
	foreach($data as $key => $val)
		if($val == $dupval) 
			$nb++;
	return $nb;
}

/*
	converts a username into the userid (this is bad)
*/
function jk_convertusernametoid($username)
{
	global $db;
	$query = $db->query("
		select id
		from users
		where username = '$username'
		and display = 'yes'
	");
	if($row = $query->fetch_assoc())
		return $row['id'];
	else
		return false;
}

/*

*/
function jk_isuniqueusername($username) {
	global $db;
	$username = $db->real_escape_string($username);
	$query = $db->query("
		select id
		from users
		where username = '$username'
	");				
	if($row = $query->fetch_assoc())
		return false;
	else
		return true;
}

function jk_isuniqueusernameminusme($username, $userid)
{
	global $db;
	$query = $db->query("
		select id
		from users
		where id != '$userid'
		and username = '$username'
	");				
	if($row = $query->fetch_assoc())
		return false;
	else
		return true;
}

/*
	converts a given location id and returns the location name
*/
function jk_convertuseridtoname($id)
{
	global $db;
	$query = $db->query("
		select username
		from users
		where id = '$id'
	");				
	if($row = $query->fetch_assoc())
		return $row['username'];
}

/*
	random generator of numbers and letters, will be used for passwords and keycode generation
*/
function keygen($length=10)
{
	$key = '';
	list($usec, $sec) = explode(' ', microtime());
	mt_srand((float) $sec + ((float) $usec * 100000));
	
	$inputs = array_merge(range(2,9),range('A','H'), range('J','K'), range('M','N'), range('P','Z'));

	for($i=0; $i<$length; $i++) {
		$key .= $inputs{mt_rand(0,30)};
	}
	return $key;
}

/*
	this is a recursive function that gets all the child users from a selected user in the users table
	
function getchildren($array, &$totalarray)
{
	global $db;
	if(is_array($array) && !empty($array)) {
		//print_r($array);
		foreach($array as $arr) {
			if(is_numeric($arr)) {
				$newchildren[] = array();
				$query = $db->query("
					select id
					from users
					where creator = '$arr'
				");
				while ($row = $query->fetch_assoc()) {
					$newchildren[] = $row['id'];
					$totalarray[] = $row['id'];
				}
				
				getchildren($newchildren, $totalarray);
			}
		}
	}
}

/*
	this is a recursive function that gets all the parent users from a selected user in the users table

function getparents($array, &$totalarray)
{
	if(is_array($array)) {
		foreach($array as $arr) {
			global $db;
			$query = $db->query("
				select creator
				from users
				where id = '$arr'
			");
			while ($row = $query->fetch_assoc()) {
				$newparents[] = $row['creator'];
				$totalarray[] = $row['creator'];
			}
			
			//return 
			getparents($newparents, $totalarray);
		}
	}
}
*/
/*
	reusable code that populated two combo boxes that allow the user to change thier views
*/
function jk_locationpicker($userid, $messageformat, $content = 'home')
{
	global $db;
	$admin = (isadmin($userid) ? true : false);
	
	echo '
	<div style="width:840px;overflow:auto;margin:0 auto;padding-bottom:15px;">
		<form style="text-align:center;" action="index.php?content='.$content.'" method="POST" name="orgform">';
			if($admin) {
				echo '
				<div style="width:350px;padding-top:15px;overflow:auto;padding-left:10px;padding-right:10px;float:left;">
					<div style="width:80px;float:left;">'.$messageformat->nav_organization().':</div>
					<select name="org" class="org" style="float:left;width:350px;">';
						echo '<option value="All">'.$messageformat->sel_all().'</option>';
					
						$orglist = $db->query("
							SELECT id, name
							FROM organizations
							where display = 'yes'
							order by name asc
						"); 
						while($row = $orglist->fetch_assoc()) {					
							echo '<option value="'.str_replace(" ", "_" ,$row['id']).'">'.$row['name'].'</option>';
						}
					echo'</select>
				</div>';
				
				
				//location combo
				echo '
				<div style="width:350px;padding-top:15px;overflow:auto;float:left;padding-left:10px;padding-right:10px;display:none;" class="unfilteredlocations">
					<div style="width:80px;float:left;">'.$messageformat->nav_location().':</div>
					<select name="unfilteredlocation" class="location" style="float:left;width:350px;">
						<option class="all" value="All">'.$messageformat->sel_all().'</option>';
							
							$loclist = $db->query("
								SELECT organizations.id as organization, locations.name as location, locations.id as id
								FROM locations, organizations
								where locations.orgid = organizations.id
								and organizations.display = 'yes'
								and locations.display = 'yes'
								order by locations.name asc
							"); 
							while($row = $loclist->fetch_assoc()) {				
								if(str_replace(" ", "_" ,$row['organization']) == '%')
									echo '<option org="all">'.$row['location'].'</option>';
								else
									echo '<option org="'.str_replace(" ", "_" ,$row['organization']).'" value="'.$row['id'].'">'.$row['location'].'</option>';
							}
					echo'</select>
				</div>';
				
				echo '
				<div style="width:350px;padding-top:15px;overflow:auto;float:left;padding-left:10px;padding-right:10px;" class="filteredlocations">
						<div style="width:80px;float:left;">'.$messageformat->nav_location().':</div>
						<select name="location" class="location" style="float:left;width:350px;">
							<option class="all" value="All">'.$messageformat->sel_all().'</option>
						</select>
				</div>';
				
				echo '
				<div style="width:80px;padding-top:15px;padding-left:5px;padding-right:5px;float:left;margin-top:10px;">
					<input type="submit" class="button" value="'.$messageformat->but_change().'" style="padding:2px 10px;" />
				</div>';
			} else {
				$orgrightlist = $db->query("
					select count(orgid) as count
					from rights_orgs
					where userid = '$userid'
					and display = 1
				");
				if($ruw = $orgrightlist->fetch_assoc()) {
					if($ruw['count'] > 1) {
						echo '
						<div style="width:350px;padding-top:15px;overflow:auto;padding-left:10px;padding-right:10px;float:left;">
							<div style="width:80px;float:left;">'.$messageformat->nav_organization().':</div>
							<select name="org" class="org" style="float:left;width:350px;">';
								echo '<option value="All">'.$messageformat->sel_all().'</option>';
							
							$rightlist = $db->query("
								select orgid, o.name
								from users u
								join rights_orgs ro on u.id = ro.userid
								join organizations o on o.id = ro.orgid and o.display = 'yes'
								where u.display = 'yes'
								and ro.display = 1
								and u.id = '$userid'
								order by o.name
							");
							while($raw = $rightlist->fetch_assoc()) {	
								echo '<option value="'.str_replace(" ", "_" ,$raw['orgid']).'">'.$raw['name'].'</option>';
							}
							echo'</select>
						</div>';
						
						//location combo
						$db->query("SET SQL_BIG_SELECTS=1");
						$rightlist = $db->query("
							select count(l.id)
							from users u
							join rights_orgs ro on u.id = ro.userid
							left join rights_locations rl on rl.userid = u.id and rl.orgid = ro.orgid and rl.display = 1
							join locations l on ((l.id = rl.locationid and l.orgid = rl.orgid) or (l.orgid = ro.orgid and rl.id is null and ro.level < 3)) and l.display = 'yes'
							join organizations o on o.id = l.orgid and o.display = 'yes'
							where u.display = 'yes'
							and ro.display = 1
							and u.id = '$userid'
							order by o.name, l.name
						");
						if($ruw = $rightlist->fetch_assoc()) {
							echo '
							<div style="width:350px;padding-top:15px;overflow:auto;float:left;padding-left:10px;padding-right:10px;display:none;" class="unfilteredlocations">
								<div style="width:80px;float:left;">'.$messageformat->nav_location().':</div>
								<select name="unfilteredlocation" class="location" style="float:left;width:350px;">
									<option class="all" value="All">'.$messageformat->sel_all().'</option>';
										$db->query("SET SQL_BIG_SELECTS=1");
										$locrightlist = $db->query("
											select l.id, l.orgid, l.name, l.id as locationid
											from users u
											join rights_orgs ro on u.id = ro.userid
											left join rights_locations rl on rl.userid = u.id and rl.orgid = ro.orgid and rl.display = 1
											join locations l on ((l.id = rl.locationid and l.orgid = rl.orgid) or (l.orgid = ro.orgid and rl.id is null and ro.level < 3)) and l.display = 'yes'
											join organizations o on o.id = l.orgid and o.display = 'yes'
											where u.display = 'yes'
											and ro.display = 1
											and u.id = '$userid'
											order by o.name, l.name
										");
										while($raw = $locrightlist->fetch_assoc()) {
											echo '<option org="'.str_replace(" ", "_" ,$raw['orgid']).'" value="'.$raw['locationid'].'">'.$raw['name'].'</option>';
										}
								echo'</select>
							</div>';
						}
						echo '
						<div style="width:350px;padding-top:15px;overflow:auto;float:left;padding-left:10px;padding-right:10px;" class="filteredlocations">
								<div style="width:80px;float:left;">'.$messageformat->nav_location().':</div>
								<select name="location" class="location" style="float:left;width:350px;">
									<option class="all" value="All">'.$messageformat->sel_all().'</option>
								</select>
						</div>';
						
						echo '
						<div style="width:80px;padding-top:15px;padding-left:5px;padding-right:5px;float:left;margin-top:10px;">
							<input type="submit" class="button" value="'.$messageformat->but_change().'" style="padding:2px 10px;" />
						</div>';
					} else {
						//one org, hidden field
						$rightlist = $db->query("
							select orgid
							from rights_orgs
							where userid = '$userid'
							and display = 1
						");
						if($raw = $rightlist->fetch_assoc()) {
							$org = $raw['orgid'];
							echo '<input type="hidden" name="org" value="'.$org.'" />';
						}
						
						//location combo
						$db->query("SET SQL_BIG_SELECTS=1");
						$rightlist = $db->query("
							select count(l.id) as count
							from users u
							join rights_orgs ro on u.id = ro.userid
							left join rights_locations rl on rl.userid = u.id and rl.orgid = ro.orgid and rl.display = 1
							join locations l on ((l.id = rl.locationid and l.orgid = rl.orgid) or (l.orgid = ro.orgid and rl.id is null and ro.level < 3)) and l.display = 'yes'
							join organizations o on o.id = l.orgid and o.display = 'yes'
							where u.display = 'yes'
							and ro.display = 1
							and u.id = '$userid'
							order by o.name, l.name
						");
						if($ruw = $rightlist->fetch_assoc()) {
							echo '
							<div style="padding-top:15px;display:inline-block;padding-left:10px;padding-right:10px;">
								<div style="width:80px;float:left;">'.$messageformat->nav_location().':</div><br />
								<select name="location" class="location" style="float:left;width:350px;">
									<option class="defaultvalue" value="All">Select One</option>
									<option class="all" value="All">'.$messageformat->sel_all().'</option>';
										$db->query("SET SQL_BIG_SELECTS=1");
										$locrightlist = $db->query("
											select l.id, l.orgid, l.name, l.id as locationid
											from users u
											join rights_orgs ro on u.id = ro.userid
											left join rights_locations rl on rl.userid = u.id and rl.orgid = ro.orgid and rl.display = 1
											join locations l on ((l.id = rl.locationid and l.orgid = rl.orgid) or (l.orgid = ro.orgid and rl.id is null and ro.level < 3)) and l.display = 'yes'
											join organizations o on o.id = l.orgid and o.display = 'yes'
											where u.display = 'yes'
											and ro.display = 1
											and u.id = '$userid'
											order by o.name, l.name
										");
										while($raw = $locrightlist->fetch_assoc()) {
											echo '<option org="'.str_replace(" ", "_" ,$raw['orgid']).'" value="'.$raw['locationid'].'">'.$raw['name'].'</option>';
										}
								echo'</select>
								
								<input type="submit" class="button" value="'.$messageformat->but_change().'" style="padding:2px 10px;margin-left:5px;" />
							</div>';
						}
					}
				}
			}
		echo '
		</form>
	</div>';
}
	
/*
	converts a given organization id and returns the organization name
*/
function jk_convertorgidtoname($id)
{
	global $db;
	$query = $db->query("
			SELECT name
			FROM organizations
			WHERE id = '$id'
			");				
	if($row = $query->fetch_assoc())
		return $row['name'];
	
}
	
/*
	converts a given location id and returns the location name
*/
function jk_convertlocidtoname($id)
{
	global $db;
	$query = $db->query("
		SELECT name
		FROM locations
		WHERE id = '$id'
	");				
	if($row = $query->fetch_assoc())
		return $row['name'];
	
}
	
/*
    Takes a user and password
	connects to database
	checks for a match using mysql password() function
	returns 'success' or 'failure'
*/

function jk_checklogin($username, $password)
{
	global $db;
	$query = $db->query("
		SELECT id, filterorg, filterlocation
		FROM users 
		WHERE username = '$username' 
		and display = 'yes'
		AND password = PASSWORD( '$password' );
	");  
	if($row = $query->fetch_assoc()) {
		$_SESSION['user'] = strtolower($row['id']);
		$_SESSION['org'] = $row['filterorg'];
		$_SESSION['location'] = $row['filterlocation'];
		
		return 'member';
	} else {
		return 'visitor';
	}
	
}
/*
	removes all the numbers from a string
*/
function remove_numbers($string) 
{
	$vowels = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", " ");
	$string = str_replace($vowels, '', $string);
	return $string;
}

/*
	checks to see if the email is valid, only works in php 5.2.14 and up
*/

function isemail($email)
{
    return filter_var( $email, FILTER_VALIDATE_EMAIL );
}

	
/*
	checks to see if phone number is a real number and formats it
*/
function checkPhone($number)
{
	if(preg_match('^[0-9]{3}+-[0-9]{3}+-[0-9]{4}^', $number)){
		return $number;
	} else {
		$items = Array('/\ /', '/\+/', '/\-/', '/\./', '/\,/', '/\(/', '/\)/', '/[a-zA-Z]/');
		$clean = preg_replace($items, '', $number);
		return substr($clean, 0, 3).'-'.substr($clean, 3, 3).'-'.substr($clean, 6, 4);
	}
}

/*
	checks to see if a field has already been entered in before (for emails and passwords)
*/
function jk_checkfordoubles($type, $table, $double) 
{
	global $db;
	$query = $db->query("
		SELECT $type
		FROM $table
		WHERE $type = '$double';
	");  
	if($row = $query->fetch_assoc()) {
		return $row[$type];
	} else {
		return 'free';
	}
	
}
	
/*
	This function will get the name of the user, first name only.  This only input to this is the username.
*/
function jk_getfname($userid) 
{
	global $db;
	$query = $db->query("
		select firstname
		from users
		where id = '$userid'
		and display = 'yes'
	");
	if($row = $query->fetch_assoc())
		return $row['firstname'];
}

/*
	this function uses the userid to get the username
*/
function jk_getemail($userid) 
{
	global $db;
	$query = $db->query("
		select username
		from users
		where id = '$userid'
		and display = 'yes'
	");
	if($row = $query->fetch_assoc())
		return $row['username'];
}

/*
	this function uses the userid to get the username
*/
function jk_getaedserial($aed_id) 
{
	global $db;
	$query = $db->query("
		select serialnumber
		from aeds
		where aed_id = '$aed_id'
		and display = 1
	");
	if($row = $query->fetch_assoc())
		return $row['serialnumber'];
}

/*
	this function will that the organization name and location and return the locationid
*/
function jk_getlocationid($org, $location)
{
	global $db;
	$query = $db->query("
		SELECT id
		FROM locations
		WHERE organization = '$org'
		and location = '$location';
	");
	if($row = $query->fetch_assoc())
		return $row['id'];
}

/*
	this function uses the userid to get the username
*/
function jk_getorgidfromlocation($locationid) 
{
	global $db;
	$query = $db->query("
		select orgid
		from locations
		where id = '$locationid'
	");
	if($row = $query->fetch_assoc())
		return $row['orgid'];
}

function jk_isorgidexternal($orgid) 
{
	global $db;
	$query = $db->query("
		select id
		from organizations
		where id = '$orgid'
		and type = 'External'
	");
	if(is_object($query)) {
		if($row = $query->fetch_assoc())
			return true;
		else
			return false;
	} else
		return false;
}

function checksetuppriv($userid, $priv = 'showsetuphazards', $page = 'setup_hazards')
{
	global $db;
	$getpriv = $db->query("
		select *
		from privileges
		where userid = '$userid'
	");
	if($row = $getpriv->fetch_assoc()) {
		if($row['showsetup'] == 'yes') {
			if($row[$priv] == 'yes')
				return $page;
			else if($row['showsetuphazards'] == 'yes')
				return 'setup_hazards';
			else if($row['showsetuptraining'] == 'yes')
				return 'setup_training';
			else if($row['showsetupimmunizations'] == 'yes')
				return 'setup_immunizations';
			else if($row['showsetuplicenses'] == 'yes')
				return 'setup_licenses';
			else if($row['showsetuphazards'] == 'yes')
				return 'setup_hazards';
			else if($row['showsetupequipment'] == 'yes')
				return 'setup_equipment';
			else if($row['showsetupfirstvoice'] == 'yes')
				return 'setup_firstvoice';
		} else
			return 'home';
	}
}


/*
	this is a recursive function that gets all the next immunizations
*/
function getnextimmunization($immunizationid, &$totalimmarray)
{
	if(is_numeric($immunizationid) && !isempty($immunizationid)) {
		global $db;
		$query = $db->query("
			select nextimmunization
			from immunizations_types
			where id = '$immunizationid'
		");
		while ($row = $query->fetch_assoc()) {
			$nextimmunization = $row['nextimmunization'];
			$totalimmarray[] = $row['nextimmunization'];
		}
		
		//return 
		if($row['nextimmunization'] != 0)
			getnextimmunization($nextimmunization, $totalimmarray);
	}
}

/*
 *  This is a function to generate a a unique fingerprint.
 *  It is possible to have duplicate ID's so we need a way to make them unique.
 *  FirstVoice Training is 1, FirstVoice Manager is 2, Tools Certs is 3, Tools Prescriptions is 4
 */
function generateFingerprint($prefix, $id){
	$location = $prefix;
	$uniqueID = $location * 16777216 + rotateBits($id);
	return str_pad(dechex($uniqueID), 8, "0", STR_PAD_LEFT);
}

/* 
 * A tool to shift 3 bits to the right and rotate extras
 */
function rotateBits($num){
	$leftBit = 0b111111111111111111111000 & $num;
	$newLeftBit = $leftBit >> 3;
	$rightBits = 0b000000000000000000000111 & $num;
	$newRightBits = $rightBits << 21;
	$rotated = $newLeftBit | $newRightBits;
	return $rotated;
}

/*
 * Creates a zip file with a filename of $zipName. Zip up all files in $filenameArray at $location
 * $location is seperated so the heirarchy of folders isn't contained in the zip.
 */
function createZip($zipName, $location, $filenameArray){
	$zip = new ZipArchive();
	$zip->open($zipName, ZipArchive::CREATE);
	foreach($filenameArray as $filename){
		$zip->addFile($location.$filename, $filename);
	}
	$zip->close();
}
?>