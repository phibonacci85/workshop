<!doctype html>
<html>
<head>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
    <script src="../scripts/jquery.tablesorter.js"></script>
</head>
<body>
<h1>Lifelong Advocacy Training Compare</h1>

<?php
// database information 
define('DBHOST', 'localhost');
define('DBUSER', 'tsdemosc_fvmnew');
define('DBPASS', '1105firstvoice');
define('DB', 'tsdemosc_fvmnew');

// old database information 
define('OLDDBHOST', 'localhost');
define('OLDDBUSER', 'tsdemosc_fvmnew');
define('OLDDBPASS', '1105firstvoice');
define('OLDDB', 'tsdemosc_fvmnew_old10315');

//global database
define('GLOBALDBHOST', 'localhost');
define('GLOBALDBUSER', 'tsdemosc_webser3');
define('GLOBALDBPASS', 'Tb.zQqUzsREf');
define('GLOBALDB', 'tsdemosc_websites_live');

@ $db = new mysqli(DBHOST, DBUSER, DBPASS, DB);
@ $olddb = new mysqli(OLDDBHOST, OLDDBUSER, OLDDBPASS, OLDDB);
@ $globaldb = new mysqli(GLOBALDBHOST, GLOBALDBUSER, GLOBALDBPASS, GLOBALDB);

$oldtrainings = $olddb->query("
        SELECT t.*, p.firstname, p.lastname, l.name AS lname
        FROM training_personswithtypes t, persons p, locations l
        WHERE t.personid = p.id
        AND p.locationid = l.id
        AND l.orgid = 1734
        AND t.id > 0
    ");
while ($oldtraining = $oldtrainings->fetch_assoc()) {
    $newtrainings = $db->query("
            SELECT t.*, p.firstname, p.lastname, l.name AS lname
            FROM training_personswithtypes t, persons p, locations l
            WHERE t.personid = p.id
            AND p.locationid = l.id
            AND l.orgid = 1734
            AND t.id = " . $oldtraining['id'] . "
        ");
    if ($newtraining = $newtrainings->fetch_assoc()) {
        if ($oldtraining != $newtraining) {
            echo '
                <table class="tablesorter">
                <col style="color:red;">
                <thead>
                    <tr>
                        <th>Table</th>
                        <th';
            if ($oldtraining['id'] != $newtraining['id']) echo ' style="background-color:pink"';
            echo '>id</th>
                        <th';
            if ($oldtraining['firstname'] != $newtraining['firstname'] || $oldtraining['lastname'] != $newtraining['lastname']) echo ' style="background-color:pink"';
            echo '>person</th>
                        <th';
            if ($oldtraining['typeid'] != $newtraining['typeid']) echo ' style="background-color:pink"';
            echo '>typeid</th>
                        <th';
            if ($oldtraining['classnumber'] != $newtraining['classnumber']) echo ' style="background-color:pink"';
            echo '>classnumber</th>
                        <th';
            if ($oldtraining['lname'] != $newtraining['lname']) echo ' style="background-color:pink"';
            echo '>location</th>
                        <th';
            if ($oldtraining['status'] != $newtraining['status']) echo ' style="background-color:pink"';
            echo '>status</th>
                        <th';
            if ($oldtraining['prefix'] != $newtraining['prefix']) echo ' style="background-color:pink"';
            echo '>prefix</th>
                        <th';
            if ($oldtraining['skillscheck'] != $newtraining['skillscheck']) echo ' style="background-color:pink"';
            echo '>skillscheck</th>
                        <th';
            if ($oldtraining['trainername'] != $newtraining['trainername']) echo ' style="background-color:pink"';
            echo '>trainername</th>
                        <th';
            if ($oldtraining['trainernumber'] != $newtraining['trainernumber']) echo ' style="background-color:pink"';
            echo '>trainernumber</th>
                        <th';
            if ($oldtraining['completiondate'] != $newtraining['completiondate']) echo ' style="background-color:pink"';
            echo '>completiondate</th>
                        <th';
            if ($oldtraining['expiration'] != $newtraining['expiration']) echo ' style="background-color:pink"';
            echo '>expiration</th>
                        <th';
            if ($oldtraining['keycode'] != $newtraining['keycode']) echo ' style="background-color:pink"';
            echo '>keycode</th>
                        <th';
            if ($oldtraining['certid'] != $newtraining['certid']) echo ' style="background-color:pink"';
            echo '>certid</th>
                        <th';
            if ($oldtraining['creation'] != $newtraining['creation']) echo ' style="background-color:pink"';
            echo '>creation</th>
                        <th';
            if ($oldtraining['updated'] != $newtraining['updated']) echo ' style="background-color:pink"';
            echo '>updated</th>
                        <th';
            if ($oldtraining['display'] != $newtraining['display']) echo ' style="background-color:pink"';
            echo '>display</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Old</td>
                        <td>' . $oldtraining['id'] . '</td>
                        <td>' . $oldtraining['firstname'] . ' ' . $oldtraining['lastname'] . '</td>
                        <td>' . $oldtraining['typeid'] . '</td>
                        <td>' . $oldtraining['classnumber'] . '</td>
                        <td>' . $oldtraining['lname'] . '</td>
                        <td>' . $oldtraining['status'] . '</td>
                        <td>' . $oldtraining['prefix'] . '</td>
                        <td>' . $oldtraining['skillscheck'] . '</td>
                        <td>' . $oldtraining['trainername'] . '</td>
                        <td>' . $oldtraining['trainernumber'] . '</td>
                        <td>' . $oldtraining['completiondate'] . '</td>
                        <td>' . $oldtraining['expiration'] . '</td>
                        <td>' . $oldtraining['keycode'] . '</td>
                        <td>' . $oldtraining['certid'] . '</td>
                        <td>' . $oldtraining['creation'] . '</td>
                        <td>' . $oldtraining['updated'] . '</td>
                        <td>' . $oldtraining['display'] . '</td>
                    </tr>
                    <tr>
                        <td>New</td>
                        <td>' . $newtraining['id'] . '</td>
                        <td>' . $newtraining['firstname'] . ' ' . $newtraining['lastname'] . '</td>
                        <td>' . $newtraining['typeid'] . '</td>
                        <td>' . $newtraining['classnumber'] . '</td>
                        <td>' . $newtraining['lname'] . '</td>
                        <td>' . $newtraining['status'] . '</td>
                        <td>' . $newtraining['prefix'] . '</td>
                        <td>' . $newtraining['skillscheck'] . '</td>
                        <td>' . $newtraining['trainername'] . '</td>
                        <td>' . $newtraining['trainernumber'] . '</td>
                        <td>' . $newtraining['completiondate'] . '</td>
                        <td>' . $newtraining['expiration'] . '</td>
                        <td>' . $newtraining['keycode'] . '</td>
                        <td>' . $newtraining['certid'] . '</td>
                        <td>' . $newtraining['creation'] . '</td>
                        <td>' . $newtraining['updated'] . '</td>
                        <td>' . $newtraining['display'] . '</td>
                    </tr>
                </tbody>
                </table>';
        }
    }
}
?>
<!--
<script>
    $(function(){
        $("table").tablesorter({widgets: ['zebra']});
    });
</script>
-->
</body>
</html>