<?php
include('library.php');
include('english.php');
include('swiftmailer/swift_required.php');
define('EMAILBACKGROUND', '#C2E0F6');
define('EMAILFRAME', '#0078C1');
define('TITLE', 'Rescue One');
define('URL', 'http://rescueoneaedmanageronline.com/');
define('LOGO', 'images/rescue_one.jpg');
define('PROGRAM', 'Rescue One AED Manager');
define('SMTP', 'mail.fvdemos.com');
define('FROMEMAIL', 'donotreply@rescueoneaedmanageronline.com');
define('FROMEMAILPASS', '1105firstvoice');
define("FVMDBHOST", "localhost");
define("FVMDBUSER", "root");
define("FVMDBPASS", "Jbake1234");
define("FVMDB", "rescueone");
@ $db = new mysqli( FVMDBHOST, FVMDBUSER, FVMDBPASS, FVMDB);
$clang = new English();
$messageformat = new English();
$servicing_id = $_GET['service_id'];

$locations = $db->query("
	select a.location_id
	from aeds a, aedservicing s
	where a.aed_id = s.aed_id
	and s.aedservicing_id = '".$servicing_id."'
");
$location = $locations->fetch_assoc();

//email the contacts
$contactlist = $db->query("
	select *
	from contacts
	where locationid = '".$location['location_id']."'
	and sendaedcheck = 1
	and display = 'yes'
");
while($rew = $contactlist->fetch_assoc()) {
	$emaillist[] = array('to'=>$rew['email'], 'name'=>$rew['name'], 'language'=>$rew['preferredlanguage']);
}
foreach($emaillist as $key => $val){
$aedservicing = $db->query("
	select s.*, a.location as aedlocation, a.serialnumber as serialnumber, o.name as oname, l.name as lname
	from aedservicing s
	join aeds a on a.aed_id = s.aed_id
	join locations l on a.location_id = l.id
	join organizations o on l.orgid = o.id
	where s.aedservicing_id = '".$servicing_id."'
	order by s.date desc
");

$row = $aedservicing->fetch_assoc();
$body = '<html><head></head><body style="color:#000001;">
										<div style="margin:0 auto;width:970px;background-color:'.EMAILBACKGROUND.';color:#000001;padding:10px 0">
											<div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid '.EMAILFRAME.';margin: 5px auto;">
												<h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
													<img alt="'.TITLE.'" src="'.URL.LOGO.'" style="float:left;">
												</h1>
												<div style="width:100%;height:30px;margin-bottom:5px;background-color:'.EMAILFRAME.';float:left; margin-top:5px;"></div>
												<h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
													'.$clang->ask_emailheading().'
												</h2>
												<br>
												<div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
													'.$clang->ask_emailtopbody($val['name'], $row['serialnumber'], $row['lname'], $row['oname']).'<br />
													<br />';
	
$date = date('m/d/Y', strtotime($row['date']));
$body .= '
															<table style="width:800px;table-layout:fixed;" border="1" cellspacing="0" cellpadding="10">
																<tr>
																	<td>'.$messageformat->ask_serial().'</td>
																	<td style="text-align:center;word-wrap:break-word;width:150px;">'.$row['serialnumber'].'</td>
																</tr>

																<tr>
																	<td>'.$messageformat->ask_date().'</td>
																	<td style="text-align:center;width:150px;">'.$date.'</td>
																</tr>

																<tr>
																	<td>'.$messageformat->ask_location().'</td>
																	<td style="text-align:center;width:150px;">'.$row['aedlocation'].'</td>
																</tr>
															</table>
								
															<h2 style="padding-top:30px;width:800px;">'.$messageformat->ask_generalinfo().'</h2>
															<table style="width:800px;table-layout:fixed;" border="1" cellspacing="0" cellpadding="10">
																<tr>
																	<td>'.$clang->ask_milesdriven().'</td>
																	<td style="text-align:center;width:150px;">'.$row['milesdriven'].'</td>
																</tr>

																<tr>
																	<td>'.$clang->ask_programmanagementexp().'</td>
																	<td style="text-align:center;width:150px;">'.$row['programmanagementexp'].'</td>
																</tr>

																<tr>
																	<td>'.$clang->ask_isaedlocation().'</td>
																	<td style="text-align:center;width:150px;">
																		'.$clang->ask_responses($row['location']).'
																	</td>
																</tr>

																<tr>
																	<td>'.$clang->ask_isaedclean().'</td>
																	<td style="text-align:center;">
																		'.$clang->ask_responses($row['condition']).'
																	</td>
																</tr>

																<tr>
																	<td>'.$clang->ask_isaedinalarm().'</td>
																	<td style="text-align:center;">
																		'.$clang->ask_responses($row['cabinet']).'
																	</td>
																</tr>';
if($row['cabinet'] == 'yes') {
	$body .= '
																		<tr dependant="cabinet">
																			<td>'.$clang->ask_isalarmworking().'</td>
																			<td style="text-align:center;">
																				'.$clang->ask_responses($row['alarm']).'
																			</td>
																		</tr>

																		<tr dependant="cabinet">
																			<td>'.$clang->ask_replacingalarmbattery().'</td>
																			<td style="text-align:center;">
																				'.$clang->ask_responses($row['alarmbattery']).'
																			</td>
																		</tr>';

	if($row['alarmbattery'] == 'yes') {
		$body .= '
																				<tr dependant="alarmbattery">
																					<td style="padding-left:60px;">'.$clang->ask_alarmbatteryinsertiondate().':</td>
																					<td style="text-align:center;">
																						'.$date.'
																					</td>
																				</tr>

																				<tr dependant="alarmbattery">
																					<td style="padding-left:60px;">'.$clang->ask_alarmbatterychangedate().':</td>
																					<td style="text-align:center;">
																						'.date('m/d/Y', strtotime($date .'+1 year')).'
																					</td>
																				</tr>

																				<tr dependant="alarmbattery">
																					<td style="padding-left:60px;">'.$clang->ask_alarmbatteryexpirationdate().':</td>
																					<td style="text-align:center;">
																						'.$row['alarmbatteryexpiration'].'
																					</td>
																				</tr>';
	}
}
	
$body .= '
																<tr>
																	<td>'.$clang->ask_kitpresent().'</td>
																	<td style="text-align:center;">
																		'.$clang->ask_responses($row['rescuekit']).'
																	</td>
																</tr>';
	
if($row['rescuekit'] == 'yes') {
	$body .= '
																		<tr dependant="rescuekit">
																			<td>'.$clang->ask_kitsealed().'</td>
																			<td style="text-align:center;">
																				'.$clang->ask_responses($row['rescuekitcond']).'
																			</td>
																		</tr>';

	if($row['rescuekitcond'] == 'no') {
		$body .= '
																			<tr ndependant="rescuekitcond">
																				<td style="padding-left:60px;">'.$clang->ask_kitinclude().'</td>
																				<td style="text-align:center;">
																					'.$clang->ask_responses($row['kitinclude']).'
																				</td>
																			</tr>';
	}
	if($row['kitinclude'] == 'no') {
		$body .= '
																			<tr ndependant="kitinclude">
																				<td style="padding-left:60px;">'.$clang->ask_kitincludereplace().'</td>
																				<td style="text-align:center;">
																					'.$row['kitincludereplace'].'
																				</td>
																			</tr>';
	}
}
$body .= '
															</table>';
	
//pad
$pad_list = $db->query("
		select t.type as c_type, pt.type as type, t.expiration, t.lot, t.attached, t.`new`, npt.type as new_type, t.new_expiration, t.new_lot
		from aedservicing_pads t
		left join aed_pad_types pt on pt.aed_pad_type_id = t.aed_pad_type_id
		left join aed_pad_types npt on npt.aed_pad_type_id = t.new_aed_pad_type_id
		where t.aedservicing_id = '$servicing_id'
		");
while($raw = $pad_list->fetch_assoc()) {
	$pads[$raw['c_type']] = array('type'=>$raw['type'], 'expiration'=>$raw['expiration'], 'lot'=>$raw['lot'], 'attached'=>$raw['attached'], 'new'=>$raw['new'], 'new_type'=>$raw['new_type'], 'new_expiration'=>$raw['new_expiration'], 'new_lot'=>$raw['new_lot'],);
}

//pak
$pak_list = $db->query("
		select t.type as c_type, pt.type as type, t.expiration, t.lot, t.attached, t.`new`, npt.type as new_type, t.new_expiration, t.new_lot
		from aedservicing_paks t
		left join aed_pak_types pt on pt.aed_pak_type_id = t.aed_pak_type_id
		left join aed_pak_types npt on npt.aed_pak_type_id = t.new_aed_pak_type_id
		where t.aedservicing_id = '$servicing_id'
		");
while($raw = $pak_list->fetch_assoc()) {
	$paks[$raw['c_type']] = array('type'=>$raw['type'], 'expiration'=>$raw['expiration'], 'lot'=>$raw['lot'], 'attached'=>$raw['attached'], 'new'=>$raw['new'], 'new_type'=>$raw['new_type'], 'new_expiration'=>$raw['new_expiration'], 'new_lot'=>$raw['new_lot'],);
}

//battery
$battery_list = $db->query("
		select t.type as c_type, pt.type as type, t.expiration, t.lot, t.attached, t.battery_charge, t.`new`, npt.type as new_type, t.new_expiration, t.new_lot
		from aedservicing_batteries t
		left join aed_battery_types pt on pt.aed_battery_type_id = t.aed_battery_type_id
		left join aed_battery_types npt on npt.aed_battery_type_id = t.new_aed_battery_type_id
		where t.aedservicing_id = '$servicing_id'
		");
while($raw = $battery_list->fetch_assoc()) {
	$batteries[$raw['c_type']] = array('type'=>$raw['type'], 'expiration'=>$raw['expiration'], 'lot'=>$raw['lot'], 'attached'=>$raw['attached'], 'battery_charge'=>$raw['battery_charge'], 'new'=>$raw['new'], 'new_type'=>$raw['new_type'], 'new_expiration'=>$raw['new_expiration'], 'new_lot'=>$raw['new_lot'],);
}

//accessory
$accessory_list = $db->query("
		select t.type as c_type, pt.type as type, t.expiration, t.lot, t.attached, t.`new`, npt.type as new_type, t.new_expiration, t.new_lot
		from aedservicing_accessories t
		left join aed_accessory_types pt on pt.aed_accessory_type_id = t.aed_accessory_type_id
		left join aed_accessory_types npt on npt.aed_accessory_type_id = t.new_aed_accessory_type_id
		where t.aedservicing_id = '$servicing_id'
		");
while($raw = $accessory_list->fetch_assoc()) {
	$accessories[$raw['c_type']] = array('type'=>$raw['type'], 'expiration'=>$raw['expiration'], 'lot'=>$raw['lot'], 'attached'=>$raw['attached'], 'new'=>$raw['new'], 'new_type'=>$raw['new_type'], 'new_expiration'=>$raw['new_expiration'], 'new_lot'=>$raw['new_lot'],);
}

//check to see if there are any pads count($pads) > 0 won't work here.
if(!isempty($pads['primary_pad']['type']) || !isempty($pads['pediatric_primary_pad']['type']) || !isempty($pads['pediatric_spare_pad']['type']) || !isempty($pads['spare_1_pad']['type']) || !isempty($pads['spare_2_pad']['type'])) {
	$body .= '
																<h2 style="padding-top:30px;">'.$clang->ack_pads().'</h2>
																<table style="width:800px;" border="1" cellspacing="0" cellpadding="10">';

	if(!isempty($pads['primary_pad']['type']) || !isempty($pads['pediatric_primary_pad']['type'])) {
		$body .= '
																		<tr>
																			<td>'.$clang->ask_ispadattached().'</td>
																			<td style="text-align:center;">&nbsp;</td>
																		</tr>';
	}

	if(!isempty($pads['primary_pad']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['primary_pad']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['primary_pad']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['primary_pad']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($pads['primary_pad']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($pads['primary_pad']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewpads().'</td>
																				<td style="text-align:center;">'.($pads['primary_pad']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($pads['primary_pad']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div>
																					<div style="width:400px;float:left;">'.$pads['primary_pad']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$pads['primary_pad']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$pads['primary_pad']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}
	if(!isempty($pads['pediatric_primary_pad']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($pads['pediatric_primary_pad']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($pads['pediatric_primary_pad']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewpads().'</td>
																				<td style="text-align:center;">'.($pads['pediatric_primary_pad']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($pads['pediatric_primary_pad']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div>
																					<div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}

	if(!isempty($pads['spare_1_pad']['type']) || !isempty($pads['spare_2_pad']['type']) || !isempty($pads['pediatric_spare_pad']['type'])) {
		$body .= '
																		<tr>
																			<td>'.$clang->ask_issparepadattached().'</td>
																			<td style="text-align:center;">&nbsp;</td>
																		</tr>';
	}

		
	if(!isempty($pads['spare_1_pad']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['spare_1_pad']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['spare_1_pad']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['spare_1_pad']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($pads['spare_1_pad']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($pads['spare_1_pad']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewsparepads().'</td>
																				<td style="text-align:center;">'.($pads['spare_1_pad']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($pads['spare_1_pad']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div>
																					<div style="width:400px;float:left;">'.$pads['spare_1_pad']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$pads['spare_1_pad']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$pads['spare_1_pad']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}
	if(!isempty($pads['spare_2_pad']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['spare_2_pad']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['spare_2_pad']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['spare_2_pad']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($pads['spare_2_pad']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($pads['spare_2_pad']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewsparepads().'</td>
																				<td style="text-align:center;">'.($pads['spare_2_pad']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($pads['spare_2_pad']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div>
																					<div style="width:400px;float:left;">'.$pads['spare_2_pad']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$pads['spare_2_pad']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$pads['spare_2_pad']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}
	if(!isempty($pads['pediatric_spare_pad']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($pads['pediatric_spare_pad']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($pads['pediatric_spare_pad']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewsparepedpads().'</td>
																				<td style="text-align:center;">'.($pads['pediatric_spare_pad']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($pads['pediatric_spare_pad']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pad().'</div>
																					<div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}

	$body .= '
																</table>';
}

//check to see if there are any paks count($paks) > 0 won't work here.
if(!isempty($paks['primary_pak']['type']) || !isempty($paks['pediatric_primary_pak']['type']) || !isempty($paks['pediatric_spare_pak']['type']) || !isempty($paks['spare_1_pak']['type']) || !isempty($paks['spare_2_pak']['type'])) {
	$body .= '
																<h2 style="padding-top:30px;">'.$clang->ack_paks().'</h2>
																<table style="width:800px;" border="1" cellspacing="0" cellpadding="10">';

	if(!isempty($paks['primary_pak']['type']) || !isempty($paks['pediatric_primary_pak']['type'])) {
		$body .= '
																		<tr>
																			<td>'.$clang->ask_ispakattached().'</td>
																			<td style="text-align:center;">&nbsp;</td>
																		</tr>';
	}

	if(!isempty($paks['primary_pak']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['primary_pak']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['primary_pak']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['primary_pak']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($paks['primary_pak']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($paks['primary_pak']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewpaks().'</td>
																				<td style="text-align:center;">'.($paks['primary_pak']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($paks['primary_pak']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div>
																					<div style="width:400px;float:left;">'.$paks['primary_pak']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$paks['primary_pak']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$paks['primary_pak']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}
	if(!isempty($paks['pediatric_primary_pak']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($paks['pediatric_primary_pak']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($paks['pediatric_primary_pak']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewpaks().'</td>
																				<td style="text-align:center;">'.($paks['pediatric_primary_pak']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($paks['pediatric_primary_pak']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div>
																					<div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}

	if(!isempty($paks['spare_1_pak']['type']) || !isempty($paks['spare_2_pak']['type']) || !isempty($paks['pediatric_spare_pak']['type'])) {
		$body .= '
																		<tr>
																			<td>'.$clang->ask_issparepakattached().'</td>
																			<td style="text-align:center;">&nbsp;</td>
																		</tr>';
	}

		
	if(!isempty($paks['spare_1_pak']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['spare_1_pak']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['spare_1_pak']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['spare_1_pak']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($paks['spare_1_pak']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($paks['spare_1_pak']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewsparepaks().'</td>
																				<td style="text-align:center;">'.($paks['spare_1_pak']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($paks['spare_1_pak']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div>
																					<div style="width:400px;float:left;">'.$paks['spare_1_pak']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$paks['spare_1_pak']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$paks['spare_1_pak']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}
	if(!isempty($paks['spare_2_pak']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['spare_2_pak']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['spare_2_pak']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['spare_2_pak']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($paks['spare_2_pak']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($paks['spare_2_pak']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewsparepaks().'</td>
																				<td style="text-align:center;">'.($paks['spare_2_pak']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($paks['spare_2_pak']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div>
																					<div style="width:400px;float:left;">'.$paks['spare_2_pak']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$paks['spare_2_pak']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$paks['spare_2_pak']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}
	if(!isempty($paks['pediatric_spare_pak']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($paks['pediatric_spare_pak']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($paks['pediatric_spare_pak']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_issparepakattached().'</td>
																				<td style="text-align:center;">'.($paks['pediatric_spare_pak']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($paks['pediatric_spare_pak']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div>
																					<div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}

	$body .= '
																</table>';
}

//check to see if there are any batteries count($batteries) > 0 won't work here.
if(!isempty($batteries['primary_battery']['type']) || !isempty($batteries['spare_battery']['type'])) {
	$body .= '
																<h2 style="padding-top:30px;">'.$clang->ack_batteries().'</h2>
																<table style="width:800px;" border="1" cellspacing="0" cellpadding="10">';

	if(!isempty($batteries['primary_battery']['type'])) {
		$body .= '
																		<tr>
																			<td>'.$clang->ask_isbatteryattached().'</td>
																			<td style="text-align:center;">&nbsp;</td>
																		</tr>';
	}

	if(!isempty($batteries['primary_battery']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_battery().'</div><div style="width:400px;float:left;">'.$batteries['primary_battery']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$batteries['primary_battery']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$batteries['primary_battery']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($batteries['primary_battery']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($batteries['primary_battery']['attached'] == 1 && $brandid == 1){
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isbatteryengerybars().'</td>
																				<td style="text-align:center;width:150px;">'.$batteries['primary_battery']['battery_charge'].'
																				</td>
																			</tr>';
		}

		if($batteries['primary_battery']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewbattery().'</td>
																				<td style="text-align:center;">'.($batteries['primary_battery']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($batteries['primary_battery']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_battery().'</div>
																					<div style="width:400px;float:left;">'.$batteries['primary_battery']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$batteries['primary_battery']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$batteries['primary_battery']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}

	if(!isempty($batteries['spare_battery']['type'])) {
		$body .= '
																		<tr>
																			<td>'.$clang->ask_issparebatteryattached().'</td>
																			<td style="text-align:center;">&nbsp;</td>
																		</tr>';
	}

		
	if(!isempty($batteries['spare_battery']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div><div style="width:400px;float:left;">'.$batteries['spare_battery']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$batteries['spare_battery']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$batteries['spare_battery']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($batteries['spare_battery']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($batteries['spare_battery']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewsparebattery().'</td>
																				<td style="text-align:center;">'.($batteries['spare_battery']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($batteries['spare_battery']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_battery().'</div>
																					<div style="width:400px;float:left;">'.$batteries['spare_battery']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$batteries['spare_battery']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$batteries['spare_battery']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}

	$body .= '
																</table>';
}

//check to see if there are any accessories count($accessories) > 0 won't work here.
if(!isempty($accessories['primary_accessory']['type']) || !isempty($accessories['spare_accessory']['type'])) {
	$body .= '
																<h2 style="padding-top:30px;">'.$clang->ack_accessories().'</h2>
																<table style="width:800px;" border="1" cellspacing="0" cellpadding="10">';

	if(!isempty($accessories['primary_accessory']['type'])) {
		$body .= '
																		<tr>
																			<td>'.$clang->ask_isaccessoryattached().'</td>
																			<td style="text-align:center;">&nbsp;</td>
																		</tr>';
	}

	if(!isempty($accessories['primary_accessory']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessory().'</div><div style="width:400px;float:left;">'.$accessories['primary_accessory']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$accessories['primary_accessory']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$accessories['primary_accessory']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($accessories['primary_accessory']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($accessories['primary_accessory']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewaccessory().'</td>
																				<td style="text-align:center;">'.($accessories['primary_accessory']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($accessories['primary_accessory']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessory().'</div>
																					<div style="width:400px;float:left;">'.$accessories['primary_accessory']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$accessories['primary_accessory']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$accessories['primary_accessory']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}

	if(!isempty($accessories['spare_accessory']['type'])) {
		$body .= '
																		<tr>
																			<td>'.$clang->ask_isspareaccessoryattached().'</td>
																			<td style="text-align:center;">&nbsp;</td>
																		</tr>';
	}

		
	if(!isempty($accessories['spare_accessory']['type'])) {
		$body .= '
																		<tr>
																			<td>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_pak().'</div><div style="width:400px;float:left;">'.$accessories['spare_accessory']['type'].'</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$accessories['spare_accessory']['lot'].'&nbsp;</div>
																				<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$accessories['spare_accessory']['expiration'].'&nbsp;</div>
																			</td>
																			<td style="text-align:center;">'.($accessories['spare_accessory']['attached'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																		</tr>';

		if($accessories['spare_accessory']['attached'] == 0) {
			$body .= '
																			<tr>
																				<td>'.$clang->ask_isinstallingnewspareaccessory().'</td>
																				<td style="text-align:center;">'.($accessories['spare_accessory']['new'] ? $clang->ack_responses('yes') : $clang->ack_responses('no')).'</td>
																			</tr>';
		}

		if($accessories['spare_accessory']['new'] == 1) {
			$body .= '
																			<tr>
																				<td>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessory().'</div>
																					<div style="width:400px;float:left;">'.$accessories['spare_accessory']['new_type'] .'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessorylot().'</div>
																					<div style="width:400px;float:left;">'.$accessories['spare_accessory']['new_lot'].'&nbsp;</div>
																					<div style="width:200px;float:left;font-weight:bold;">'.$clang->aed_accessoryexp().'</div>
																					<div style="width:400px;float:left;">'.$accessories['spare_accessory']['new_expiration'] .'&nbsp;</div>
																				</td>
																				<td style="text-align:center;">&nbsp;</td>
																			</tr>';
		}
	}

	$body .= '
																</table>';
}
	
$body .= '
															<h2 style="padding-top:30px;">'.$clang->ask_servicingby().'</h2>
															<table style="width:800px;" border="1" cellspacing="0" cellpadding="10" id="servicertable">
																<tr>
																	<td colspan="2">'.$clang->ask_servicerpromise().'</td>
																</tr>
																<tr>
																	<td>'.$clang->ask_digitalsignature().'</td>
																	<td>'.$row['digitalsignature'].'</td>
																</tr>
																<tr>
																	<td>'.$clang->ask_dateverified().'</td>
																	<td>'.$row['date'].'</td>
																</tr>
																<tr>
																	<td colspan="2">'.$clang->ask_servicerepverified().'</td>
																</tr>
																<tr>
																	<td>'.$clang->ask_clientrepname().'</td>
																	<td>'.$row['clientrepname'].'</td>
																</tr>
																<tr>
																	<td>'.$clang->ask_clientreptitle().'<span class="required">*</span></td>
																	<td>'.$row['clientreptitle'].'</td>
																</tr>
																<tr>
																	<td>'.$clang->ask_clientrepemail().'</td>
																	<td>'.$row['clientrepemail'].'</td>
																</tr>
																<tr>
																	<td>'.$clang->ask_clientrepphone().'</td>
																	<td>'.$row['clientrepphone'].'</td>
																</tr>
																<tr>
																	<td>'.$clang->ask_dateverifiedclientlocation().'</td>
																	<td>'.$row['date'].'</td>
																</tr>
															</table>
								
															<h2 style="padding-top:30px;width:800px;">'.$clang->ask_comments().'</h2>
															<p style="padding:10px;overflow:auto;width:780px;height:100px;border:1px solid black;margin-bottom:10px;" name="comments">'.nl2br($row['comment']).'</p>
													</div>
												</div>
											 </div>
											</div>
										</div>
										<div style="width:970px;margin:0 auto;color:#000001;background-color:white;">'.$clang->mes_pleasedonotreply().'</div>
										<br>
										<div style="width:970px;margin:0 auto;color:#000001;">'.$clang->mes_tscopyright().'</div>
									</body></html>';

echo $body."<br />";
$addbody = preg_replace ( "'<[^>]+>'U", "", $body);
$addbody = preg_replace("/\n+\s/", "\n", trim($addbody));

//send email to contact
$transport = Swift_SmtpTransport::newInstance(SMTP, 25)
->setUsername(FROMEMAIL)
->setPassword(FROMEMAILPASS);
$mailer = Swift_Mailer::newInstance($transport);
$message = Swift_Message::newInstance()
->setSubject('servicing receipt')
->setFrom(array(FROMEMAIL => PROGRAM))
->setTo('jbaker@think-safe.com')
->setBody($body, 'text/html')
->addPart($addbody, 'text/plain');
$result = $mailer->send($message);
}
?>