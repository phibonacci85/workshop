<?php
    require_once('swiftmailer/swift_required.php');
    $emailTo = array('jbaker@think-safe.com');
    $bodyHTML = '
        <html>
        <head></head>
        <body style="width:800px;">
            <table style="display:table;background-color:#FFF;width:800px;border:5px solid #0078C1;border-collapse:collapse;">
                <tr>
                    <td style="text-align:center;"><h1 style="margin:10px;">New Locations</h1></td>
                </tr>
                <tr style="display:table-row;height:1em;background-color:#0078C1;">
                    <td></td>
                </tr>
                <tr style="display:table-row;">
                    <td style="padding:10px;width:100%;">
                        <table>
                            <tr><td><h3 style="margin:10px 0 5px 0;">Koko FitClub of Harvard - Customized Exercise & Nutrition</h3></td></tr>
                            <tr><td style="width:25px">Address: </td><td>111 Big Muscles Ave, Iowa City, IA 52246</td></tr>
                            <tr><td style="width:25px">Phone: </td><td>(515) 555-5555</td></tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        </html>
    ';

    $kokoBodyHTML = '
        <html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>
        <body style="width:800px;">
            <table style="display:table;background-color:#FFF;width:800px;border:5px solid #0078C1;border-collapse:collapse;">
                <tr>
                    <td style="text-align:center;"><h1 style="margin:10px;">Koko Fit Club</h1></td>
                </tr>
                <tr style="display:table-row;height:1em;background-color:#0078C1;">
                    <td></td>
                </tr>
                <tr style="display:table-row;">
                    <td style="padding:10px;width:100%;">
                        <table>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Harvard - Customized Exercise &amp; Nutrition</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>285 Ayer Rd, Harvard, MA 01451, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(978) 772-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub Cedarhurst</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>134 Cedarhurst Ave, Cedarhurst, NY 11516, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(516) 295-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>12520 Perkins Rd #104, Baton Rouge, LA 70810, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(225) 223-6540</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>1 NE 2nd St, Oklahoma City, OK 73104, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(405) 605-3020</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>55 S Valle Verde Dr, Henderson, NV 89012, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(702) 487-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>17614 140th Ave NE, Woodinville, WA 98072, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(425) 398-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club Roseville North</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>711 Pleasant Grove Blvd #120, Roseville, CA 95678, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(916) 783-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>15 Columbia Rd, Pembroke, MA 02359, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(617) 784-0981</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>190 East Stacy Road #207, Allen, TX 75002, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(972) 912-3005</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>5105 Eldorado Pkwy, Frisco, TX 75033, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(817) 488-5552</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>16510 Cleveland St, Redmond, WA 98052, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(425) 250-7737</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>1799 W Broadway St, Idaho Falls, ID 83402, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(208) 552-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Sandy</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>1842 9400 S, Sandy, UT 84093, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(801) 571-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>388 Overland Ave, Burley, ID 83318, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(208) 679-3488</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>2970 St Rose Pkwy #140, Henderson, NV 89052, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(702) 487-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>4485 First St, Livermore, CA 94551, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(925) 443-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>499 San Ramon Valley Blvd, Danville, CA 94526, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(925) 743-0802</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>4542 Dublin Blvd, Dublin, CA 94568, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(925) 556-5559</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Tonka Bay/Excelsior</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>5633 Manitou Rd, Excelsior, MN 55331, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(952) 955-9766</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Chanhassen</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>820 W 78th St, Chanhassen, MN 55317, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(952) 679-7401</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>1626 S Wells Ave Suite #110, Meridian, ID 83642, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(208) 906-2135</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>3160 E 17th St #120, Ammon, ID 83406, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(208) 552-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Rapid City</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>5312 Sheridan Lake Rd #105, Rapid City, SD 57702, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(605) 299-4866</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Savage</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>7709 Egan Dr, Savage, MN 55378, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(952) 679-7400</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>1148 Winston Churchill Blvd, Oakville, ON L6J 0A3, Canada</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(905) 829-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>6245 E Bell Rd #113, Scottsdale, AZ 85254, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(480) 588-6602</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Grand Rapids</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>2830 E Beltline Ave NE, Grand Rapids, MI 49525, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(616) 288-2918</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Grand Rapids</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>Grand Rapids, MI 49546, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(616) 301-5366</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Grand Rapids - Cascade</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>2845 Thornhills Ave SE, Grand Rapids, MI 49546, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(616) 301-5366</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>42545 Ford Rd, Canton, MI 48187, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(734) 667-2971</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>Orchard Lake Rd, West Bloomfield Township, MI 48322, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(248) 862-2409</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Nashua</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>14 Broad St, Nashua, NH 03064, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(603) 509-3798</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Westford - Customized Exercise &amp; Nutrition</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>174 Littleton Road, Exit 32 off 495, Westford, MA 01886, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(978) 692-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>234 Washington St, Hudson, MA 01749, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(508) 319-1861</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Sudbury</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>447 Boston Post Rd, Sudbury, MA 01776, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(978) 443-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>117 W Central St, Natick, MA 01760, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(508) 651-0353</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub of Needham</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>850 Highland Ave, Needham, MA 02494, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(781) 444-0011</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>193 Boston Post Rd W, Marlborough, MA 01752, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(508) 481-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>342 Great Rd, Acton, MA 01720, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(978) 263-9348</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>39 Harvard St, Brookline, MA 02445, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(617) 566-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>77 Spring St, West Roxbury, MA 02132, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(617) 325-4800</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>907 Main St, Walpole, MA 02081, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(508) 921-3230</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>200 Ledgewood Pl, Rockland, MA 02370, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(781) 753-9495</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>377 Chauncy St, Mansfield, MA 02048, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(508) 339-3741</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>25 Taunton St, Plainville, MA 02762, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(774) 643-6082</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>15 Robert Dr #2a, South Easton, MA 02375, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(508) 230-5656</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>1899 Ocean St, Marshfield, MA 02050, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(781) 834-3200</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub Shrewsbury</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>97 Boston Turnpike, Shrewsbury, MA 01545, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(508) 425-3277</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko FitClub Hanover</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>1978 Washington St, Hanover, MA 02339, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(781) 878-8422</td></tr>
                            
                                <tr><td colspan="2"><h3 style="margin:10px 0 5px 0;">Koko Fit Club</h3></td></tr>
                                <tr><td style="width:25px">Address: </td><td>2519 S Shields St, Fort Collins, CO 80526, United States</td></tr>
                                <tr><td style="width:25px">Phone: </td><td>(970) 658-9882</td></tr>
                            
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        </html>
    ';

    //create email
    $transport = Swift_SmtpTransport::newInstance('server.tsdemos.com', 25)
        ->setUsername('donotreply@tsdemos.com')
        ->setPassword('1105firstvoice');
    $mailer = Swift_Mailer::newInstance($transport);
    $message = Swift_Message::newInstance()
        ->setSubject('New Locations')
        ->setFrom(array('donotreply@tsdemos.com' => 'Location Search'))
        ->setTo($emailTo)
        ->setBody($kokoBodyHTML, 'text/html')
        ->addPart(strip_tags($bodyHTML), 'text/plain');
    $mailer->send($message);
?>