<?php 
	define('FVMDBHOST', 'localhost');
	define('FVMDBUSER', 'root');
	define('FVMDBPASS', 'Jbake1234');
	define('FVMDB', 'fvmbeta');
	@ $fvmdb = new mysqli( FVMDBHOST, FVMDBUSER, FVMDBPASS, FVMDB);
	
	echo "Checking Batteries...";
	$batteries = $fvmdb->query("
		select *
		from aed_batteries b
		join aeds a on a.aed_id = b.aed_id
		where b.display = '1'
	");
	while($battery = $batteries->fetch_assoc()){
		$modelID = $battery['aed_model_id'];
		$typeID = $battery['aed_battery_type_id'];
		$locationID = $battery['location_id'];
		$valid = $fvmdb->query("
			select *
			from aed_battery_types_on_models
			where aed_battery_type_id = '".$typeID."'
			and aed_model_id = '".$modelID."'
		");
		if($valid->num_rows == 0){
			$information = $fvmdb->query("
				select l.name, m.model, t.type, b.brand, o.name as org
				from locations l, aed_models m, aed_battery_types t, organizations o, aed_brands b
				where l.id = '".$locationID."'
				and m.aed_model_id = '".$modelID."'
				and t.aed_battery_type_id = '".$typeID."'
				and o.id = l.orgid
				and b.aed_brand_id = m.aed_brand_id
			");
			$info = $information->fetch_assoc();
			echo '<br />INCOMPATIBLE Battery Type: '.$typeID.'  Model ID: '.$modelID.' Location ID: '.$locationID;
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Organization: '.$info['org'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Location: '.$info['name'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brand: '.$info['brand'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Model: '.$info['model'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type: '.$info['type'];
		}
	}
	
	echo "<br /><br />Checking Accessories...";
	$accessories = $fvmdb->query("
		select *
		from aed_accessories ac
		join aeds a on a.aed_id = ac.aed_id
	");
	while($accessory = $accessories->fetch_assoc()){
		$modelID = $accessory['aed_model_id'];
		$typeID = $accessory['aed_accessory_type_id'];
		$locationID = $accessory['location_id'];
		$valid = $fvmdb->query("
			select *
			from aed_accessory_types_on_models
			where aed_accessory_type_id = '".$typeID."'
			and aed_model_id = '".$modelID."'
		");
		if($valid->num_rows == 0){
			$information = $fvmdb->query("
				select l.name, m.model, t.type, b.brand, o.name as org
				from locations l, aed_models m, aed_accessory_types t, organizations o, aed_brands b
				where l.id = '".$locationID."'
				and m.aed_model_id = '".$modelID."'
				and t.aed_accessory_type_id = '".$typeID."'
				and o.id = l.orgid
				and b.aed_brand_id = m.aed_brand_id
			");
			$info = $information->fetch_assoc();
			echo '<br />INCOMPATIBLE Accessory Type: '.$typeID.'  Model ID: '.$modelID.' Location ID: '.$locationID;
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Organization: '.$info['org'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Location: '.$info['name'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brand: '.$info['brand'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Model: '.$info['model'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type: '.$info['type'];
		}
	}
	
	echo "<br /><br />Checking Pads...";
	$pads = $fvmdb->query("
		select *
		from aed_pads p
		join aeds a on a.aed_id = p.aed_id
	");
	while($pad = $pads->fetch_assoc()){
		$modelID = $pad['aed_model_id'];
		$typeID = $pad['aed_pad_type_id'];
		$locationID = $pad['location_id'];
		$valid = $fvmdb->query("
			select *
			from aed_pad_types_on_models
			where aed_pad_type_id = '".$typeID."'
			and aed_model_id = '".$modelID."'
		");
		if($valid->num_rows == 0){
			$information = $fvmdb->query("
				select l.name, m.model, t.type, b.brand, o.name as org
				from locations l, aed_models m, aed_pad_types t, organizations o, aed_brands b
				where l.id = '".$locationID."'
				and m.aed_model_id = '".$modelID."'
				and t.aed_pad_type_id = '".$typeID."'
				and o.id = l.orgid
				and b.aed_brand_id = m.aed_brand_id
			");
			$info = $information->fetch_assoc();
			echo '<br />INCOMPATIBLE Pad Type: '.$typeID.'  Model ID: '.$modelID.' Location ID: '.$locationID;
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Organization: '.$info['org'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Location: '.$info['name'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brand: '.$info['brand'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Model: '.$info['model'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type: '.$info['type'];
		}
	}
	
	echo "<br /><br />Checking Paks...";
	$paks = $fvmdb->query("
		select *
		from aed_paks p
		join aeds a on a.aed_id = p.aed_id
	");
	while($pak = $paks->fetch_assoc()){
		$modelID = $pak['aed_model_id'];
		$typeID = $pak['aed_pak_type_id'];
		$locationID = $pak['location_id'];
		$valid = $fvmdb->query("
			select *
			from aed_pak_types_on_models
			where aed_pak_type_id = '".$typeID."'
			and aed_model_id = '".$modelID."'
		");
		if($valid->num_rows == 0){
			$information = $fvmdb->query("
				select l.name, m.model, t.type, b.brand, o.name as org
				from locations l, aed_models m, aed_pak_types t, organizations o, aed_brands b
				where l.id = '".$locationID."'
				and m.aed_model_id = '".$modelID."'
				and t.aed_pak_type_id = '".$typeID."'
				and o.id = l.orgid
				and b.aed_brand_id = m.aed_brand_id
			");
			$info = $information->fetch_assoc();
			echo '<br />INCOMPATIBLE Pak Type: '.$typeID.'  Model ID: '.$modelID.' Location ID: '.$locationID;
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Organization: '.$info['org'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Location: '.$info['name'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Brand: '.$info['brand'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Model: '.$info['model'];
			echo '<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Type: '.$info['type'];
		}
	}
	
	echo "<br /><br />DONE...";
?>