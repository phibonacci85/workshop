<?php
  define("FVMDBHOST", "localhost");
  define("FVMDBUSER", "tsdemosc_fvmnew");
  define("FVMDBPASS", "1105firstvoice");
  define("FVMDB", "tsdemosc_fvmnew");
  @ $fvmdb = new mysqli( FVMDBHOST, FVMDBUSER, FVMDBPASS, FVMDB);

  $aeds = $fvmdb->query("
    select *
    from aeds
    where display = 1
  ");
  while($aed = $aeds->fetch_assoc()){
    $aedid = $aed['aed_id'];
    $aedlocation = $aed['location_id'];
    
    $pads = $fvmdb->query("
      select *
      from aed_pads
      where aed_id = ".$aedid."
    ");
    while($pad = $pads->fetch_assoc()){
      if($pad['location_id'] != $aedlocation){
        echo "AED location: ".$aedlocation."   Pad Location: ".$pad['location_id'].'<br />';
        $updated = $fvmdb->query("
          update aed_pads
          set location_id = ".$aedlocation."
          where aed_pad_id = ".$pad['aed_pad_id']."
        ");
        if(!$updated)
          echo $fvmdb->error;
      }
    }
    
    $paks = $fvmdb->query("
      select *
      from aed_paks
      where aed_id = ".$aedid."
    ");
    while($pak = $paks->fetch_assoc()){
      if($pak['location_id'] != $aedlocation){
        echo "AED location: ".$aedlocation."   Pak Location: ".$pak['location_id'].'<br />';
        $updated = $fvmdb->query("
          update aed_paks
          set location_id = ".$aedlocation."
          where aed_pak_id = ".$pak['aed_pak_id']."
        ");
        if(!$updated)
          echo $fvmdb->error;
      }
    }
    
    $batteries = $fvmdb->query("
      select *
      from aed_batteries
      where aed_id = ".$aedid."
    ");
    while($battery = $batteries->fetch_assoc()){
      if($battery['location_id'] != $aedlocation){
        echo "AED location: ".$aedlocation."   Battery Location: ".$battery['location_id'].'<br />';
        $updated = $fvmdb->query("
          update aed_batteries
          set location_id = ".$aedlocation."
          where aed_battery_id = ".$battery['aed_battery_id']."
        ");
        if(!$updated)
          echo $fvmdb->error;
      }
    }
    
    $accessories = $fvmdb->query("
      select *
      from aed_accessories
      where aed_id = ".$aedid."
    ");
    while($accessory = $accessories->fetch_assoc()){
      if($accessory['location_id'] != $aedlocation){
        echo "AED location: ".$aedlocation."   Accessory Location: ".$accessory['location_id'].'<br />';
        $updated = $fvmdb->query("
          update aed_accessories
          set location_id = ".$aedlocation."
          where aed_accessory_id = ".$accessory['aed_accessory_id']."
        ");
        if(!$updated)
          echo $fvmdb->error;
      }
    }
  }
?>