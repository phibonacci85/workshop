<?php
	class DBManager {
		public static function getFVM($localhost = true) {
			$host = 'tsdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew');
		}

		public static function getFVMBeta($localhost = true) {
			$host = 'tsdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmbeta');
		}

		public static function getRescueOne($localhost = true) {
			$host = 'fvdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'fvdemos', 'Hawkeye17!', 'fvdemos_rescueone');
		}

		public static function getRescueReady($localhost = true) {
			$host = 'fvdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'fvdemos', 'Hawkeye17!', 'fvdemos_rescueready');
		}

		public static function getFVT($localhost = true) {
			$host = 'tsdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoice');
		}

		public static function getWEB($localhost = true) {
			$host = 'tsdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_websites_live');
		}

		public static function getWEBTesting($localhost = true) {
			$host = 'tsdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_websites_live_testing');
		}

		public static function getStore($localhost = true) {
			$host = 'tsdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew_store');
		}

		public static function getBetaStore($localhost = true) {
			$host = 'tsdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmbeta_store');
		}

		public static function getEMS($localhost = true) {
			$host = 'tsdemos.com';
			if($localhost) $host = 'localhost';
			return new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_ems');
		}
	}
?>