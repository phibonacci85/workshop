<?php

$filename = 'non_closed_opportunities.csv';

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}


@ $db = new mysqli( $host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
$fp = fopen('non_closed_opportunities_readable.csv', 'w');
fputcsv($fp, array('opportunity', 'account', 'date_modified', 'deleted', 'name', 'date_entered', 'date_modified', 'Modified User Id', 'created by', 'description', 'deleted', 'assigned user id', 'opportunity_type', 'campaign', 'lead source', 'amount', 'usdollars', 'currency', 'closed date', 'next step', 'sales stage', 'probability'));


if (($handle = fopen($filename, "r")) !== FALSE) {
    $line = 0;
    while (($data = fgetcsv($handle, 9000, ",")) !== FALSE) {
        if($line == 0) {
            // echo $data[0]."&nbsp".$data[1]."<br />"; // Column Names
        } else {
            fputcsv($fp, array($data[1], getAccount($data[2]), $data[3], $data[4], $data[7], $data[8], $data[9], getUser($data[10]), getUser($data[11]), $data[12], $data[13], getUser($data[14]), $data[15], $data[16], $data[17], $data[18], $data[19], $data[20], $data[21], $data[22], $data[23], $data[24]));
            echo 'Inserted Account: '.getAccount($data[2]).'<br />';
        }
        $line++;
    }
    fclose($handle);
    fclose($fp);
} else {
    echo 'Can not open file';
}
?>