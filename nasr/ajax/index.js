/**
 * Created by John Baker on 6/16/2016.
 */
$(document).ready(function () {
    $('#userform').submit(function (e) {
        e.preventDefault();
        $.ajax({
                type: 'POST',
                url: 'dosomething.php',
                data: $('#userform').serialize(),
                dataType: 'json',
                beforeSend: function () {
                    //put a loading spinner somewhere
                }
            })
            .done(function (data) {
                console.log(data);
                $('#results').empty();

                if (!data.success) {
                    $('#results').append("failed");
                } else {
                    $('#results').append("success!");
                    $('#results').append(data['firstName'] + " " + data['last']);
                }
            })
            .fail(function (data, textStatus, errorThrown) {
                console.log('ajax failed');
                console.log(data);
                console.log(textStatus);
                console.log(errorThrown);
            });
    });
});