<?php
require('../swiftmailer/swift_required.php');

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

date_default_timezone_set('America/Chicago');

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicetraining');

$users = $db->query("
    SELECT *
    FROM users u, coursecompletion cc
    WHERE u.username = cc.username
    AND courseName = 'Train the Trainer'
");
while ($user = $users->fetch_assoc()) {
    $to = $user['email'];
    if (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
        echo 'ERROR invalid email: ' . $user['email'] . '<br />';
        continue;
    }
    $subject = 'Important Update Guidelines 2015/2016 IOHSA Urgent';
    $body = '
        <html><head></head>
        <body>
        <table>
            <tr>
                <td style="width:50%"><img align="left" src="http://iohsa.org/images/IOHSA-block.jpg" width="250" height="84" /></td>
                <td></td>
                <td></td>
                <td style="width:50%"><img align="right" src="http://r.firstvoice.us/images/logos/FirstVoice_logo.jpg" width="301" height="84" /></td>
            </tr>
        </table>
        
        <table style="border:2px #0078c1 solid;">
            <tr>
                <td>
                    <table>
                        <tr>
        
                            <td style="background-color:#c2e0f6;border-bottom:2px #0078c1 solid;">
                    
                                <p style="font-family:Arial;">Call with any questions on content of email and if it applies to you.</p>
        
                                <p style="font-family:Arial;">Many of you have been waiting to update your Instructor Certification with us until we finalized the 2015/2016 Guidelines update. Thank you for your patience!</p>
        
                                <p style="font-family:Arial;">We are contacting you today to let you know that, in accordance with updates from the International Liaison Committee on Resuscitation (ILCOR) and the Emergency
                                Cardiovascular Care (ECC) headed up by the American Heart Association, Red Cross (and other member organizations) that released in October 2015, you must complete new training modules to show you understand the updated guidelines.  In order to remain in good standing as an International Occupational Health &Safety Association (IOHSA) and First Voice Training Network instructor, these training modules must be completed by the following date: <span style="color:#da2028;">July 31, 2016.</span></p>
        
                                <p style="font-family:Arial;">For more information on these updates and on how to complete the required training modules, please read on.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:2px #0078c1 solid;">
        
                                <p style="font-family:Arial;font-weight:bold;color:#da2028;text-decoration:underline;">What Exactly Is Being Updated?</p>
        
                                <p style="font-family:Arial;">In October 2015, ILCOR released the 2015 International Consensus on Cardiopulmonary Resuscitation (CPR) and Emergency Cardiovascular Science (ECC) with Treatment Recommendations (CoSTR). Directly related to this document, the AHA also released its 2015 American Heart Association (AHA) Guidelines Update for CPR and ECC to include first aid updates.</p> 
        
                                <p style="font-family:Arial;">These updates have a direct impact on <span style="font-weight:bold;background-color:yellow;text-decoration:underline;">ALL</span> First Voice and IOHSA CPR and first aid training programs and reference materials. Consequently, First Voice has been working with IOHSA diligently to update our training programs and materials so that you have access to the most current, accurate resources and can keep providing the quality educational services that make First Voice trainers so exceptional.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:2px #0078c1 solid;">
                                <p style="font-family:Arial;font-weight:bold;color:#da2028;text-decoration:underline;">Are My Current Programs and Materials Safe to Use?</p>
        
                                <p style="font-family:Arial;">Yes. While the 2015 updates are intended to improve the way we manage medical emergencies, your current training materials are still effective and safe. However, you may use them only until the July 31, 2016, deadline. After that deadline, you need to have completed all of the updated courses and appropriate training modules before we can issue print cards, or IOHSA certificates for your training center.  Any instructor that does not renew and update your course requirements will not be allowed to maintain your standing as a qualified IOHSA and First Voice instructor.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:2px #0078c1 solid;">
                                <p style="font-family:Arial;font-weight:bold;color:#da2028;text-decoration:underline;">Got It. Now, How Do I Get the Updated Programs and Materials?</p>
        
                                <p style="font-family:Arial;">We\'re glad you asked! There are a few things you need to know, and we\'ve covered them below.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:2px #0078c1 solid;">
                                <p style="font-family:Arial;font-weight:bold;color:#0078c1;text-decoration:underline;">Train the Trainer G2015 Course</p>
        
                                <p style="font-family:Arial;">The first thing you\'ll need to do in order to teach the new 2015 programs is to successfully complete the Train the Trainer G2015 Course by July 31, 2016. This will come with a new course for CPR, AED, First Aid, and BBP based upon the program you choose:  <a href="http://trainershop.think-safe.com/includes/product.php?na=1&o=1">Lay Rescuer</a>  or <a href="http://trainershop.think-safe.com/includes/product.php?na=1&o=2">Basic Life Support (BLS) Healthcare Provider (HCP)</a>. You can purchase the course update and required updated materials <a href="http://trainershop.think-safe.com/includes/home.php?na=1&o=1">here</a>.</p>
        
                                <p style="font-family:Arial;"><b>NOTE:</b> You will receive a new 2-year certificate upon completion of this course and not have to pay any renewal fees for another 2 years!  
                                <span style="background-color:yellow;">Plus, it includes your updated video / presentation materials and updated handouts (no charge) for your unlimited reprint ongoing!</span></p>
        
                                <p style="font-family:Arial;">As a trainer, we know you have an extremely important job. You educate. You empower. You make the world safer. And we appreciate the good work you do. That\'s why, upon purchasing and completing the course, you\'ll receive the 2-year updated certificate-to offset the cost and ensure you receive the value from the time you invest in keeping current within the field of CPR and first aid science.  We also are providing you with an UNCOMPLETED (you finish at your leisure) CPR, AED, First Aid, and BBP course that you should review and pass on a timely basis in the coming year.</p>
        
                                <p style="font-family:Arial;font-weight:bold;color:#0078c1;">DVD Releases and <u>NEW!</u> PowerPoint Updates -</p>
                                For those of you who use DVD modules in your CPR, first aid, and/or AED lessons, new DVD releases are included where appropriate. The BLS HCP module includes your DVD in the renewal cost!</p>
        
        
                                <p style="font-family:Arial;">FOR THOSE OF YOU that are training under Lay Rescuer module, IOHSA has chosen to update their materials to include Power points that can be taught during your skills review sessions or at your in person classes.  A G2015-compliant PowerPoint for Lay Rescuer CPR, AED, First aid, BBP, and/or universal precautions course has been provided and is INCLUDED AT NO CHARGE (2 versions). The PowerPoint summarizes the IOHSA program into a three- to four hour custom presentation and it is available in Adult or Adult-Child-Infant versions.</p>
        
                                <p style="font-family:Arial;"><a href="http://trainershop.think-safe.com/includes/product.php?na=1&o=3">Click here</a> to purchase the power points or your appropriate Instructor Package. The power point is accompanied by a 1 hour orientation and webinar/conference call session with a lead trainer at Think Safe to explain the presentation aspects (customized for industrial/workplaces) of this high acclaimed customized course. If you want to purchase the power point as an add-on you can do that as well (BLS HCP module does NOT include the power points).It will be shipped to you on a flash drive or there are download options available.</p>
        
                                <p style="font-family:Arial;font-weight:bold;color:#0078c1;text-decoration:underline;">G2015-Compliant Course Materials</p>
        
                                <p style="font-family:Arial;">Although you can use your current programs and materials until the July 31 deadline, we understand that some of you may wish to start integrating the new updates as soon as possible. Therefore, <a href="http://trainershop.think-safe.com/includes/home.php?na=1&o=1">BUY NOW!</a></p>
        
                                <p style="font-family:Arial;">Once you have purchased your 2 year renewal, we\'ve created new G2015-compliant training and reference materials for immediate use. Feel free to use these in lessons and as handouts. You can find these G2015 updated training materials by logging into your First Voice Training account and checking the Skills Refresher section. Please note that this section includes the following materials for your use as a trainer: Videos, Cognitive Tests and Answer Keys, Motor Skills Check/Test Forms, Training Refresher Suggestions, Training Handouts, Training Scenarios (over 70 pages of scenarios to practice), Intake Questionnaire Templates (record keeping form), Roster Templates, and other Reference materials (under Test and Answer Keys section). Please look at your Refresher Skills section after your purchase (and entry of your new keycode into <a target="_blank" href="http://www.firstvoicetraining.com/">www.firstvoicetraining.com</a>) and you\'ll find the large assortment of useful tools we have provided you and updated for G2015!</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="">
                                <p style="font-family:Arial;font-weight:bold;color:#da2028;text-decoration:underline;">Thank you.</p>
        
                                <p style="font-family:Arial;">Thank you for your patience as we continue to adjust our programs and materials for the 2015 updates. We will stay in touch with you about the upcoming deadline of July 31st to help you transition.  In the meantime, please <a href="http://think-safe.com/site/contact">contact us</a> with any questions or concerns.</p>
        
                                <p style="font-family:Arial;font-weight:bold;">And thank you, also, for helping us to daily make minutes matter.</p>
                            </td>
                        </tr>
                        
                    </table>
        
                </td>								
            </tr>
        </table>
        
        <table>
            <tr>
                <td style="width:50%" align="left">
                    <table>
                        <tr>
                            <td align="center"><img src="http://iohsa.org/images/IOHSA-block.jpg" width="350" height="128" /></td>
                        </tr>
                        <tr>
                            <td align="center">International Occupational Health & Safety Association</td>
                        </tr>
                        <tr>
                            <td align="center"><a href="www.iohsa.org">www.iohsa.org</td>
                        </tr>
                        <tr>
                            <td align="center">Copyright 2011-2016</td>
                        </tr>
                    </table>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="width:50%" align="right">
                    <table>
                        <tr>
                            <td align="center"><img src="http://r.firstvoice.us/images/logos/FirstVoice_logo.jpg" width="301" height="84" /></td>
                        </tr>
                        <tr>
                            <td align="center">4080 1st Avenue NE, Suite 110</td>
                        </tr>
                        <tr>
                            <td align="center">Cedar Rapids, IA 52402</td>
                        </tr>
                        <tr>
                            <td align="center"><a href="http://www.firstvoicetraining.com">http://www.firstvoicetraining.com</a></td>
                        </tr>
                        <tr>
                            <td align="center">Toll Free: 888-473-1777</td>
                        </tr>
                        <tr>
                            <td align="center">Phone: (319)377-5125</td>
                        </tr>
                        <tr>
                            <td align="center">Fax: (319)377-4224</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </body></html>
    ';

    $transport = Swift_SmtpTransport::newInstance('server.tsdemos.com', 25)
        ->setUsername('donotreply@tsdemos.com')
        ->setPassword('1105firstvoice');
    $mailer = Swift_Mailer::newInstance($transport);
    $message = Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom(array('donotreply@tsdemos.com' => 'First Voice Training'))
        ->setTo($to)
        ->setBody($body, 'text/html');
    $result = $mailer->send($message);
    echo 'Sent to: ' . $to . '<br />';
}
?>