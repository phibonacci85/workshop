<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 4/4/2016
 * Time: 8:19 AM
 */
include('scripts/library.php');
include('scripts/swiftmailer/swift_required.php');

if ($_SERVER['HTTP_HOST'] == 'tsdemos.com') {
    @ $fvtdb = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicetraining');
    @ $fvmdb = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew');
} else if ($_SERVER['HTTP_HOST'] == 'tsdemos.com') {
    @ $fvtdb = new mysqli('tsdemos.com', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicetraining');
    @ $fvmdb = new mysqli('tsdemos.com', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew');
} else {
    @ $fvtdb = new mysqli('tsdemos.com', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicetraining');
    @ $fvmdb = new mysqli('tsdemos.com', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew');
}

$count = 0;
$keycodes = $fvtdb->query("
    SELECT *
    FROM keycodes
    WHERE (orderNum = 'ROCKWELL-CORALV-012216' OR orderNum = 'ROCKWELL-CR-012216')
    AND packageName = 'Adult CPR / AED & First Aid / BloodBorne Pathogens Part 1'
");
while ($keycode = $keycodes->fetch_assoc()) {
    $completions = $fvtdb->query("
        SELECT *
        FROM coursecompletion
        WHERE keycode = '" . $keycode['code'] . "'
        AND courseName = 'Adult CPR'
    ");
    if ($completion = $completions->fetch_assoc()) {
        $count++;

        $persons = $fvmdb->query("
            SELECT *
            FROM persons
            WHERE email = '" . $completion['username'] . "'
        ");
        $person = $persons->fetch_assoc();

        $name = $person['firstname'] . ' ' . $person['lastname'];
        $sid = $person['id'];
        $un = $completion['username'];
        $kc = $keycode['code'];
        $demo = $keycode['demo'];

        $fvmdb->query("
					insert into training_personswithtypes
					(personid, typeid, status, prefix, skillscheck, completiondate, expiration, keycode, creation, display, certid)
					values
					('$sid', '308', 'uncompleted', 'none', 'no', '', '', '" . $kc . "', '" . date('Y-m-d H:i:s') . "', 'yes', 'NULL')
				");
        $cid = $fvmdb->insert_id;

        $insertCourse = $fvtdb->query("
            INSERT INTO coursecompletion (
                        username,
                        keycode,
                        fvmclassid,
                        courseName,
                        demo,
                        variant,
                        attempt,
                        grade,
                        completed,
                        downloadcode,
                        creationDate,
                        lastUpdated,
                        certid
					)VALUES(
                        '" . $un . "',
                        '" . $kc . "',
                        '" . $cid . "',
                        'Emergency Oxygen',
                        '" . $demo . "',
                        'nm',
                        '0',
                        '0',
                        'no',
                        'none',
                        '" . date('Y-m-d H:i:s') . "',
                        '" . date('Y-m-d H:i:s') . "',
                        'NULL'
                    )
        ");

        $insertId = $fvtdb->insert_id;
        $certid = generateFingerprint(1, $insertId);

        $fvmdb->query("
            UPDATE training_personswithtypes
            SET certid = '" . $certid . "'
            WHERE id = '" . $cid . "'
        ");
        $fvtdb->query("
            UPDATE coursecompletion
            SET certid = '" . $certid . "'
            WHERE id = '" . $insertId . "'
        ");


        echo 'Person Id: ' . $sid . '<br />';
        echo 'Username: ' . $un . '<br />';
        echo 'Keycode: ' . $kc . '<br />';
        echo 'Class Id (training_personswithtypes): ' . $cid . '<br />';
        echo 'Completion Id (coursecompletion): ' . $insertId . '<br />';
        echo 'Cert Id: ' . $certid . '<br />';

        //send email to contact
        $transport = Swift_SmtpTransport::newInstance('mail.fvdemos.com', 25)
            ->setUsername('noreply@firstvoicetraining.com')
            ->setPassword('1105firstvoice');
        $mailer = Swift_Mailer::newInstance($transport);
        $message = Swift_Message::newInstance()
            ->setSubject('New First Voice Training Courses has been added to your account')
            ->setFrom(array('noreply@firstvoicetraining.com' => 'First Voice Training'))
            ->setTo($un)
            ->setBcc(array('jbaker@think-safe.com', 'cullrich@think-safe.com'))
            ->setBody('<html><head></head><body style="color:black;">
						 <div style="margin:0 auto;width:970px;background-color:#C2E0F6;color:black;padding: 5px 10px;">
							<div style="width:910px;margin:20px;background-color:#0078C1;padding: 5px 10px;">
								<div style="width:870px;margin:10px;color:black;background-color:white;padding: 5px 10px;">
									<h1 style="font-size:18pt;width:850px;">
										<img alt="First Voice Training" src="http://firstvoicetraining.com/images/FVTLogo.png">
									</h1>
									<h1 style="font-size:18pt;width:850px;">' . $name . ',</h1>
									<p style="width:850px;font-size:12pt;">
										A refresher course has been added to your profile.  Please go to firstvoicetraining.com to complete it.
									</p>
									<br />
									 If you have any questions or concerns please contact 319-377-5125 or <a href="http://firstvoicetraining.com/index.php?content=contact">
									 support@firstvoicetraining.com</a>.
								</div>
							</div>
						 </div><div style="width:850px;margin:0 auto;color:black;background-color:white;">Please do not reply to this email.  We are unable to respond to inquiries sent to this address.  For immediate answers to your questions, visit our Contact Us page at the top of any First Voice Training page.</div><br /><div style="width:850px;margin:0 auto">Copyright &copy;  2014 Think Safe Inc.</div></body></html>', 'text/html')
            ->addPart($name . ', A refresher course has been added to your profile..  Please go to firstvoicetraining.com to complete it', 'text/plain');

        $result = $mailer->send($message);
    }
}
echo '<br /><br />Total Count: ' . $count;
