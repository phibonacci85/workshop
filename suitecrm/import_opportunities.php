<?php

/**
 * Connect to Database
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');

function getUserId($userName)
{
    global $db;
    $name = explode(' ', $userName);
    $userId = '';
    $users = $db->query("
        SELECT *
        FROM users
        WHERE first_name = '" . $name[0] . "'
        AND last_name = '" . $name[1] . "'
    ");
    if ($user = $users->fetch_assoc()) {
        $userId = $user['id'];
    }
    return $userId;
}

function getAccountId($accountName)
{
    global $db;
    $accountId = '';
    $accounts = $db->query("
        SELECT *
        FROM accounts
        WHERE name = '" . $accountName . "'
    ");
    if ($account = $accounts->fetch_assoc()) {
        $accountId = $account['id'];
    }
    return $accountId;
}


$filename = 'new_opps.csv';

if (($handle = fopen($filename, "r")) !== FALSE) {
    $line = 0;
    while (($data = fgetcsv($handle, 9000, ",")) !== FALSE) {
        if ($line == 0) {
            // echo $data[0]."&nbsp".$data[1]."<br />"; // Column Names
        } else {

            $opportunities = $db->query("
                SELECT *
                FROM opportunities
                WHERE id = '" . $data[0] . "'
            ");
            if ($opportunities->num_rows > 0) {
                $oppName = str_replace("'", "''", $data[1]);
                $updateOpp = $db->query("
                    UPDATE opportunities
                    SET name = '" . $oppName . "', 
                        date_entered = '" . date('Y-m-d H:i:s', strtotime($data[2])) . "', 
                        date_modified = '" . date('Y-m-d H:i:s', strtotime($data[3])) . "', 
                        modified_user_id = '" . getUserId($data[4]) . "', 
                        created_by = '" . getUserId($data[5]) . "', 
                        description = '" . $data[6] . "', 
                        deleted = '" . $data[7] . "', 
                        assigned_user_id = '" . getUserId($data[8]) . "', 
                        opportunity_type = '" . $data[9] . "', 
                        campaign_id = '" . $data[10] . "', 
                        lead_source = '" . $data[11] . "', 
                        amount = '" . $data[12] . "', 
                        amount_usdollar = '" . $data[13] . "', 
                        currency_id = '" . $data[14] . "', 
                        date_closed = '" . date('Y-m-d H:i:s', strtotime($data[15])) . "', 
                        next_step = '" . $data[16] . "', 
                        sales_stage = '" . $data[17] . "', 
                        probability = '" . $data[18] . "'
                    WHERE id = '" . $data[0] . "'
                ");
                if (!$updateOpp) {
                    echo 'ERROR updating opportunity: ' . $db->error;
                }
            } else {
                $insertOpp = $db->query("
                    INSERT INTO opportunities
                    (id, name, date_entered, date_modified, modified_user_id, created_by, description, deleted, assigned_user_id, opportunity_type, campaign_id, lead_source, amount, amount_usdollar, currency_id, date_closed, next_step, sales_stage, probability)
                    VALUES(
                      '" . $data[0] . "', 
                      '" . $oppName . "', 
                      '" . date('Y-m-d H:i:s', strtotime($data[2])) . "', 
                      '" . date('Y-m-d H:i:s', strtotime($data[3])) . "', 
                      '" . getUserId($data[4]) . "', 
                      '" . getUserId($data[5]) . "', 
                      '" . $data[6] . "', 
                      '" . $data[7] . "', 
                      '" . getUserId($data[8]) . "', 
                      '" . $data[9] . "', 
                      '" . $data[10] . "', 
                      '" . $data[11] . "', 
                      '" . $data[12] . "', 
                      '" . $data[13] . "', 
                      '" . $data[14] . "', 
                      '" . date('Y-m-d H:i:s', strtotime($data[15])) . "', 
                      '" . $data[16] . "', 
                      '" . $data[17] . "', 
                      '" . $data[18] . "'
                    )
                ");
                if (!$insertOpp) {
                    echo 'ERROR inserting opportunity: ' . $db->error;
                    echo '<br />Id: ' . $data[0];
                }
            }

            $accountsOpportunities = $db->query("
                SELECT *
                FROM accounts_opportunities
                WHERE opportunity_id = '" . $data[0] . "'
                AND account_id = '" . getAccountId($data[19]) . "'
            ");
            if ($accountsOpportunities->num_rows == 0) {
                $insertAccountOpportunity = $db->query("
                    INSERT INTO accounts_opportunities
                    (id, opportunity_id, account_id)
                    VALUES 
                    ('" . $data[0] . "', '" . $data[0] . "', '" . getAccountId($data[19]) . "')
                ");
                if (!$insertAccountOpportunity) {
                    echo 'ERROR inserting account opportunity link: ' . $db->error;
                }
            }

            $customs = $db->query("
                SELECT *
                FROM opportunities_cstm
                WHERE id_c = '" . $data[0] . "'
            ");
            if ($customs->num_rows == 0) {
                $insertCustom = $db->query("
                    INSERT INTO opportunities_cstm
                    (id_c)
                    VALUES 
                    ('" . $data[0] . "')
                ");
                if (!$insertCustom) {
                    echo 'ERROR inserting opportnity customs: ' . $db->error;
                }
            }
        }
        $line++;
    }
    fclose($handle);
} else {
    echo 'Can not open file';
}
?>