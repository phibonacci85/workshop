<?php
/**
 * Connect to Database
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');

function getUserId($userName)
{
    $userName = str_replace("'", "''", $userName);
    global $db;
    $name = explode(' ', $userName);
    $userId = $userName;
    $users = $db->query("
        SELECT *
        FROM users
        WHERE first_name = '" . $name[0] . "'
        AND last_name = '" . $name[1] . "'
    ");
    if ($user = $users->fetch_assoc()) {
        $userId = $user['id'];
    }
    return $userId;
}

function getContactId($contactName)
{
    $contactName = str_replace("'", "''", $contactName);
    global $db;
    $name = explode(' ', $contactName);
    $contactId = $contactName;
    $contacts = $db->query("
        SELECT *
        FROM contacts
        WHERE first_name = '" . $name[0] . "'
        AND last_name = '" . $name[1] . "'
    ");
    if ($contact = $contacts->fetch_assoc()) {
        $contactId = $contact['id'];
    }
    return $contactId;
}

function getAccountId($accountName)
{
    $accountName = str_replace("'", "''", $accountName);
    global $db;
    $accountId = $accountName;
    $accounts = $db->query("
        SELECT *
        FROM accounts
        WHERE name = '" . $accountName . "'
    ");
    if ($account = $accounts->fetch_assoc()) {
        $accountId = $account['id'];
    }
    return $accountId;
}

function getLeadId($leadName)
{
    $leadName = str_replace("'", "''", $leadName);
    global $db;
    $name = explode(' ', $leadName);
    $leadId = $leadName;
    $leads = $db->query("
        SELECT *
        FROM leads
        WHERE first_name = '".$name[0]."'
        and last_name = '".$name[1]."'
    ");
    if ($lead = $leads->fetch_assoc()) {
        $leadId= $lead['id'];
    }
    return $leadId;
}


$filename = 'new_tasks.csv';

if (($handle = fopen($filename, "r")) !== FALSE) {
    $line = 0;
    $newTasksAdded = 0;
    if ($line == 319) {
        $DEBUG = true;
    }
    while (($data = fgetcsv($handle, 9000, ",")) !== FALSE) {
        if ($line == 0) {
            // echo $data[0]."&nbsp".$data[1]."<br />"; // Column Names
        } else {
            echo $line . ' ';
            $contactName = $data[16];
            $contactName = str_replace('CONTACT: ', '', $contactName);
            $contactName = str_replace('LEAD: ', '', $contactName);
            $accountName = str_replace("'", "''", $data[15]);
            $taskName = str_replace("'", "''", $data[1]);
            $taskDescription = str_replace("'", "''", $data[6]);
            $tasks = $db->query("
                SELECT *
                FROM tasks
                WHERE id = '" . $data[0] . "'
            ");
            if ($tasks->num_rows == 0) {
                $insertTask = $db->query("
                    INSERT INTO tasks
                    (id, name, date_entered, date_modified, modified_user_id, created_by, description, deleted, assigned_user_id, status, date_due_flag, date_due, date_start_flag, date_start, parent_type, parent_id, contact_id, priority)
                    VALUES (
                      '" . $data[0] . "', 
                      '" . $taskName . "', 
                      '" . date('Y-m-d H:i:s', strtotime($data[2])) . "', 
                      '" . date('Y-m-d H:i:s', strtotime($data[3])) . "', 
                      '" . getUserId($data[8]) . "', 
                      '" . getUserId($data[8]) . "', 
                      '" . $taskDescription . "', 
                      '" . $data[7] . "', 
                      '" . getUserId($data[8]) . "', 
                      '" . $data[9] . "', 
                      '" . $data[10] . "', 
                      '" . date('Y-m-d H:i:s', strtotime('+17 hour', strtotime($data[11]))) . "', 
                      '" . $data[12] . "', 
                      '" . date('Y-m-d H:i:s', strtotime('+17 hour', strtotime($data[13]))) . "', 
                      '" . $data[14] . "', 
                      '" . getAccountId($accountName) . "', 
                      '" . getContactId($contactName) . "', 
                      '" . $data[17] . "'
                    )
                ");
                if (!$insertTask) {
                    echo 'ERROR: inserting ' . $db->error;
                } else {
                    echo 'New Task Added<br />';
                    $newTasksAdded++;
                }
            } else {
                echo 'Already Exists: ' . $data[0] . '...';
                $updateTask = $db->query("
                    UPDATE tasks
                    SET name = '" . $taskName . "', 
                        date_entered = '" . date('Y-m-d H:i:s', strtotime($data[2])) . "', 
                        date_modified = '" . date('Y-m-d H:i:s', strtotime($data[3])) . "', 
                        modified_user_id = '" . getUserId($data[8]) . "', 
                        created_by = '" . getUserId($data[8]) . "', 
                        description = '" . $taskDescription . "', 
                        deleted = '" . $data[7] . "', 
                        assigned_user_id = '" . getUserId($data[8]) . "', 
                        STATUS = '" . $data[9] . "', 
                        date_due_flag = '" . $data[10] . "', 
                        date_due = '" . date('Y-m-d H:i:s', strtotime('+17 hour', strtotime($data[11]))) . "', 
                        date_start_flag = '" . $data[12] . "', 
                        date_start = '" . date('Y-m-d H:i:s', strtotime('+17 hour', strtotime($data[13]))) . "', 
                        parent_type = '" . $data[14] . "', 
                        parent_id = '" . getAccountId($accountName) . "', 
                        contact_id = '" . getContactId($contactName) . "', 
                        priority = '" . $data[17] . "'
                    WHERE id = '" . $data[0] . "'
                ");
                echo 'Updated!<br />';
            }
        }
        $line++;
    }
    echo '<br /><br />New Tasks added: ' . $newTasksAdded;
    fclose($handle);
} else {
    echo 'Can not open file';
}

?>