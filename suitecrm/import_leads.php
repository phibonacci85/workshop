<?php

/**
 * Connect to Database
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');

function getUser($userId)
{
    global $db;
    $userId = str_replace("'", "''", $userId);
    $userName = $userId;
    $users = $db->query("
        SELECT *
        FROM users
        WHERE id = '" . $userId . "'
    ");
    if ($user = $users->fetch_assoc()) {
        $userName = $user['first_name'] . ' ' . $user['last_name'];
    }
    return $userName;
}

function getAccount($accountId)
{
    global $db;
    $accountId = str_replace("'", "''", $accountId);
    $accountName = $accountId;
    $accounts = $db->query("
        SELECT *
        FROM accounts
        WHERE id = '" . $accountId . "'
    ");
    if ($account = $accounts->fetch_assoc()) {
        $accountName = $account['name'];
    }
    return $accountName;
}

function getContact($contactId)
{
    global $db;
    $contactId = str_replace("'", "''", $contactId);
    $contactName = $contactId;
    $contacts = $db->query("
        SELECT *
        FROM contacts
        WHERE id = '" . $contactId . "'
    ");
    if ($contact = $contacts->fetch_assoc()) {
        $contactName = 'CONTACT: ' . $contact['first_name'] . ' ' . $contact['last_name'];
    }
    return $contactName;
}

function getLead($leadId)
{
    global $db;
    $leadId = str_replace("'", "''", $leadId);
    $leadName = $leadId;
    $leads = $db->query("
        SELECT *
        FROM leads
        WHERE id = '" . $leadId . "'
    ");
    if ($lead = $leads->fetch_assoc()) {
        $leadName = 'LEAD: ' . $lead['first_name'] . ' ' . $lead['last_name'];
    }
    return $leadName;
}

$line = 0;
$leads = $sfdb->query("
    SELECT *
    FROM Lead
");
while ($lead = $leads->fetch_assoc()) {
    if ($lead['IsConverted'] == '1'){
        $line++;
        continue;
    }
    $insertLead = $db->query("
        INSERT INTO leads (
          id,
          date_entered,
          date_modified,
          modified_user_id,
          created_by,
          description,
          deleted,
          assigned_user_id,
          salutation, 
          first_name, 
          last_name, 
          title, 
          photo, 
          department, 
          do_not_call, 
          phone_home, 
          phone_mobile, 
          phone_work, 
          phone_other, 
          phone_fax, 
          primary_address_street, 
          primary_address_city, 
          primary_address_state, 
          primary_address_postalcode, 
          primary_address_country, 
          alt_address_street, 
          alt_address_city, 
          alt_address_state, 
          alt_address_postalcode, 
          alt_address_country, 
          assistant, 
          assistant_phone, 
          converted, 
          refered_by, 
          lead_source, 
          lead_source_description, 
          status, 
          status_description, 
          reports_to_id, 
          account_name, 
          account_description, 
          contact_id, 
          account_id, 
          opportunity_id, 
          opportunity_name, 
          opportunity_amount, 
          campaign_id, 
          birthdate, 
          portal_name, 
          portal_app, 
          website
        ) VALUES (
          '" . str_replace("'", "''", $lead['Id']) . "',
          '" . str_replace("'", "''", $lead['CreatedDate']) . "',
          '" . str_replace("'", "''", $lead['LastModifiedDate']) . "',
          '" . str_replace("'", "''", $lead['LastModifiedById']) . "',
          '" . str_replace("'", "''", $lead['CreatedById']) . "',
          '" . str_replace("'", "''", $lead['Description']) . "',
          '" . str_replace("'", "''", $lead['IsDeleted']) . "',
          '" . str_replace("'", "''", $lead['OwnerId']) . "',
          '" . str_replace("'", "''", $lead['Salutation']) . "',
          '" . str_replace("'", "''", $lead['FirstName']) . "',
          '" . str_replace("'", "''", $lead['LastName']) . "',
          '" . str_replace("'", "''", $lead['Title']) . "',
          '',
          '" . str_replace("'", "''", $lead['Department__c']) . "',
          '" . str_replace("'", "''", $lead['DoNotCall']) . "',
          '" . str_replace("'", "''", $lead['Phone']) . "',
          '" . str_replace("'", "''", $lead['MobilePhone']) . "',
          '',
          '" . str_replace("'", "''", $lead['Alt_Phone__c']) . "',
          '" . str_replace("'", "''", $lead['Fax']) . "',
          '" . str_replace("'", "''", $lead['Street']) . "',
          '" . str_replace("'", "''", $lead['City']) . "',
          '" . str_replace("'", "''", $lead['State']) . "',
          '" . str_replace("'", "''", $lead['PostalCode']) . "',
          '" . str_replace("'", "''", $lead['Country']) . "',
          '',
          '',
          '',
          '',
          '',
          '" . str_replace("'", "''", $lead['Asst_Name__c']) . "',
          '',
          '" . str_replace("'", "''", $lead['IsConverted']) . "',
          '" . str_replace("'", "''", $lead['Ref_d_by__c']) . "',
          '" . str_replace("'", "''", $lead['LeadSource']) . "',
          '',
          '" . str_replace("'", "''", $lead['Status']) . "',
          '',
          '',
          '" . str_replace("'", "''", $lead['Company']) . "',
          '',
          '',
          '',
          '',
          '',
          '',
          '',
          '',
          '',
          '',
          ''
        )
    ");
    if (!$insertLead) {
        echo '<br />ERROR: ' . $lead['Id'] . ', ' . $db->error . '<br />';
    }

    $insertLeadCustom = $db->query("
        INSERT INTO leads_cstm (
          id_c, 
          jjwg_maps_lng_c, 
          jjwg_maps_lat_c, 
          jjwg_maps_geocode_status_c, 
          jjwg_maps_address_c, 
          compnay_c, 
          email_c, 
          industry_c, 
          rating_c, 
          annualrevenue_c, 
          currency_id, 
          email_opt_out_c, 
          assistant_name_c, 
          nickname_c, 
          parent_company_c, 
          ext_c, 
          opportunity_c, 
          cell_c, 
          alteranate_phone_c, 
          mail_address_c, 
          alternate_email_c, 
          assistant_email_c, 
          county_c, 
          grant_prospect_c, 
          sic_code_c, 
          sic_description_c, 
          alternate_contact_c, 
          type_c
        ) VALUES (
          '" . str_replace("'", "''", $lead['Id']) . "',
          '" . str_replace("'", "''", $lead['Longitude']) . "',
          '" . str_replace("'", "''", $lead['Latitude']) . "',
          '',
          '',
          '" . str_replace("'", "''", $lead['Company']) . "',
          '" . str_replace("'", "''", $lead['Email']) . "',
          '" . str_replace("'", "''", $lead['Industry']) . "',
          '" . str_replace("'", "''", $lead['Rating']) . "',
          '" . str_replace("'", "''", $lead['AnnualRevenue']) . "',
          '',
          '" . str_replace("'", "''", $lead['HasOptedOutOfEmail']) . "',
          '" . str_replace("'", "''", $lead['Asst_Name__c']) . "',
          '" . str_replace("'", "''", $lead['Nickname__c']) . "',
          '" . str_replace("'", "''", $lead['Parent_Company__c']) . "',
          '" . str_replace("'", "''", $lead['Ext__c']) . "',
          '" . str_replace("'", "''", $lead['Opportunity__c']) . "',
          '" . str_replace("'", "''", $lead['Cell__c']) . "',
          '" . str_replace("'", "''", $lead['Alt_Phone__c']) . "',
          '" . str_replace("'", "''", $lead['Mail_Address__c']) . "',
          '" . str_replace("'", "''", $lead['Alt_email__c']) . "',
          '" . str_replace("'", "''", $lead['Asst_email__c']) . "',
          '" . str_replace("'", "''", $lead['County__c']) . "',
          '" . str_replace("'", "''", $lead['Grant__c']) . "',
          '" . str_replace("'", "''", $lead['SIC_Code__c']) . "',
          '" . str_replace("'", "''", $lead['SIC_Description__c']) . "',
          '" . str_replace("'", "''", $lead['Alternate_Contact__c']) . "',
          '" . str_replace("'", "''", $lead['Type__c']) . "'
        )
    ");
    if (!$insertLeadCustom) {
        echo '<br />ERROR: ' . $lead['Id'] . ', ' . $db->error . '<br />';
    }

    echo $line . ', Inserted ID: ' . $lead['Id'] . '<br />';
    $line++;
}

?>