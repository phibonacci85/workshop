<?php

/**
 * Connect to Database
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');

$notes = $sfdb->query("
    SELECT *
    FROM Note
");
while ($note = $notes->fetch_assoc()) {
    $title = str_replace("'", "''", $note['Title']);
    $description = str_replace("'", "''", $note['Body']);
    $parentType = 'unknown';
    $accountId = $note['AccountId'];
    if ($accountId == '000000000000000AAA') $accountId = '';

    $suiteNotes = $db->query("
        SELECT *
        FROM notes
        WHERE id = '" . $note['Id'] . "'
    ");
    if ($suiteNotes->num_rows > 0) {
        $exists = true;
    } else {
        $exists = false;
    }

    $opportunities = $sfdb->query("
        SELECT *
        FROM Opportunity
        WHERE Id = '" . $note['ParentId'] . "'
    ");
    if ($opportunities->num_rows == 1) {
        echo 'Opportunity Note: ' . $note['Id'] . '<br />';
        $parentType = 'Opportunities';
    }

    $leads = $sfdb->query("
        SELECT *
        FROM Lead
        WHERE Id = '" . $note['ParentId'] . "'
    ");
    if ($leads->num_rows == 1) {
        echo 'Lead Note: ' . $note['Id'] . '<br />';
        $parentType = 'Leads';
    }

    $accounts = $sfdb->query("
        SELECT *
        FROM Account
        WHERE Id = '" . $note['ParentId'] . "'
    ");
    if ($accounts->num_rows == 1) {
        echo 'Account Note: ' . $note['Id'] . '<br />';
        $parentType = 'Accounts';
    }

    $contacts = $sfdb->query("
        SELECT *
        FROM Contact
        WHERE Id = '" . $note['ParentId'] . "'
    ");
    if ($contacts->num_rows == 1) {
        echo 'Contact Note: ' . $note['Id'] . '<br />';
        $parentType = 'Contacts';
    }

    if ($parentType == 'Contacts') {
        if ($exists) {
            $updateNote = $db->query("
                UPDATE notes
                SET assigned_user_id = '" . $note['OwnerId'] . "', 
                    date_entered = '" . date('Y-m-d H:i:s', strtotime($note['CreatedDate'])) . "', 
                    date_modified = '" . date('Y-m-d H:i:s', strtotime($note['LastModifiedDate'])) . "', 
                    modified_user_id = '" . $note['LastModifiedById'] . "', 
                    created_by = '" . $note['CreatedById'] . "', 
                    NAME = '" . $title . "', 
                    file_mime_type = '', 
                    filename = '', 
                    parent_type = 'Accounts', 
                    parent_id = '" . $accountId . "', 
                    contact_id = '" . $note['ParentId'] . "', 
                    portal_flag = '', 
                    embed_flag = '', 
                    description = '" . $description . "', 
                    deleted = '" . $note['IsDeleted'] . "'
                WHERE id = '" . $note['Id'] . "'
            ");
            if ($updateNote) {
                echo 'Updated Note: ' . $note['Id'] . '<br />';
            } else {
                echo 'ERROR updating: ' . $note['Id'] . '<br />';
            }
        } else {
            $insertNote = $db->query("
                INSERT INTO notes (
                  assigned_user_id, 
                  id, 
                  date_entered, 
                  date_modified, 
                  modified_user_id, 
                  created_by, 
                  name, 
                  file_mime_type, 
                  filename, 
                  parent_type, 
                  parent_id, 
                  contact_id, 
                  portal_flag, 
                  embed_flag, 
                  description, 
                  deleted
                ) VALUES (
                  '" . $note['OwnerId'] . "', 
                  '" . $note['Id'] . "', 
                  '" . date('Y-m-d H:i:s', strtotime($note['CreatedDate'])) . "', 
                  '" . date('Y-m-d H:i:s', strtotime($note['LastModifiedDate'])) . "', 
                  '" . $note['LastModifiedById'] . "', 
                  '" . $note['CreatedById'] . "', 
                  '" . $title . "', 
                  '', 
                  '', 
                  'Accounts', 
                  '" . $accountId . "', 
                  '" . $note['ParentId'] . "', 
                  '', 
                  '', 
                  '" . $description . "', 
                  '" . $note['IsDeleted'] . "'
                )
            ");
            if ($insertNote) {
                echo 'Inserted Note: ' . $note['Id'] . ' < br />';
            } else {
                echo 'ERROR inserting note: ' . $note['Id'] . ' => ' . $db->error . ' < br />';
            }
        }
    } else {
        if ($exists) {
            $updateNote = $db->query("
                UPDATE notes
                SET assigned_user_id = '" . $note['OwnerId'] . "', 
                    date_entered = '" . date('Y-m-d H:i:s', strtotime($note['CreatedDate'])) . "', 
                    date_modified = '" . date('Y-m-d H:i:s', strtotime($note['LastModifiedDate'])) . "', 
                    modified_user_id = '" . $note['LastModifiedById'] . "', 
                    created_by = '" . $note['CreatedById'] . "', 
                    NAME = '" . $title . "', 
                    file_mime_type = '', 
                    filename = '', 
                    parent_type = '" . $parentType . "', 
                    parent_id = '" . $note['ParentId'] . "', 
                    contact_id = '', 
                    portal_flag = '', 
                    embed_flag = '', 
                    description = '" . $description . "', 
                    deleted = '" . $note['IsDeleted'] . "'
                WHERE id = '" . $note['Id'] . "'
            ");
            if ($updateNote) {
                echo 'Updated Note: ' . $note['Id'] . '<br />';
            } else {
                echo 'ERROR updating: ' . $note['Id'] . '<br />';
            }
        } else {
            $insertNote = $db->query("
                INSERT INTO notes (
                  assigned_user_id, 
                  id, 
                  date_entered, 
                  date_modified, 
                  modified_user_id, 
                  created_by, 
                  name, 
                  file_mime_type, 
                  filename, 
                  parent_type, 
                  parent_id, 
                  contact_id, 
                  portal_flag, 
                  embed_flag, 
                  description, 
                  deleted
                ) VALUES (
                  '" . $note['OwnerId'] . "', 
                  '" . $note['Id'] . "', 
                  '" . date('Y-m-d H:i:s', strtotime($note['CreatedDate'])) . "', 
                  '" . date('Y-m-d H:i:s', strtotime($note['LastModifiedDate'])) . "', 
                  '" . $note['LastModifiedById'] . "', 
                  '" . $note['CreatedById'] . "', 
                  '" . $title . "', 
                  '', 
                  '', 
                  '" . $parentType . "', 
                  '" . $note['ParentId'] . "', 
                  '', 
                  '', 
                  '', 
                  '" . $description . "', 
                  '" . $note['IsDeleted'] . "'
                )
            ");
            if ($insertNote) {
                echo 'Inserted Note: ' . $note['Id'] . ' < br />';
            } else {
                echo 'ERROR inserting note: ' . $note['Id'] . ' => ' . $db->error . ' < br />';
            }
        }
    }
}