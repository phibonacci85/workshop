<?php

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');

$tasks = $db->query("
    SELECT *
    FROM tasks
");
while ($task = $tasks->fetch_assoc()) {
    $sfTasks = $sfdb->query("
        SELECT *
        FROM Task
        WHERE Id = '" . $task['id'] . "'
    ");
    if ($sfTask = $sfTasks->fetch_assoc()) {
        if ($sfTask['WhoId'] == '000000000000000AAA' && $sfTask['WhatId'] != '000000000000000AAA' && $sfTask['WhatId'] != $sfTask['AccountId']) {
            $updateOpportunity = $db->query("
                UPDATE tasks
                SET parent_type = 'Opportunities', 
                    parent_id = '" . $sfTask['WhatId'] . "',
                    contact_id = ''
                WHERE id = '" . $sfTask['Id'] . "'
            ");
            if (!$updateOpportunity) {
                echo 'ERROR updating opportunity: ' . $sfTask['Id'] . '<br />';
            }
        } else if ($sfTask['WhoId'] == '000000000000000AAA' && $sfTask['WhatId'] != '000000000000000AAA' && $sfTask['WhatId'] == $sfTask['AccountId']) {
            $updateAccountNoContact = $db->query("
                UPDATE tasks
                SET parent_type = 'Accounts', 
                    parent_id = '" . $sfTask['WhatId'] . "',
                    contact_id = ''
                WHERE id = '" . $sfTask['Id'] . "'
            ");
            if (!$updateAccountNoContact) {
                echo 'ERROR updating account no contact: ' . $sfTask['Id'] . '<br />';
            }
        } else if ($sfTask['WhoId'] != '000000000000000AAA' && $sfTask['WhatId'] == '000000000000000AAA' && $sfTask['AccountId'] == '') {
            $sfLeads = $sfdb->query("
                SELECT *
                FROM Lead
                WHERE Id = '" . $sfTask['WhoId'] . "'
            ");
            $sfContacts = $sfdb->query("
                SELECT *
                FROM Contact
                WHERE Id = '" . $sfTask['WhoId'] . "'
            ");
            if ($sfLeads->num_rows == 1 && $sfContacts->num_rows == 0) {
                $updateLeadNoAccount = $db->query("
                    UPDATE tasks
                    SET parent_type = 'Leads', 
                        parent_id = '" . $sfTask['WhoId'] . "',
                        contact_id = ''
                    WHERE id = '" . $sfTask['Id'] . "'
                ");
                if (!$updateLeadNoAccount) {
                    echo 'ERROR updating lead no account: ' . $sfTask['Id'] . '<br />';
                }
            } else if ($sfLeads->num_rows == 0 && $sfContacts->num_rows == 1) {
                $updateContactNoAccount = $db->query("
                    UPDATE tasks
                    SET parent_type = 'Contacts', 
                        parent_id = '" . $sfTask['WhoId'] . "',
                        contact_id = ''
                    WHERE id = '" . $sfTask['Id'] . "'
                ");
                if (!$updateContactNoAccount) {
                    echo 'ERROR updating contact no account: ' . $sfTask['Id'] . '<br />';
                }
            }
        } else if ($sfTask['WhoId'] != '000000000000000AAA' && $sfTask['WhatId'] != '000000000000000AAA') {
            $updateAccountWithContact = $db->query("
                UPDATE tasks
                SET parent_type = 'Accounts', 
                    parent_id = '" . $sfTask['AccountId'] . "',
                    contact_id = '".$sfTask['WhoId']."'
                WHERE id = '" . $sfTask['Id'] . "'
            ");
            if (!$updateAccountWithContact) {
                echo 'ERROR updating account with contact: ' . $sfTask['Id'] . '<br />';
            }
        }
    }
}

?>