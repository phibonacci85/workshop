<?php
/**
 * Connect to Database
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');


echo 'Checking leads...<br />';
$missingLeadCount = 0;
$leads = $sfdb->query("
    SELECT *
    FROM Lead
    WHERE IsConverted = 0
");
while ($lead = $leads->fetch_assoc()) {
    $leadExists = $db->query("
        SELECT *
        FROM leads
        WHERE id = '" . $lead['Id'] . "'
    ");
    if ($leadExists->num_rows != 1) {
        echo 'MISSING: ' . $lead['Id'] . '<br />';
        $missingLeadCount++;
        $description = str_replace("'", "''", $lead['Description']);
        $salutation = str_replace("'", "''", $lead['Salutation']);
        $firstName = str_replace("'", "''", $lead['FirstName']);
        $lastName = str_replace("'", "''", $lead['LastName']);
        $title = str_replace("'", "''", $lead['Title']);
        $department = str_replace("'", "''", $lead['Department__c']);
        $street = str_replace("'", "''", $lead['Street']);
        $city = str_replace("'", "''", $lead['City']);
        $state = str_replace("'", "''", $lead['City']);
        $country = str_replace("'", "''", $lead['Country']);
        $asstName = str_replace("'", "''", $lead['Asst_Name__c']);
        $refDBy = str_replace("'", "''", $lead['Ref_d_by__c']);
        $leadSource = str_replace("'", "''", $lead['LeadSource']);
        $status = str_replace("'", "''", $lead['Status']);
        $company = str_replace("'", "''", $lead['Company']);
        $industry = str_replace("'", "''", $lead['Industry']);
        $nickName = str_replace("'", "''", $lead['Nickname__c']);
        $parentCompany = str_replace("'", "''", $lead['Parent_Company__c']);
        $mailAddress = str_replace("'", "''", $lead['Mail_Address__c']);
        $county = str_replace("'", "''", $lead['County__c']);
        $sicDescription = str_replace("'", "''", $lead['SIC_Description__c']);
        $altContact = str_replace("'", "''", $lead['Alternate_Contact__c']);
        $insertLead = $db->query("
            INSERT INTO leads (
              id,
              date_entered,
              date_modified,
              modified_user_id,
              created_by,
              description,
              deleted,
              assigned_user_id,
              salutation, 
              first_name, 
              last_name, 
              title, 
              photo, 
              department, 
              do_not_call, 
              phone_home, 
              phone_mobile, 
              phone_work, 
              phone_other, 
              phone_fax, 
              primary_address_street, 
              primary_address_city, 
              primary_address_state, 
              primary_address_postalcode, 
              primary_address_country, 
              alt_address_street, 
              alt_address_city, 
              alt_address_state, 
              alt_address_postalcode, 
              alt_address_country, 
              assistant, 
              assistant_phone, 
              converted, 
              refered_by, 
              lead_source, 
              lead_source_description, 
              status, 
              status_description, 
              reports_to_id, 
              account_name, 
              account_description, 
              contact_id, 
              account_id, 
              opportunity_id, 
              opportunity_name, 
              opportunity_amount, 
              campaign_id, 
              birthdate, 
              portal_name, 
              portal_app, 
              website
            ) VALUES (
              '" . $lead['Id'] . "',
              '" . $lead['CreatedDate'] . "',
              '" . $lead['LastModifiedDate'] . "',
              '" . $lead['LastModifiedById'] . "',
              '" . $lead['CreatedById'] . "',
              '" . $description . "',
              '" . $lead['IsDeleted'] . "',
              '" . $lead['OwnerId'] . "',
              '" . $salutation . "',
              '" . $firstName . "',
              '" . $lastName . "',
              '" . $title . "',
              '',
              '" . $department . "',
              '" . $lead['DoNotCall'] . "',
              '" . $lead['Phone'] . "',
              '" . $lead['MobilePhone'] . "',
              '',
              '" . $lead['Alt_Phone__c'] . "',
              '" . $lead['Fax'] . "',
              '" . $street . "',
              '" . $city . "',
              '" . $state . "',
              '" . $lead['PostalCode'] . "',
              '" . $country . "',
              '',
              '',
              '',
              '',
              '',
              '" . $asstName . "',
              '',
              '" . $lead['IsConverted'] . "',
              '" . $refDBy . "',
              '" . $leadSource . "',
              '',
              '" . $status . "',
              '',
              '',
              '" . $company . "',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              ''
            )
        ");
        if (!$insertLead) {
            echo '<br />ERROR: ' . $lead['Id'] . ', ' . $db->error . '<br />';
        }

        $insertLeadCustom = $db->query("
            INSERT INTO leads_cstm (
              id_c, 
              jjwg_maps_lng_c, 
              jjwg_maps_lat_c, 
              jjwg_maps_geocode_status_c, 
              jjwg_maps_address_c, 
              compnay_c, 
              email_c, 
              industry_c, 
              rating_c, 
              annualrevenue_c, 
              currency_id, 
              email_opt_out_c, 
              assistant_name_c, 
              nickname_c, 
              parent_company_c, 
              ext_c, 
              opportunity_c, 
              cell_c, 
              alteranate_phone_c, 
              mail_address_c, 
              alternate_email_c, 
              assistant_email_c, 
              county_c, 
              grant_prospect_c, 
              sic_code_c, 
              sic_description_c, 
              alternate_contact_c, 
              type_c
            ) VALUES (
              '" . $lead['Id'] . "',
              '" . $lead['Longitude'] . "',
              '" . $lead['Latitude'] . "',
              '',
              '',
              '" . $company . "',
              '" . $lead['Email'] . "',
              '" . $industry . "',
              '" . $lead['Rating'] . "',
              '" . $lead['AnnualRevenue'] . "',
              '',
              '" . $lead['HasOptedOutOfEmail'] . "',
              '" . $asstName . "',
              '" . $nickName . "',
              '" . $parentCompany . "',
              '" . $lead['Ext__c'] . "',
              '" . $lead['Opportunity__c'] . "',
              '" . $lead['Cell__c'] . "',
              '" . $lead['Alt_Phone__c'] . "',
              '" . $mailAddress . "',
              '" . $lead['Alt_email__c'] . "',
              '" . $lead['Asst_email__c'] . "',
              '" . $county . "',
              '" . $lead['Grant__c'] . "',
              '" . $lead['SIC_Code__c'] . "',
              '" . $sicDescription . "',
              '" . $altContact . "',
              '" . $lead['Type__c'] . "'
            )
        ");
        if (!$insertLeadCustom) {
            echo '<br />ERROR: ' . $lead['Id'] . ', ' . $db->error . '<br />';
        }

        echo $line . ', Inserted ID: ' . $lead['Id'] . '<br />';
    }
}

echo '<br />Leads completed.<br />Checking Accounts<br />';
$missingAccountCount = 0;
$accounts = $sfdb->query("
    SELECT *
    FROM Account
");
while ($account = $accounts->fetch_assoc()) {
    $accountExists = $db->query("
        SELECT *
        FROM accounts
        WHERE id = '" . $account['Id'] . "'
    ");
    if ($accountExists->num_rows != 1) {
        echo 'Missing: ' . $account['Id'] . '<br />';
        $missingAccountCount++;
        $name = str_replace("'", "''", $account['Name']);
        $description = str_replace("'", "''", $account['Description']);
        $type = str_replace("'", "''", $account['Type']);
        $industry = str_replace("'", "''", $account['Industry']);
        $billingStreet = str_replace("'", "''", $account['BillingStreet']);
        $billingCity = str_replace("'", "''", $account['BillingCity']);
        $billingState = str_replace("'", "''", $account['BillingState']);
        $billingCountry = str_replace("'", "''", $account['BillingCountry']);
        $shippingStreet = str_replace("'", "''", $account['ShippingStreet']);
        $shippingCity = str_replace("'", "''", $account['ShippingCity']);
        $shippingState = str_replace("'", "''", $account['ShippingState']);
        $shippingCountry = str_replace("'", "''", $account['ShippingCountry']);
        $distributor = str_replace("'", "''", $account['Distributor__c']);
        $type_c = str_replace("'", "''", $account['Type__c']);
        $insertAccount = $db->query("
            INSERT INTO accounts (
              id, 
              name, 
              date_entered, 
              date_modified, 
              modified_user_id, 
              created_by, 
              description, 
              deleted, 
              assigned_user_id, 
              account_type, 
              industry, 
              annual_revenue, 
              phone_fax, 
              billing_address_street, 
              billing_address_city, 
              billing_address_state, 
              billing_address_postalcode, 
              billing_address_country, 
              rating, 
              phone_office, 
              phone_alternate, 
              website, 
              ownership, 
              employees, 
              ticker_symbol, 
              shipping_address_street,
              shipping_address_city, 
              shipping_address_state, 
              shipping_address_postalcode, 
              shipping_address_country, 
              parent_id, 
              sic_code, 
              campaign_id
            ) VALUES (
              '" . $account['Id'] . "', 
              '" . $name . "', 
              '" . $account['CreatedDate'] . "', 
              '" . $account['LastModifiedDate'] . "', 
              '" . $account['LastModifiedById'] . "', 
              '" . $account['CreatedById'] . "', 
              '" . $description . "', 
              '" . $account['IsDeleted'] . "', 
              '" . $account['OwnerId'] . "', 
              '" . $type . "', 
              '" . $industry . "', 
              '" . $account['AnnualRevenue'] . "', 
              '" . $account['Fax'] . "', 
              '" . $billingStreet . "', 
              '" . $billingCity . "', 
              '" . $billingState . "', 
              '" . $account['BillingPostalCode'] . "', 
              '" . $billingCountry . "', 
              '" . $account['Rating'] . "', 
              '" . $account['Phone'] . "', 
              '" . $account['Alternate_Contact_Phone__c'] . "', 
              '" . $account['Website'] . "', 
              '" . $account['Ownership'] . "', 
              '" . $account['NumberOfEmployees'] . "', 
              '" . $account['TickerSymbol'] . "', 
              '" . $shippingStreet . "',
              '" . $shippingCity . "', 
              '" . $shippingState . "', 
              '" . $account['ShippingPostalCode'] . "', 
              '" . $shippingCountry . "', 
              '" . $account['ParentId'] . "', 
              '" . $account['Sic'] . "', 
              ''
            )
        ");
        if (!$insertAccount) {
            echo '<br />ERROR: ' . $account['Id'] . ', ' . $db->error . '<br />';
        }

        $insertAccountCustom = $db->query("
            INSERT INTO accounts_cstm (
              id_c, 
              jjwg_maps_lng_c, 
              jjwg_maps_lat_c, 
              jjwg_maps_geocode_status_c, 
              jjwg_maps_address_c, 
              distributor_c, 
              type_c, 
              club_status_c
            ) VALUES (
              '" . $account['Id'] . "', 
              '', 
              '', 
              '', 
              '', 
              '" . $distributor . "', 
              '" . $type_c . "', 
              ''
            )
        ");
        if (!$insertAccountCustom) {
            echo '<br />ERROR: ' . $account['Id'] . ', ' . $db->error . '<br />';
        }
    }
}

echo '<br />Accounts completed.<br />Checking Contacts<br />';
$missingContactCount = 0;
$contacts = $sfdb->query("
    SELECT *
    FROM Contact
");
while ($contact = $contacts->fetch_assoc()) {
    $contactExists = $db->query("
        SELECT *
        FROM contacts
        WHERE id = '" . $contact['Id'] . "'
    ");
    if ($contactExists->num_rows != 1) {
        echo 'Missing: ' . $contact['Id'] . '<br />';
        $missingContactCount++;
        $salutation = str_replace("'", "''", $contact['Salutation']);
        $firstName = str_replace("'", "''", $contact['FirstName']);
        $lastName = str_replace("'", "''", $contact['LastName']);
        $title = str_replace("'", "''", $contact['Title']);
        $department = str_replace("'", "''", $contact['Department']);
        $mailingStreet = str_replace("'", "''", $contact['MailingStreet']);
        $mailingCity = str_replace("'", "''", $contact['MailingCity']);
        $mailingState = str_replace("'", "''", $contact['MailingState']);
        $mailingCountry = str_replace("'", "''", $contact['MailingCountry']);
        $otherStreet = str_replace("'", "''", $contact['OtherStreet']);
        $otherCity = str_replace("'", "''", $contact['OtherCity']);
        $otherState = str_replace("'", "''", $contact['OtherState']);
        $otherCountry = str_replace("'", "''", $contact['OtherCountry']);
        $asstName = str_replace("'", "''", $contact['AssistantName']);
        $leadSource = str_replace("'", "''", $contact['LeadSource']);
        $parentCompany = str_replace("'", "''", $contact['Parent_Company__c']);
        $type_c = str_replace("'", "''", $contact['Type__c']);
        $refDBy = str_replace("'", "''", $contact['Ref_d_by__c']);
        $insertContact = $db->query("
            INSERT INTO contacts (
              id, 
              date_entered, 
              date_modified, 
              modified_user_id, 
              created_by, 
              description, 
              deleted, 
              assigned_user_id, 
              salutation, 
              first_name, 
              last_name, 
              title, 
              photo, 
              department, 
              do_not_call, 
              phone_home, 
              phone_mobile, 
              phone_work, 
              phone_other, 
              phone_fax, 
              primary_address_street, 
              primary_address_city, 
              primary_address_state, 
              primary_address_postalcode, 
              primary_address_country, 
              alt_address_street, 
              alt_address_city, 
              alt_address_state, 
              alt_address_postalcode, 
              alt_address_country, 
              assistant, 
              assistant_phone, 
              lead_source, 
              reports_to_id, 
              birthdate, 
              campaign_id, 
              joomla_account_id, 
              portal_account_disabled, 
              portal_user_type
            ) VALUES (
              '" . $contact['Id'] . "', 
              '" . $contact['CreatedDate'] . "', 
              '" . $contact['LastModifiedDate'] . "', 
              '" . $contact['LastModifiedById'] . "', 
              '" . $contact['CreatedById'] . "', 
              '" . $contact['Description'] . "', 
              '" . $contact['IsDeleted'] . "', 
              '" . $contact['OwnerId'] . "', 
              '" . $salutation . "', 
              '" . $firstName . "', 
              '" . $lastName . "', 
              '" . $title . "', 
              '', 
              '" . $department . "', 
              '" . $contact['DoNotCall'] . "', 
              '" . $contact['HomePhone'] . "', 
              '" . $contact['MobilePhone'] . "', 
              '" . $contact['Phone'] . "', 
              '" . $contact['OtherPhone'] . "', 
              '" . $contact['Fax'] . "', 
              '" . $mailingStreet . "', 
              '" . $mailingCity . "', 
              '" . $mailingState . "', 
              '" . $contact['MailingPostalCode'] . "', 
              '" . $mailingCountry . "', 
              '" . $otherStreet . "', 
              '" . $otherCity . "', 
              '" . $otherState . "', 
              '" . $contact['OtherPostalCode'] . "', 
              '" . $otherCountry . "', 
              '" . $asstName . "', 
              '" . $contact['AssistantPhone'] . "', 
              '" . $leadSource . "', 
              '" . $contact['ReportsToId'] . "', 
              '" . $contact['Birthdate'] . "', 
              '', 
              '', 
              '', 
              ''
            )
        ");
        if (!$insertContact) {
            echo '<br />ERROR: ' . $contact['Id'] . ', ' . $db->error . '<br />';
        }

        $insertContactCustom = $db->query("
            INSERT INTO contacts_cstm (
              id_c, 
              jjwg_maps_lng_c, 
              jjwg_maps_lat_c, 
              jjwg_maps_geocode_status_c, 
              jjwg_maps_address_c, 
              parent_company_c, 
              customer_type_c, 
              type_c, 
              referredby_c
            ) VALUES (
              '" . $contact['Id'] . "', 
              '', 
              '', 
              '', 
              '', 
              '" . $parentCompany . "', 
              '', 
              '" . $type_c . "', 
              '" . $refDBy . "'
            )
        ");
        if (!$insertContactCustom) {
            echo '<br />ERROR: ' . $contact['Id'] . ', ' . $db->error . '<br />';
        }
    }
}

echo '<br />Accounts completed.<br />';
?>