<?php

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');

$sfLeads = $sfdb->query("
    SELECT *
    FROM Lead
    WHERE Email != ''
");
$count = 0;
while ($sfLead = $sfLeads->fetch_assoc()) {
    $email = $sfLead['Email'];
    $email = str_replace("'", "''", $email);
    $timestamp = date('Y-m-d H:i:s');
    $emailId = uniqid('', true);
    $insertEmail = $db->query("
        INSERT INTO email_addresses 
        (id, email_address, email_address_caps, invalid_email, opt_out, date_created, date_modified, deleted) 
        VALUES 
        ('" . $emailId . "', '" . $email . "', '" . strtoupper($email) . "', '0', '0', '" . $timestamp . "', '" . $timestamp . "', '0')
    ");
    if (!$insertEmail) {
        echo 'ERROR inserting email: ' . $email . ' => ' . $db->error . '<br />';
    }

    $relationId = uniqid('', true);
    $insertRelation = $db->query("
        INSERT INTO email_addr_bean_rel
        (id, email_address_id, bean_id, bean_module, primary_address, reply_to_address, date_created, date_modified, deleted)
        VALUES 
        ('" . $relationId . "', '" . $emailId . "', '" . $sfLead['Id'] . "', 'Leads', '1', '0', '" . $timestamp . "', '" . $timestamp . "', '0')
    ");
    if (!$insertRelation) {
        echo 'ERROR inserting relation: ' . $email . ', ' . $emailId . ' => ' . $db->error . '<br />';
    }
    $count++;
}

?>