<?php

/**
 * Connect to Database
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');

function getUser($userId)
{
    global $db;
    $userId = str_replace('\'', '\'\'', $userId);
    $userName = $userId;
    $users = $db->query("
        SELECT *
        FROM users
        WHERE id = '" . $userId . "'
    ");
    if ($user = $users->fetch_assoc()) {
        $userName = $user['first_name'] . ' ' . $user['last_name'];
    }
    return $userName;
}

function getAccount($accountId)
{
    global $db;
    $accountId = str_replace('\'', '\'\'', $accountId);
    $accountName = $accountId;
    $accounts = $db->query("
        SELECT *
        FROM accounts
        WHERE id = '" . $accountId . "'
    ");
    if ($account = $accounts->fetch_assoc()) {
        $accountName = $account['name'];
    }
    return $accountName;
}

function getContact($contactId)
{
    global $db;
    $contactId = str_replace('\'', '\'\'', $contactId);
    $contactName = $contactId;
    $contacts = $db->query("
        SELECT *
        FROM contacts
        WHERE id = '".$contactId."'
    ");
    if ($contact = $contacts->fetch_assoc()) {
        $contactName = 'CONTACT: '.$contact['first_name'].' '.$contact['last_name'];
    }
    return $contactName;
}

function getLead($leadId) {
    global $db;
    $leadId = str_replace('\'', '\'\'', $leadId);
    $leadName = $leadId;
    $leads = $db->query("
        SELECT *
        FROM leads
        WHERE id = '".$leadId."'
    ");
    if ($lead = $leads->fetch_assoc()) {
        $leadName = 'LEAD: '.$lead['first_name'].' '.$lead['last_name'];
    }
    return $leadName;
}

function isLead($leadId) {
    global $db;
    $leadId = str_replace('\'', '\'\'', $leadId);
    $leads = $db->query("
        SELECT *
        FROM leads
        WHERE id = '".$leadId."'
    ");
    if ($lead = $leads->fetch_assoc()) {
        return true;
    }
    return false;
}

$opportunities = $db->query("
    select *
    from opportunities
");
while($opportunity = $opportunities->fetch_assoc()) {
    $customs = $db->query("
        select *
        from opportunities_cstm
        where id_c = '".$opportunity['id']."'
    ");
    if($customs->num_rows == 0) {
        $insertCustom = $db->query("
            insert into opportunities_cstm
            (id_c)
            VALUES 
            ('".$opportunity['id']."')
        ");
        if(!$insertCustom) {
            echo 'ERROR inserting opportunity customs: '.$db->error;
        }
    }
}