<?php

/**
 * Connect to Database
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');

$notes = $sfdb->query("
    SELECT *
    FROM Note
");
while ($note = $notes->fetch_assoc()) {
    $found = false;

    $opportunities = $sfdb->query("
        SELECT *
        FROM Opportunity
        WHERE Id = '" . $note['ParentId'] . "'
    ");
    if ($opportunities->num_rows == 1) {
        echo 'Opportunity Note: ' . $note['Id'] . '<br />';
        $found = true;
    }

    $leads = $sfdb->query("
        SELECT *
        FROM Lead
        WHERE Id = '" . $note['ParentId'] . "'
    ");
    if ($leads->num_rows == 1) {
        echo 'Lead Note: ' . $note['Id'] . '<br />';
        $found = true;
    }

    $accounts = $sfdb->query("
        SELECT *
        FROM Account
        WHERE Id = '" . $note['ParentId'] . "'
    ");
    if ($accounts->num_rows == 1) {
        echo 'Account Note: ' . $note['Id'] . '<br />';
        $found = true;
    }

    $contacts = $sfdb->query("
        SELECT *
        FROM Contact
        WHERE Id = '" . $note['ParentId'] . "'
    ");
    if ($contacts->num_rows == 1) {
        echo 'Contact Note: ' . $note['Id'] . '<br />';
        $found = true;
    }

    if (!$found) {
        echo 'Not Found: ' . $note['Id'] . '<br />';
    }
}
?>