<?php

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}


@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');
@ $sfdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_salesforce');
$fp = fopen('incomplete.csv', 'w');
fputcsv($fp, array('id', 'name', 'date_entered', 'date_modified', 'modiefied_user_id', 'created_by', 'description', 'deleted', 'assigned_user_id', 'status', 'date_due_flag', 'date_due', 'date_start_flag', 'date_start', 'parent_type', 'parent_id', 'contact_id', 'priority'));

function getUser($userId)
{
    global $db;
    $userId = str_replace('\'', '\'\'', $userId);
    $userName = $userId;
    $users = $db->query("
        SELECT *
        FROM users
        WHERE id = '" . $userId . "'
    ");
    if ($user = $users->fetch_assoc()) {
        $userName = $user['first_name'] . ' ' . $user['last_name'];
    }
    return $userName;
}

function getAccount($accountId)
{
    global $db;
    $accountId = str_replace('\'', '\'\'', $accountId);
    $accountName = $accountId;
    $accounts = $db->query("
        SELECT *
        FROM accounts
        WHERE id = '" . $accountId . "'
    ");
    if ($account = $accounts->fetch_assoc()) {
        $accountName = $account['name'];
    }
    return $accountName;
}

function getContact($contactId)
{
    global $db;
    $contactId = str_replace('\'', '\'\'', $contactId);
    $contactName = $contactId;
    $contacts = $db->query("
        SELECT *
        FROM contacts
        WHERE id = '".$contactId."'
    ");
    if ($contact = $contacts->fetch_assoc()) {
        $contactName = 'CONTACT: '.$contact['first_name'].' '.$contact['last_name'];
    }
    return $contactName;
}

function getLead($leadId) {
    global $db;
    $leadId = str_replace('\'', '\'\'', $leadId);
    $leadName = $leadId;
    $leads = $db->query("
        SELECT *
        FROM leads
        WHERE id = '".$leadId."'
    ");
    if ($lead = $leads->fetch_assoc()) {
        $leadName = 'LEAD: '.$lead['first_name'].' '.$lead['last_name'];
    }
    return $leadName;
}

function isLead($leadId) {
    global $db;
    $leadId = str_replace('\'', '\'\'', $leadId);
    $leads = $db->query("
        SELECT *
        FROM leads
        WHERE id = '".$leadId."'
    ");
    if ($lead = $leads->fetch_assoc()) {
        return true;
    }
    return false;
}

$line = 0;
$tasks = $sfdb->query("
    SELECT *
    FROM Task
");
while ($task = $tasks->fetch_assoc()) {
    $description = str_replace("\\'", "'", $task['Description']);
    $description = str_replace("'", "''", $description);
    if($line == 2793) {
        $debug = true;
    }
    if ($task['Status'] == 'Completed') {
        /*
        $contactId = '';
        if ($task['WhoId'] != '000000000000000AAA') $contactId = $task['WhoId'];
        $insertTask = $db->query("
            INSERT INTO tasks (
              id,
              name,
              date_entered,
              date_modified,
              modified_user_id,
              created_by,
              description,
              deleted,
              assigned_user_id,
              status,
              date_due_flag,
              date_due,
              date_start_flag,
              date_start,
              parent_type,
              parent_id,
              contact_id,
              priority
            ) VALUES (
              '" . str_replace('\'', '\'\'', $task['Id']) . "',
              '" . str_replace('\'', '\'\'', $task['Subject']) . "',
              '" . date('Y-m-d H:i:s', strtotime($task['CreatedDate'])) . "',
              '" . date('Y-m-d H:i:s', strtotime($task['LastModifiedDate'])) . "',
              '" . str_replace('\'', '\'\'', $task['LastModifiedById']) . "',
              '" . str_replace('\'', '\'\'', $task['CreatedById']) . "',
              '" . $description . "',
              '" . str_replace('\'', '\'\'', $task['IsDeleted']) . "',
              '" . str_replace('\'', '\'\'', $task['OwnerId']) . "',
              '" . str_replace('\'', '\'\'', $task['Status']) . "',
              '',
              '" . date('Y-m-d H:i:s', strtotime($task['ActivityDate'])) . "',
              '',
              '" . date('Y-m-d H:i:s', strtotime($task['ActivityDate'])) . "',
              'Accounts',
              '" . str_replace('\'', '\'\'', $task['AccountId']) . "',
              '" . $contactId . "',
              '" . str_replace('\'', '\'\'', $task['Priority']) . "'
            )
        ");
        if (!$insertTask) {
            echo '<br />ERROR: ' . $db->error . '<br />';
        }
        echo $line . ', Inserted ID: ' . $task['Id'] . '<br />';
        */
    } else {
        $id = str_replace('\'', '\'\'', $task['Id']);
        $modifiedBy = str_replace('\'', '\'\'', $task['LastModifiedById']);
        $createdBy = str_replace('\'', '\'\'', $task['CreatedById']);
        $assignedUser = str_replace('\'', '\'\'', $task['OwnerId']);
        $contactId = str_replace('\'', '\'\'', $task['WhoId']);
        $accountId = str_replace('\'', '\'\'', $task['AccountId']);

        $modifiedBy = getUser($modifiedBy);
        $createdBy = getUser($createdBy);
        $assignedUser = getUser($assignedUser);
        $contactId = getContact($contactId);
        $contactId = getLead($contactId);
        $accountId = getAccount($accountId);

        if ($contactId == '000000000000000AAA') $contactId = '';

        fputcsv($fp, array($task['Id'], $task['Subject'], $task['CreatedDate'], $task['LastModifiedDate'], $modifiedBy, $createdBy, $task['Description'], $task['IsDeleted'], $assignedUser, $task['Status'], '', $task['ActivityDate'], '', $task['ActivityDate'], 'Accounts', $accountId, $contactId, $task['Priority']));

        echo $line . ', ADDED to CSV: ' . $task['Id'] . '<br />';
    }
    $line++;
}

fclose($handle);
fclose($fp);
?>