<?php

@ $db = new mysqli( 'tsdemos.com', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');

$prices = array();
if (($handle = fopen("prices.csv", "r")) !== FALSE) {
    $line = 0;
    while (($data = fgetcsv($handle, 9999, ",")) !== FALSE) {
        if($line == 0) {

        } else {
            $prices[$data[1]] = array(
                'name' => $data[0],
                'partnumber' => $data[1],
                'altpartnumber' => $data[2],
                'msrp' => $data[3],
                'map' => $data[4],
                'directprice' => $data[5],
                'distributor' => $data[6],
                'fastenal' => $data[7],
                'conney' => $data[8],
                'grainger' => $data[9],
                'vail' => $data[10],
                'rockwell' => $data[11],
                'alliant' => $data[12],
                'amazon' => $data[13],
                'walmart' => $data[14],
                'cost' => $data[15],
                'exceptions' => $data[16],
                'discontinued' => $data[17]
            );
        }
        $line++;
    }
    fclose($handle);
} else {
    echo 'ERRROR: opening sales file';
}

$products = array();
if (($handle = fopen("products.csv", "r")) !== FALSE) {
    $line = 0;
    while (($data = fgetcsv($handle, 9999, ",")) !== FALSE) {
        if($line == 0) {

        } else {
            $products[$data[2]] = array(
                'name' => $data[1],
                'partnumber' => $data[2],
                'altpartnumber' => $data[3],
                'thinksafecosts' => $data[4],
                'vendor' => $data[5],
                'upc' => $data[6],
                'bullet1' => $data[7],
                'bullet2' => $data[8],
                'bullet3' => $data[9],
                'bullet4' => $data[10],
                'bullet5' => $data[11],
                'shortdescription' => $data[12],
                'longdescription' => $data[13],
                'details' => $data[14],
                'curriculum' => $data[15],
                'brand' => $data[16],
                'length' => $data[17],
                'width' => $data[18],
                'height' => $data[19],
                'weight' => $data[20],
                'resourceheader' => $data[21],
                'resourcefooter' => $data[22],
                'fill' => $data[23],
                'boxcontent' => $data[24],
                'DELETE_iflogin' => $data[25],
                'fastenalpn' => $data[26],
                'conneypn' => $data[27],
                'graingerpn' => $data[28],
                'amazonpn' => $data[29],
                'tonsofpn' => $data[30],
                'globalpn' => $data[31],
                'id' => $data[32],
                'productid' => $data[33],
                'siteid' => $data[34],
                'msrp' => $data[35],
                'map' => $data[36],
                'price' => $data[37],
                'distributor' => $data[38],
                'fastenal' => $data[39],
                'conney' => $data[40]
            );
        }
        $line++;
    }
    fclose($handle);
} else {
    echo 'ERRROR: opening products file';
}

$mps = fopen('master_price_sheet.csv', 'w');
fputcsv($mps, array('id', 'name', 'partnumber', 'altpartnumber', 'Think Safe COSTS', 'Vendor', 'upc', 'bullet1', 'bullet2', 'bullet3', 'bullet4', 'bullet5', 'shortdescription', 'longdescription', 'details', 'curriculum', 'brand', 'length', 'width', 'height', 'weight', 'resourceheader', 'resourcefooter', 'fill', 'boxcontent', 'DELETE_iflogin', 'fastenalpn', 'conneypn', 'graingerpn', 'amazonpn', 'tonsofpn', 'globalpn', 'id', 'productid', 'siteid', 'msrp', 'map', 'price', 'distributor', 'fastenal', 'conney', 'rockwell', 'vail', 'alliant', 'walmart', 'amazon'));
$line = 0;
foreach($products as $k => $v) {
    $fastenal = $v['fastenal'];
    $msrp = $v['msrp'];
    $price = $v['price'];
    $map = $v['map'];
    $distributor = $v['distributor'];
    $conney = $v['conney'];
    $cost = $v['thinksafecosts'];
    $amazon = '';
    if(array_key_exists($v['partnumber'], $prices)) {
        $other = $prices[$v['partnumber']];
        $fastenal = ($v['fastenal'] != $prices[$v['partnumber']]['fastenal']) ? $prices[$v['partnumber']]['fastenal'] : $v['fastenal'];
        $msrp = ($v['msrp'] != $prices[$v['partnumber']]['msrp']) ? $prices[$v['partnumber']]['msrp'] : $v['msrp'];
        $price = ($v['price'] != $prices[$v['partnumber']]['directprice']) ? $prices[$v['partnumber']]['directprice'] : $v['price'];
        $map = ($v['map'] != $prices[$v['partnumber']]['map']) ? $prices[$v['partnumber']]['map'] : $v['map'];
        $distributor = ($v['distributor'] != $prices[$v['partnumber']]['distributor']) ? $prices[$v['partnumber']]['distributor'] : $v['distributor'];
        $conney = ($v['conney'] != $prices[$v['partnumber']]['conney']) ? $prices[$v['partnumber']]['conney'] : $v['conney'];
        $cost = ($v['thinksafecosts'] != $prices[$v['partnumber']]['cost']) ? $prices[$v['partnumber']]['cost'] : $v['thinksafecosts'];
        $amazon = $prices[$v['partnumber']]['amazon'];
    }
    fputcsv($mps, array(
        $v['id'],
        $v['name'],
        $v['partnumber'],
        $v['altpartnumber'],
        $cost,
        $v['vendor'],
        $v['upc'],
        $v['bullet1'],
        $v['bullet2'],
        $v['bullet3'],
        $v['bullet4'],
        $v['bullet5'],
        $v['shortdescription'],
        $v['longdescription'],
        $v['details'],
        $v['curriculum'],
        $v['brand'],
        $v['length'],
        $v['width'],
        $v['height'],
        $v['weight'],
        $v['resourceheader'],
        $v['resourcefooter'],
        $v['fill'],
        $v['boxcontent'],
        $v['DELETE_iflogin'],
        $v['fastenalpn'],
        $v['conneypn'],
        $v['graingerpn'],
        $v['amazonpn'],
        $v['tonsofpn'],
        $v['globalpn'],
        $v['id'],
        $v['productid'],
        $v['siteid'],
        $msrp,
        $map,
        $price,
        $distributor,
        $fastenal,
        $conney,
        $amazon
    ));
    $line++;
}

fclose($mps);


$notlisted = fopen('notlisted.csv', 'w');
fputcsv($notlisted, array('name', 'partnumber', 'altpartnumber', 'msrp', 'map', 'direct price', 'distributor', 'fastenal', 'conney', 'grainger', 'vail', 'rockwell', 'alliant', 'amazon', 'walmart', 'cost', 'exceptions', 'discontinued'));
foreach($prices as $k => $v) {
    if(!array_key_exists($v['partnumber'], $products)) {
        fputcsv($notlisted, array(
            $v['name'],
            $v['partnumber'],
            $v['altpartnumber'],
            $v['msrp'],
            $v['map'],
            $v['directprice'],
            $v['distributor'],
            $v['fastenal'],
            $v['conney'],
            $v['grainger'],
            $v['vail'],
            $v['rockwell'],
            $v['alliant'],
            $v['amazon'],
            $v['walmart'],
            $v['cost'],
            $v['exceptions'],
            $v['discontinued']
        ));
    }
}
fclose($notlisted);
?>