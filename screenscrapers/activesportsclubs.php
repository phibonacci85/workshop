<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Active Sports Club';
  $filename = 'activesportsclub';
  $fp = fopen('reports/activesportsclub.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'City', 'State', 'Zip', 'Phone'));
  
	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://activesportsclubs.com/");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
    $links = $directoryhtml->find('li.section');
	foreach($links as $e)
	{  
        $name = $e->innertext;
        $store = $e;
        while($store = $store->next_sibling()){
          $gymurl = 'http://activesportsclubs.com'.$store->find('a', 0)->getAttribute('href');
          
          $ch2 = curl_init();
          curl_setopt($ch2, CURLOPT_URL, $gymurl);
          curl_setopt($ch2, CURLOPT_HEADER, false);
          curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
          $result2 = curl_exec($ch2);
          curl_close($ch2);

          $locationhtml = new simple_html_dom();
          $locationhtml->load($result2);
          $title = trim($locationhtml->find('.locationDetails h4', 0)->innertext);
          $details = trim($locationhtml->find('.locationDetails p', 0)->innertext);
          $details = explode(',', $details);
          $statezip = trim($details[1]);
          $statezip = preg_replace('/\s+/',',', $statezip);
          $statezip = explode(',', $statezip);
          $address = explode('<br />', $details[0]);
          $city = trim($address[1]);
          $address = trim($address[0]);
          $state = $statezip[0];
          $zip = $statezip[1];
          $phone = explode('<br />',$locationhtml->find('.locationDetails p', 1)->innertext);
          $phone = trim(str_replace('<span class="phoneType">tel</span>', '', $phone[0]));
          fputcsv($fp, array($title, $address, $city, $state, $zip, $phone));
        }
	}

  fclose($fp);
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
?>