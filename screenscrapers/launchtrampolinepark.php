<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Launch Trampoline Park';
  $filename = 'launchtrampolinepark';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Phone'));

  $url = 'http://launchtrampolinepark.com/';

  $ch = curl_init();

  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

  $result = curl_exec($ch);
  curl_close($ch);
  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $links = $directoryhtml->find('form option');
  foreach($links as $link){
    if($link->getAttribute('value') != ''){
      $url = $link->getAttribute('value');
      $url = str_replace(' ', '', $url);
      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL, $url);
      curl_setopt($ch,CURLOPT_HEADER, false);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
      $result = curl_exec($ch);
      curl_close($ch);
      $directoryhtml = new simple_html_dom();
      $directoryhtml->load($result);
      $text = $directoryhtml->find('.cell', 0);
      $location = $text->innertext;
      echo $link->innertext.'<br />';
      echo $url.'<br />';
      echo $location.'<br /><br />';
    }
  }

  fclose($fp);
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);

?>