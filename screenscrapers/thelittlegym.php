<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'The Little Gym';
  $filename = 'thelittlegym';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));

  $ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');
  foreach($ab_state as $state){
    $url = 'http://www.thelittlegym.com/Pages/LocatorResult.aspx?action=3&param='.$state;
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_HEADER, false);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    $directoryhtml = new simple_html_dom();
    $directoryhtml->load($result);
    $stateLocations = $directoryhtml->find('table tr');
    $count = 0;
    foreach($stateLocations as $locations){
      if($count > 3){
        $addressInfo = $locations->find('td', 0)->innertext;
        $contactInfo = $locations->find('td', 1)->innertext;
        $addressInfo = explode('<br/>', $addressInfo);
        $contactInfo = explode('<br/>', $contactInfo);
        $name = trim(strip_tags($addressInfo[0]));
        $address = trim(strip_tags($addressInfo[1]));
        if(count($addressInfo)-2 == 3) $address .= ', '.$addressInfo[2];
        $locale = trim(strip_tags($addressInfo[count($addressInfo)-2]));
        $phone = trim(strip_tags(str_replace('Phone:', '', $contactInfo[0])));

        fputcsv($fp, array($name, $address, $locale, $phone));
      }
      $count++;
    }
  }
  fclose($fp);
  
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  
?>