<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Iron Tribe';
  $filename = 'irontribe';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Phone'));

  $url = 'http://www.irontribefitness.com/wp-admin/admin-ajax.php';
  $fields = array(
          'action' => 'getLocation',
          'lat' => '0',
          'lon' => '0',
          'all' => '1'
  );


  $fields_string = '';
  foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
  rtrim($fields_string, '&');

  $ch = curl_init();

  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch,CURLOPT_POST, true);
  curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);


  $result = curl_exec($ch);
  curl_close($ch);
  $locations = json_decode($result);
  foreach($locations->title as $key => $location){
    fputcsv($fp, array($locations->title[$key], $locations->address[$key], $locations->phone[$key]));
  }

  fclose($fp);
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);

?>