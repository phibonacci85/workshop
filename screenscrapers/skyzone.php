<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Skyzone';
  $filename = 'skyzone';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'City', 'State', 'Zip', 'Phone'));

  $url = 'http://www.skyzone.com/getalllocsnew.asp';

  $ch = curl_init();

  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  //curl_setopt($ch,CURLOPT_POST, true);
  //curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);


  $result = curl_exec($ch);
  curl_close($ch);
	
  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $links = $directoryhtml->find('a');
  foreach($links as $link){
    $location = $link->getAttribute('href');
    echo $location.'<br />';
  }

  fclose($fp);

  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
?>