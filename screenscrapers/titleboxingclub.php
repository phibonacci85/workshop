<?php
	ini_set('max_execution_time', 3600);
	include('simple_html_dom.php');
	require_once('./swiftmailer/swift_required.php');

	define('DBHOST', 'localhost');  
	define('DBUSER', 'tsdemosc_jbaker');
	define('DBPASS', 'Jbake1234');
	define('DB', 'tsdemosc_screenscrapers_jb');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);

	$to = array('jbaker@think-safe.com');
	$salesRep = array('tdorsey@think-safe.com', 'pwickham@think-safe.com', 'jbaker@think-safe.com');
	$subject = 'Title Boxing Club';
	$filename = 'titleboxingclub';
	$fp = fopen('reports/'.$filename.'.csv', 'w');
	$nfp = fopen('reports/new_'.$filename.'.csv', 'w');
	$newPlaces = false;
	fputcsv($fp, array('Name', 'Address', 'City', 'State', 'Zip', 'Phone'));
	fputcsv($nfp, array('Name', 'Address', 'City', 'State', 'Zip', 'Phone'));

		//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://titleboxingclub.com/find-a-club/");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);

	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);

	foreach($directoryhtml->find('.clubs-sub-sub-menu li a') as $e)
	{
		$gymurl = $e->getAttribute('href');
		$name = $e->innertext;
		
		$ch2 = curl_init();
		curl_setopt($ch2, CURLOPT_URL, $gymurl);
		curl_setopt($ch2, CURLOPT_HEADER, false);
		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
		$result2 = curl_exec($ch2);
		curl_close($ch2);

		$locationhtml = new simple_html_dom();
		$locationhtml->load($result2);

		if($locationhtml){
			$streetAddress = $locationhtml->find('span[itemprop=streetAddress]', 0)->innertext;
			$city = $locationhtml->find('span[itemprop=addressLocality]', 0)->innertext;
			$state = $locationhtml->find('span[itemprop=addressRegion]', 0)->innertext;
			$zip = $locationhtml->find('span[itemprop=postalCode]', 0)->innertext;
			$phone = $locationhtml->find('span[itemprop=telephone] .lmc_sessions', 0)->innertext;
			fputcsv($fp, array($name, $streetAddress, $city, $state, $zip, $phone));

			$places = $db->query("
				select *
				from titleboxingclub
				where name = '".str_replace("'", "''", $name)."'
			");
			if($places && $places->num_rows > 0){
				// already exists
			} else {
				$newPlaces = true;
				$db->query("
					insert into titleboxingclub
					(name, address, city, state, zip, phone)
					values
					('".str_replace("'", "''", $name)."', '".str_replace("'", "''", $streetAddress)."', '".str_replace("'", "''", $city)."', '".$state."', '".$zip."', '".$phone."')
				");
				fputcsv($nfp, array($name, $streetAddress, $city, $state, $zip, $phone));
			}
		}
	}

	fclose($fp);
	fclose($nfp);

	  //create the email
	$transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
	->setUsername('donotreply@firstvoicemanager.com')
	->setPassword('1105firstvoice');
	$mailer = Swift_Mailer::newInstance($transport);
	$message = Swift_Message::newInstance()
	->setSubject($subject)
	->setFrom(array('donotreply@tsdemos.com' => $subject))
	->setTo($to)
	->setBody($subject, 'text/html')
	->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
		->setFilename($filename.'.csv'));
	$result = $mailer->send($message);

    if($newPlaces == true){
        //create the email
        $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
        ->setUsername('donotreply@firstvoicemanager.com')
        ->setPassword('1105firstvoice');
        $mailer = Swift_Mailer::newInstance($transport);
        $message = Swift_Message::newInstance()
          ->setSubject($subject)
          ->setFrom(array('donotreply@tsdemos.com' => $subject))
          ->setTo($salesRep)
          ->setBody('There have been changes to '.$subject, 'text/html')
          ->attach(Swift_Attachment::fromPath('reports/new_'.$filename.'.csv')
              ->setFilename('new_'.$filename.'.csv'));
        $result = $mailer->send($message);
    }
?>