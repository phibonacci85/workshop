<?php
ini_set('max_execution_time', 3600);
include('simple_html_dom.php');
require_once('./swiftmailer/swift_required.php');
@ $db = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_screenscrapers_jb');

$to = array('jbaker@think-safe.com');
$subject = 'American Legion';
$filename = 'americanlegion';
$fp = fopen('reports/' . $filename . '.csv', 'w');
fputcsv($fp, array('Name', 'Address', 'Address 2', 'Phone'));

for($i = 101; $i <= 99999; $i += 100) {
    $zip = str_pad($i, 5, '0', STR_PAD_LEFT);
    echo $zip.'<br />';
    $url = 'http://www.members.legion.org/CGI-BIN/lansaweb?webapp=MYLEPOST+webrtn=wr_editlcr+ml=LANSA:XHTML+partition=TAL+language=ENG';
    $fields = array(
        '_SERVICENAME' => 'MYLEPOST_wr_dsplcr',
        '_WEBAPP' => 'MYLEPOST',
        '_WEBROUTINE' => 'wr_dsplcr',
        '_PARTITION' => 'TAL',
        '_LANGUAGE' => 'ENG',
        '_SESSIONKEY' => '',
        '_LW3TRCID' => 'false',
        'COUNTRY' => '',
        'CITY' => '',
        'STATE' => '',
        'ZIP' => $zip,
        'ENTMILES' => '100',
        'PST_LIST..' => '0'
    );

    $fields_string = '';
    foreach ($fields as $key => $value) {
        $fields_string .= $key . '=' . $value . '&';
    }
    rtrim($fields_string, '&');

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);


    $result = curl_exec($ch);
    curl_close($ch);

    $directoryhtml = new simple_html_dom();
    $directoryhtml->load($result);
    $tableRows = $directoryhtml->find('#findPostResults tbody tr');

    foreach ($tableRows as $row) {
        $name = strtolower(trim(strip_tags($row->find('h3', 1))));
        $addressParts = explode('<br/>', $row->find('a', 0));
        $address1 = strtolower(trim(strip_tags($addressParts[0])));
        $address2 = strtolower(trim(strip_tags($addressParts[1])));
        $phone = strtolower(trim(strip_tags($row->find('a', 1))));
        //echo $name . '<br />' . $address1 . '<br />' . $address2 . '<br />' . $phone . '<br /><br />';

        $existing = $db->query("
            SELECT *
            FROM americanlegion
            WHERE name = '" . $name . "'
            AND address1 = '" . $address1 . "'
            AND address2 = '" . $address2 . "'
            AND phone = '" . $phone . "'
        ");
        if ($existing->num_rows == 0) {
            fputcsv($fp, array($name, $address1, $address2, $phone));
            $insertRecord = $db->query("
                INSERT INTO americanlegion
                (name, address1, address2, phone)
                VALUES
                ('" . $name . "', '" . $address1 . "', '" . $address2 . "', '" . $phone . "')
            ");
        }
    }
}


fclose($fp);
//create the email
$transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
$mailer = Swift_Mailer::newInstance($transport);
$message = Swift_Message::newInstance()
    ->setSubject($subject)
    ->setFrom(array('donotreply@tsdemos.com' => $subject))
    ->setTo($to)
    ->setBody($subject, 'text/html')
    ->attach(Swift_Attachment::fromPath('reports/' . $filename . '.csv')
        ->setFilename($filename . '.csv'));
$result = $mailer->send($message);

?>