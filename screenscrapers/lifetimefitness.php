<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Lifetime Fitness';
  $filename = 'lifetimefitness';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));
  
	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://www.lifetimefitness.com/en/locate-club/view-all-clubs.html");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	curl_close($ch);
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
    $links = $directoryhtml->find('.allClubsContainer a');
	foreach($links as $link)
	{  
        $name = $link->innertext;
        $url = 'https://www.lifetimefitness.com'.$link->getAttribute('href');
        $url = str_replace('.html', '/your-lt.html', $url);
        //get webpage
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $locationshtml = new simple_html_dom();
        $locationshtml->load($result);
        $location = $locationshtml->find('.clubInfoBar p', 0)->innertext;
        $location = explode('<br />', $location);
        $location[1] =  preg_replace('/\s+/', '', $location[1]);
        $location[1] = trim(strip_tags(str_replace('&nbsp;', ' ', $location[1])));
        $name = trim(strip_tags($name));
        $address = trim(strip_tags($location[0]));
        $locale = substr($location[1], 0, strlen($location[1])-12);
        $phone = substr($location[1], strlen($location[1])-12, 12);
        fputcsv($fp, array($name, $address, $locale, $phone));
	}
  
  fclose($fp);
  
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  
?>