<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	$DEBUG_MODE = false;
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$countycounter = 0;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://www.assistedlivingfacilities.org/directory/?t=1");
	curl_setopt($ch, CURLOPT_HEADER, false);
	//curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	$roundcount = 0;
	foreach($directoryhtml->find('table a') as $e)
	{
		$stateurl = $e->getAttribute('href');
		
		if($stateurl == '/directory/al/')
			$roundcount++;
		if($roundcount == 2)
			break;
			
		if(strpos($stateurl, 'directory') !== false)
		{
			$name = ',';
			$address = ',';
			$phone = ',';
			$capacity = ',';
			
			if($DEBUG_MODE)
				echo 'http://www.assistedlivingfacilities.org'.$stateurl.'<br />';
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://www.assistedlivingfacilities.org'.$stateurl.'?t=1');
			curl_setopt($ch, CURLOPT_HEADER, false);
			//curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$statewide = curl_exec($ch);
			curl_close($ch);
			$statecounter++;
			
			$statehtml = new simple_html_dom();
			$statehtml->load($statewide);
			
			foreach($statehtml->find('table a') as $e2)
			{
				$countyurl = $e2->getAttribute('href');
				if(strpos($countyurl, $stateurl) !== false)
				{
					if($DEBUG_MODE)
						echo "\t".'http://www.assistedlivingfacilities.org'.$countyurl.'<br />';
					
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'http://www.assistedlivingfacilities.org'.$countyurl);
					curl_setopt($ch, CURLOPT_HEADER, false);
					//curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$countywide = curl_exec($ch);
					curl_close($ch);
					$countycounter++;
					
					$countyhtml = new simple_html_dom();
					$countyhtml->load($countywide);
					
					if(is_object($countyhtml->find('table.lined', -1)))
					{
						foreach($countyhtml->find('table.lined', -1)->find('tr') as $e3)
						{
							$thiscount = 0;
							foreach($e3->find('td') as $e4)
							{
								if(!strpos('Click Here', $e4->innertext))
								{
									if(!in_array($e4->innertext, array('Name', 'Address', 'Phone', 'Capacity', 'Photo', '', ' ', '<br>'))) {
										if($thiscount == 0)
											$line = strtoupper(str_replace('directory','',str_replace('/','',$stateurl)));
											
										$thiscount++;
										$line .= ','.str_replace('<br>', '', str_replace(',', ' ', $e4->innertext));
									}
								} else
									break;
							}
							if($thiscount > 0 && strlen($line) > 10 && strlen($line) != 510)
							{
								if($DEBUG_MODE)
									echo $line.'<br />';
								$line = '';
							}
						}
					}
					flush();
				}
			}
		} //not a valid url
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />This page was created in '.$totaltime.' seconds<br />;
		When used '.$statecounter.' states and '.$countycounter.' counties which makes for a total of '.($statecounter+$countycounter).' links.<br />';
?>
</body>
</html>



