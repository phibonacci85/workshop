<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
	
	$DEBUG_MODE = false;
	
	function abbreviate_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($full_state, $ab_state, $state_to_abreviate);
	}
	function full_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($ab_state, $full_state, $state_to_abreviate);
	}
	
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$gymcounter = 0;
	$results = array();

	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://tigerrockmartialarts.com/find-academy");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	
	for($i = 0;$i >= 0;$i++) {
		if($directoryhtml->find('.academyaddress', $i)) {
			$gymcounter++;
		
			$name = trim($directoryhtml->find('.academylabel', $i)->innertext);
			$additionalinformation = $directoryhtml->find('.academylabel', $i)->getAttribute('href');
			
			$details = explode('<br />', $directoryhtml->find('.academyaddress', $i)->innertext);
			$address = trim(preg_replace('!\s+!', ' ', $details[0]));
			
			$details = explode(',', $details[1]);
			$city = trim($details[0]);
			$zip = substr(trim($details[1]), -5, 5);
			$state = trim(str_replace($zip, '', $details[1]));
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://tigerrockmartialarts.com'.$additionalinformation);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			
			$gymhtml = new simple_html_dom();
			$gymhtml->load($result);
			
			$details = explode('<br />', $gymhtml->find('.academycontact',0));
			if($gymhtml->find('.academycontact a',0))
				$website = trim($gymhtml->find('.academycontact a',0)->innertext);
			$phone = trim(str_replace('Phone:', '', $details[1]));
			$fax = trim(str_replace('Fax:', '', $details[2]));
			
			$gymid = str_replace('http://trma', '', str_replace('.tigerrockmartialarts.com', '', $website));
			
			
			$results[] = array(
				'name'=> $name,
				'gymid'=> $gymid,
				'address'=> $address,
				'city'=> $city,
				'state'=> $state,
				'zip'=> $zip,
				'phone'=> $phone,
				'fax'=> $fax,
				'website'=> $website
			);
		} else
			break;
	}
	if($DEBUG_MODE)
		echo '<pre>'.print_r($results,1).'</pre>';
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />
		This page was created in '.$totaltime.' seconds<br />
		Link Counts<br />
		---------------<br />
		States:'.$statecounter.'<br />
		Gyms:'.$gymcounter.'<br />';
	
	// UPDATE ON CHANGE 
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$changes = array();
	$updatecounter = 0;
	$insertcounter = 0;
	
	//check all old data
	foreach($results as $key => $val) {
		echo $val['name'].','.$val['gymid'].','.$val['address'].','.$val['city'].','.$val['state'].','.$val['zip'].','.$val['phone'].','.$val['fax'].','.$val['website'].'<br />';
			
		$getolddata = $db->query("
			select *
			from tigerrockmartialarts
			where gymid = '".$db->real_escape_string($val['gymid'])."'
		");
		if($row = $getolddata->fetch_assoc()) {
			//data is already in database
			//update record
			$db->query("
				update tigerrockmartialarts
				set phone = '".$db->real_escape_string($val['phone'])."',
					name = '".$db->real_escape_string($val['name'])."',
					address = '".$db->real_escape_string($val['address'])."',
					city = '".$db->real_escape_string($val['city'])."',
					state = '".$db->real_escape_string($val['state'])."',
					zip = '".$db->real_escape_string($val['zip'])."',
					fax = '".$db->real_escape_string($val['fax'])."',
					website = '".$db->real_escape_string($val['website'])."'
				where id = '".$row['id']."'
			");
			
			//$changes[] = $results[$key];
			$updatecounter++;
			
			//remove from array
			unset($results[$key]);
		}
	}
	
	//insert data we could not find information for
	if(count($results) > 0) {
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $key => $val) {
			if($insertcounter == 0)
				$query = 'insert into tigerrockmartialarts (name, gymid, address, city, state, zip, phone, fax, website, creation) values '."('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['gymid'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['fax'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			else
				$query .= ",('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['gymid'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['fax'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			$insertcounter++;
			
			$results[$key]['creation'] = date('Y-m-d H:i:s');
			$results[$key]['updated'] = date('Y-m-d H:i:s');
			$changes[] = $results[$key];
		}
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	echo '
	Processing Queries completed in '.$totaltime.' seconds<br />
	Updated '.$updatecounter.' rows in the database.<br />
	Inserted '.$insertcounter.' rows into the database.<br />';
	
	//echo '<pre>'.print_r($changes,1).'</pre>';
	
	//check to see if we did any changes
	if(count($changes) > 0)
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		$rowcounter = 1;
		
		//add phpexcel caching
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//initilize the spreedsheet
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Think Safe");
		$objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
		$objPHPExcel->getProperties()->setTitle("Tiger Rock Martial Arts Report");
		$objPHPExcel->getProperties()->setSubject("Tiger Rock Martial Arts  Report");
		$objPHPExcel->getProperties()->setDescription("A report for new changes to the Tiger Rock Martial Arts database");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$changesheet = $objPHPExcel->getActiveSheet();
		$changesheet
			->SetCellValue('A1', 'Name')
			->SetCellValue('B1', 'Gym ID')
			->SetCellValue('C1', 'Address')
			->SetCellValue('D1', 'City')
			->SetCellValue('E1', 'State')
			->SetCellValue('F1', 'Zip')
			->SetCellValue('G1', 'Phone')
			->SetCellValue('H1', 'Fax')
			->SetCellValue('I1', 'Website')
			->SetCellValue('J1', 'First Recorded')
			->SetCellValue('K1', 'Last Changed');
			
		foreach($changes as $key => $val)
		{
			$rowcounter++;
			$changesheet
				->SetCellValue('A'.$rowcounter, html_entity_decode($val['name']))
				->SetCellValue('B'.$rowcounter, html_entity_decode($val['gymid']))
				->SetCellValue('C'.$rowcounter, html_entity_decode($val['address']))
				->SetCellValue('D'.$rowcounter, html_entity_decode($val['city']))
				->SetCellValue('E'.$rowcounter, html_entity_decode($val['state']))
				->SetCellValue('F'.$rowcounter, html_entity_decode($val['zip']))
				->SetCellValue('G'.$rowcounter, html_entity_decode($val['phone']))
				->SetCellValue('H'.$rowcounter, html_entity_decode($val['fax']))
				->SetCellValue('I'.$rowcounter, html_entity_decode($val['website']))
				->SetCellValue('J'.$rowcounter, html_entity_decode($val['creation']))
				->SetCellValue('K'.$rowcounter, html_entity_decode($val['updated']));
				
			$changesheet->getColumnDimension('A')->setAutoSize(true);
			$changesheet->getColumnDimension('B')->setAutoSize(true);
			$changesheet->getColumnDimension('C')->setAutoSize(true);
			$changesheet->getColumnDimension('D')->setAutoSize(true);
			$changesheet->getColumnDimension('F')->setAutoSize(true);
			$changesheet->getColumnDimension('G')->setAutoSize(true);
			$changesheet->getColumnDimension('H')->setAutoSize(true);	
			$changesheet->getColumnDimension('I')->setAutoSize(true);		
			$changesheet->getColumnDimension('J')->setAutoSize(true);		
			$changesheet->getColumnDimension('k')->setAutoSize(true);		
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$report = '_'.date("Y-m-d_H:i:s", time());
		$objWriter->save('reports/tigerrockmartialarts_report'.$report.'.xlsx');
		
		$subject = 'Tiger Rock Martial Arts Report';
		$to = '';
		$body = 'There are new changes to Tiger Rock Martial Arts!  Here are the new changes.(see attached) This report uses a scrubbing mechanism so if some data does not display correctly please see your web systems admin.';
		$attachments = 'reports/tigerrockmartialarts_report'.$report.'.xlsx';
		
		
		//create the email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array('donotreply@tsdemos.com' => 'Tiger Rock Martial Arts Reports'))
			->setTo(array('mboyles@think-safe.com'))
			//->setTo(array('aarox04@gmail.com'))
			->setCc(array('pwickham@think-safe.com'))
			->setBody($body, 'text/html')
			->addPart($body, 'text/html')
			->attach(Swift_Attachment::fromPath($attachments)
				->setFilename('tigerrockmartialarts.xlsx'));
		$result = $mailer->send($message);
		
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$totaltime = ($endtime - $starttime); 
		echo '
		Creating spreadsheet completed in '.$totaltime.' seconds<br />
		Added '.$rowcounter.' rows to the spreadsheet.<br />';
	} else {
		echo '
		No changes were made, no email was sent.<br />';
	}
?>
</body>
</html>