<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	$DEBUG_MODE = false;
	$DATABASE = 'clubmetrousa';
	$SITENAME = 'Club Metro USA';
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$gymcounter = 0;
	$results = array();

	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://www.clubmetrousa.com/find-a-club/");
	curl_setopt($ch, CURLOPT_HEADER, false);
	//curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	foreach($directoryhtml->find('.entry-content a') as $e)
	{
		$gymurl = $e->getAttribute('href');
		$website = $gymurl;
		
		//get each state
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $website);
		curl_setopt($ch, CURLOPT_HEADER, false);
		//curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$gymcurl = curl_exec($ch);
		curl_close($ch);
		
		$gymhtml = new simple_html_dom();
		$gymhtml->load($gymcurl);
		
		//gather the data!
		foreach($gymhtml->find('#franchise_info p') as $e2) {
			$info = explode('<br />', $e2->innertext);
			$name = trim(str_replace('Club Metro of ', '', $info[0]));
			
			
			//check if has address 2
			if(count($info) == 5) {
				//has address 2
				$address = trim($info[1]);
				$address2 = trim($info[2]);
				$citystatezip = $info[3];
				$phone = trim($info[4]);
			} else {
				//no address 2
				$address = trim($info[1]);
				$address2 = '';
				$citystatezip = $info[2];
				$phone = trim($info[3]);
			}
			$citystatezip = explode(',', $citystatezip);
			$city = trim($citystatezip[0]);
			$state = trim(substr($citystatezip[1], 1, 2));
			$zip = trim(substr(trim(str_replace("&nbsp;", '', $citystatezip[1])), 3));
			
			//get phone
				
			$results[] = array(
				'name'=> $name,
				'address'=> $address,
				'address2'=> $address2,
				'city'=> $city,
				'state'=> $state,
				'zip'=> $zip,
				'phone'=> $phone,
				'website'=> $website
			);
			
			$gymcounter++;
		}
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<pre>'.print_r($results,1).'</pre>';
		
	if($DEBUG_MODE)
		echo '<br /><br />
		This page was created in '.$totaltime.' seconds<br />
		Link Counts<br />
		---------------<br />
		Gyms:'.$gymcounter.'<br />';
	
	/* UPDATE ON CHANGE */
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$changes = array();
	$insertcounter = 0;
	$updatecounter = 0;
	
	//check all old data
	foreach($results as $key => $val) {
		if($DEBUG_MODE)
			echo $val['name'].','.$val['address'].','.$val['address2'].','.$val['city'].','.$val['state'].','.$val['zip'].','.$val['phone'].','.$val['website'].'<br />';
			
		$getolddata = $db->query("
			select *
			from ".$DATABASE."
			where website = '".$db->real_escape_string($val['website'])."'
		");
		if($row = $getolddata->fetch_assoc()) {
			if($val['address'] != $row['address']
			|| $val['address2'] != $row['address2']
			|| $val['city'] != $row['city']
			|| $val['state'] != $row['state']
			|| $val['zip'] != $row['zip']
			|| $val['phone'] != $row['phone']
			|| $val['name'] != $row['name']
			) {
				//updated data
				$results[$key]['creation'] = $row['creation'];
				$results[$key]['updated'] = date('Y-m-d H:i:s');
				$query = "
					update ".$DATABASE."
					set address = '".$db->real_escape_string($val['address'])."',
						address2 = '".$db->real_escape_string($val['address2'])."',
						city = '".$db->real_escape_string($val['city'])."',
						state = '".$db->real_escape_string($val['state'])."',
						zip = '".$db->real_escape_string($val['zip'])."',
						phone = '".$db->real_escape_string($val['phone'])."',
						name = '".$db->real_escape_string($val['name'])."'
					where id = '".$row['id']."'
				";
				
				if($DEBUG_MODE)
					echo $query;
					
				$db->query($query);
				$changes[] = $results[$key];
				$updatecounter++;
				unset($results[$key]);
			} else
				//data is already in database, remove from array
				unset($results[$key]);
		}
	}
	
	//insert data we could not find information for
	if(count($results) > 0)
	{
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $key => $value)
		{
			if($insertcounter == 0)
				$query = 'insert into '.$DATABASE.' (name, city, state, zip, address, address2, phone, website, creation) values ';
			else
				$query .= ',';
			
			$query .= "('".$db->real_escape_string($value['name'])."','".$db->real_escape_string($value['city'])."','".$db->real_escape_string($value['state'])."','".$db->real_escape_string($value['zip'])."','".$db->real_escape_string($value['address'])."','".$db->real_escape_string($value['address2'])."','".$db->real_escape_string($value['phone'])."','".$db->real_escape_string($value['website'])."', '".date('Y-m-d H:i:s')."')";
			$results[$key]['creation'] = date('Y-m-d H:i:s');
			$results[$key]['updated'] = date('Y-m-d H:i:s');
			
			$changes[] = $results[$key];
			$insertcounter++;
		}
		if($DEBUG_MODE)
			echo $query;
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '
		Processing Queries completed in '.$totaltime.' seconds<br />
		Inserted '.$insertcounter.' rows into the database.<br />
		Updated '.$updatecounter.' rows into the database.<br />';
	
	//check to see if we did any changes
	if(count($changes) > 0)
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		$rowcounter = 1;
		
		//add phpexcel caching
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//initilize the spreedsheet
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Think Safe");
		$objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
		$objPHPExcel->getProperties()->setTitle($SITENAME." Report");
		$objPHPExcel->getProperties()->setSubject($SITENAME."  Report");
		$objPHPExcel->getProperties()->setDescription("A report for new changes to the Lucille Roberts database");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$changesheet = $objPHPExcel->getActiveSheet();
		$changesheet
			->SetCellValue('A1', 'Name')
			->SetCellValue('B1', 'City')
			->SetCellValue('C1', 'State')
			->SetCellValue('D1', 'Zip')
			->SetCellValue('E1', 'Address')
			->SetCellValue('F1', 'Address2')
			->SetCellValue('G1', 'Phone')
			->SetCellValue('H1', 'Website')
			->SetCellValue('I1', 'First Recorded')
			->SetCellValue('J1', 'Updated');
			
		foreach($changes as $key => $value)
		{
			$rowcounter++;
			$changesheet
				->SetCellValue('A'.$rowcounter, html_entity_decode($value['name']))
				->SetCellValue('B'.$rowcounter, html_entity_decode($value['city']))
				->SetCellValue('C'.$rowcounter, html_entity_decode($value['state']))
				->SetCellValue('D'.$rowcounter, html_entity_decode($value['zip']))
				->SetCellValue('E'.$rowcounter, html_entity_decode($value['address']))
				->SetCellValue('F'.$rowcounter, html_entity_decode($value['address2']))
				->SetCellValue('G'.$rowcounter, html_entity_decode($value['phone']))
				->SetCellValue('H'.$rowcounter, html_entity_decode($value['website']))
				->SetCellValue('I'.$rowcounter, html_entity_decode($value['creation']))
				->SetCellValue('J'.$rowcounter, html_entity_decode($value['updated']));
				
			$changesheet->getColumnDimension('A')->setAutoSize(true);
			$changesheet->getColumnDimension('B')->setAutoSize(true);
			$changesheet->getColumnDimension('C')->setAutoSize(true);
			$changesheet->getColumnDimension('D')->setAutoSize(true);
			$changesheet->getColumnDimension('E')->setAutoSize(true);
			$changesheet->getColumnDimension('F')->setAutoSize(true);
			$changesheet->getColumnDimension('G')->setAutoSize(true);
			$changesheet->getColumnDimension('H')->setAutoSize(true);	
			$changesheet->getColumnDimension('I')->setAutoSize(true);	
			$changesheet->getColumnDimension('J')->setAutoSize(true);
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$report = '_'.date("Y-m-d_H:i:s", time());
		$objWriter->save('reports/'.$DATABASE.'_report'.$report.'.xlsx');
		
		$subject = $SITENAME.' Report';
		$to = '';
		$body = 'There are new changes to '.$SITENAME.'!  Here are the new changes. (see attached)';
		$attachments = 'reports/'.$DATABASE.'_report'.$report.'.xlsx';
		
		
		//create the email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array('donotreply@tsdemos.com' => $SITENAME.' Reports'))
			->setBody($body, 'text/html')
			->addPart($body, 'text/html')
			->attach(Swift_Attachment::fromPath($attachments)
				->setFilename($DATABASE.'.xlsx'));
				
		if($DEBUG_MODE)
			$message->setTo(array('aarox04@gmail.com'));
		else {
			$message->setTo(array('Mboyles@think-safe.com'));
			$message->setCc(array('pwickham@think-safe.com'));
		}
		
		$result = $mailer->send($message);
		
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$totaltime = ($endtime - $starttime); 
		
		if($DEBUG_MODE)
			echo '
			Creating spreadsheet completed in '.$totaltime.' seconds<br />
			Added '.$rowcounter.' rows to the spreadsheet.<br />';
	} else {
		if($DEBUG_MODE)
			echo 'No changes were made, no email was sent.<br />';
	}
?>
</body>
</html>