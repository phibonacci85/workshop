<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	$DEBUG_MODE = false;
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$gymcounter = 0;
	$results = array();

	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://www.anytimefitness.com/find-gym/list/start");
	curl_setopt($ch, CURLOPT_HEADER, false);
	//curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	foreach($directoryhtml->find('#states optgroup[label="United States"] option') as $e)
	{
		$stateurl = $e->getAttribute('value');
		$statecounter++;
		
		//get each state
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://www.anytimefitness.com/find-gym/list/'.$stateurl);
		curl_setopt($ch, CURLOPT_HEADER, false);
		//curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$statecurl = curl_exec($ch);
		curl_close($ch);
		
		$statehtml = new simple_html_dom();
		$statehtml->load($statecurl);
		
		//gather the data!
		foreach(array_slice($statehtml->find('table tbody tr'),1) as $e2) {
			$status = str_replace(',', ' ', $e2->find('td',0)->innertext);
			$location = str_replace('&#39;', '\'', str_replace(',', ' ', $e2->find('td',1)->find('strong', 0)->innertext));
			if($e2->find('td',2)->find('span',0))
				$address = str_replace('&#39;', '\'', preg_replace('/<[^>]*>/', '', str_replace(',', ' ', str_replace('<br />', '', $e2->find('td',2)->find('span',0)->innertext))));
			//$zip = str_replace(',', ' ', $e2->find('td',2)->find('span',1)->innertext);
			$phone = str_replace(',', ' ', $e2->find('td',3)->innertext);
			
			$website = $e2->find('td',4)->find('a', 0)->getAttribute('href');
			if(substr($website, 0, 6) == '/gyms/')
				$website = 'http://www.anytimefitness.com'.$website;
			
			$result = preg_replace('!\s+!', ' ', $stateurl.','.$status.','.$location.','.$address.','.$phone.','.$website);
			
			if($DEBUG_MODE)
				echo $result.'<br />';
				
			$results[] = explode(',', $result);
			$gymcounter++;
		}
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />
		This page was created in '.$totaltime.' seconds<br />
		Link Counts<br />
		---------------<br />
		States:'.$statecounter.'<br />
		Gyms:'.$gymcounter.'<br />';
	
	/* UPDATE ON CHANGE */
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$changes = array();
	$updatecounter = 0;
	$insertcounter = 0;
	
	//check all old data
	foreach($results as $value => $key) {
		$getolddata = $db->query("
			select *
			from anytimefitness
			where state = '".$key[0]."'
			and location = '".$db->real_escape_string($key[2])."'
			and website = '".$key[5]."'
		");
		if($row = $getolddata->fetch_assoc()) {
			//is the status the same as the old data?
			if($key[1] != $row['status']) {
				$updatecounter++;
				
				//new data!
				$db->query("
					update anytimefitness
					set status = '".$key[1]."',
						address = '".$key[3]."',
						phone = '".$key[4]."'
					where id = '".$row['id']."'
				");
				$key[] = $row['creation'];
				$key[] = date('Y-m-d H:i:s');
				$key[] = $row['status'];
				$changes[] = $key;
			}
			//remove from array
			unset($results[$value]);
		}
	}
	
	//insert data we could not find information for
	if(count($results) > 0)
	{
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $value => $key)
		{
			if($insertcounter == 0)
				$query = 'insert into anytimefitness (state, status, location, address, phone, website, creation) values '."('".$db->real_escape_string($key[0])."','".$db->real_escape_string($key[1])."','".$db->real_escape_string($key[2])."','".$db->real_escape_string($key[3])."','".$db->real_escape_string($key[4])."','".$db->real_escape_string($key[5])."', '".date('Y-m-d H:i:s')."')";
			else
				$query .= ",('".$db->real_escape_string($key[0])."','".$db->real_escape_string($key[1])."','".$db->real_escape_string($key[2])."','".$db->real_escape_string($key[3])."','".$db->real_escape_string($key[4])."','".$db->real_escape_string($key[5])."', '".date('Y-m-d H:i:s')."')";
			$insertcounter++;
			
			$key[] = date('Y-m-d H:i:s');
			$key[] = date('Y-m-d H:i:s');
			$key[] = '(new location)';
			$changes[] = $key;
		}
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '
		Processing Queries completed in '.$totaltime.' seconds<br />
		Updated '.$updatecounter.' rows in the database.<br />
		Inserted '.$insertcounter.' rows into the database.<br />';
	
	if($DEBUG_MODE)
		echo '<pre>'.print_r($changes,1).'</pre>';
	
	//check to see if we did any changes
	if(count($changes) > 0)
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		$rowcounter = 1;
		
		//add phpexcel caching
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//initilize the spreedsheet
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Think Safe");
		$objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
		$objPHPExcel->getProperties()->setTitle("Anytime Fitness Report");
		$objPHPExcel->getProperties()->setSubject("Anytime Fitness  Report");
		$objPHPExcel->getProperties()->setDescription("A report for new changes to the Anytime Fitness database");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$changesheet = $objPHPExcel->getActiveSheet();
		$changesheet
			->SetCellValue('A1', 'State')
			->SetCellValue('B1', 'Old Status')
			->SetCellValue('C1', 'New Status')
			->SetCellValue('D1', 'Location')
			->SetCellValue('E1', 'Address')
			->SetCellValue('F1', 'Phone')
			->SetCellValue('g1', 'Website')
			->SetCellValue('H1', 'First Recorded')
			->SetCellValue('I1', 'Last Changed');
			
		foreach($changes as $value => $key)
		{
			$rowcounter++;
			$changesheet
				->SetCellValue('A'.$rowcounter, html_entity_decode($key[0]))
				->SetCellValue('B'.$rowcounter, html_entity_decode($key[8]))
				->SetCellValue('C'.$rowcounter, html_entity_decode($key[1]))
				->SetCellValue('D'.$rowcounter, html_entity_decode($key[2]))
				->SetCellValue('E'.$rowcounter, html_entity_decode($key[3]))
				->SetCellValue('F'.$rowcounter, html_entity_decode($key[4]))
				->SetCellValue('G'.$rowcounter, html_entity_decode($key[5]))
				->SetCellValue('H'.$rowcounter, html_entity_decode($key[6]))
				->SetCellValue('I'.$rowcounter, html_entity_decode($key[7]));
				
			$changesheet->getColumnDimension('A')->setAutoSize(true);
			$changesheet->getColumnDimension('B')->setAutoSize(true);
			$changesheet->getColumnDimension('C')->setAutoSize(true);
			$changesheet->getColumnDimension('D')->setAutoSize(true);
			$changesheet->getColumnDimension('E')->setAutoSize(true);
			$changesheet->getColumnDimension('F')->setAutoSize(true);
			$changesheet->getColumnDimension('G')->setAutoSize(true);
			$changesheet->getColumnDimension('H')->setAutoSize(true);	
			$changesheet->getColumnDimension('I')->setAutoSize(true);	
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$report = '_'.date("Y-m-d_H:i:s", time());
		$objWriter->save('reports/anytimefitness_report'.$report.'.xlsx');
		
		$subject = 'Anytime Fitness Report';
		$to = '';
		$body = 'There are new changes to Anytime Fitness!  Here are the new changes. (see attached)';
		$attachments = 'reports/anytimefitness_report'.$report.'.xlsx';
		
		
		//create the email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array('donotreply@tsdemos.com' => 'Anytime Fitness Reports'))
			->setTo(array('Mboyles@think-safe.com'))
			//->setTo(array('aarox04@gmail.com'))
			->setCc(array('pwickham@think-safe.com'))
			->setBody($body, 'text/html')
			->addPart($body, 'text/html')
			->attach(Swift_Attachment::fromPath($attachments)
				->setFilename('anytimefitnessreport.xlsx'));
		$result = $mailer->send($message);
		
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$totaltime = ($endtime - $starttime); 
		
		if($DEBUG_MODE)
			echo '
			Creating spreadsheet completed in '.$totaltime.' seconds<br />
			Added '.$rowcounter.' rows to the spreadsheet.<br />';
	} else {
		if($DEBUG_MODE)
			echo 'No changes were made, no email was sent.<br />';
	}
?>
</body>
</html>