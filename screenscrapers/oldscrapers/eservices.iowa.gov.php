<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
	
	$DEBUG_MODE = false;
	
	function abbreviate_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($full_state, $ab_state, $state_to_abreviate);
	}
	function full_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($ab_state, $full_state, $state_to_abreviate);
	}
	
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$gymcounter = 0;
	$locations = array();
	$results = array();

	//get webpage
	for($x=1;$x<120;$x++) {//to 120
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://eservices.iowa.gov/pmsb/index.php/licenses/index/page:".$x."/License.license_trade_id:4");
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		
		$directoryhtml = new simple_html_dom();
		$directoryhtml->load($result);
		
		
		$e = $directoryhtml->find('table',1);
		$details = array_slice($e->find('tr'),1);
		foreach($details as $det) {
			$gymcounter++;
			
			//add to results array
			$results[] = array(
				'firstname'=> trim($det->find('td', 1)->innertext),
				'middlename'=> trim($det->find('td', 2)->innertext),
				'lastname'=> trim($det->find('td', 0)->innertext),
				'licensenumber'=> trim($det->find('td', 3)->innertext),
				'profession'=> trim($det->find('td', 4)->innertext),
				'status'=> trim($det->find('td', 5)->innertext),
				'issued'=> trim($det->find('td', 6)->innertext),
				'expires'=> trim($det->find('td', 7)->innertext),
				'discipline'=> trim($det->find('td', 8)->innertext)
			);
		}
	}
	if($DEBUG_MODE)
		foreach($results as $key => $val) {
			echo "'".$db->real_escape_string($val['firstname'])."','".$db->real_escape_string($val['middlename'])."','".$db->real_escape_string($val['lastname'])."','".$db->real_escape_string($val['licensenumber'])."','".$db->real_escape_string($val['profession'])."','".$db->real_escape_string($val['status'])."','".$db->real_escape_string($val['issued'])."','".$db->real_escape_string($val['expires'])."','".$db->real_escape_string($val['discipline'])."<br />";
		}
		
	if($DEBUG_MODE)
		echo '<pre>'.print_r($results,1).'</pre>';
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />
		This page was created in '.$totaltime.' seconds<br />
		Link Counts<br />
		---------------<br />
		Licenses:'.$gymcounter.'<br />';
	
	// UPDATE ON CHANGE
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$insertcounter = 0;
	
	//insert data we could not find information for
	if(count($results) > 0) {
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $key => $val) {
			if($insertcounter == 0)
				$query = 'insert into eservicesiowa (firstname, middlename, lastname, trade, licensenumber, profession, status, issued, expires, discipline) values '."('".$db->real_escape_string($val['firstname'])."','".$db->real_escape_string($val['middlename'])."','".$db->real_escape_string($val['lastname'])."','refrigeration','".$db->real_escape_string($val['licensenumber'])."','".$db->real_escape_string($val['profession'])."','".$db->real_escape_string($val['status'])."','".$db->real_escape_string($val['issued'])."','".$db->real_escape_string($val['expires'])."','".$db->real_escape_string($val['discipline'])."')";
			else
				$query .= ",('".$db->real_escape_string($val['firstname'])."','".$db->real_escape_string($val['middlename'])."','".$db->real_escape_string($val['lastname'])."','refrigeration','".$db->real_escape_string($val['licensenumber'])."','".$db->real_escape_string($val['profession'])."','".$db->real_escape_string($val['status'])."','".$db->real_escape_string($val['issued'])."','".$db->real_escape_string($val['expires'])."','".$db->real_escape_string($val['discipline'])."')";
			$insertcounter++;
		}
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '
		Processing Queries completed in '.$totaltime.' seconds<br />
		Inserted '.$insertcounter.' rows into the database.<br />';
?>
</body>
</html> 