<?php
	ini_set('max_execution_time', 3600); 
	header('Content-Type: text/html; charset=utf-8');
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
	
	$DEBUG_MODE = false;
	
	function abbreviate_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($full_state, $ab_state, $state_to_abreviate);
	}
	function full_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($ab_state, $full_state, $state_to_abreviate);
	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$gymcounter = 0;
	$results = array();

	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://www.planetfitness.com/PlanetFitnessMap.aspx");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	foreach($directoryhtml->find('#stateDropDown_stateDropDown option') as $e)
	{
		$statecounter++;
		$state = $e->getAttribute('value');
		$fullstate = $e->innertext;
		
		//get each state
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://www.planetfitness.com/gyms/'.$state);
		curl_setopt($ch, CURLOPT_HEADER, false);
		//curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$statecurl = curl_exec($ch);
		curl_close($ch);
		
		$statehtml = new simple_html_dom();
		$statehtml->load($statecurl);
		
		//gather the data!
		foreach($statehtml->find('.club') as $e2) {
			$gymcounter++;
			
			if($e2->find('h3',0)) {
				$status = $e2->find('h3',0)->innertext;
				if($DEBUG_MODE)
					echo 'NAME:'.$name.' STATUS:'.$status.'<br />';
					
				if(strpos($status, 'Pre-Grand Opening Sale') !== false)
					$status = 'Pre-Grand Opening Sale';
				 else if(strpos($status, 'Now Open') !== false)
					$status = 'Now Open';//'Now Open';
				else if($status == '<span style="color:#FFCC00;"><strong></strong><br /></span>' || strpos($status, '<span></span>') !== false)
					$status = 'Open';
				else
					$status = 'UNKNOWN';
			} else
				$status = 'UNKNOWN';
				
			$name = str_replace(',', '', $e2->find('.clubtitleIcon',0)->innertext);
			$details = explode('<br/>', $e2->find('.clubinfo',0)->innertext);	
			
			if($DEBUG_MODE)
				echo '<pre>'.print_r($details,1).'</pre>';
			
			$detailstart = 0;
			if(strpos($details[$detailstart], 'Club Address:') !== false)
				$detailstart++;
			if(strpos($details[$detailstart], '<a href="') !== false)
				$detailstart++;
			$address = str_replace(',', '', $details[$detailstart]);
			$detailstart++;
			
			while(!is_numeric(str_replace(' ', '_', substr($details[$detailstart], -5, 5))))
				$detailstart++;
			$zip = substr($details[$detailstart], -5, 5);
			$city = str_replace(', '.$state.' '.$zip, '', $details[$detailstart]);
			$phone = substr($details[$detailstart+1], -12);	
			
			if($DEBUG_MODE)
				echo 'ZIP:'.$zip.' CITY:'.$city.' PHONE:'.$phone.' START:'.$detailstart.'<br />';
			
			$website = 'http://www.planetfitness.com'.$e2->find('.clubright a',0)->getAttribute('href');
			
			$results[] = array(
				'name'=> $name,
				'status'=> $status,
				'address'=> $address,
				'city'=> $city,
				'state'=> $fullstate,
				'zip'=> $zip,
				'phone'=> $phone,
				'website'=> $website
			);
		} 
	}
	if($DEBUG_MODE)
		echo '<pre>'.print_r($results,1).'</pre>';
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />
		This page was created in '.$totaltime.' seconds<br />
		Link Counts<br />
		---------------<br />
		States:'.$statecounter.'<br />
		Gyms:'.$gymcounter.'<br />';
	
	/* UPDATE ON CHANGE */
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$changes = array();
	$updatecounter = 0;
	$insertcounter = 0;
	
	//check all old data
	foreach($results as $key => $val) {
		if($DEBUG_MODE)
			echo $val['name'].','.$val['status'].','.$val['address'].','.$val['city'].','.$val['state'].','.$val['zip'].','.$val['phone'].','.$val['website'].'<br />';
			
		$getolddata = $db->query("
			select *
			from planetfitness
			where name = '".$db->real_escape_string($val['name'])."'
		");
		if($row = $getolddata->fetch_assoc()) {
			//data is already in database
			if($row['status'] != $val['status']) {
				//update record
				$db->query("
					update planetfitness
					set status = '".$db->real_escape_string($val['status'])."',
						phone = '".$db->real_escape_string($val['phone'])."',
						address = '".$db->real_escape_string($val['address'])."',
						city = '".$db->real_escape_string($val['city'])."',
						state = '".$db->real_escape_string($val['state'])."',
						zip = '".$db->real_escape_string($val['zip'])."',
						website = '".$db->real_escape_string($val['website'])."'
					where id = '".$row['id']."'
				");
				//add old status
				$results[$key]['oldstatus'] = $row['status'];
				$results[$key]['creation'] = $row['creation'];
				$results[$key]['updated'] = date('Y-m-d H:i:s');
				$changes[] = $results[$key];
				$updatecounter++;
			}
				
			//remove from array as we have most recent data
			unset($results[$key]);
		}
	}
	
	//insert data we could not find information for
	if(count($results) > 0) {
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $key => $val) {
			if($insertcounter == 0)
				$query = 'insert into planetfitness (name, status, address, city, state, zip, phone, website, creation) values '."('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['status'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			else
				$query .= ",('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['status'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			$insertcounter++;
			
			$results[$key]['oldstatus'] = 'New Location';
			$results[$key]['creation'] = date('Y-m-d H:i:s');
			$results[$key]['updated'] = date('Y-m-d H:i:s');
			$changes[] = $results[$key];
		}
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '
		Processing Queries completed in '.$totaltime.' seconds<br />
		Updated '.$updatecounter.' rows in the database.<br />
		Inserted '.$insertcounter.' rows into the database.<br />';
	
	if($DEBUG_MODE)	
		echo '<pre>'.print_r($changes,1).'</pre>';
	
	//check to see if we did any changes
	if(count($changes) > 0)
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		$rowcounter = 1;
		
		//add phpexcel caching
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//initilize the spreedsheet
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Think Safe");
		$objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
		$objPHPExcel->getProperties()->setTitle("Planet Fitness Report");
		$objPHPExcel->getProperties()->setSubject("Planet Fitness  Report");
		$objPHPExcel->getProperties()->setDescription("A report for new changes to the Planet Fitness database");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$changesheet = $objPHPExcel->getActiveSheet();
		$changesheet
			->SetCellValue('A1', 'Name')
			->SetCellValue('B1', 'Old Status')
			->SetCellValue('C1', 'Status')
			->SetCellValue('D1', 'Address')
			->SetCellValue('E1', 'City')
			->SetCellValue('F1', 'State')
			->SetCellValue('G1', 'Zip')
			->SetCellValue('H1', 'Phone')
			->SetCellValue('I1', 'Website')
			->SetCellValue('J1', 'First Recorded')
			->SetCellValue('K1', 'Last Changed');
			
		foreach($changes as $key => $val)
		{
			$rowcounter++;
			$changesheet
				->SetCellValue('A'.$rowcounter, $val['name'])
				->SetCellValue('B'.$rowcounter, $val['oldstatus'])
				->SetCellValue('C'.$rowcounter, $val['status'])
				->SetCellValue('D'.$rowcounter, $val['address'])
				->SetCellValue('E'.$rowcounter, $val['city'])
				->SetCellValue('F'.$rowcounter, $val['state'])
				->SetCellValue('G'.$rowcounter, $val['zip'])
				->SetCellValue('H'.$rowcounter, $val['phone'])
				->SetCellValue('I'.$rowcounter, $val['website'])
				->SetCellValue('J'.$rowcounter, $val['creation'])
				->SetCellValue('K'.$rowcounter, $val['updated']);
				
			$changesheet->getColumnDimension('A')->setAutoSize(true);
			$changesheet->getColumnDimension('B')->setAutoSize(true);
			$changesheet->getColumnDimension('C')->setAutoSize(true);
			$changesheet->getColumnDimension('D')->setAutoSize(true);
			$changesheet->getColumnDimension('F')->setAutoSize(true);
			$changesheet->getColumnDimension('G')->setAutoSize(true);
			$changesheet->getColumnDimension('H')->setAutoSize(true);	
			$changesheet->getColumnDimension('I')->setAutoSize(true);		
			$changesheet->getColumnDimension('J')->setAutoSize(true);		
			$changesheet->getColumnDimension('K')->setAutoSize(true);		
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$report = '_'.date("Y-m-d_H:i:s", time());
		$objWriter->save('reports/planetfitness_report'.$report.'.xlsx');
		
		$subject = 'Planet Fitness Report';
		$to = '';
		$body = 'There are new changes to Planet Fitness!  Here are the new changes.(see attached) This report uses a scrubbing mechanism so if some data does not display correctly please see your web systems admin.';
		$attachments = 'reports/planetfitness_report'.$report.'.xlsx';
		
		
		//create the email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array('donotreply@tsdemos.com' => 'Planet Fitness Reports'))
			->setTo(array('Mboyles@think-safe.com'))
			//->setTo(array('aarox04@gmail.com'))
			->setCc(array('pwickham@think-safe.com'))
			->setBody($body, 'text/html')
			->addPart($body, 'text/html')
			->attach(Swift_Attachment::fromPath($attachments)
				->setFilename('planetfitness.xlsx'));
		$result = $mailer->send($message);
		
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$totaltime = ($endtime - $starttime); 
		
		if($DEBUG_MODE)
			echo '
			Creating spreadsheet completed in '.$totaltime.' seconds<br />
			Added '.$rowcounter.' rows to the spreadsheet.<br />';
	} else {
		if($DEBUG_MODE)
			echo 'No changes were made, no email was sent.<br />';
	}
?>
</body>
</html>