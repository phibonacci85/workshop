<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
	
	$DEBUG_MODE = false;
	
	function abbreviate_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($full_state, $ab_state, $state_to_abreviate);
	}
	function full_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($ab_state, $full_state, $state_to_abreviate);
	}
	
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$gymcounter = 0;
	$locations = array();
	$results = array();

	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://mygym.com/locations");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	foreach($directoryhtml->find('script') as $e)
	{
		if(strpos($e, 'var markers = [') !== false) {
			$details = explode('{', $e->innertext);
			foreach(array_slice($details,1) as $det) {
				$gymcounter++;
				if(strpos($det, '\'country\': "usa",') !== false) {
					$eachdet = explode(':', $det);
					
					if($DEBUG_MODE)
						echo '<pre>'.print_r($eachdet,1).'</pre>';
					
					//$name = trim(str_replace('"', '', substr(trim(str_replace("'latitude'", '', $eachdet[2])), 0, -2)));
					$placeid = trim(str_replace('"', '', substr(trim(str_replace("'addrdisplay'", '', $eachdet[6])), 0, -2)));
					if(strpos($eachdet[11], 'http') !== false || trim(str_replace('"', '', substr(trim(str_replace("'getdirectionsurl'", '', $eachdet[11])), 0, -2)) == '')) {
						$name = str_replace(',', '', trim(str_replace('"', '', substr(trim(str_replace("'latitude'", '', $eachdet[2])), 0, -2))));
						$city = trim(str_replace('"', '', substr(trim(str_replace("'country'", '', $eachdet[9])), 0, -2)));
						$state = full_state(trim(str_replace('"', '', substr(trim(str_replace("'city'", '', $eachdet[8])), 0, -2))));
						
						if(is_numeric(str_replace('(', '', str_replace(')', '', str_replace('.', '', str_replace(' ', '', str_replace('-', '', trim(str_replace('"', '', substr(trim(str_replace("'state'", '', $eachdet[7])), -15, 5))))))))))
							$phone = trim(str_replace('"', '', substr(trim(str_replace("'state'", '', $eachdet[7])), -15, -2)));
						else
							$phone = '';
						
						if(trim(str_replace('"', '', substr(trim(str_replace("'getdirectionsurl'", '', $eachdet[11])), 0, -2)) == ''))
							$website = '';
						else
							$website = 'http://'.trim(str_replace('"', '', substr(trim(str_replace("'getdirectionsurl'", '', $eachdet[12])), 2, -2)));
							
						$results[] = array(
							'name'=> $name,
							'placeid'=> $placeid,
							'address'=> '',
							'city'=> $city,
							'state'=> $state,
							'zip'=> '',
							'phone'=> $phone,
							'map'=> '',
							'website'=> $website
						);
					} else {
						$website = 'http://mygym.com/'.trim(str_replace('"', '', substr(trim(str_replace("'getdirectionsurl'", '', $eachdet[11])), 0, -2)));
						$map = $website.'/map';
						
						$agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

						$ch2 = curl_init();
						curl_setopt($ch2, CURLOPT_URL, $website);
						//curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, 1);
						curl_setopt($ch2, CURLOPT_HEADER, false);
						curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch2, CURLOPT_USERAGENT, $agent);				
						$gymcurl = curl_exec($ch2);
						curl_close($ch2);

						$gymhtml = new simple_html_dom();
						$gymhtml->load($gymcurl);
						
						if($DEBUG_MODE)
							echo 'HTML:'.$gymhtml;
							
						$gymdets = explode('<br />', $gymhtml->find('#gyminfo', 0)->innertext);
						
						if($DEBUG_MODE)
							echo '<pre>'.print_r($gymdets,1).'</pre>';
							
						$name = $gymhtml->find('#gyminfo b', 0)->innertext;
						
						$row = 1;
						if(is_numeric(substr(trim($gymdets[$row]), 0,1)))
							$row = $row;
						else
							$row++;
						
						$address = str_replace(',', '', trim($gymdets[$row]));
						$row++;
						
						if(is_numeric(substr(trim($gymdets[$row]), -5,5)))
							$row = $row;
						else
							$row++;
						
						$cszdet = explode(', ', trim($gymdets[$row]));
						$city = $cszdet[0];
						$state = full_state(substr($cszdet[1], 0, 2));
						$zip = substr($cszdet[1], -5, 5);
						$row++;
						
						$phone = trim($gymdets[$row]);
						if(is_numeric(str_replace('(', '', str_replace(')', '', str_replace('.', '', str_replace(' ', '', str_replace('-', '', substr(trim($gymdets[$row]), 0, 5))))))))
							$phone = trim($gymdets[$row]);
						else
							$phone = '';
						
						
						//add to results array
						$results[] = array(
							'name'=> $name,
							'placeid'=> $placeid,
							'address'=> $address,
							'city'=> $city,
							'state'=> $state,
							'zip'=> $zip,
							'phone'=> $phone,
							'map'=> $map,
							'website'=> $website
						);
					}
				} else
					continue;
			}
			break;
		} else
			continue;
	}
	
	if($DEBUG_MODE)
		echo '<pre>'.print_r($results,1).'</pre>';
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />
		This page was created in '.$totaltime.' seconds<br />
		Link Counts<br />
		---------------<br />
		Gyms:'.$gymcounter.'<br />';
	
	// UPDATE ON CHANGE
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$changes = array();
	$updatecounter = 0;
	$insertcounter = 0;
	
	//check all old data
	foreach($results as $key => $val) {
		if($DEBUG_MODE)
			echo $val['name'].','.$val['placeid'].','.$val['address'].','.$val['city'].','.$val['state'].','.$val['zip'].','.$val['phone'].','.$val['website'].'<br />';
			
		$getolddata = $db->query("
			select *
			from mygym
			where placeid = '".$val['placeid']."'
		");
		if($row = $getolddata->fetch_assoc()) {
			//data is already in database
			//update record
			$db->query("
				update mygym
				set phone = '".$db->real_escape_string($val['phone'])."',
					name = '".$db->real_escape_string($val['name'])."',
					address = '".$db->real_escape_string($val['address'])."',
					city = '".$db->real_escape_string($val['city'])."',
					state = '".$db->real_escape_string($val['state'])."',
					zip = '".$db->real_escape_string($val['zip'])."',
					website = '".$db->real_escape_string($val['website'])."'
				where id = '".$row['id']."'
			");
			
			$updatecounter++;
			
			//remove from array as we have most recent data
			unset($results[$key]);
		}
	}
	
	//insert data we could not find information for
	if(count($results) > 0) {
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $key => $val) {
			if($insertcounter == 0)
				$query = 'insert into mygym (name, placeid, address, city, state, zip, phone, website, creation) values '."('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['placeid'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			else
				$query .= ",('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['placeid'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			$insertcounter++;
			
			$results[$key]['creation'] = date('Y-m-d H:i:s');
			$results[$key]['updated'] = date('Y-m-d H:i:s');
			$changes[] = $results[$key];
		}
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '
		Processing Queries completed in '.$totaltime.' seconds<br />
		Updated '.$updatecounter.' rows in the database.<br />
		Inserted '.$insertcounter.' rows into the database.<br />';
	
	if($DEBUG_MODE)
		echo '<pre>'.print_r($changes,1).'</pre>';
	
	//check to see if we did any changes
	if(count($changes) > 0)
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		$rowcounter = 1;
		
		//add phpexcel caching
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//initilize the spreedsheet
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Think Safe");
		$objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
		$objPHPExcel->getProperties()->setTitle("My Gym Report");
		$objPHPExcel->getProperties()->setSubject("My Gym  Report");
		$objPHPExcel->getProperties()->setDescription("A report for new changes to the My Gym database");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$changesheet = $objPHPExcel->getActiveSheet();
		$changesheet
			->SetCellValue('A1', 'Name')
			->SetCellValue('B1', 'Place ID')
			->SetCellValue('C1', 'Address')
			->SetCellValue('D1', 'City')
			->SetCellValue('E1', 'State')
			->SetCellValue('F1', 'Zip')
			->SetCellValue('G1', 'Phone')
			->SetCellValue('H1', 'Website')
			->SetCellValue('I1', 'First Recorded')
			->SetCellValue('J1', 'Last Changed');
			
		foreach($changes as $key => $val)
		{
			$rowcounter++;
			$changesheet
				->SetCellValue('A'.$rowcounter, html_entity_decode($val['name']))
				->SetCellValue('B'.$rowcounter, html_entity_decode($val['placeid']))
				->SetCellValue('C'.$rowcounter, html_entity_decode($val['address']))
				->SetCellValue('D'.$rowcounter, html_entity_decode($val['city']))
				->SetCellValue('E'.$rowcounter, html_entity_decode($val['state']))
				->SetCellValue('F'.$rowcounter, html_entity_decode($val['zip']))
				->SetCellValue('G'.$rowcounter, html_entity_decode($val['phone']))
				->SetCellValue('H'.$rowcounter, html_entity_decode($val['website']))
				->SetCellValue('I'.$rowcounter, html_entity_decode($val['creation']))
				->SetCellValue('J'.$rowcounter, html_entity_decode($val['updated']));
				
			$changesheet->getColumnDimension('A')->setAutoSize(true);
			$changesheet->getColumnDimension('B')->setAutoSize(true);
			$changesheet->getColumnDimension('C')->setAutoSize(true);
			$changesheet->getColumnDimension('D')->setAutoSize(true);
			$changesheet->getColumnDimension('F')->setAutoSize(true);
			$changesheet->getColumnDimension('G')->setAutoSize(true);
			$changesheet->getColumnDimension('H')->setAutoSize(true);	
			$changesheet->getColumnDimension('I')->setAutoSize(true);		
			$changesheet->getColumnDimension('J')->setAutoSize(true);		
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$report = '_'.date("Y-m-d_H:i:s", time());
		$objWriter->save('reports/mygym_report'.$report.'.xlsx');
		
		$subject = 'My Gym Report';
		$to = '';
		$body = 'There are new changes to My Gym!  Here are the new changes.(see attached) This report uses a scrubbing mechanism so if some data does not display correctly please see your web systems admin.';
		$attachments = 'reports/mygym_report'.$report.'.xlsx';
		
		
		//create the email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array('donotreply@tsdemos.com' => 'My Gym Reports'))
			->setTo(array('mboyles@think-safe.com'))
			//->setTo(array('aarox04@gmail.com'))
			->setCc(array('pwickham@think-safe.com'))
			->setBody($body, 'text/html')
			->addPart($body, 'text/html')
			->attach(Swift_Attachment::fromPath($attachments)
				->setFilename('mygym.xlsx'));
		$result = $mailer->send($message);
		
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$totaltime = ($endtime - $starttime); 
		
		if($DEBUG_MODE)
			echo '
			Creating spreadsheet completed in '.$totaltime.' seconds<br />
			Added '.$rowcounter.' rows to the spreadsheet.<br />';
	} else {
		if($DEBUG_MODE)
			echo 'No changes were made, no email was sent.<br />';
	}
?>
</body>
</html> 