<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
	
	$DEBUG_MODE = true;
	
	function abbreviate_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($full_state, $ab_state, $state_to_abreviate);
	}
	function full_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($ab_state, $full_state, $state_to_abreviate);
	}
	
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
  $filename = 'titleboxingclub_'.date('Y-m-d_H:i:s');
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'City', 'State', 'Zip', 'Phone'));
  
	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://titleboxingclub.com/find-a-club/");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	foreach($directoryhtml->find('.clubs-sub-sub-menu li a') as $e)
	{
		$gymurl = $e->getAttribute('href');
		$name = $e->innertext;
      
		$gymcounter++;
	
		$ch2 = curl_init();
		curl_setopt($ch2, CURLOPT_URL, $gymurl);
		curl_setopt($ch2, CURLOPT_HEADER, false);
		curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
		$result2 = curl_exec($ch2);
		curl_close($ch2);
		
		$locationhtml = new simple_html_dom();
		$locationhtml->load($result2);
		
		$streetAddress = $locationhtml->find('span[itemprop=streetAddress]', 0)->innertext;
        $city = $locationhtml->find('span[itemprop=addressLocality]', 0)->innertext;
        $state = $locationhtml->find('span[itemprop=addressRegion]', 0)->innertext;
        $zip = $locationhtml->find('span[itemprop=postalCode]', 0)->innertext;
        $phone = $locationhtml->find('span[itemprop=telephone] .lmc_sessions', 0)->innertext;
        echo $name.'<br />';
        echo $streetAddress.'<br />';
        echo $city.'<br />';
        echo $state.'<br />';
        echo $zip.'<br />';
        echo $phone.'<br /><br />';
        fputcsv($fp, array($name, $streetAddress, $city, $state, $zip, $phone));
	}
  
  //create the email
  $transport = Swift_MailTransport::newInstance();
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => 'Title Boxing Club Reports'))
      ->setTo(array('jbaker@thinks-safe.com'))
      ->setBody('Title Boxing Club', 'text/html')
      ->addPart('Title Boxing Club', 'text/html')
      ->attach(Swift_Attachment::fromPath($attachments)
          ->setFilename('reports/'.$filename.'.csv'));
  $result = $mailer->send($message);
  fclose($fp);
?>
</body>
</html>