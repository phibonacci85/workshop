<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	$DEBUG_MODE = false;
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$gymcounter = 0;
	$results = array();

	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://www.skyzone.com/getalllocs.asp?ReturnUrl=%252fdefault.aspx&popUp=true");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	foreach($directoryhtml->find('li') as $e)
	{
		$stateurl = $e->getAttribute('value');
		$gymcounter++;
		
		//check to see if there is secondary link
		if($e->find('a',0)) {
			$status = str_replace('(', '', str_replace(')', '', $e->find('span',0)->innertext));
			$nameandstate = $e->find('a',0)->innertext;
			$nameandstate = explode(', ', $nameandstate);
			$name = $nameandstate[0];
			$state = str_replace('</a>', '', $nameandstate[1]);
			$website = $e->find('a',0)->getAttribute('href');
			
			//get the short name for use later
			$shortname = str_replace('.aspx', '', str_replace('http://www.skyzonesports.com/', '', str_replace('http://www.skyzone.com/', '', $e->find('a',0)->getAttribute('href'))));
			
			//get each gym
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://www.skyzone.com/getaddress.asp?shortname='.$shortname);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$gymcurl = curl_exec($ch);
			curl_close($ch);
			
			$gymhtml = new simple_html_dom();
			$gymhtml->load($gymcurl);
			
			//gather the data!
			if($gymhtml->find('span',0)) {
				if($DEBUG_MODE)
					echo 'http://www.skyzone.com/getaddress.asp?shortname='.$shortname;
					
				$addresses = $gymhtml->find('span',0)->innertext;
				$addresses = explode('<br />', $addresses);
				
				if($DEBUG_MODE)
					echo '<pre>'.print_r($addresses,1).'</pre>';
					
				$address = str_replace(',', ' ', $addresses[0]);
				$addresses[1] = str_replace("\n\r", "\n", $addresses[1]);
				
				$getcity = explode(',', $addresses[1]);
				$city = $getcity[0];
				
				$getzip = explode('|', $addresses[1]);
				$zip = $getzip[1];
				
				$phone = str_replace('Phone: ', '', str_replace("\n", "", str_replace("\n\r", "", $addresses[2])));
				if(strpos($addresses[3],'Independely Owned & Operated Franchise'))
					$fax = '';
				else
					$fax = str_replace('Fax: ', '', $addresses[3]);
			}
		} else {
			$status = str_replace('(', '', str_replace(')', '', $e->find('span',1)->innertext));
			$nameandstate = $e->find('span',0)->innertext;
			if(strpos($nameandstate, ',')) {
				$nameandstate = explode(', ', $nameandstate);
				$name = $nameandstate[0];
				$state = str_replace('</a>', '', $nameandstate[1]);
			} else {
				$name = $nameandstate;
				$state = '';
			}
			$website = '';
			$address = '';
			$city = '';
			$zip = '';
			$phone = '';
			$fax = '';
		}
		
		$result = preg_replace('!\s+!', ' ', trim($name).','.trim($status).','.trim($address).','.trim($city).','.trim($state).','.trim($zip).','.trim($phone).','.trim($fax).','.trim($website));
		if($DEBUG_MODE)
			echo $result.'<br />';
		$results[] = explode(',', $result);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />
		This page was created in '.$totaltime.' seconds<br />
		Link Counts<br />
		---------------<br />
		Gyms:'.$gymcounter.'<br />';
	
	/* UPDATE ON CHANGE */
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$changes = array();
	$updatecounter = 0;
	$insertcounter = 0;
	
	//check all old data
	foreach($results as $value => $key) {
		if($DEBUG_MODE)
			echo '<pre>'.print_r($results[$value],1).'</pre>';
			
		$getolddata = $db->query("
			select *
			from skyzone
			where name = '".$key[0]."'
		");
		if($row = $getolddata->fetch_assoc()) {
			//is the status the same as the old data?
			if($key[1] != $row['status']) {
				$updatecounter++;
				
				//new data!
				$db->query("
					update skyzone
					set status = '".$key[1]."',
						address = '".$key[3]."',
						phone = '".$key[6]."',
						fax = '".$key[7]."',
						website = '".$key[8]."'
					where id = '".$row['id']."'
				");
				$key[] = $row['creation'];
				$key[] = date('Y-m-d H:i:s');
				$key[] = $row['status'];
				$changes[] = $key;
			}
			//remove from array
			unset($results[$value]);
		} else {
			if($DEBUG_MODE)
				echo "<pre>BAD:
				
				select *
				from skyzone
				where state = '".$key[4]."'
				and name = '".$key[0]."'</pre>";
		}
	}
	
	//insert data we could not find information for
	if(count($results) > 0) {
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $value => $key) {
			if($insertcounter == 0)
				$query = 'insert into skyzone (name, status, address, city, state, zip, phone, fax, website, creation) values '."('".$db->real_escape_string($key[0])."','".$db->real_escape_string($key[1])."','".$db->real_escape_string($key[2])."','".$db->real_escape_string($key[3])."','".$db->real_escape_string($key[4])."','".$db->real_escape_string($key[5])."','".$db->real_escape_string($key[6])."','".$db->real_escape_string($key[7])."','".$db->real_escape_string($key[8])."', '".date('Y-m-d H:i:s')."')";
			else
				$query .= ",('".$db->real_escape_string($key[0])."','".$db->real_escape_string($key[1])."','".$db->real_escape_string($key[2])."','".$db->real_escape_string($key[3])."','".$db->real_escape_string($key[4])."','".$db->real_escape_string($key[5])."','".$db->real_escape_string($key[6])."','".$db->real_escape_string($key[7])."','".$db->real_escape_string($key[8])."', '".date('Y-m-d H:i:s')."')";
			$insertcounter++;
			
			$key[] = date('Y-m-d H:i:s');
			$key[] = date('Y-m-d H:i:s');
			$key[] = '(new location)';
			$changes[] = $key;
		}
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '
		Processing Queries completed in '.$totaltime.' seconds<br />
		Updated '.$updatecounter.' rows in the database.<br />
		Inserted '.$insertcounter.' rows into the database.<br />';
	
	if($DEBUG_MODE)
		echo '<pre>'.print_r($changes,1).'</pre>';
	
	//check to see if we did any changes
	if(count($changes) > 0)
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		$rowcounter = 1;
		
		//add phpexcel caching
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//initilize the spreedsheet
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Think Safe");
		$objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
		$objPHPExcel->getProperties()->setTitle("Skyzone Report");
		$objPHPExcel->getProperties()->setSubject("Skyzone  Report");
		$objPHPExcel->getProperties()->setDescription("A report for new changes to the Skyzone database");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$changesheet = $objPHPExcel->getActiveSheet();
		$changesheet
			->SetCellValue('A1', 'Name')
			->SetCellValue('B1', 'Old Status')
			->SetCellValue('C1', 'New Status')
			->SetCellValue('D1', 'Address')
			->SetCellValue('E1', 'City')
			->SetCellValue('F1', 'State')
			->SetCellValue('G1', 'Zip')
			->SetCellValue('H1', 'Phone')
			->SetCellValue('I1', 'Fax')
			->SetCellValue('J1', 'Website')
			->SetCellValue('K1', 'First Recorded')
			->SetCellValue('L1', 'Last Changed');
			
		foreach($changes as $value => $key)
		{
			$rowcounter++;
			$changesheet
				->SetCellValue('A'.$rowcounter, html_entity_decode($key[0]))
				->SetCellValue('B'.$rowcounter, html_entity_decode($key[11]))
				->SetCellValue('C'.$rowcounter, html_entity_decode($key[1]))
				->SetCellValue('D'.$rowcounter, html_entity_decode($key[2]))
				->SetCellValue('E'.$rowcounter, html_entity_decode($key[3]))
				->SetCellValue('F'.$rowcounter, html_entity_decode($key[4]))
				->SetCellValue('G'.$rowcounter, html_entity_decode($key[5]))
				->SetCellValue('H'.$rowcounter, html_entity_decode($key[6]))
				->SetCellValue('I'.$rowcounter, html_entity_decode($key[7]))
				->SetCellValue('J'.$rowcounter, html_entity_decode($key[8]))
				->SetCellValue('K'.$rowcounter, html_entity_decode($key[9]))
				->SetCellValue('L'.$rowcounter, html_entity_decode($key[10]));
				
			$changesheet->getColumnDimension('A')->setAutoSize(true);
			$changesheet->getColumnDimension('B')->setAutoSize(true);
			$changesheet->getColumnDimension('C')->setAutoSize(true);
			$changesheet->getColumnDimension('D')->setAutoSize(true);
			$changesheet->getColumnDimension('F')->setAutoSize(true);
			$changesheet->getColumnDimension('G')->setAutoSize(true);
			$changesheet->getColumnDimension('H')->setAutoSize(true);	
			$changesheet->getColumnDimension('I')->setAutoSize(true);	
			$changesheet->getColumnDimension('J')->setAutoSize(true);	
			$changesheet->getColumnDimension('K')->setAutoSize(true);	
			$changesheet->getColumnDimension('L')->setAutoSize(true);	
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$report = '_'.date("Y-m-d_H:i:s", time());
		$objWriter->save('reports/skyzone_report'.$report.'.xlsx');
		
		$subject = 'Skyzone Report';
		$to = '';
		$body = 'There are new changes to Skyzone!  Here are the new changes. (see attached)';
		$attachments = 'reports/skyzone_report'.$report.'.xlsx';
		
		
		//create the email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array('donotreply@tsdemos.com' => 'Skyzone Reports'))
			->setTo(array('mboyles@think-safe.com'))
			->setCc(array('mboyles@think-safe.com'))
			->setBody($body, 'text/html')
			->addPart($body, 'text/html')
			->attach(Swift_Attachment::fromPath($attachments)
				->setFilename('skyzonereport.xlsx'));
		$result = $mailer->send($message);
		
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$totaltime = ($endtime - $starttime); 
		
		if($DEBUG_MODE)
			echo '
			Creating spreadsheet completed in '.$totaltime.' seconds<br />
			Added '.$rowcounter.' rows to the spreadsheet.<br />';
	} else {
		if($DEBUG_MODE)
			echo 'No changes were made, no email was sent.<br />';
	}
?>
</body>
</html>