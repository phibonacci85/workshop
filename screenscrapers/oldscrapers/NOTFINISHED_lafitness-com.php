<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
	
	$DEBUG_MODE = false;
	
	function abbreviate_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($full_state, $ab_state, $state_to_abreviate);
	}
	function full_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($ab_state, $full_state, $state_to_abreviate);
	}
	
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$gymcounter = 0;
	$results = array();

	//get webpage
	$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');
	foreach($ab_state as $state)
	{
		$statecounter++;
		$fullstate = full_state($state);
		
		//get each state
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.lafitness.com/Pages/findclubresultszip.aspx?state='.$state);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, '
			ctl00$ScriptManager1=ctl00$MainContent$updFindClub|ctl00$MainContent$ddlPageSize
			&RaiseException=/wEFJGYyMWY2MWVkLTNmOTEtNDdlZi1iZmU3LWFlNzg0NDQ3MDA1NQ==
			&__EVENTTARGET=ctl00$MainContent$ddlPageSize
			&__EVENTARGUMENT=
			&__LASTFOCUS=
			&__VIEWSTATE=/wEPDwULLTEyNTA3MTUxODMPFgoeBXN0YXRlBQJOSh4GTWFwVXJsBWRodHRwczovL3d3dy5sYWZpdG5lc3MuY29tOjQ0My9wYWdlcy9BZXJvYmljQ2xhc3NJbWFnZS5hc3B4P21hcHR5cGU9c3RhdGUmY3Q9MTYmc3RhdGU9Tkomc2l6ZT00NTAsNTUwHhJGaW5kQ2x1YlJlc3VsdFR5cGULKY4BTEFGaXRuZXNzLlBhZ2VzLmZpbmRDbHViUmVzdWx0c1ppcCtSZXN1bHRUeXBlLCBBcHBfV2ViX2ZpbmRjbHVicmVzdWx0c3ppcC5hc3B4LmY5YjA4MjFlLCBWZXJzaW9uPTAuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49bnVsbAEeDEN1c3RvbUZpbHRlcgspakxBRml0bmVzcy5QdWJsaWMuQ2FjaGVkQ2x1YnMrQ3VzdG9tRmlsdGVyLCBBcHBfQ29kZSwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPW51bGwBHhFUb3RhbFJlY29yZHNDb3VudAICFgJmD2QWAgIDDxYCHgZvbmxvYWQFCUdldE1hcCgpOxYCAgEPZBYIAgMPZBYCAgEPDxYEHgtOYXZpZ2F0ZVVybAUSfi9QYWdlcy9sb2dpbi5hc3B4HgRUZXh0BQ5NRU1CRVIgICBMT0dJTmRkAgUPZBYGZg8PFgYeCENzc0NsYXNzZR8HZR4EXyFTQgICZGQCAQ9kFgICBQ9kFgJmD2QWCmYPDxYCHgdWaXNpYmxlaGRkAgIPEGRkFgFmZAIEDw8WAh8KaGRkAgUPEGRkFgFmZAIHDw8WAh8KaGRkAgIPDxYCHwpoZBYCAgEPFgIfCmhkAgkPZBYEAgMPDxYCHwpnZBYCAgMPFgIeD1NpdGVNYXBQcm92aWRlcgUZTEFGX1NpdGVtYXBGb290ZXJQcm92aWRlcmQCBQ8PFgIfCmdkZAINDw8WAh8HBRxGLTAxODAgNi8yOC8yMDEzIDEwOjQ4OjU3IEFNZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFJmN0bDAwJEdsb2JhbEhlYWRlcjEkYnRuU2VhcmNoQ2x1YkJ5WmlwBRhjdGwwMCRNYWluQ29udGVudCRidG5BU0M7LgL9cUmqcsasCnq9ntCLKObQSQ==
			&__EVENTVALIDATION=/wEWEAKpqva/BALHxdeBBALKovfZDAK4ysbtCALVuPb0AgLzhqLpBQKqjfDnCQKn4tqJBQKk4tqJBQKl4tqJBQL/oYeVBALDs4j0DgLM3OKZAgLN3OKZAgLI3OKZAgLrqOPJB7bSZqIUP7nfsbKHfvM0ouazk4Yi
			&ctl00$GlobalHeader1$txtZip=
			&ctl00$GlobalHeader1$txtWaterMark_ClientState=
			&ctl00$MainContent$ddlSortBy=3
			&ctl00$MainContent$ddlPageSize=50
			&__ASYNCPOST=true
		');
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$statecurl = curl_exec($ch);
		curl_close($ch);
		
		$statehtml = new simple_html_dom();
		$statehtml->load($statecurl);
		
		if($DEBUG_MODE)
			echo '<pre>'.$statecurl.'</pre>';
		//gather the data!
		/*foreach(array_slice($statehtml->find('.BoxTable tr'),1) as $e2) {
			$gymcounter++;
			$results[] = array(
				'name'=> $e2->find('td a',0)->innertext,
				'address'=> str_replace('<br />', ' ', $e2->find('td',1)->innertext),
				'city'=> $e2->find('td',2)->innertext,
				'state'=> $fullstate,
				'zip'=> $e2->find('td',3)->innertext,
				'phone'=> $e2->find('td',4)->innertext,
				'map'=>'http://www.extremebodyshaping.com'.$e2->find('td a',1)->getAttribute('href'),
				'website'=>'http://www.extremebodyshaping.com/'.$e2->find('td a',0)->getAttribute('href')
			);
			
		}*/
	}
	if($DEBUG_MODE)
		echo '<pre>'.print_r($results,1).'</pre>';
	/*
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	echo '<br /><br />
	This page was created in '.$totaltime.' seconds<br />
	Link Counts<br />
	---------------<br />
	States:'.$statecounter.'<br />
	Gyms:'.$gymcounter.'<br />';
	
	// UPDATE ON CHANGE
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$changes = array();
	$updatecounter = 0;
	$insertcounter = 0;
	
	//check all old data
	foreach($results as $key => $val) {
		echo $val['name'].','.$val['address'].','.$val['city'].','.$val['state'].','.$val['zip'].','.$val['phone'].','.$val['website'].'<br />';
			
		$getolddata = $db->query("
			select *
			from extremebodyshaping
			where name = '".$val['name']."'
			and state = '".$val['state']."'
		");
		if($row = $getolddata->fetch_assoc()) {
			//data is already in database, remove from array
			unset($results[$key]);
		}
	}
	
	//insert data we could not find information for
	if(count($results) > 0) {
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $key => $val) {
			if($insertcounter == 0)
				$query = 'insert into extremebodyshaping (name, address, city, state, zip, phone, map, website, creation) values '."('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['map'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			else
				$query .= ",('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['map'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			$insertcounter++;
			
			$results[$key]['creation'] = date('Y-m-d H:i:s');
			$results[$key]['updated'] = date('Y-m-d H:i:s');
			$changes[] = $results[$key];
		}
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	echo '
	Processing Queries completed in '.$totaltime.' seconds<br />
	Updated '.$updatecounter.' rows in the database.<br />
	Inserted '.$insertcounter.' rows into the database.<br />';
	
	//echo '<pre>'.print_r($changes,1).'</pre>';
	
	//check to see if we did any changes
	if(count($changes) > 0)
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		$rowcounter = 1;
		
		//add phpexcel caching
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//initilize the spreedsheet
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Think Safe");
		$objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
		$objPHPExcel->getProperties()->setTitle("Extremebodyshaping Report");
		$objPHPExcel->getProperties()->setSubject("Extremebodyshaping  Report");
		$objPHPExcel->getProperties()->setDescription("A report for new changes to the Extremebodyshaping database");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$changesheet = $objPHPExcel->getActiveSheet();
		$changesheet
			->SetCellValue('A1', 'Name')
			->SetCellValue('B1', 'Address')
			->SetCellValue('C1', 'City')
			->SetCellValue('D1', 'State')
			->SetCellValue('E1', 'Zip')
			->SetCellValue('F1', 'Phone')
			->SetCellValue('G1', 'Map')
			->SetCellValue('H1', 'Website')
			->SetCellValue('I1', 'First Recorded')
			->SetCellValue('J1', 'Last Changed');
			
		foreach($changes as $key => $val)
		{
			$rowcounter++;
			$changesheet
				->SetCellValue('A'.$rowcounter, html_entity_decode($val['name']))
				->SetCellValue('B'.$rowcounter, html_entity_decode($val['address']))
				->SetCellValue('C'.$rowcounter, html_entity_decode($val['city']))
				->SetCellValue('D'.$rowcounter, html_entity_decode($val['state']))
				->SetCellValue('E'.$rowcounter, html_entity_decode($val['zip']))
				->SetCellValue('F'.$rowcounter, html_entity_decode($val['phone']))
				->SetCellValue('G'.$rowcounter, html_entity_decode($val['map']))
				->SetCellValue('H'.$rowcounter, html_entity_decode($val['website']))
				->SetCellValue('I'.$rowcounter, html_entity_decode($val['creation']))
				->SetCellValue('J'.$rowcounter, html_entity_decode($val['updated']));
				
			$changesheet->getColumnDimension('A')->setAutoSize(true);
			$changesheet->getColumnDimension('B')->setAutoSize(true);
			$changesheet->getColumnDimension('C')->setAutoSize(true);
			$changesheet->getColumnDimension('D')->setAutoSize(true);
			$changesheet->getColumnDimension('F')->setAutoSize(true);
			//$changesheet->getColumnDimension('G')->setAutoSize(true);
			$changesheet->getColumnDimension('H')->setAutoSize(true);	
			$changesheet->getColumnDimension('I')->setAutoSize(true);		
			$changesheet->getColumnDimension('J')->setAutoSize(true);		
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$report = '_'.date("Y-m-d_H:i:s", time());
		$objWriter->save('reports/extremebodyshaping_report'.$report.'.xlsx');
		
		$subject = 'Extreme Body Shaping Report';
		$to = '';
		$body = 'There are new changes to Extreme Body Shaping!  Here are the new changes.(see attached) This report uses a scrubbing mechanism so if some data does not display correctly please see your web systems admin.';
		$attachments = 'reports/extremebodyshaping_report'.$report.'.xlsx';
		
		
		//create the email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array('donotreply@tsdemos.com' => 'Extreme Body Shaping Reports'))
			->setTo(array('mboyles@think-safe.com'))
			//->setTo(array('aarox04@gmail.com'))
			->setCc(array('pwickham@think-safe.com'))
			->setBody($body, 'text/html')
			->addPart($body, 'text/html')
			->attach(Swift_Attachment::fromPath($attachments)
				->setFilename('extremebodyshaping.xlsx'));
		$result = $mailer->send($message);
		
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$totaltime = ($endtime - $starttime); 
		echo '
		Creating spreadsheet completed in '.$totaltime.' seconds<br />
		Added '.$rowcounter.' rows to the spreadsheet.<br />';
	} else {
		echo '
		No changes were made, no email was sent.<br />';
	}*/
?>
</body>
</html>