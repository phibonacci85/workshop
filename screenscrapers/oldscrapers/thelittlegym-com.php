<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
	
	$DEBUG_MODE = false;
	
	function abbreviate_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($full_state, $ab_state, $state_to_abreviate);
	}
	function full_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');	
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($ab_state, $full_state, $state_to_abreviate);
	}
	
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$gymcounter = 0;
	$results = array();

	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://clubs.thelittlegym.com/Life-Time-Fitness-Clubs/");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');
	foreach($ab_state as $state)
	{
		$statecounter++;
		
		//get each state
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://www.thelittlegym.com/Pages/LocatorResult.aspx?action=3&param='.$state);
		curl_setopt($ch, CURLOPT_HEADER, false);
		//curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$statecurl = curl_exec($ch);
		curl_close($ch);
		
		$statehtml = new simple_html_dom();
		$statehtml->load($statecurl);
		
		//gather the data!
		if($statehtml->find('#gymlocator table',0)) {
			foreach(array_slice($statehtml->find('#gymlocator table tr'),1) as $e) {
				$city = '';
				$state = '';
				$address = '';
				$zip = '';
				$name = '';
				$map = '';
				$phone = '';
				$email = '';
				$website = '';
				$fax = '';
				$status = '';
				$gymcounter++;
				
				$name = $e->find('td strong',0)->innertext;
				
				$details = explode('<br/>', $e->find('td',0)->innertext);
				if(strpos($details[1], 'Coming Soon') !== false) {
					$status = 'Coming Soon';
					
					//is it just the state listed?
					if(strlen($details[2] == 2)) {
						$state = full_state($details[2]);
						$city = '';
						$address = '';
						$zip = '';
					//is it state/city/zip?
					} else if(strpos($details[2], ',') !== false) {
						//do we have a valid zip?
						if(is_numeric(substr($details[2], -5, 5))) {
							$zip = ' '.substr($details[2], -5, 5);
							$state = substr($details[2], -8, 2);
						} else {
							$zip = '';
							$state = substr($details[2], -2, 2);
						}
						$city = str_replace(', '.$state.$zip, '', $details[2]);
						$zip = trim($zip);
						$state = full_state($state);
					} else {
						$address = $details[2];
						
						//is it just the state listed?
						if(strlen($details[3] == 2)) {
							$state = full_state($details[3]);
							$city = '';
							$address = '';
							$zip = '';
						//is it state/city/zip?
						} else if(strpos($details[3], ',') !== false) {
							//do we have a valid zip?
							if(is_numeric(substr($details[3], -5, 5))) {
								$zip = ' '.substr($details[3], -5, 5);
								$state = substr($details[3], -8, 2);
							} else {
								$zip = '';
								$state = substr($details[3], -2, 2);
							}
							$city = str_replace(', '.$state.$zip, '', $details[3]);
							$zip = trim($zip);
							$state = full_state($state);
						}
					}
					if($DEBUG_MODE)
						echo '<pre>'.print_r($details,1).'</pre>';
				} else {
					if(strpos($details[1], 'Opening Soon!') !== false) {
						$address = trim(str_replace('Opening Soon!', '', $details[1]));
						$status = 'Opening Soon!';
					} else if(strpos($details[1], 'New Location') !== false) {
						$address = trim(str_replace('"New Location"', '', $details[1]));
						$status = 'New Location';
					} else {
						$status = 'Open';
						$address = $details[1];
					}	
					
					if(!is_numeric(str_replace(' ', '_', substr($details[2], -5, 5)))) {
						$address .= ' '.$details[2];
						$csz = 3;
					} else
						$csz = 2;
						
					$cistzi = explode(', ', $details[$csz]);
					$city = $cistzi[0];
					$state = substr($cistzi[1], 0, 2);
					$zip = substr($cistzi[1], 3);
					$state = full_state($state);
				}	
				//check to see if there is a phone/fax/website/email
				$details = explode('<br/>', $e->find('td',1)->innertext);
				foreach($details as $det) {
					if(strpos($det, '<strong>Phone:</strong>') !== false) {
						$phone = trim(str_replace('<strong>Phone:</strong>', '', $det));
					} else if(strpos($det, '<strong>Email:</strong>') !== false) {
						$email = trim(str_replace('<strong>Email:</strong>', '', $det));
					} else if(strpos($det, '<strong>Website:</strong>') !== false) {
						$website = $e->find('td',1)->find('a',0)->getAttribute('href');
					} else if(strpos($det, '<strong>Fax:</strong>') !== false) {
						$fax = trim(str_replace('<strong>Fax:</strong>', '', $det));
					}
				}
				
				if($e->find('.map a',0))
					$map = $e->find('.map a',0)->getAttribute('href');
				
				$results[] = array(
					'name'=> $name,
					'status'=> $status,
					'address'=> $address,
					'city'=> $city,
					'state'=> $state,
					'zip'=> $zip,
					'phone'=> $phone,
					'fax'=> $fax,
					'email'=> $email,
					'map'=> $map,
					'website'=> $website
				);
				
			}
		}
	}
	if($DEBUG_MODE)
		echo '<pre>'.print_r($results,1).'</pre>';
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />
		This page was created in '.$totaltime.' seconds<br />
		Link Counts<br />
		---------------<br />
		States:'.$statecounter.'<br />
		Gyms:'.$gymcounter.'<br />';
	
	// UPDATE ON CHANGE 
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$changes = array();
	$updatecounter = 0;
	$insertcounter = 0;
	
	//check all old data
	foreach($results as $key => $val) {
		if($DEBUG_MODE)
			echo $val['name'].','.$val['status'].','.$val['address'].','.$val['city'].','.$val['state'].','.$val['zip'].','.$val['phone'].','.$val['fax'].','.$val['email'].','.$val['map'].','.$val['website'].'<br />';
			
		$getolddata = $db->query("
			select *
			from thelittlegym
			where name = '".$db->real_escape_string($val['name'])."'
		");
		if($row = $getolddata->fetch_assoc()) {
			//data is already in database
			if($row['status'] != $val['status']) {
				$updatecounter++;
				$results[$key]['oldstatus'] = $row['status'];
				$changes[] = $results[$key];
			}
			
			//update record
			$db->query("
				update thelittlegym
				set status = '".$db->real_escape_string($val['status'])."',
					name = '".$db->real_escape_string($val['name'])."',
					address = '".$db->real_escape_string($val['address'])."',
					city = '".$db->real_escape_string($val['city'])."',
					state = '".$db->real_escape_string($val['state'])."',
					zip = '".$db->real_escape_string($val['zip'])."',
					phone = '".$db->real_escape_string($val['phone'])."',
					fax = '".$db->real_escape_string($val['fax'])."',
					email = '".$db->real_escape_string($val['email'])."',
					map = '".$db->real_escape_string($val['map'])."',
					website = '".$db->real_escape_string($val['website'])."'
				where id = '".$row['id']."'
			");
			
			//remove from array
			unset($results[$key]);
		} else
			$results[$key]['oldstatus'] = 'New Location';
	}
	
	//insert data we could not find information for
	if(count($results) > 0) {
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $key => $val) {
			if($insertcounter == 0)
				$query = 'insert into thelittlegym (name, status, address, city, state, zip, phone, fax, email, map, website, creation) values '."('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['status'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['fax'])."','".$db->real_escape_string($val['email'])."','".$db->real_escape_string($val['map'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			else
				$query .= ",('".$db->real_escape_string($val['name'])."','".$db->real_escape_string($val['status'])."','".$db->real_escape_string($val['address'])."','".$db->real_escape_string($val['city'])."','".$db->real_escape_string($val['state'])."','".$db->real_escape_string($val['zip'])."','".$db->real_escape_string($val['phone'])."','".$db->real_escape_string($val['fax'])."','".$db->real_escape_string($val['email'])."','".$db->real_escape_string($val['map'])."','".$db->real_escape_string($val['website'])."', '".date('Y-m-d H:i:s')."')";
			$insertcounter++;
			
			$results[$key]['creation'] = date('Y-m-d H:i:s');
			$results[$key]['updated'] = date('Y-m-d H:i:s');
			$changes[] = $results[$key];
		}
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '
		Processing Queries completed in '.$totaltime.' seconds<br />
		Updated '.$updatecounter.' rows in the database.<br />
		Inserted '.$insertcounter.' rows into the database.<br />';
	
	if($DEBUG_MODE)
		echo '<pre>'.print_r($changes,1).'</pre>';
	
	//check to see if we did any changes
	if(count($changes) > 0)
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		$rowcounter = 1;
		
		//add phpexcel caching
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//initilize the spreedsheet
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Think Safe");
		$objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
		$objPHPExcel->getProperties()->setTitle("The Little Gym Report");
		$objPHPExcel->getProperties()->setSubject("The Little Gym  Report");
		$objPHPExcel->getProperties()->setDescription("A report for new changes to the The Little Gym database");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$changesheet = $objPHPExcel->getActiveSheet();
		$changesheet 
			->SetCellValue('A1', 'Name')
			->SetCellValue('B1', 'Status')
			->SetCellValue('C1', 'Address')
			->SetCellValue('D1', 'City')
			->SetCellValue('E1', 'State')
			->SetCellValue('F1', 'Zip')
			->SetCellValue('G1', 'Phone')
			->SetCellValue('H1', 'Fax')
			->SetCellValue('I1', 'Email')
			->SetCellValue('J1', 'Map')
			->SetCellValue('K1', 'Website')
			->SetCellValue('L1', 'First Recorded')
			->SetCellValue('M1', 'Last Changed');
			
		foreach($changes as $key => $val)
		{
			$rowcounter++;
			$changesheet
				->SetCellValue('A'.$rowcounter, html_entity_decode($val['name']))
				->SetCellValue('B'.$rowcounter, html_entity_decode($val['status']))
				->SetCellValue('C'.$rowcounter, html_entity_decode($val['address']))
				->SetCellValue('D'.$rowcounter, html_entity_decode($val['city']))
				->SetCellValue('E'.$rowcounter, html_entity_decode($val['state']))
				->SetCellValue('F'.$rowcounter, html_entity_decode($val['zip']))
				->SetCellValue('G'.$rowcounter, html_entity_decode($val['phone']))
				->SetCellValue('H'.$rowcounter, html_entity_decode($val['fax']))
				->SetCellValue('I'.$rowcounter, html_entity_decode($val['email']))
				->SetCellValue('J'.$rowcounter, html_entity_decode($val['map']))
				->SetCellValue('K'.$rowcounter, html_entity_decode($val['website']))
				->SetCellValue('L'.$rowcounter, html_entity_decode($val['creation']))
				->SetCellValue('M'.$rowcounter, html_entity_decode($val['updated']));
				
			$changesheet->getColumnDimension('A')->setAutoSize(true);
			$changesheet->getColumnDimension('B')->setAutoSize(true);
			$changesheet->getColumnDimension('C')->setAutoSize(true);
			$changesheet->getColumnDimension('D')->setAutoSize(true);
			$changesheet->getColumnDimension('F')->setAutoSize(true);
			$changesheet->getColumnDimension('G')->setAutoSize(true);
			$changesheet->getColumnDimension('H')->setAutoSize(true);	
			$changesheet->getColumnDimension('I')->setAutoSize(true);		
			$changesheet->getColumnDimension('J')->setAutoSize(true);		
			$changesheet->getColumnDimension('K')->setAutoSize(true);		
			$changesheet->getColumnDimension('L')->setAutoSize(true);		
			$changesheet->getColumnDimension('M')->setAutoSize(true);		
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$report = '_'.date("Y-m-d_H:i:s", time());
		$objWriter->save('reports/thelittlegym_report'.$report.'.xlsx');
		
		$subject = 'The Little Gym Report';
		$to = '';
		$body = 'There are new changes to The Little Gym!  Here are the new changes.(see attached) This report uses a scrubbing mechanism so if some data does not display correctly please see your web systems admin.';
		$attachments = 'reports/thelittlegym_report'.$report.'.xlsx';
		
		
		//create the email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array('donotreply@tsdemos.com' => 'The Little Gym Reports'))
			->setTo(array('mboyles@thinks-safe.com'))
			//->setTo(array('aarox04@gmail.com'))
			->setCc(array('pwickham@think-safe.com'))
			->setBody($body, 'text/html')
			->addPart($body, 'text/html')
			->attach(Swift_Attachment::fromPath($attachments)
				->setFilename('thelittlegym.xlsx'));
		$result = $mailer->send($message);
		
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$totaltime = ($endtime - $starttime); 
		
		if($DEBUG_MODE)
			echo '
			Creating spreadsheet completed in '.$totaltime.' seconds<br />
			Added '.$rowcounter.' rows to the spreadsheet.<br />';
	} else {
		if($DEBUG_MODE)
			echo 'No changes were made, no email was sent.<br />';
	}
?>
</body>
</html>