<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	define('DBHOST', 'localhost');	
	define('DBUSER', 'tsdemosc_scscrap');
	define('DBPASS', '1105firstvoice');
	define('DB', 'tsdemosc_screenscrapes');
	@ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);
	
	require_once('./swiftmailer/swift_required.php');
	require_once('./phpexcel/PHPExcel.php');
	require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
	date_default_timezone_set('America/Chicago');
	
	$DEBUG_MODE = true;
	
	function abbreviate_state($state_to_abreviate)
	{
		//This information never changes
		$full_state = array('Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','WestVirginia','Wisconsin','Wyoming');
		$ab_state = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');

		//Here we search and replace the state with the abbreviation
		return str_replace($full_state, $ab_state, $state_to_abreviate);
	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$gymcounter = 0;
	$results = array();

	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://www.ckokickboxing.com/view-all-locations.html");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	foreach($directoryhtml->find('.paragraph') as $e)
	{
		$gymcounter++;
	
		$values = explode('<br />', str_replace('&nbsp;', ' ', abbreviate_state($e)));
		//$values = explode('<br />', str_replace('&nbsp;', ' ', $e));
		if($DEBUG_MODE)
			echo '<pre>'.print_r($values,1).'</pre>';
		
		$name = $e->find('strong',0)->innertext;
		if(!strpos($e->find('strong',1), 'Visit Us!') || count($values) == 7) {
			$name .= ' - '.$e->find('strong',1)->innertext;
			
			$address = str_replace(',', ' ', str_replace('</strong>', '', $values[2]));
			
			if(strpos($values[3], 'Suite') === false)
			{
				$address = str_replace(',', ' ', str_replace('</strong>', '', $values[2]));
				if(is_numeric(substr($values[3], -5, 5))) {
					$city = substr($values[3], 0, -9);
					$state = substr($values[3], -8, 2);
					$zip = substr($values[3], -5, 5);
				} else {
					$zip = ''; //no zip given
					$state = substr($values[3], -2, 2);
					$city = substr($values[3], 0, -3);
				} 
				$phone = preg_replace('/<span[^>]*?>([\\s\\S]*?)<\/span>/','\\1', str_replace('Phone:', '', $values[4]));
			} else {
				$address = str_replace(',', ' ', str_replace('</strong>', '', $values[2].' '.$values[3]));
				if(is_numeric(substr($values[4], -5, 5))) {
					$city = substr($values[4], 0, -9);
					$state = substr($values[4], -8, 2);
					$zip = substr($values[4], -5, 5);
				} else {
					$zip = ''; //no zip given
					$state = substr($values[4], -2, 2);
					$city = substr($values[4], 0, -3);
				} 
				$phone = preg_replace('/<span[^>]*?>([\\s\\S]*?)<\/span>/','\\1', str_replace('Phone:', '', $values[5]));
			}			
			if($e->find('a', 0))
			 $website = 'http://www.ckokickboxing.com'.$e->find('a', 0)->getAttribute('href');
		} else {
			if(strpos($values[2], 'Suite') === false)
			{
				$address = str_replace(',', ' ', str_replace('</strong>', '', $values[1]));
				if(is_numeric(substr($values[2], -5, 5))) {
					$city = substr($values[2], 0, -9);
					$state = substr($values[2], -8, 2);
					$zip = substr($values[2], -5, 5);
				} else {
					$zip = ''; //no zip given
					$state = substr($values[2], -2, 2);
					$city = substr($values[2], 0, -3);
				}
				$phone = preg_replace('/<span[^>]*?>([\\s\\S]*?)<\/span>/','\\1', str_replace('Phone:', '', $values[3]));
			} else {
				$address = str_replace(',', ' ', str_replace('</strong>', '', $values[1].' '.$values[2]));
				if(is_numeric(substr($values[3], -5, 5))) {
					$city = substr($values[3], 0, -9);
					$state = substr($values[3], -8, 2);
					$zip = substr($values[3], -5, 5);
				} else {
					$zip = ''; //no zip given
					$state = substr($values[3], -2, 2);
					$city = substr($values[3], 0, -3);
				}
				$phone = preg_replace('/<span[^>]*?>([\\s\\S]*?)<\/span>/','\\1', str_replace('Phone:', '', $values[4]));
			}
			
            if($e->find('a', 0))
			 $website = 'http://www.ckokickboxing.com'.$e->find('a', 0)->getAttribute('href');
		}
		$name = str_replace('<br />', '', $name);
		
		/*
		<div class="paragraph" style="text-align:left;">
			<strong style="">BROOKLYN</strong>
			<br />
			<strong style="">BAY RIDGE</strong>
			<br />
			9106 4th Ave.<br />Brooklyn NY 11209
			<br />
			Phone: 347-497-4272<br />
			<a href="/brooklyn-bay-ridge.html" title="">
				<strong>Visit Us!</strong>
			</a>
			<br />
		</div>
		*/
		
		$result = preg_replace('!\s+!', ' ', trim(ucwords(strtolower($name))).','.trim($address).','.trim($city).','.trim($state).','.trim($zip).','.trim($phone).','.trim($website));
		
		if($DEBUG_MODE)
			echo $result.'<br />';
		$results[] = explode(',', $result);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />
		This page was created in '.$totaltime.' seconds<br />
		Link Counts<br />
		---------------<br />
		Gyms:'.$gymcounter.'<br />';
	
	/* UPDATE ON CHANGE */
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$changes = array();
	$updatecounter = 0;
	$insertcounter = 0;
	
	//check all old data
	foreach($results as $value => $key) {
		if($DEBUG_MODE)
			echo '<pre>'.print_r($results[$value],1).'</pre>';
			
		$getolddata = $db->query("
			select *
			from ckokickboxing
			where name = '".$key[0]."'
		");
		if($row = $getolddata->fetch_assoc()) {
			//data is already in database, do nothing!
			
			//remove from array
			unset($results[$value]);
		}
	}
	
	//insert data we could not find information for
	if(count($results) > 0) {
		//loop remaining data
		$insertcounter = 0;
		foreach($results as $value => $key) {
			if($insertcounter == 0)
				$query = 'insert into ckokickboxing (name, address, city, state, zip, phone, website, creation) values '."('".$db->real_escape_string($key[0])."','".$db->real_escape_string($key[1])."','".$db->real_escape_string($key[2])."','".$db->real_escape_string($key[3])."','".$db->real_escape_string($key[4])."','".$db->real_escape_string($key[5])."','".$db->real_escape_string($key[6])."', '".date('Y-m-d H:i:s')."')";
			else
				$query .= ",('".$db->real_escape_string($key[0])."','".$db->real_escape_string($key[1])."','".$db->real_escape_string($key[2])."','".$db->real_escape_string($key[3])."','".$db->real_escape_string($key[4])."','".$db->real_escape_string($key[5])."','".$db->real_escape_string($key[6])."', '".date('Y-m-d H:i:s')."')";
			$insertcounter++;
			
			$key[] = date('Y-m-d H:i:s');
			$key[] = date('Y-m-d H:i:s');
			$changes[] = $key;
		}
		$db->query($query);
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '
		Processing Queries completed in '.$totaltime.' seconds<br />
		Updated '.$updatecounter.' rows in the database.<br />
		Inserted '.$insertcounter.' rows into the database.<br />';
	
	if($DEBUG_MODE)	
		echo '<pre>'.print_r($changes,1).'</pre>';
	
	//check to see if we did any changes
	if(count($changes) > 0)
	{
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$starttime = $mtime; 
		$rowcounter = 1;
		
		//add phpexcel caching
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize' => '32MB');
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

		//initilize the spreedsheet
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("Think Safe");
		$objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
		$objPHPExcel->getProperties()->setTitle("Ckokickboxing Report");
		$objPHPExcel->getProperties()->setSubject("Ckokickboxing  Report");
		$objPHPExcel->getProperties()->setDescription("A report for new changes to the Ckokickboxing database");
		
		$objPHPExcel->setActiveSheetIndex(0);
		$changesheet = $objPHPExcel->getActiveSheet();
		$changesheet
			->SetCellValue('A1', 'Name')
			->SetCellValue('B1', 'Address')
			->SetCellValue('C1', 'City')
			->SetCellValue('D1', 'State')
			->SetCellValue('E1', 'Zip')
			->SetCellValue('F1', 'Phone')
			->SetCellValue('G1', 'Website')
			->SetCellValue('H1', 'First Recorded')
			->SetCellValue('I1', 'Last Changed');
			
		foreach($changes as $value => $key)
		{
			$rowcounter++;
			$changesheet
				->SetCellValue('A'.$rowcounter, html_entity_decode($key[0]))
				->SetCellValue('B'.$rowcounter, html_entity_decode($key[1]))
				->SetCellValue('C'.$rowcounter, html_entity_decode($key[2]))
				->SetCellValue('D'.$rowcounter, html_entity_decode($key[3]))
				->SetCellValue('E'.$rowcounter, html_entity_decode($key[4]))
				->SetCellValue('F'.$rowcounter, html_entity_decode($key[5]))
				->SetCellValue('G'.$rowcounter, html_entity_decode($key[6]))
				->SetCellValue('H'.$rowcounter, html_entity_decode($key[7]))
				->SetCellValue('I'.$rowcounter, html_entity_decode($key[8]));
				
			$changesheet->getColumnDimension('A')->setAutoSize(true);
			$changesheet->getColumnDimension('B')->setAutoSize(true);
			$changesheet->getColumnDimension('C')->setAutoSize(true);
			$changesheet->getColumnDimension('D')->setAutoSize(true);
			$changesheet->getColumnDimension('F')->setAutoSize(true);
			$changesheet->getColumnDimension('G')->setAutoSize(true);
			$changesheet->getColumnDimension('H')->setAutoSize(true);	
			$changesheet->getColumnDimension('I')->setAutoSize(true);		
		}
		
		$objPHPExcel->setActiveSheetIndex(0);
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$report = '_'.date("Y-m-d_H:i:s", time());
		$objWriter->save('reports/ckokickboxing_report'.$report.'.xlsx');
		
		$subject = 'Ckokickboxing Report';
		$to = '';
		$body = 'There are new changes to Ckokickboxing!  Here are the new changes.(see attached) This report uses a scrubbing mechanism so if some data does not display correctly please see your web systems admin.';
		$attachments = 'reports/ckokickboxing_report'.$report.'.xlsx';
		
		
		//create the email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array('donotreply@tsdemos.com' => 'Ckokickboxing Reports'))
			->setCc(array('jbaker@think-safe.com'))
			->setBody($body, 'text/html')
			->addPart($body, 'text/html')
			->attach(Swift_Attachment::fromPath($attachments)
				->setFilename('ckokickboxingreport.xlsx'));
		$result = $mailer->send($message);
		
		$mtime = microtime(); 
		$mtime = explode(" ",$mtime); 
		$mtime = $mtime[1] + $mtime[0]; 
		$endtime = $mtime; 
		$totaltime = ($endtime - $starttime); 
		
		if($DEBUG_MODE)
			echo '
			Creating spreadsheet completed in '.$totaltime.' seconds<br />
			Added '.$rowcounter.' rows to the spreadsheet.<br />';
	} else {
		if($DEBUG_MODE)
			echo 'No changes were made, no email was sent.<br />';
	}
?>
</body>
</html>