<?php
ini_set('max_execution_time', 3600);
header('Content-Type: text/html; charset=utf-8');
include('simple_html_dom.php');

define('DBHOST', 'localhost');
define('DBUSER', 'tsdemosc_scscrap');
define('DBPASS', '1105firstvoice');
define('DB', 'tsdemosc_screenscrapes');
@ $db = new mysqli(DBHOST, DBUSER, DBPASS, DB);

require_once('./swiftmailer/swift_required.php');
require_once('./phpexcel/PHPExcel.php');
require_once('./phpexcel/PHPExcel/Writer/Excel2007.php');
date_default_timezone_set('America/Chicago');

$DEBUG_MODE = false;

function abbreviate_state($state_to_abreviate)
{
    //This information never changes
    $full_state = array('Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'WestVirginia', 'Wisconsin', 'Wyoming');
    $ab_state = array('AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY');

    //Here we search and replace the state with the abbreviation
    return str_replace($full_state, $ab_state, $state_to_abreviate);
}

function full_state($state_to_abreviate)
{
    //This information never changes
    $full_state = array('Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'WestVirginia', 'Wisconsin', 'Wyoming');
    $ab_state = array('AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY');

    //Here we search and replace the state with the abbreviation
    return str_replace($ab_state, $full_state, $state_to_abreviate);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body>
<?php
//time the script
$mtime = microtime();
$mtime = explode(" ", $mtime);
$mtime = $mtime[1] + $mtime[0];
$starttime = $mtime;
$statecounter = 0;
$gymcounter = 0;
$results = array();

//get webpage
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://laboxing.com/connect/find-a-gym/");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);

$directoryhtml = new simple_html_dom();
$directoryhtml->load($result);

foreach ($directoryhtml->find('.gym-item .box p') as $e) {
    $gymcounter++;
    $name = $e->find('strong a', 0)->innertext;
    $website = $e->find('strong a', 0)->getAttribute('href');

    $details = explode('<br>', $e->innertext);
    //$details[0] is the name
    $address = trim($details[1]);
    $state = substr($details[2], -8, 2);
    $fullstate = full_state($state);
    $zip = trim(substr($details[2], -5, 5));
    $city = trim(str_replace(', ' . $state . ' ' . $zip, '', $details[2]));
    $phone = trim(str_replace('Phone: ', '', $details[3]));

    $results[] = array(
        'name' => $name,
        'address' => $address,
        'city' => $city,
        'state' => $fullstate,
        'zip' => $zip,
        'phone' => $phone,
        'website' => $website
    );
}
if ($DEBUG_MODE)
    echo '<pre>' . print_r($results, 1) . '</pre>';

$mtime = microtime();
$mtime = explode(" ", $mtime);
$mtime = $mtime[1] + $mtime[0];
$endtime = $mtime;
$totaltime = ($endtime - $starttime);

if ($DEBUG_MODE)
    echo '<br /><br />
		This page was created in ' . $totaltime . ' seconds<br />
		Link Counts<br />
		---------------<br />
		Gyms:' . $gymcounter . '<br />';

/* UPDATE ON CHANGE */
$mtime = microtime();
$mtime = explode(" ", $mtime);
$mtime = $mtime[1] + $mtime[0];
$starttime = $mtime;
$changes = array();
$updatecounter = 0;
$insertcounter = 0;

//check all old data
foreach ($results as $key => $val) {
    if ($DEBUG_MODE)
        echo $val['name'] . ',' . $val['address'] . ',' . $val['city'] . ',' . $val['state'] . ',' . $val['zip'] . ',' . $val['phone'] . ',' . $val['website'] . '<br />';

    $getolddata = $db->query("
			SELECT *
			FROM laboxing
			WHERE name = '" . $db->real_escape_string($val['name']) . "'
		");
    if ($row = $getolddata->fetch_assoc()) {
        //data is already in database
        //update record
        $db->query("
				UPDATE laboxing
				SET phone = '" . $db->real_escape_string($val['phone']) . "',
					address = '" . $db->real_escape_string($val['address']) . "',
					city = '" . $db->real_escape_string($val['city']) . "',
					state = '" . $db->real_escape_string($val['state']) . "',
					zip = '" . $db->real_escape_string($val['zip']) . "',
					website = '" . $db->real_escape_string($val['website']) . "'
				WHERE id = '" . $row['id'] . "'
			");

        //$changes[] = $results[$key];
        $updatecounter++;

        //remove from array as we have most recent data
        unset($results[$key]);
    }
}

//insert data we could not find information for
if (count($results) > 0) {
    //loop remaining data
    $insertcounter = 0;
    foreach ($results as $key => $val) {
        if ($insertcounter == 0)
            $query = 'INSERT INTO laboxing (name, address, city, state, zip, phone, website, creation) VALUES ' . "('" . $db->real_escape_string($val['name']) . "','" . $db->real_escape_string($val['address']) . "','" . $db->real_escape_string($val['city']) . "','" . $db->real_escape_string($val['state']) . "','" . $db->real_escape_string($val['zip']) . "','" . $db->real_escape_string($val['phone']) . "','" . $db->real_escape_string($val['website']) . "', '" . date('Y-m-d H:i:s') . "')";
        else
            $query .= ",('" . $db->real_escape_string($val['name']) . "','" . $db->real_escape_string($val['address']) . "','" . $db->real_escape_string($val['city']) . "','" . $db->real_escape_string($val['state']) . "','" . $db->real_escape_string($val['zip']) . "','" . $db->real_escape_string($val['phone']) . "','" . $db->real_escape_string($val['website']) . "', '" . date('Y-m-d H:i:s') . "')";
        $insertcounter++;

        $results[$key]['creation'] = date('Y-m-d H:i:s');
        $results[$key]['updated'] = date('Y-m-d H:i:s');
        $changes[] = $results[$key];
    }
    $db->query($query);
}

$mtime = microtime();
$mtime = explode(" ", $mtime);
$mtime = $mtime[1] + $mtime[0];
$endtime = $mtime;
$totaltime = ($endtime - $starttime);

if ($DEBUG_MODE)
    echo '
		Processing Queries completed in ' . $totaltime . ' seconds<br />
		Updated ' . $updatecounter . ' rows in the database.<br />
		Inserted ' . $insertcounter . ' rows into the database.<br />';

if ($DEBUG_MODE)
    echo '<pre>' . print_r($changes, 1) . '</pre>';

//check to see if we did any changes
if (count($changes) > 0) {
    $mtime = microtime();
    $mtime = explode(" ", $mtime);
    $mtime = $mtime[1] + $mtime[0];
    $starttime = $mtime;
    $rowcounter = 1;

    //add phpexcel caching
    $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
    $cacheSettings = array('memoryCacheSize' => '32MB');
    PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);

    //initilize the spreedsheet
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Think Safe");
    $objPHPExcel->getProperties()->setLastModifiedBy("Think Safe");
    $objPHPExcel->getProperties()->setTitle("LA Boxing Report");
    $objPHPExcel->getProperties()->setSubject("LA Boxing  Report");
    $objPHPExcel->getProperties()->setDescription("A report for new changes to the LA Boxing database");

    $objPHPExcel->setActiveSheetIndex(0);
    $changesheet = $objPHPExcel->getActiveSheet();
    $changesheet
        ->SetCellValue('A1', 'Name')
        ->SetCellValue('B1', 'Address')
        ->SetCellValue('C1', 'City')
        ->SetCellValue('D1', 'State')
        ->SetCellValue('E1', 'Zip')
        ->SetCellValue('F1', 'Phone')
        ->SetCellValue('G1', 'Website')
        ->SetCellValue('H1', 'First Recorded')
        ->SetCellValue('I1', 'Last Changed');

    foreach ($changes as $key => $val) {
        $rowcounter++;
        $changesheet
            ->SetCellValue('A' . $rowcounter, $val['name'])
            ->SetCellValue('B' . $rowcounter, $val['address'])
            ->SetCellValue('C' . $rowcounter, $val['city'])
            ->SetCellValue('D' . $rowcounter, $val['state'])
            ->SetCellValue('E' . $rowcounter, $val['zip'])
            ->SetCellValue('F' . $rowcounter, $val['phone'])
            ->SetCellValue('G' . $rowcounter, $val['website'])
            ->SetCellValue('H' . $rowcounter, $val['creation'])
            ->SetCellValue('I' . $rowcounter, $val['updated']);

        $changesheet->getColumnDimension('A')->setAutoSize(true);
        $changesheet->getColumnDimension('B')->setAutoSize(true);
        $changesheet->getColumnDimension('C')->setAutoSize(true);
        $changesheet->getColumnDimension('D')->setAutoSize(true);
        $changesheet->getColumnDimension('F')->setAutoSize(true);
        $changesheet->getColumnDimension('G')->setAutoSize(true);
        $changesheet->getColumnDimension('H')->setAutoSize(true);
        $changesheet->getColumnDimension('I')->setAutoSize(true);
    }

    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $report = '_' . date("Y-m-d_H:i:s", time());
    $objWriter->save('reports/laboxing_report' . $report . '.xlsx');

    $subject = 'LA Boxing Report';
    $to = '';
    $body = 'There are new changes to LA Boxing!  Here are the new changes.(see attached) This report uses a scrubbing mechanism so if some data does not display correctly please see your web systems admin.';
    $attachments = 'reports/laboxing_report' . $report . '.xlsx';


    //create the email
    $transport = Swift_MailTransport::newInstance();
    $mailer = Swift_Mailer::newInstance($transport);
    $message = Swift_Message::newInstance()
        ->setSubject($subject)
        ->setFrom(array('donotreply@tsdemos.com' => 'LA Boxing Reports'))
        ->setTo(array('mboyles@think-safe.com'))
        //->setTo(array('aarox04@gmail.com'))
        ->setCc(array('pwickham@think-safe.com'))
        ->setBody($body, 'text/html')
        ->addPart($body, 'text/html')
        ->attach(Swift_Attachment::fromPath($attachments)
            ->setFilename('laboxing.xlsx'));
    $result = $mailer->send($message);

    $mtime = microtime();
    $mtime = explode(" ", $mtime);
    $mtime = $mtime[1] + $mtime[0];
    $endtime = $mtime;
    $totaltime = ($endtime - $starttime);

    if ($DEBUG_MODE)
        echo '
			Creating spreadsheet completed in ' . $totaltime . ' seconds<br />
			Added ' . $rowcounter . ' rows to the spreadsheet.<br />';
} else {
    if ($DEBUG_MODE)
        echo 'No changes were made, no email was sent.<br />';
}
?>
</body>
</html>