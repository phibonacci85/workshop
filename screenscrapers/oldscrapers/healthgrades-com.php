<?php
	ini_set('max_execution_time', 3600); 
	include('simple_html_dom.php');
	
	$DEBUG_MODE = false;
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<?php
	//time the script
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$starttime = $mtime; 
	$statecounter = 0;
	$citycounter = 0;
	$physiciancounter = 0;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://www.healthgrades.com/family-practice-directory");
	curl_setopt($ch, CURLOPT_HEADER, false);
	//curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
	
	foreach($directoryhtml->find('.stateBlock h4 a') as $e)
	{
		$stateurl = $e->getAttribute('href');
		
		$letter = 'a';
		
		while($letter != 'aa')
		{
			if($DEBUG_MODE)
				echo 'http://www.healthgrades.com'.$stateurl.'_'.$letter;flush();
		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://www.healthgrades.com'.$stateurl.'_'.$letter);
			curl_setopt($ch, CURLOPT_HEADER, false);
			//curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$cityletterwide = curl_exec($ch);
			curl_close($ch);
			$statecounter++;
			
			$cityletterhtml = new simple_html_dom();
			$cityletterhtml->load($cityletterwide);
		
			foreach($cityletterhtml->find('.cityColumn a') as $e3)
			{
				$citycounter++;
				$cityurl = $e3->getAttribute('href');
				//checking all the pages of listings(20 per page)
				for($i = 1;$i<2;$i++) {
					if($DEBUG_MODE)
						echo 'CITY:http://www.healthgrades.com/family-practice-directory/'.$cityurl.'_'.$i;
						
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'http://www.healthgrades.com/family-practice-directory/'.$cityurl.'_'.$i);
					curl_setopt($ch, CURLOPT_HEADER, false);
					//curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$citywide = curl_exec($ch);
					curl_close($ch);
					$citycounter++;
					
					$cityhtml = new simple_html_dom();
					$cityhtml->load($citywide);
					
					//check to see if any results
					if($cityhtml->find('#providerSearchResults',0))
					{
						foreach($cityhtml->find('#providerSearchResults .listing') as $e4)
						{
							//reset previous data
							$phone = '';
							$doctor = '';
							$physicianpage = '';
							$address = '';
							$practice = '';
							$location = '';
							$hospital = '';
							$insurance = '';
							
							//gather contact information
							$doctor = str_replace(',', ' ', $e4->find('.listingHeaderLeftColumn h2 a', 0)->innertext);
							$physicianpage = $e4->find('.listingHeaderLeftColumn h2 a', 0)->getAttribute('href');
							$address = str_replace(',', ' ', $e4->find('.listingHeaderLeftColumn .addresses', 0)->firstChild('.address')->innertext);
							
							foreach($e4->find('.listingBody .listingCenterColumn .listingProfileContent ul .dataDebug') as $information)
							{
								if(strpos($information->innertext, 'Practice')) {
									$practice = str_replace(' Year of Practice', '', str_replace(' Years of Practice', '', $information->find('a',0)->innertext));
								} else if(strpos($information->innertext, 'Office Location')) {
									//$location = str_replace(' Office Location', '', str_replace(' Office Locations', '', $information->find('a',0)->innertext));
									$location = 1;
								} else if(strpos($information->innertext, 'Hospital Affiliation')) {
									$hospital = str_replace(' Hospital Affiliation', '', str_replace(' Hospital Affiliations', '', $information->find('a',0)->innertext));
								} else if(strpos($information->innertext, 'Insurance Carrier')) {
									$insurance = str_replace(' Insurance Carrier', '', str_replace(' Insurance Carriers', '', $information->find('a',0)->innertext));
								} else {
									if($DEBUG_MODE)
										echo '<p style="color:red;">'.$information->innertext.'</p>';
								}
							}
							flush();
							
							//try and get the phone number for this physician
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, 'http://www.healthgrades.com'.$physicianpage.'/address');
							curl_setopt($ch, CURLOPT_HEADER, false);
							//curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							$physicianwide = curl_exec($ch);
							curl_close($ch);
							$physiciancounter++;
							
							$physicianhtml = new simple_html_dom();
							$physicianhtml->load($physicianwide);
							
							$addressparts = explode(',', $e4->find('.listingHeaderLeftColumn .addresses', 0)->firstChild('.address')->innertext);
							
							foreach($physicianhtml->find('.listingInformationColumn') as $e5)
							{
								if(strpos($e5->find('div', 0), $addressparts[0]) || strpos($e5->find('div', 1), $addressparts[0]) || strpos($e5->find('div', 2), $addressparts[0]))
								{
									foreach($e5->find('.phoneNumber') as $phones)
									{
										$phone .= $phones->innertext.' ';
									}
									//we found what we were looking for, no need to continue
									break;
								}
							}
							
							array($doctor, $address, $practice, $location, $hospital, $insurance, $phone);
							flush();
						}
					} else
						//break out of the inifinte loop
						break;
				}
			}
			flush();
			$letter++;
		}
		if($DEBUG_MODE)
			echo ' <br />';
	}
	
	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - $starttime); 
	
	if($DEBUG_MODE)
		echo '<br /><br />This page was created in ".$totaltime." seconds<br />
		Link Counts<br />
		---------------<br />
		State:'.$statecounter.'<br />
		City:'.$citycounter.'<br />
		Physician:'.$physiciancounter.'<br />';
	
?>
</body>
</html>