<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Fitness 1440';
  $filename = 'fitness1440';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));

  $url = 'http://www.fitness1440.com/locations/';
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);
  curl_close($ch);
  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);

  $links = $directoryhtml->find('article li a');
  foreach($links as $link){
    if(!strpos($link->innertext, 'Coming Soon')){
      echo $link->innertext.'<br />';
      /*
        $url = $link->getAttribute('href').'contact/';
        $url = str_replace(' ', '', $url);
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HEADER, false);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $directoryhtml = new simple_html_dom();
        $directoryhtml->load($result);
        
        $info = $directoryhtml->find('.textwidget', 0);
        $details = explode('<br>', $info->innertext);
        $name = trim(strip_tags($details[0]));
        $address = trim(strip_tags($details[1]));
        $locale = trim(strip_tags($details[2]));
        $phone = trim(strip_tags($details[3]));
        
        fputcsv($fp, array($name, $address, $locale, $phone));
        */
    }
  }

  fclose($fp);
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
?>