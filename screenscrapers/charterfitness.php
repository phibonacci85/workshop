<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Charter Fitness';
  $filename = 'charterfitness';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));
  
  $url = 'http://www.charterfitness.com/locations/find-a-location/?address=50211&miles=2000';
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);
  curl_close($ch);

  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $locations = $directoryhtml->find('#location_list_map tr');
  $count = 0;
  foreach($locations as $location){
    if($count != 0){
      $name = $location->find('td', 1)->find('a', 0)->innertext;
      $contactInfo = $location->find('td', 2);
      $contactInfo = explode('<br />', $contactInfo->innertext);
      $address = trim(strip_tags($contactInfo[0]));
      $locale = trim(strip_tags($contactInfo[1]));
      $phone = trim(strip_tags($contactInfo[2]));
      fputcsv($fp, array($name, $address, $locale, $phone));
    }
    $count++;
  }

  fclose($fp);
  
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  
?>