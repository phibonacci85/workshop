<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Total Woman Spa';
  $filename = 'totalwomanspa';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));


  $url = 'http://www.totalwomanspa.com/all-locations';
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);
  curl_close($ch);

  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $links = $directoryhtml->find('article.uk-article a');
  foreach($links as $link){
    $url = 'http://www.totalwomanspa.com'.$link->getAttribute('href');
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_HEADER, false);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    $directoryhtml = new simple_html_dom();
    $directoryhtml->load($result);
    $locationInfo = $directoryhtml->find('article.wk-content div.uk-grid', 1);
    if($locationInfo){
      $addressInfo = $locationInfo->find('div.uk-width-medium-1-3', 0)->innertext;
      $contactInfo = $locationInfo->find('div.uk-width-medium-1-3', 1)->innertext;
      $addressInfo = explode('<br />', $addressInfo);
      $contactInfo = explode('<br />', $contactInfo);

      $name = $link->innertext;
      $address = preg_replace('/\s+/', ' ', trim(strip_tags($addressInfo[0])));
      $locale = preg_replace('/\s+/', ' ', trim(strip_tags($addressInfo[1])));
      $phone = preg_replace('/\s+/', ' ', trim(strip_tags(str_replace('Phone', '', $contactInfo[0]))));

      fputcsv($fp, array($name, $address, $locale, $phone));
    }
  }

  fclose($fp);
  
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  
?>