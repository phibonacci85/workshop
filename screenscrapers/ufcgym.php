<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'UFC Gym';
  $filename = 'ufcgym';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));

  $url = 'http://ufcgym.com/locations';
  $fields = array(
          'search_country' => '228',
          'search_state' => '26',
          'search_city' => '',
          'search_zip' => '',
          'search_distance' => '5000',
          'location_submit' => 'SEARCH'
  );


  $fields_string = '';
  foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
  rtrim($fields_string, '&');

  $ch = curl_init();

  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch,CURLOPT_POST, true);
  curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);


  $result = curl_exec($ch);
  curl_close($ch);

  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $locations = $directoryhtml->find('.locations_c');
  foreach($locations as $location){
    $name = trim(strip_tags($location->find('span', 0)->innertext));
    $address = trim(strip_tags($location->find('span', 1)->innertext));
    $locale = str_replace('&nbsp;', ' ', preg_replace('/\s+/', '', trim(strip_tags($location->find('span', 2)->innertext))));
    $phone = trim(strip_tags($location->find('span', 3)->innertext));
    fputcsv($fp, array($name, $address, $locale, $phone));
  }


  fclose($fp);
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  
?>