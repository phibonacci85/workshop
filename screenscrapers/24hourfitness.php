<?php
    ini_set('max_execution_time', 3600);
    include('simple_html_dom.php');
    require_once('./swiftmailer/swift_required.php');

    define('DBHOST', 'localhost');  
    define('DBUSER', 'tsdemosc_jbaker');
    define('DBPASS', 'Jbake1234');
    define('DB', 'tsdemosc_screenscrapers_jb');
    @ $db = new mysqli( DBHOST, DBUSER, DBPASS, DB);

    $to = array('jbaker@think-safe.com');
    $salesRep = array('mhild@think-safe.com', 'pwickham@think-safe.com');
    $subject = '24 Hour Fitness';
    $filename = '24hourfitness';
    $fp = fopen('reports/'.$filename.'.csv', 'w');
    $nfp = fopen('reports/new_'.$filename.'.csv', 'w');
    $newPlaces = false;
    fputcsv($fp, array('Name', 'Address', 'City', 'State', 'Zip', 'Phone'));
    fputcsv($nfp, array('Name', 'Address', 'City', 'State', 'Zip', 'Phone'));

    $url = 'http://www.24hourfitness.com/Website/ClubLocation/OpenClubs';

    $ch = curl_init();

    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_HEADER, false);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
      //curl_setopt($ch,CURLOPT_POST, true);
      //curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);


    $result = curl_exec($ch);
    curl_close($ch);

    $locations = json_decode($result);
    $locations = $locations->clubs;
    foreach($locations as $location){
        fputcsv($fp, array($location->name, $location->address->street, $location->address->city, $location->address->state, $location->address->zip, $location->phoneNumber));
        $places = $db->query("
            select *
            from 24hourfitness
            where name = '".str_replace("'", "''", $location->name)."'
        ");
        if($places && $places->num_rows){
            // place already exists in database
        } else {
            $newPlaces = true;
            $db->query("
                insert into 24hourfitness
                (name, address, city, state, zip, phone)
                values
                ('".str_replace("'", "''", $location->name)."', '".str_replace("'", "''", $location->address->street)."', '".str_replace("'", "''", $location->address->city)."', '".$location->address->state."', '".$location->address->zip."', '".$location->phoneNumber."')
            ");
            fputcsv($nfp, array($location->name, $location->address->street, $location->address->city, $location->address->state, $location->address->zip, $location->phoneNumber));
        }
    }

      //create the email
    $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
    $mailer = Swift_Mailer::newInstance($transport);
    $message = Swift_Message::newInstance()
    ->setSubject($subject)
    ->setFrom(array('donotreply@tsdemos.com' => $subject))
    ->setTo($to)
    ->setBody($subject, 'text/html')
    ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
      ->setFilename($filename.'.csv'));
    $result = $mailer->send($message);

    if($newPlaces == true){
        //create the email
        $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
        ->setUsername('donotreply@firstvoicemanager.com')
        ->setPassword('1105firstvoice');
        $mailer = Swift_Mailer::newInstance($transport);
        $message = Swift_Message::newInstance()
          ->setSubject($subject)
          ->setFrom(array('donotreply@tsdemos.com' => $subject))
          ->setTo($to)
          ->setBody('There have been changes to '.$subject, 'text/html')
          ->attach(Swift_Attachment::fromPath('reports/new_'.$filename.'.csv')
              ->setFilename('new_'.$filename.'.csv'));
        $result = $mailer->send($message);
    }
?>