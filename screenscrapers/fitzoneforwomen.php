<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'FitZone for Women';
  $filename = 'fitzoneforwomen';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Address 2', 'City', 'State', 'Zip', 'Phone'));

  $url = 'http://fitzoneforwomen.com/wp-content/plugins/store-locator/sl-xml.php?mode=gen&lat=42.0202919&lng=-91.63197029999998&radius=5000';

  $ch = curl_init();

  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);


  $result = curl_exec($ch);
  curl_close($ch);

  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $locations = $directoryhtml->find('marker');
  foreach($locations as $location){
    fputcsv($fp, array(str_replace('&amp;#44;', '', $location->name), $location->street, $location->street2, $location->city, $location->state, $location->zip, $location->phone));
  }
  

  fclose($fp);
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  
?>