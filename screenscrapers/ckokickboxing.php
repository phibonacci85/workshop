<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');
  
  $to = array('jbaker@think-safe.com');
  $subject = 'CKO Kickboxing';
  $filename = 'ckokickboxing';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));
  
	//get webpage
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://www.ckokickboxing.com/view-all-locations.html");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	$directoryhtml = new simple_html_dom();
	$directoryhtml->load($result);
    $links = $directoryhtml->find('div.paragraph');
	foreach($links as $e)
	{  
        $info = $e->innertext;
        $info = explode('<br />', $info);
        if(count($info) == 7){
          $name = preg_replace("/&#?[a-z0-9]{2,8};/i", '', strip_tags($info[0])).' '.preg_replace("/&#?[a-z0-9]{2,8};/i", '', strip_tags($info[1]));
          $address = preg_replace("/&#?[a-z0-9]{2,8};/i", '', strip_tags($info[2]));
          $locale = preg_replace("/&#?[a-z0-9]{2,8};/i", '', strip_tags($info[3]));
          $phone = preg_replace("/&#?[a-z0-9]{2,8};/i", '', strip_tags($info[4]));
          $phone = preg_replace("/&#?[a-z0-9]{2,8};/i", '', str_replace('Phone:', '', $phone));
          $phone = preg_replace("/&#?[a-z0-9]{2,8};/i", '', str_replace('&nbsp;', '', $phone));
        }else{
          $name = preg_replace("/&#?[a-z0-9]{2,8};/i", '', str_replace('&npsp;', ' ', strip_tags($info[0])));
          $address = preg_replace("/&#?[a-z0-9]{2,8};/i", '', strip_tags($info[1]));
          $locale = preg_replace("/&#?[a-z0-9]{2,8};/i", '', strip_tags($info[2]));
          $phone = preg_replace("/&#?[a-z0-9]{2,8};/i", '', strip_tags($info[3]));
          $phone = preg_replace("/&#?[a-z0-9]{2,8};/i", '', str_replace('Phone:', '', $phone));
          $phone = preg_replace("/&#?[a-z0-9]{2,8};/i", '', str_replace('&nbsp;', '', $phone));
        }
        echo $name.'<br />';
        echo $address.'<br />';
        echo $locale.'<br />';
        echo $phone.'<br /><br />';
        fputcsv($fp, array($name, $address, $locale, $phone));
	}
  
  fclose($fp);
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
?>