<?php
ini_set('max_execution_time', 3600);
include('simple_html_dom.php');
require_once('./swiftmailer/swift_required.php');
@ $db = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_screenscrapers_jb');

$to = array('jbaker@think-safe.com');
$subject = 'American Legion';
$filename = 'americanlegion';
$fp = fopen('reports/' . $filename . '.csv', 'w');
fputcsv($fp, array('Name', 'Address', 'Address 2', 'Phone'));

$locations = $db->query("
    select *
    from americanlegion
");
while($location = $locations->fetch_assoc()) {
    fputcsv($fp, array($location['name'], $location['address1'], $location['address2'], $location['phone']));
}

fclose($fp);
//create the email
$transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
$mailer = Swift_Mailer::newInstance($transport);
$message = Swift_Message::newInstance()
    ->setSubject($subject)
    ->setFrom(array('donotreply@tsdemos.com' => $subject))
    ->setTo($to)
    ->setBody($subject, 'text/html')
    ->attach(Swift_Attachment::fromPath('reports/' . $filename . '.csv')
        ->setFilename($filename . '.csv'));
$result = $mailer->send($message);

?>