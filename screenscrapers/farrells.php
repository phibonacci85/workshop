<?php
ini_set('max_execution_time', 3600);
include('simple_html_dom.php');
require_once('./swiftmailer/swift_required.php');

$to = array('jbaker@think-safe.com');
$subject = 'Farrells';
$filename = 'farrells';
$fp = fopen('reports/'.$filename.'.csv', 'w');
fputcsv($fp, array('Name', 'Address', 'City', 'State', 'Zip', 'Phone'));

	//get webpage
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "http://www.extremebodyshaping.com/find-a-location.cfm");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);

$directoryhtml = new simple_html_dom();
$directoryhtml->load($result);
$states = $directoryhtml->find('#stateDropdown option');
  // print_r($locations);
foreach($states as $state)
{  
    $st = $state->getAttribute('value');

        //get webpage
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://www.extremebodyshaping.com/find-a-location.cfm?state=".$st);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    $locationshtml = new simple_html_dom();
    $locationshtml->load($result);
    $locations = $locationshtml->find('table.BoxTable tr');

    $count = 0;
    foreach($locations as $location){
        if($count++ == 0) continue;
        $name = trim(strip_tags($location->find('td', 0)->innertext));
        $address = trim(strip_tags($location->find('td', 1)->innertext));
        $city = trim(strip_tags($location->find('td', 2)->innertext));
        $zip = trim(strip_tags($location->find('td', 3)->innertext));
        $phone = trim(strip_tags($location->find('td', 4)->innertext));
        fputcsv($fp, array($name, $address, $city, $st, $zip, $phone));
    }
}

fclose($fp);

  //create the email
$transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
$mailer = Swift_Mailer::newInstance($transport);
$message = Swift_Message::newInstance()
    ->setSubject($subject)
    ->setFrom(array('donotreply@tsdemos.com' => $subject))
    ->setTo($to)
    ->setBody($subject, 'text/html')
    ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
        ->setFilename($filename.'.csv'));
$result = $mailer->send($message);
?>