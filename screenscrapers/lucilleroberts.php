<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Lucille Roberts';
  $filename = 'lucilleroberts';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));


  $url = 'http://www.lucilleroberts.com/locations';
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);
  curl_close($ch);

  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $links = $directoryhtml->find('.locations-list a');
  foreach($links as $link){
    $url = $link->getAttribute('href');
    $name = $link->innertext;
    
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_HEADER, false);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    $directoryhtml = new simple_html_dom();
    $directoryhtml->load($result);
    $details = $directoryhtml->find('.club-details p', 0)->innertext;
    $details = explode('<br />', $details);
    $address = trim(strip_tags($details[0]));
    $locale = trim(strip_tags($details[1]));
    $phone = trim(strip_tags(str_replace('P: ', '', $details[2])));
    fputcsv($fp, array($name, $address, $locale, $phone));
  }
  
  fclose($fp);
  
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  
?>