<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Tiger Rock Martial Arts';
  $filename = 'tigerrockmartialarts';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));


  $url = 'http://tigerrockmartialarts.com/find-academy';
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);
  curl_close($ch);

  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $links = $directoryhtml->find('a.academylabel');
  foreach($links as $link){
    $url = 'http://tigerrockmartialarts.com'.$link->getAttribute('href');
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_HEADER, false);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    $directoryhtml = new simple_html_dom();
    $directoryhtml->load($result);
    
    $addressInfo = $directoryhtml->find('.academyaddress', 0)->innertext;
    $contactInfo = $directoryhtml->find('.academycontact', 0)->innertext;
    $addressInfo = explode('<br />', $addressInfo);
    $contactInfo = explode('<br />', $contactInfo);

    $name = preg_replace('/\s+/', ' ', trim(strip_tags($addressInfo[0])));
    $address = preg_replace('/\s+/', ' ', trim(strip_tags($addressInfo[1])));
    $locale = preg_replace('/\s+/', ' ', trim(strip_tags($addressInfo[2])));
    $phone = preg_replace('/\s+/', ' ', trim(strip_tags(str_replace('Phone:', '', $contactInfo[1]))));
    fputcsv($fp, array($name, $address, $locale, $phone));
    
  }

  fclose($fp);
  
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  
?>