<?php
ini_set('max_execution_time', 3600);
include('simple_html_dom.php');
require_once('./swiftmailer/swift_required.php');
@ $db = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_screenscrapes');
@ $crmdb = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_suitecrmnew');

$ab_state = array('AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY');
$newLocations = 0;

foreach ($ab_state as $state) {
    $url = 'https://www.anytimefitness.com/locations/us/' . $state . '/';

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);


    $result = curl_exec($ch);
    curl_close($ch);
    $directoryhtml = new simple_html_dom();
    $directoryhtml->load($result);
    $tableRows = $directoryhtml->find('table tbody tr');

    foreach ($tableRows as $row) {

        $websiteLink = $row->find('a', 0);
        $website = $websiteLink->href;
        $cityState = $websiteLink->innertext;
        $cityState = explode(',', $cityState);
        $city = str_replace("'", "''", trim($cityState[0]));
        $state = str_replace("'", "''", trim($cityState[1]));
        $address = str_replace("'", "''", trim(strip_tags($row->find('td', 1))) . ' ' . $city);
        $phone = trim(strip_tags($row->find('td', 2)));
        $status = strtolower(trim(strip_tags($row->find('td', 3))));

        if (!(empty($city))) {
            $locations = $db->query("
                SELECT *
                FROM anytimefitness
                WHERE location LIKE '" . $city . " " . $state . "%'
                AND address LIKE '" . $address . "%'
            ");
            $statusChange = false;
            if ($location = $locations->fetch_assoc()) {
                //echo 'Found!<br />';
                if ($location['status'] != $status) {
                    //echo 'Status Changed: ' . $location['status'] . ' => ' . $status;
                }
            } else {
                $newLocations++;

                $insertNewLocation = $db->query("
                    INSERT INTO anytimefitness
                    (state, status, location, address, phone, website)
                    VALUES 
                    ('" . $state . "', '" . $status . "', '" . $city . " " . $state . "', '" . $address . "', '" . $phone . "', '" . $website . "')
                ");

                $uniqueId = uniqid('');

                $insertCrmAccount = $crmdb->query("
                    INSERT INTO accounts
                    (id, name, date_entered, date_modified, modified_user_id, created_by, description, assigned_user_id, account_type, industry, annual_revenue, phone_fax, billing_address_street, billing_address_city, billing_address_state, billing_address_postalcode, billing_address_country, rating, phone_office, phone_alternate, website, ownership, employees, ticker_symbol, shipping_address_street, shipping_address_city, shipping_address_state, shipping_address_postalcode, shipping_address_country, parent_id, sic_code, campaign_id)
                    VALUES (
                      '" . $uniqueId . "', 
                      'AnytimeFitness - " . $address . "', 
                      '" . date('Y-m-d H:i:s') . "', 
                      '" . date('Y-m-d H:i:s') . "', 
                      '1f8d0df1-cbe4-d290-f575-574f492e636f', 
                      '1f8d0df1-cbe4-d290-f575-574f492e636f', 
                      '',  
                      '00532000004zEQQAA2', 
                      '', 
                      'Fitness', 
                      '', 
                      '', 
                      '" . $address . "', 
                      '" . $city . "', 
                      '" . $state . "', 
                      '', 
                      '', 
                      '', 
                      '" . $phone . "', 
                      '', 
                      '" . $website . "', 
                      '', 
                      '', 
                      '', 
                      '', 
                      '', 
                      '', 
                      '', 
                      '', 
                      '', 
                      '', 
                      '8c412cc3-68aa-09c8-7c4b-574f78710e54'
                    )
                ");
                if($insertCrmAccount) {
                    //echo 'ADDED Acount<br />';
                } else {
                    //echo 'ERROR: '.$db->error.'<br />';
                }

                $insertCrmAccountCstm = $crmdb->query("
                    INSERT INTO accounts_cstm
                    (id_c, type_c, club_status_c)
                    VALUES
                    ('" . $uniqueId . "', 'Prospect', '" . $status . "')
                ");


                $dueDate = new DateTime("now");
                $dueDate->add(new DateInterval('P1D'));
                if ($dueDate->format('l') == 'Saturday') {
                    $dueDate->add(new DateInterval('P2D'));
                } else if ($dueDate->format('l') == 'Sunday') {
                    $dueDate->add(new DateInterval('P1D'));
                }

                //create a task for tobias on next business day
                $uniqueTaskId = uniqid('');
                //echo $uniqueTaskId;
                $insertTask = $crmdb->query("
                    INSERT INTO tasks
                    (id, name, date_entered, date_modified, modified_user_id, created_by, description, deleted, assigned_user_id, status, date_due_flag, date_due, date_start_flag, date_start, parent_type, parent_id, contact_id, priority)
                    VALUES (
                      '" . $uniqueTaskId . "', 
                      'New Anytime Fitness Club', 
                      '" . date('Y-m-d H:i:s') . "', 
                      '" . date('Y-m-d H:i:s') . "', 
                      '1f8d0df1-cbe4-d290-f575-574f492e636f', 
                      '1f8d0df1-cbe4-d290-f575-574f492e636f', 
                      '', 
                      '0', 
                      '00532000004zEQQAA2', 
                      '', 
                      '', 
                      '" . $dueDate->format('Y-m-d H:i:s') . "', 
                      '', 
                      '" . $dueDate->format('Y-m-d H:i:s') . "', 
                      'Accounts', 
                      '" . $uniqueId . "', 
                      '', 
                      'High'
                    )
                ");
            }
        }
    }

}

?>