<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'Gym Guyz';
  $filename = 'gymguyz';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));


  $url = 'http://gymguyz.com/locations';
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);
  curl_close($ch);

  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $links = $directoryhtml->find('tr div.jrContentTitle a');
  $distinctlinks = array();
  foreach($links as $link){
    $href = $link->getAttribute('href');
    if(!in_array($href, $distinctlinks, true)){
      $name =  $link->innertext.'<br />';
      $distinctlinks[] = $href;
      $ch = curl_init();
      curl_setopt($ch,CURLOPT_URL, $href);
      curl_setopt($ch,CURLOPT_HEADER, false);
      curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
      $result = curl_exec($ch);
      curl_close($ch);
      echo $result.'<br />';
      $directoryhtml = new simple_html_dom();
      $directoryhtml->load($result);
      $contactInfo = $directoryhtml->find('div.item-page');
    }
  }
  fclose($fp);
  /*
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  */
?>