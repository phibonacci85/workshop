<?php
  ini_set('max_execution_time', 3600);
  include('simple_html_dom.php');
  require_once('./swiftmailer/swift_required.php');

  $to = array('jbaker@think-safe.com');
  $subject = 'My Gym';
  $filename = 'mygym';
  $fp = fopen('reports/'.$filename.'.csv', 'w');
  fputcsv($fp, array('Name', 'Address', 'Locale', 'Phone'));


  $url = 'http://www.mygym.com/locations';
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL, $url);
  curl_setopt($ch,CURLOPT_HEADER, false);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
  $result = curl_exec($ch);
  curl_close($ch);

  $directoryhtml = new simple_html_dom();
  $directoryhtml->load($result);
  $scripts = $directoryhtml->find('script');
  $count = 0;
  foreach($scripts as $script){
    if($count == 8){
      $locationsJSON = $script->innertext.'<br /><br />';
    }
    $count++;
  }
  $sub = substr($locationsJSON, strpos($locationsJSON, '['), strlen($locationsJSON));
  $locationsJSON = substr($sub, 0, strpos($sub, ']')+1);
  $locationsJSON = str_replace("'", '"', $locationsJSON);
  $locationsJSON = strip_tags($locationsJSON);
  $locationsJSON = str_replace('\n', ' ', $locationsJSON);
  $locations = json_decode($locationsJSON, false, 2048);
  print_r($locations);

  fclose($fp);
  /*
  //create the email
  $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
    ->setUsername('donotreply@firstvoicemanager.com')
    ->setPassword('1105firstvoice');
  $mailer = Swift_Mailer::newInstance($transport);
  $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setFrom(array('donotreply@tsdemos.com' => $subject))
      ->setTo($to)
      ->setBody($subject, 'text/html')
      ->attach(Swift_Attachment::fromPath('reports/'.$filename.'.csv')
          ->setFilename($filename.'.csv'));
  $result = $mailer->send($message);
  */
?>