<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 7/12/2016
 * Time: 2:46 PM
 */

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $webdb = new mysqli($host, 'tsdemosc_webser3', 'Tb.zQqUzsREf', 'tsdemosc_websites_live');


$itemInfoHTML = '';
$itemInfoPlain = '';

$items = $webdb->query("
    select *
    from national_accounts_processingorder po, national_accounts_package_items pi
    where po.transaction_id = '72M83667G6234862D'
    and po.part_number = pi.part_number
");
$i = 1;
while($item = $items->fetch_asso()) {
    $itemInfoHTML .= '
        <tr><td><h3 style="margin:10px 0 5px 0;">Item '.$i.' Info </h3></td></tr>
        <tr><td style="width:150px">Name: </td><td>'.$item['product_name'].'</td></tr>
        <tr><td style="width:150px">Part Number: </td><td>'.$item['part_number'].'</td></tr>
        <tr><td style="width:150px">Quantity: </td><td>'.$item['qty'].'</td></tr>
        <tr><td style="width:150px">Gross: </td><td>$'.$item['gross'].'</td></tr>
    ';
    $itemInfoPlain .= '
        Item '.$i.' Information
        Name: '.$item['product_name'].'
        Part Number: '.$item['part_number'].'
        Quantity: '.$item['qty'].'
        Gross: $'.$item['gross'].'
    ';
    $i++;
}

$registrationHTML = '';
$registrations = $webdb->query("
            select *
            from national_accounts_customer_info ci, national_accounts_customer_address ca, sales_reps sr
            where ci.national_accounts_customer_info_id = '".$_POST['invoice']."'
            and ci.national_accounts_customer_info_id = ca.national_accounts_customer_info_id
            and sr.sales_reps_id = ci.sales_rep_id
        ");
if($registration = $registrations->fetch_assoc()){
    $registrationHTML = '
                <tr><td><h3 style="margin:10px 0 5px 0;">Registration Information</h3></td></tr>
                <tr><td style="width:150px">Organization: </td><td>'.$registration['organization_name'].'</td></tr>
                <tr><td style="width:150px">Contact: </td><td>'.$registration['contact_person_name'].'</td></tr>
                <tr><td style="width:150px">Contact Phone: </td><td>'.$registration['contact_phone_number'].'</td></tr>
                <tr><td style="width:150px">Contact Email: </td><td>'.$registration['contact_email'].'</td></tr>
                <tr><td style="width:150px">Org Address: </td><td>'.$registration['organization_address1'].'</td></tr>
                <tr><td style="width:150px">Org Address 2: </td><td>'.$registration['organization_address2'].'</td></tr>
                <tr><td style="width:150px">Org City: </td><td>'.$registration['organization_city'].'</td></tr>
                <tr><td style="width:150px">Org State: </td><td>'.$registration['organization_state'].'</td></tr>
                <tr><td style="width:150px">Org Zip: </td><td>'.$registration['organization_zip'].'</td></tr>
                <tr><td style="width:150px">Ship Address: </td><td>'.$registration['ship_address1'].'</td></tr>
                <tr><td style="width:150px">Ship Address 2: </td><td>'.$registration['ship_address2'].'</td></tr>
                <tr><td style="width:150px">Ship City: </td><td>'.$registration['ship_city'].'</td></tr>
                <tr><td style="width:150px">Ship State: </td><td>'.$registration['ship_state'].'</td></tr>
                <tr><td style="width:150px">Ship Zip: </td><td>'.$registration['ship_zip'].'</td></tr>
                <tr><td style="width:150px">Comments: </td><td>'.$registration['comments'].'</td></tr>
                <tr><td><h3 style="margin:10px 0 5px 0;">Sales Representative</h3></td></tr>
                <tr><td style="width:150px">First Name: </td><td>'.$registration['first_name'].'</td></tr>
                <tr><td style="width:150px">Last Name: </td><td>'.$registration['last_name'].'</td></tr>
                <tr><td style="width:150px">Email: </td><td>'.$registration['email'].'</td></tr>
                <tr><td style="width:150px">Phone: </td><td>'.$registration['phone'].'</td></tr>
                <tr><td style="width:150px">Extension: </td><td>'.$registration['ext'].'</td></tr>
            ';
} else {
    error_log("\n Couldn't get registration info: ".$_POST['invoice']);
}
$bodyHTML = '
<html>

<head></head>

<body style="width:800px;">
    <table style="display:table;background-color:#FFF;width:800px;border:5px solid #0078C1;border-collapse:collapse;">
        <tr>
            <td style="text-align:center;"><h1 style="margin:10px;">National Account Purchase</h1></td>
        </tr>
        <tr style="display:table-row;height:1em;background-color:#0078C1;">
            <td></td>
        </tr>
        <tr style="display:table-row;">
            <td style="padding:10px;width:100%;">
                <table>
                    '.$itemInfoHTML.'
                    <tr><td><h3 style="margin:10px 0 5px 0;">Paypal Buyer Information</h3></td></tr>
                    <tr><td style="width:150px">First Name: </td><td>'.$_POST['first_name'].'</td></tr>
                    <tr><td style="width:150px">Last Name: </td><td>'.$_POST['last_name'].'</td></tr>
                    <tr><td style="width:150px">Email: </td><td>'.$_POST['payer_email'].'</td></tr>
                    <tr><td style="width:150px">Address: </td><td>'.$_POST['address_name'].'</td></tr>
                    <tr><td style="width:150px">Street: </td><td>'.$_POST['address_street'].'</td></tr>
                    <tr><td style="width:150px">City: </td><td>'.$_POST['address_city'].'</td></tr>
                    <tr><td style="width:150px">State: </td><td>'.$_POST['address_state'].'</td></tr>
                    <tr><td style="width:150px">ZIP: </td><td>'.$_POST['address_zip'].'</td></tr>
                    <tr><td style="width:150px">Country: </td><td>'.$_POST['address_country'].'</td></tr>
                    '.$registrationHTML.'
                    <tr><td><h3 style="margin:10px 0 5px 0;">Transaction</h3></td></tr>
                    <tr><td style="width:150px">Transaction Id: </td><td>'.$_POST['txn_id'].'</td></tr>
                    <tr><td style="width:150px">Type: </td><td>'.$_POST['txn_type'].'</td></tr>
                    <tr><td style="width:150px">Payment Status: </td><td>'.$_POST['payment_status'].'</td></tr>
                    <tr><td style="width:150px">Total Gross: </td><td>$'.$_POST['mc_gross'].'</td></tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
        ';

$bodyPlain = '
'.$itemInfoPlain.'
Type: '.$_POST['custom'].'
Name: '.$_POST['item_name'].'
Part Number: '.$_POST['item_number'].'
Quantity: </td><td>'.$_POST['quantity'].'
Gross: $'.$_POST['mc_gross'].'
First Name: '.$_POST['first_name'].'
Last Name: '.$_POST['last_name'].'
Email: '.$_POST['payer_email'].'
Address: '.$_POST['address_name'].'
Street: '.$_POST['address_street'].'
City: '.$_POST['address_city'].'
State: '.$_POST['address_state'].'
ZIP: '.$_POST['address_zip'].'
Country: '.$_POST['address_country'].'
Transaction Id: '.$_POST['txn_id'].'
Type: '.$_POST['txn_type'].'
Payment Status: '.$_POST['payment_status'].'
        ';

$sales = $webdb->query("
            select email
            from national_accounts n, national_accounts_offers o, sales_reps s, national_accounts_packages p
            where p.national_accounts_packages_id = '".$_POST['custom']."'
            and p.national_accounts_offer_id = o.national_accounts_offers_id
            and o.national_accounts_id = n.national_accounts_id
            and n.national_accounts_id = o.national_accounts_id
            and n.sales_reps_id = s.sales_reps_id
        ");
$sale = $sales->fetch_assoc();

//create email
$transport = Swift_SmtpTransport::newInstance('server.tsdemos.com', 25)
    ->setUsername('donotreply@tsdemos.com')
    ->setPassword('1105firstvoice');
$mailer = Swift_Mailer::newInstance($transport);
$message = Swift_Message::newInstance()
    ->setSubject('National Account Purchase')
    ->setFrom(array('donotreply@tsdemos.com' => 'National Account'))
    ->setTo(array('cullrich@think-safe.com', 'admin@firstvoice.us', 'orderprocessing@firstvoice.us', $sale['email']))
    ->setBody($bodyHTML, 'text/html')
    ->addPart($bodyPlain, 'text/plain');
$mailer->send($message);

?>