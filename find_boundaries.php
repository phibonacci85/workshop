<?php

$url = 'http://maps.huge.info/zipv0.pl?ZIP=52246';
$fields = array(
    'address' => 'Alabama USA',
    'country[id]' => '1',
    'country[name]' => 'USA',
    'region[id]' => '37',
    'region[name]' => 'Alabama',
    'city[id]' => '308',
    'city[name]' => 'Birmingham'
);
$fields_string = '';
foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
rtrim($fields_string, '&');

$ch = curl_init();
curl_setopt($ch,CURLOPT_URL, $url);
curl_setopt($ch,CURLOPT_HEADER, false);
curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);

//$ch = curl_init();
//curl_setopt($ch,CURLOPT_URL, $url);
//curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//curl_setopt($ch,CURLOPT_POST, true);
//curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
//$result = curl_exec($ch);
//curl_close($ch);

$polylines = array();
$resultXML = new SimpleXMLElement($result);
foreach($resultXML as $key => $val) {
    $polylines[$key][] = $val->attributes();
}

echo json_encode($polylines);
?>