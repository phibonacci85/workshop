<?php
include('scripts/definitions.php');
include('scripts/ini.php');
include('scripts/functions.php');

if(!isset($_GET['o']) || isempty($_GET['o'])) {
	//no course set
	header('Location: heart-2-heart.php');
	exit();
} else {
	//get information from database
	$processingorder = $db->query("
		select ho.*, ap.price, p.packageName
		from processingorder ho, alternatepricing ap, packages p
		where ho.id = '".$_GET['o']."'
		and ap.courseid = ho.courseid
		and ap.type = 'heart-2-heart'
		and p.id = ho.courseid
	");
	if($row = $processingorder->fetch_assoc()) {
		$course = $row['packageName'];
		$price = $row['price'];
		
		$firstname = $row['firstname'];
		$lastname = $row['lastname'];
		$phone = $row['phone'];
		$email = $row['email'];
	} else {
		//no course found
		header('Location: heart-2-heart.php');
		exit();
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	
	<link rel="shortcut icon" href="favicon.ico" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/skin.css" />
	<title>Confirm Purchase</title>
<body>
<div id="container">
	<div id="header">
		<a href="index.php?content=home">
			<div id="mainpic">
				<img src="images/FVTLogo.png" alt="" border="0" />
			</div>
		</a>
		<div id="welcomebar">
			<div class="message" style="width:200px;float:left;">&nbsp;</div>
		</div>
	</div>
	<div id="content">
		<h1>Confirm Purchase Information</h1>
		<div>Course: <?php echo $course; ?></div>
		<div>Price: $<?php echo $price; ?></div>

		<h2>Contact Information</h2>
		<div>Name: <?php echo $firstname.' '.$lastname; ?></div>
		<div>Phone: <?php echo $phone; ?></div>
		<div>Email <?php echo $email; ?></div>


		<form style="float:left;" name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="cmd" value="_xclick">
			<input type="hidden" name="business" value="admin@firstvoicetraining.com">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="item_name" value="<?php echo $course; ?>">
			<input type="hidden" name="amount" value="<?php echo $price; ?>">
			<input type="hidden" name="custom" value="<?php echo $_GET['o']; ?>">
			<input type="hidden" name="return" value="http://firstvoicetraining.com">
			<input type="hidden" name="notify_url" value="http://h2h.firstvoicetraining.com/ipn.php">
			<input type="hidden" name="ipn_notification_url" value="http://h2h.firstvoicetraining.com/ipn.php">
			<input type="image" src="http://www.paypal.com/en_US/i/btn/btn_buynow_LG.gif" border="0" name="submit" alt="Make payments with PayPal - it\'s fast, free and secure!">
		</form>
	</div>
</div>
</body>