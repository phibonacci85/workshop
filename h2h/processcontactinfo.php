<?php
include('scripts/definitions.php');
include('scripts/ini.php');
include('scripts/functions.php');

//check if data was passed
if(isset($_POST['submit']) && !isempty($_POST['course'])) {
	//gather information from form
	$firstname = $db->real_escape_string($_POST['firstname']);
	$lastname = $db->real_escape_string($_POST['lastname']);
	$email = $db->real_escape_string($_POST['email']);
	$phone = $db->real_escape_string($_POST['phone']);
	$course = $db->real_escape_string($_POST['course']);
	
	//check to see if information valid
	if(!isempty($firstname)
	&& !isempty($lastname)
	&& !isempty($email) //&& isemail($email)
	&& !isempty($phone)) {
		//insert into database
		$processingorder = $db->query("
			insert into processingorder
			(courseid, type, firstname, lastname, email, phone)
			values
			('$course', 'heart-2-heart', '$firstname', '$lastname', '$email', '$phone')
		");
		$h2hid = $db->insert_id;
		
		//send to confirm order
		header('Location: confirmorder.php?o='.$h2hid);
		exit();
	} else {
		//not valid input
		header('Location: contactinfo.php?e=1&c='.$_POST['course']);
		exit();
	}
} else {
	//no form set, send back to form
	header('Location: contactinfo.php');
	exit();
}
?>