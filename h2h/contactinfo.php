<?php
include('scripts/definitions.php');
include('scripts/ini.php');
include('scripts/functions.php');

if(!isset($_GET['c']) || isempty($_GET['c'])) {
	//no course set
	header('Location: http://heart-2-heart.php');
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	
	<link rel="shortcut icon" href="favicon.ico" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/skin.css" />
	<title>Contact Information</title>
<body>
<div id="container">
	<div id="header">
		<a href="index.php?content=home">
			<div id="mainpic">
				<img src="images/FVTLogo.png" alt="" border="0" />
			</div>
		</a>
		<div id="welcomebar">
			<div class="message" style="width:200px;float:left;">&nbsp;</div>
		</div>
	</div>
	<div id="content">
		<h1>Contact Information</h1>
		<form action="processcontactinfo.php" method="post" style="width:410px;margin:0 auto;">
			<?php
				if(isset($_GET['e']))
					echo '<p style="color:red;">You must enter in valid information.</p>';
			?>
			<div style="width:200px;float:left;">First Name</div>
			<div style="width:200px;float:left;margin-left:10px;">Last Name</div>
			<div style="width:200px;float:left;clear:left;"><input type="text" name="firstname" style="width:100%;" /></div>
			<div style="width:200px;float:left;margin-left:10px;"><input type="text" name="lastname" style="width:100%;" /></div>
			
			<div style="width:200px;float:left;clear:both;">Phone</div>
			<div style="width:200px;float:left;margin-left:10px;">Email</div>
			<div style="width:200px;float:left;clear:left;"><input type="text" name="phone" style="width:100%;" /></div>
			<div style="width:200px;float:left;margin-left:10px;"><input type="text" name="email" style="width:100%;" /></div>
			
			<input type="hidden" value="<?php echo $_GET['c']; ?>" name="course" />
			<input type="submit" name="submit" value="Submit" class="button" style="clear:both;float:left;margin-top:5px;"/>
		</form>
	</div>
</div>
</body>