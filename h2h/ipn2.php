<?php
// tell PHP to log errors to ipn_errors.log in this directory
ini_set('log_errors', true);
ini_set('error_log', dirname(__FILE__).'/ipn_errors.log');

// intantiate the IPN listener
include('ipnlistener.php');
$listener = new IpnListener();

// tell the IPN listener to use the PayPal test sandbox
$listener->use_sandbox = false;

// try to process the IPN POST
try {
    $listener->requirePostMethod();
    $verified = $listener->processIpn();
} catch (Exception $e) {
    error_log($e->getMessage());
    exit(0);
}

//Handle IPN Response here
if ($verified) {
	error_log("\n".'verified');
	$ordernum = $_POST['custom'];
	
    $errmsg = '';   // stores errors from fraud checks
    
    // 1. Make sure the payment status is "Completed" 
    if ($_POST['payment_status'] != 'Completed') { 
        // simply ignore any IPN that is not completed
        exit(0); 
    }

	error_log("\n".'completed');
	
    // 2. Make sure seller email matches your primary account email.
    if ($_POST['receiver_email'] != 'Rene@Heart-2-Heartcpr.com') {
        $errmsg .= "'receiver_email' does not match: ";
        $errmsg .= $_POST['receiver_email']."\n";
    }
    
	//get price of order
	$db_name = "tsdemosc_firstvoicetraining";
	$connection = @mysql_connect("localhost", "tsdemosc_fvt", "40gpIvwCZ5lF") or die(mysql_error());
	$db = @mysql_select_db($db_name, $connection) or die(mysql_error());
	$sql = "select ap.price
			from processingorder po, alternatepricing ap
			where po.id = '$ordernum'
			and po.courseid = ap.courseid
			";
	$result = @mysql_query($sql, $connection) or die(mysql_error());
	if($row = mysql_fetch_array($result))
	{
		$price = $row['price'];
	} else {
        $errmsg .= "'mc_gross' does not match: ";
        $errmsg .= $_POST['mc_gross']."\n";
	}
	
    // 3. Make sure the amount(s) paid match
    if ($_POST['mc_gross'] != $price) {
        $errmsg .= "'mc_gross' does not match: ";
        $errmsg .= $_POST['mc_gross']."\n";
    }
    
    // 4. Make sure the currency code matches
    if ($_POST['mc_currency'] != 'USD') {
        $errmsg .= "'mc_currency' does not match: ";
        $errmsg .= $_POST['mc_currency']."\n";
    }

    // 5. Ensure the transaction is not a duplicate.
    mysql_connect('localhost', 'tsdemosc_fvt', '40gpIvwCZ5lF') or exit(0);
    mysql_select_db('tsdemosc_firstvoicetraining') or exit(0);

    $txn_id = mysql_real_escape_string($_POST['txn_id']);
    $sql = "SELECT COUNT(*) FROM orders WHERE txn_id = '$txn_id'";
    $r = mysql_query($sql);
    
    if (!$r) {
        error_log(mysql_error());
        exit(0);
    }
	
	error_log("\n".'txn');
    
    $exists = mysql_result($r, 0);
    mysql_free_result($r);
    
    if ($exists) {
        $errmsg .= "'txn_id' has already been processed: ".$_POST['txn_id']."\n";
    }
    
    if (!empty($errmsg)) {
		error_log("\n".'errors!!!!');
        // manually investigate errors from the fraud checking
        $body = "IPN failed fraud checks: \n$errmsg\n\n";
        $body .= $listener->getTextReport();
        mail('cullrich@think-safe.com', 'IPN Fraud Warning', $body);
        
    } else {
		error_log("\n".'no errors');
		require_once('../scripts/library.php');	
		require_once('../lib/swift_required.php');
	
        // add this order to a table of completed orders
		$payer_email = mysql_real_escape_string($_POST['payer_email']);
		$mc_gross = mysql_real_escape_string($_POST['mc_gross']);
		$sql = "INSERT INTO orders VALUES 
				(NULL, '$txn_id', '$payer_email', $mc_gross, NULL)";
		
		if (!mysql_query($sql)) {
			error_log(mysql_error());
			exit(0);
		}
		
		error_log("\n".'sql good');
		
		
		//collect information for keycode
		$db_name = "tsdemosc_firstvoicetraining";
		$connection = @mysql_connect("localhost", "tsdemosc_fvt", "40gpIvwCZ5lF") or die(mysql_error());
		$db = @mysql_select_db($db_name, $connection) or die(mysql_error());
		$sql = "select *
				from processingorder po, packages p
				where po.id = '$ordernum'
				and po.courseid = p.id
				";
		$result = @mysql_query($sql, $connection) or die(mysql_error());
		if($row = mysql_fetch_array($result))
		{
			$email = $row['email'];
			$package = $row['packageName'];
		}
		
		//track completed
		$sql = "update processingorder
				set completed = 'yes'
				where id = '$ordernum'
				";
		$result = @mysql_query($sql, $connection) or die(mysql_error());
		
		//get dates
		$date = date("m-d-Y");
		$expiration = date("m-d-Y", strtotime("+2 year"));
		
		//generate keycode
		$code = keygen(10);

		//validate keycode
		$validatekey = jk_validatekey($code);
		while($validatekey == "no")
		{
			error_log("\n".'validate keycode');
			$validatekey = jk_validatekey($code);
		}
		error_log("\n".'valid keycode');
		$sql = ("
					insert into keycodes 
					(
						code,
						packageName,
						distributorName,
						refresherTime,
						demo,
						inperson,
						download,
						orderNum,
						customerName,
						creation, 	
						expiration,
						`usage`,
						`uselimit`
					) values (
						'$validatekey', '$package', '20', '00', 'no', 'no', 'no', 'online', 'None', '$date', '$expiration', 1, 1
					)
				");
				error_log("\n".$sql);
		$insertcode = @mysql_query($sql, $connection) or die(mysql_error());
		
		error_log("\n".'emailing');
		
		//create email
		$transport = Swift_MailTransport::newInstance();
		$mailer = Swift_Mailer::newInstance($transport);
		$message = Swift_Message::newInstance()
			->setSubject('First Voice Training Keycode')
			->setFrom(array('keycodes@firstvoicetraining.com' => 'First Voice Training'))
			->setTo(array($email))
			->setBcc(array('rene@heart-2-heartcpr.com' => 'Heart-2-Heart', 'cullrich@think-safe.com' => 'FirstVoice', 'pwickham@think-safe.com' => 'FirstVoice',  'mightyegor@gmail.com' => 'FirstVoice', 'h2horders@firstvoicetraining.com' => 'FirstVoice'))
			->setBody('<html><head></head><body style="color:black;">
					<div style="margin:0 auto;width:970px;background-color:#C2E0F6;color:black;padding: 5px 10px;">
						<div style="width:910px;margin:20px;background-color:#0078C1;padding: 5px 10px;">
							<div style="width:870px;margin:10px;background-color:white;padding: 5px 10px;">
								<h1 style="font-size:18pt;width:850px;">
									<img alt="First Voice Training" src="http:firstvoicetraining.com/images/FVTLogo.png"></h1>
								<h1 style="width:850px;">
									Your Purchase has been processed!
								</h1>
								Thank you for purchasing an online training course from Heart 2 Heart Mobile CPR. You may want to print these instructions or refer to them as you log into First Voice Online Training.<br />
								<br />
								Use this keycode to access the program: '.$validatekey.'<br />
								<br /><br />							 
								1. To access First Voice Training go to firstvoicetraining.com. You should save (bookmark) this web page as a favorite for quick future reference during refresher or for new course training.<br /><br />
								
								2. First time students should select "New Student" and Copy & Paste your keycode(s) from the attached file(s) into the appropriate field(s). You will be required to register and create a username and password. Submit the form.<br /><br />
								
								3. A login screen will appear. Login with your newly created username & password.<br /><br />
								
								4. You will see the student section upon login. "Uncompleted Courses" section will list any courses you have purchased.<br /><br />
								
								5. Select "Take Now" next to the Uncompleted Course you want to take. If you start a course and can not finish, it will automatically bookmark where you stopped. You do not need to start over from the beginning when you return to the course. The course is self-paced; you can move forward by selecting "Next" and review prior course material by selecting "Previous".<br /><br />
								
								6. The course contains audio, text, videos and illustrations with appropriate course curriculum. When you reach the end of the course take the test. Click on the "Take Test" button (bottom right corner of the page) to start your test.<br /><br />
								
								7. Answer the questions on the test. You can retake the test as many times as you need to in order to pass the test.  Upon successful completion of the test you can print out your 8 x 10 Wall Certificate or Wallet Card. Once the course is passed, you can review (refresh) the course material as often as you would like. The certificate you receive is only good for 2 years however.<br /><br />
								
								8. If you would like, a Wallet Card can be mailed for a small handling & postage fee. Requests for a mailed card take approximately 5 days for processing.<br /><br />
								
								If you have any technical questions or concerns please contact 319-377-5125 or <a href="http://firstvoicetraining.com/index.php?content=contact">support@firstvoicetraining.com</a>.<br /><br />

								Regards,<br /> 
								Renee Miller<br />
								Heart to Heart CPR<br />
								971-221-8719<br /><br />
															
							</div>
						</div>
					 </div><div style="width:850px;margin:0 auto;color:black;background-color:white;">Please do not reply to this email.  We are unable to respond to inquiries sent to this address.  
					 For immediate answers to your questions, visit our Contact Us page at the top of any First Voice Training page.</div><br />
					 <div style="width:850px;margin:0 auto">Copyright &copy;  2013</div></body></html>', 'text/html')
			// And optionally an alternative body
			->addPart(		
				'Your order for inperson First Voice Training has been processed and completed.  You are now able to look at the online material at http://www.firstvoicetraining.com', 'text/plain');
			
		// Send the message to student	
		$mailer->send($message);
	}
    
} else {
    // manually investigate the invalid IPN
    mail('aarox04@gmail.com', 'Invalid IPN', $listener->getTextReport());
}


?>