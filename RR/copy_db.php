<?php
/**
 * Created by PhpStorm.
 * User: John
 * Date: 10/9/2017
 * Time: 16:53
 */

@ $oldRR = new mysqli('fvdemos.com', 'fvdemos', 'Hawkeye17!', 'fvdemos_rescueready');
@ $newRR = new mysqli('tsdemos.com', 'tsdemosc', 'Hawkeye17!', 'tsdemosc_rescue_ready');

$olds = $oldRR->query("
    SELECT *
    FROM aedcheck
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO aedcheck (
          aedcheck_id, 
          aed_id, 
          date, 
          location, 
          `condition`, 
          cabinet, 
          alarm, 
          alarmbattery, 
          alarmbatteryexpiration, 
          rescuekit, 
          rescuekitcond, 
          gloves, 
          razor, 
          scissors, 
          towel, 
          towlette, 
          mask, 
          status, 
          comment, 
          location_id, 
          display, 
          lastupdated
        ) VALUES (
          '" . $old['aedcheck_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['date'] . "', 
          '" . $old['location'] . "', 
          '" . $old['condition'] . "', 
          '" . $old['cabinet'] . "', 
          '" . $old['alarm'] . "', 
          '" . $old['alarmbattery'] . "', 
          '" . $old['alarmbatteryexpiration'] . "', 
          '" . $old['rescuekit'] . "', 
          '" . $old['rescuekitcond'] . "', 
          '" . $old['gloves'] . "', 
          '" . $old['razor'] . "', 
          '" . $old['scissors'] . "', 
          '" . $old['towel'] . "', 
          '" . $old['towlette'] . "', 
          '" . $old['mask'] . "', 
          '" . $old['status'] . "', 
          '" . $old['comment'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['display'] . "', 
          '" . $old['lastupdated'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedcheck_accessories
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedcheck_accessories (
          aedcheck_accessory_id, 
          aedcheck_id, 
          aed_accessory_type_id, 
          type, 
          expiration, 
          lot, 
          attached, 
          new, 
          new_aed_accessory_type_id, 
          new_expiration, 
          new_lot, 
          creation
        ) VALUES (
          '" . $old['aedcheck_accessory_id'] . "', 
          '" . $old['aedcheck_id'] . "', 
          '" . $old['aed_accessory_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['attached'] . "', 
          '" . $old['new'] . "', 
          '" . $old['new_aed_accessory_type_id'] . "', 
          '" . $old['new_expiration'] . "', 
          '" . $old['new_lot'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedcheck_batteries
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedcheck_batteries (
          aedcheck_battery_id, 
          aedcheck_id, 
          aed_battery_type_id, 
          type, 
          expiration, 
          lot, 
          attached, 
          new, 
          new_aed_battery_type_id, 
          new_expiration, 
          new_lot, 
          creation
        ) VALUES (
          '" . $old['aedcheck_battery_id'] . "', 
          '" . $old['aedcheck_id'] . "', 
          '" . $old['aed_battery_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['attached'] . "', 
          '" . $old['new'] . "', 
          '" . $old['new_aed_battery_type_id'] . "', 
          '" . $old['new_expiration'] . "', 
          '" . $old['new_lot'] . "', 
          '" . $old['creation'] . "' 
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedcheck_checker
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedcheck_checker (
          aedcheck_checker_id, 
          aedcheck_id, 
          name, 
          email
        ) VALUES ( 
          '" . $old['aedcheck_checker_id'] . "', 
          '" . $old['aedcheck_id'] . "', 
          '" . $old['name'] . "', 
          '" . $old['email'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedcheck_pads
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedcheck_pads (
          aedcheck_pad_id, 
          aedcheck_id, 
          aed_pad_type_id, 
          type, 
          expiration, 
          lot, 
          attached, 
          new, 
          new_aed_pad_type_id, 
          new_expiration, 
          new_lot
        ) VALUES (
          '" . $old['aedcheck_pad_id'] . "', 
          '" . $old['aedcheck_id'] . "', 
          '" . $old['aed_pad_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['attached'] . "', 
          '" . $old['new'] . "', 
          '" . $old['new_aed_pad_type_id'] . "', 
          '" . $old['new_expiration'] . "', 
          '" . $old['new_lot'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedcheck_paks
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedcheck_paks (
          aedcheck_pak_id, 
          aedcheck_id, 
          aed_pak_type_id, 
          type, 
          expiration, 
          lot, 
          attached, 
          new, 
          new_aed_pak_type_id, 
          new_expiration, 
          new_lot
        ) VALUES (
          '" . $old['aedcheck_pak_id'] . "', 
          '" . $old['aedcheck_id'] . "', 
          '" . $old['aed_pak_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['attached'] . "', 
          '" . $old['new'] . "', 
          '" . $old['new_aed_pak_type_id'] . "', 
          '" . $old['new_expiration'] . "', 
          '" . $old['new_lot'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM aeds
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aeds (
          aed_id, 
          aed_model_id, 
          serialnumber, 
          installdate, 
          sitecoordinator, 
          site_coordinator_contact_id, 
          dealer_contact_id, 
          purchasetype, 
          purchasedate, 
          warranty, 
          location, 
          plantype, 
          plandate, 
          planexpiration, 
          current, 
          hasservicecheck, 
          servicechecklength, 
          lastcheck, 
          lastservice, 
          pad_program, 
          notes, 
          accessibility, 
          mobility, 
          asset_number, 
          location_id, 
          created_by_id, 
          creation, 
          updated, 
          latitude, 
          longitude, 
          custom_image, 
          approved, 
          display
        ) VALUES (
          '" . $old['aed_id'] . "', 
          '" . $old['aed_model_id'] . "', 
          '" . $old['serialnumber'] . "', 
          '" . $old['installdate'] . "', 
          '" . $old['sitecoordinator'] . "', 
          '" . $old['site_coordinator_contact_id'] . "', 
          '" . $old['dealer_contact_id'] . "', 
          '" . $old['purchasetype'] . "', 
          '" . $old['purchsedate'] . "', 
          '" . $old['warranty'] . "', 
          '" . $old['location'] . "', 
          '" . $old['plantype'] . "', 
          '" . $old['plandate'] . "', 
          '" . $old['planexpiration'] . "', 
          '" . $old['current'] . "', 
          '" . $old['hasservicecheck'] . "', 
          '" . $old['servicechecklength'] . "', 
          '" . $old['lastcheck'] . "', 
          '" . $old['lastservice'] . "', 
          '" . $old['pad_program'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['accessibility'] . "', 
          '" . $old['mobility'] . "', 
          '" . $old['asset_number'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['created_by_id'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['latitude'] . "', 
          '" . $old['longitude'] . "', 
          '" . $old['custom_image'] . "', 
          '" . $old['approved'] . "', 
          '" . $old['display'] . "'
        ) 
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedservicing
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedservicing (
          aedservicing_id, 
          aed_id, 
          date, 
          milesdriven, 
          programmanagementexp, 
          location, 
          `condition`, 
          cabinet, 
          alarm, 
          alarmbattery, 
          alarmbatteryexpiration, 
          rescuekit, 
          rescuekitcond, 
          kitinclude, 
          kitincludereplace, 
          status, 
          digitalsignature, 
          clientrepname, 
          clientreptitle, 
          clientrepemail, 
          clientrepphone, 
          comment, 
          location_id, 
          display, 
          lastupdated
        ) VALUES (
          '" . $old['aedservicing_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['date'] . "', 
          '" . $old['milesdriven'] . "', 
          '" . $old['programmanagementexp'] . "', 
          '" . $old['location'] . "', 
          '" . $old['condition'] . "', 
          '" . $old['cabinet'] . "', 
          '" . $old['alarm'] . "', 
          '" . $old['alarmbattery'] . "', 
          '" . $old['alarmbatteryexpiration'] . "', 
          '" . $old['rescuekit'] . "', 
          '" . $old['rescuekitcond'] . "', 
          '" . $old['kitinclude'] . "', 
          '" . $old['kitincludereplace'] . "', 
          '" . $old['status'] . "', 
          '" . $old['digitalsignature'] . "', 
          '" . $old['clientrepname'] . "', 
          '" . $old['clientreptitle'] . "', 
          '" . $old['clientrepemail'] . "', 
          '" . $old['clientrepphone'] . "', 
          '" . $old['comment'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['display'] . "', 
          '" . $old['lastupdated'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedservicing_accessories
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedservicing_accessories (
          aedservicing_accessory_id, 
          aedservicing_id, 
          aed_accessory_type_id, 
          type, 
          expiration, 
          lot, 
          attached, 
          new, 
          new_aed_accessory_type_id, 
          new_expiration, 
          new_lot, 
          creation
        ) VALUES (
          '" . $old['aedservicing_accessory_id'] . "', 
          '" . $old['aedservicing_id'] . "', 
          '" . $old['aed_accessory_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['attached'] . "', 
          '" . $old['new'] . "', 
          '" . $old['new_aed_accessory_type_id'] . "', 
          '" . $old['new_expiration'] . "', 
          '" . $old['new_lot'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedservicing_batteries
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedservicing_batteries (
          aedservicing_battery_id, 
          aedservicing_id, 
          aed_battery_type_id, 
          type, 
          expiration, 
          lot, 
          attached, 
          battery_charge, 
          new, 
          new_aed_battery_type_id, 
          new_expiration, 
          new_lot, 
          creation
        ) VALUES (
          '" . $old['aedservicing_battery_id'] . "', 
          '" . $old['aedservicing_id'] . "', 
          '" . $old['aed_battery_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['attached'] . "', 
          '" . $old['battery_charge'] . "', 
          '" . $old['new'] . "', 
          '" . $old['new_aed_battery_type_id'] . "', 
          '" . $old['new_expiration'] . "', 
          '" . $old['new_lot'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedservicing_pads
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedservicing_pads (
          aedservicing_pad_id, 
          aedservicing_id, 
          aed_pad_type_id, 
          type, 
          expiration, 
          lot, 
          attached, 
          new, 
          new_aed_pad_type_id, 
          new_expiration, 
          new_lot
        ) VALUES (
          '" . $old['aedservicing_pad_id'] . "', 
          '" . $old['aedservicing_id'] . "', 
          '" . $old['aed_pad_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['attached'] . "', 
          '" . $old['new'] . "', 
          '" . $old['new_aed_pad_type_id'] . "', 
          '" . $old['new_expiration'] . "', 
          '" . $old['new_lot'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aedservicing_paks
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aedservicing_paks (
          aedservicing_pak_id, 
          aedservicing_id, 
          aed_pak_type_id, 
          type, 
          expiration, 
          lot, 
          attached, 
          new, 
          new_aed_pak_type_id, 
          new_expiration, 
          new_lot
        ) VALUES (
          '" . $old['aedservicing_pak_id'] . "', 
          '" . $old['aedservicing_id'] . "', 
          '" . $old['aed_pak_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['attached'] . "', 
          '" . $old['new'] . "', 
          '" . $old['new_aed_pak_type_id'] . "', 
          '" . $old['new_expiration'] . "', 
          '" . $old['new_lot'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM aed_accessories
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_accessories (
          aed_accessory_id, 
          location_id, 
          aed_id, 
          aed_accessory_type_id, 
          expiration, 
          lot, 
          udi, 
          spare, 
          display, 
          updated
        ) VALUES (
          '" . $old['aed_accessory_id'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['aed_accessory_type_id'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['udi'] . "', 
          '" . $old['spare'] . "', 
          '" . $old['display'] . "', 
          '" . $old['updated'] . "'
        ) 
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aed_accessory_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_accessory_types (
          aed_accessory_type_id, 
          type, 
          partnum, 
          length, 
          buylink
        ) VALUES (
          '" . $old['aed_accessory_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['partnum'] . "', 
          '" . $old['length'] . "', 
          '" . $old['buylink'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aed_accessory_types_on_models
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_accessory_types_on_models (
          aed_accessory_types_on_model_id, 
          aed_accessory_type_id, 
          aed_model_id, 
          creation
        ) VALUES (
          '" . $old['aed_accessory_types_on_model_id'] . "', 
          '" . $old['aed_accessory_type_id'] . "', 
          '" . $old['aed_model_id'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM aed_batteries
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_batteries (
          aed_battery_id, 
          location_id, 
          aed_id, 
          aed_battery_type_id, 
          expiration, 
          lot, 
          udi, 
          spare, 
          display, 
          updated
        ) VALUES (
          '" . $old['aed_battery_id'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['aed_battery_type_id'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['udi'] . "', 
          '" . $old['spare'] . "', 
          '" . $old['display'] . "', 
          '" . $old['updated'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM aed_battery_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_battery_types (
          aed_battery_type_id, 
          type, 
          partnum, 
          price, 
          length
        ) VALUES (
          '" . $old['aed_battery_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['partnum'] . "', 
          '" . $old['price'] . "', 
          '" . $old['length'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aed_battery_types_on_models
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_battery_types_on_models (
          aed_battery_types_on_model_id, 
          aed_battery_type_id, 
          aed_model_id, 
          creation
        ) VALUES (
          '" . $old['aed_battery_types_on_model_id'] . "', 
          '" . $old['aed_battery_type_id'] . "', 
          '" . $old['aed_model_id'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aed_brands
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_brands (
          aed_brand_id, 
          brand, 
          logo
        ) VALUES (
          '" . $old['aed_brand_id'] . "', 
          '" . $old['brand'] . "', 
          '" . $old['logo'] . "'
        )
    ");
}

//TODO aed_consumables_alternate_part_numbers

//TODO aed_images

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM aed_models
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_models (
          aed_model_id, 
          aed_brand_id, 
          model, 
          trainer, 
          picture, 
          mobile_image
        ) VALUES (
          '" . $old['aed_model_id'] . "', 
          '" . $old['aed_brand_id'] . "', 
          '" . $old['model'] . "', 
          '" . $old['trainer'] . "', 
          '" . $old['picture'] . "', 
          '" . $old['mobile_image'] . "'
        )
    ");
}

//TODO aed_model_part_numbers

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM aed_pads
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_pads (
          aed_pad_id, 
          location_id, 
          aed_id, 
          aed_pad_type_id, 
          expiration, 
          lot, 
          udi, 
          spare, 
          display, 
          updated
        ) VALUES (
          '" . $old['aed_pad_id'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['aed_pad_type_id'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['udi'] . "', 
          '" . $old['spare'] . "', 
          '" . $old['display'] . "', 
          '" . $old['updated'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM aed_pad_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_pad_types (
          aed_pad_type_id, 
          type, 
          pediatric, 
          partnum, 
          price, 
          length
        ) VALUES (
          '" . $old['aed_pad_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['pediatric'] . "', 
          '" . $old['partnum'] . "', 
          '" . $old['price'] . "', 
          '" . $old['length'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aed_pad_types_on_models
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_pad_types_on_models (
          aed_pad_types_on_model_id, 
          aed_pad_type_id, 
          aed_model_id, 
          creation
        ) VALUES (
          '" . $old['aed_pad_types_on_model_id'] . "', 
          '" . $old['aed_pad_type_id'] . "', 
          '" . $old['aed_model_id'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM aed_paks
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_paks (
          aed_pak_id, 
          location_id, 
          aed_id, 
          aed_pak_type_id, 
          expiration, 
          lot, 
          udi, 
          spare, 
          display, 
          updated
        ) VALUES (
          '" . $old['aed_pak_id'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['aed_pak_type_id'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['udi'] . "', 
          '" . $old['spare'] . "', 
          '" . $old['display'] . "', 
          '" . $old['updated'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aed_pak_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_pak_types (
          aed_pak_type_id, 
          type, 
          pediatric, 
          partnum, 
          price, 
          length, 
          buylink
        ) VALUES (
          '" . $old['aed_pak_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['pediatric'] . "', 
          '" . $old['partnum'] . "', 
          '" . $old['price'] . "', 
          '" . $old['length'] . "', 
          '" . $old['buylink'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM aed_pak_types_on_models
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.aed_pak_types_on_models (
          aed_pak_types_on_model_id, 
          aed_pak_type_id, 
          aed_model_id, 
          creation
        ) VALUES (
          '" . $old['aed_pak_types_on_model_id'] . "', 
          '" . $old['aed_pak_type_id'] . "', 
          '" . $old['aed_model_id'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM alerts
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.alerts (
          id, 
          locationid, 
          email, 
          level, 
          type, 
          language, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['email'] . "', 
          '" . $old['level'] . "', 
          '" . $old['type'] . "', 
          '" . $old['language'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

//TODO certification_templates

$olds = $oldRR->query("
    SELECT *
    FROM config_admins
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.config_admins (
          id, 
          userid
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['userid'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM config_internalemails
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.config_internalemails (
          id, 
          email
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['email'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM config_privileges
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.config_privileges (
          id, 
          `order`, 
          privilege, 
          parent, 
          pusage, 
          nusage, 
          display, 
          views
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['order'] . "', 
          '" . $old['privilege'] . "', 
          '" . $old['parent'] . "', 
          '" . $old['pusage'] . "', 
          '" . $old['nusage'] . "', 
          '" . $old['display'] . "', 
          '" . $old['views'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM config_trainingcardtemplate
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.config_trainingcardtemplate (
          id, 
          orgid, 
          templatenumber, 
          trainingcenter
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['templatenumber'] . "', 
          '" . $old['trainingcenter'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM contacts
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.contacts (
          id, 
          org_id, 
          name, 
          title, 
          company, 
          department, 
          contacttype, 
          preferredlanguage, 
          website, 
          date, 
          phone, 
          fax, 
          cell, 
          receive_texts, 
          email, 
          address, 
          city, 
          province, 
          country, 
          zip, 
          notes, 
          sendaedcheck, 
          s_name, 
          s_title, 
          s_company, 
          s_department, 
          s_contacttype, 
          s_date, 
          s_phone, 
          s_fax, 
          s_cell, 
          s_email, 
          s_address, 
          s_city, 
          s_province, 
          s_country, 
          s_zip, 
          s_notes, 
          creation, 
          updated, 
          locationid, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['org_id'] . "', 
          '" . $old['name'] . "', 
          '" . $old['title'] . "', 
          '" . $old['company'] . "', 
          '" . $old['department'] . "', 
          '" . $old['contacttype'] . "', 
          '" . $old['preferredlanguage'] . "', 
          '" . $old['website'] . "', 
          '" . $old['date'] . "', 
          '" . $old['phone'] . "', 
          '" . $old['fax'] . "', 
          '" . $old['cell'] . "', 
          '" . $old['receive_texts'] . "', 
          '" . $old['email'] . "', 
          '" . $old['address'] . "', 
          '" . $old['city'] . "', 
          '" . $old['province'] . "', 
          '" . $old['country'] . "', 
          '" . $old['zip'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['sendaedcheck'] . "', 
          '" . $old['s_name'] . "', 
          '" . $old['s_title'] . "', 
          '" . $old['s_company'] . "', 
          '" . $old['s_department'] . "', 
          '" . $old['s_contacttype'] . "', 
          '" . $old['s_date'] . "', 
          '" . $old['s_phone'] . "', 
          '" . $old['s_fax'] . "', 
          '" . $old['s_cell'] . "', 
          '" . $old['s_email'] . "', 
          '" . $old['s_address'] . "', 
          '" . $old['s_city'] . "', 
          '" . $old['s_province'] . "', 
          '" . $old['s_country'] . "', 
          '" . $old['s_zip'] . "', 
          '" . $old['s_notes'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM documents
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.documents (
          id, 
          typeid, 
          name, 
          status, 
          date, 
          nextdate, 
          employee, 
          supervisor, 
          file, 
          locationid, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['status'] . "', 
          '" . $old['date'] . "', 
          '" . $old['nextdate'] . "', 
          '" . $old['employee'] . "', 
          '" . $old['supervisor'] . "', 
          '" . $old['file'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM documents_categories
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.documents_categories (
          id, 
          name, 
          uploads, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['name'] . "', 
          '" . $old['uploads'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM documents_persons
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.documents_persons (
          id, 
          personid, 
          documentid, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['personid'] . "', 
          '" . $old['documentid'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM documents_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.documents_types (
          id, 
          orgid, 
          categoryid, 
          compliance, 
          notes, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['categoryid'] . "', 
          '" . $old['compliance'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM documents_typesalerts
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.documents_typesalerts (
          id, 
          typeid, 
          days, 
          statusalert, 
          employee, 
          supervisor, 
          constantemail, 
          subject, 
          body, 
          attachmentonefile, 
          attachmenttwofile, 
          attachmentthreefile, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['days'] . "', 
          '" . $old['statusalert'] . "', 
          '" . $old['employee'] . "', 
          '" . $old['supervisor'] . "', 
          '" . $old['constantemail'] . "', 
          '" . $old['subject'] . "', 
          '" . $old['body'] . "', 
          '" . $old['attachmentonefile'] . "', 
          '" . $old['attachmenttwofile'] . "', 
          '" . $old['attachmentthreefile'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM documents_typeslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.documents_typeslang (
          id, 
          typeid, 
          name, 
          language, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['language'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM documents_uploads
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.documents_uploads (
          id, 
          documentid, 
          file
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['documentid'] . "', 
          '" . $old['file'] . "'
        )
    ");
}

//TODO drop_alerts

//TODO drop_plans

$olds = $oldRR->query("
    SELECT *
    FROM emails
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.emails (
          id, 
          userid, 
          `to`, 
          subject, 
          body, 
          attachments, 
          sent
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['userid'] . "', 
          '" . $old['to'] . "', 
          '" . $old['subject'] . "', 
          '" . $old['body'] . "', 
          '" . $old['attachments'] . "', 
          '" . $old['sent'] . "'
        )
    ");
}

//TODO emergencies

$olds = $oldRR->query("
    SELECT *
    FROM equipment
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment (
          id, 
          styleid, 
          locationid, 
          location, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['location'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipmentcheck
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipmentcheck (
          id, 
          equipmentid, 
          compliance1, 
          compliance2, 
          comments, 
          creation, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['equipmentid'] . "', 
          '" . $old['compliance1'] . "', 
          '" . $old['compliance2'] . "', 
          '" . $old['comments'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipmentcheck_checker
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipmentcheck_checker (
          id, 
          checkid, 
          name, 
          email
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['checkid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['email'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipmentcheck_content
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipmentcheck_content (
          id, 
          checkid, 
          answer, 
          uploadname, 
          type
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['checkid'] . "', 
          '" . $old['answer'] . "', 
          '" . $old['uploadname'] . "', 
          '" . $old['type'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipmentcheck_contentlang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipmentcheck_contentlang (
          id, 
          contentid, 
          content, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['contentid'] . "', 
          '" . $old['content'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipmentcheck_questions
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipmentcheck_questions (
          id, 
          checkid, 
          answer, 
          type
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['checkid'] . "', 
          '" . $old['answer'] . "', 
          '" . $old['type'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipmentcheck_questionslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipmentcheck_questionslang (
          id, 
          questionid, 
          question, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['questionid'] . "', 
          '" . $old['question'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipment_additionalcontent
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment_additionalcontent (
          id, 
          styleid, 
          orgid, 
          type, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['type'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipment_additionalcontentanswers
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment_additionalcontentanswers (
          id, 
          equipmentid, 
          additionalcontentid, 
          content, 
          uploadname, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['equipmentid'] . "', 
          '" . $old['additionalcontentid'] . "', 
          '" . $old['content'] . "', 
          '" . $old['uploadname'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipment_additionalcontentlang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment_additionalcontentlang (
          id, 
          contentid, 
          title, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['contentid'] . "', 
          '" . $old['title'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipment_alerts
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment_alerts (
          id, 
          styleid, 
          orgid, 
          checkfrequency, 
          levelone, 
          leveltwo, 
          levelthree, 
          creator, 
          creatorip, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['checkfrequency'] . "', 
          '" . $old['levelone'] . "', 
          '" . $old['leveltwo'] . "', 
          '" . $old['levelthree'] . "', 
          '" . $old['creator'] . "', 
          '" . $old['creatorip'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipment_checkquestions
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment_checkquestions (
          id, 
          styleid, 
          orgid, 
          type, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['type'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipment_checkquestionslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment_checkquestionslang (
          id, 
          questionid, 
          question, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['questionid'] . "', 
          '" . $old['question'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipment_styles
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment_styles (
          id, 
          typeid, 
          orgid, 
          checkfrequency, 
          levelone, 
          leveltwo, 
          levelthree, 
          creator, 
          creatorip, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['checkfrequency'] . "', 
          '" . $old['levelone'] . "', 
          '" . $old['leveltwo'] . "', 
          '" . $old['levelthree'] . "', 
          '" . $old['creator'] . "', 
          '" . $old['creatorip'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM equipment_styleslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO equipment_styleslang (
          id, 
          styleid, 
          name, 
          `set`, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['set'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipment_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment_types (
          id, 
          orgid, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM equipment_typeslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.equipment_typeslang (
          id, 
          typeid, 
          name, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM erp
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.erp (
          id, 
          locationid, 
          type, 
          name, 
          file, 
          registrationdate, 
          registrationexpiration, 
          registrationagency, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['type'] . "', 
          '" . $old['name'] . "', 
          '" . $old['file'] . "', 
          '" . $old['registrationdate'] . "', 
          '" . $old['registrationexpiration'] . "', 
          '" . $old['retistrationagency'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

//TODO erp_default_files

$olds = $oldRR->query("
    SELECT *
    FROM events
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.events (
          id, 
          locationid, 
          name, 
          incidentfile, 
          incidentdisplayname, 
          ecgfile, 
          ecgdisplayname, 
          otherfile, 
          otherdisplayname, 
          display, 
          creation
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['incidentfile'] . "', 
          '" . $old['incidentdisplayname'] . "', 
          '" . $old['ecgfile'] . "', 
          '" . $old['ecgdisplayname'] . "', 
          '" . $old['otherfile'] . "', 
          '" . $old['otherdisplayname'] . "', 
          '" . $old['display'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice (
          id, 
          styleid, 
          locationid, 
          location, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['location'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoicecheck
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoicecheck (
          id, 
          equipmentid, 
          compliance1, 
          compliance2, 
          comments, 
          creation, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['equipmentid'] . "', 
          '" . $old['compliance1'] . "', 
          '" . $old['compliance2'] . "', 
          '" . $old['comments'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoicecheck_checker
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoicecheck_checker (
          id, 
          checkid, 
          name, 
          email
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['checkid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['email'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoicecheck_content
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoicecheck_content (
          id, 
          checkid, 
          answer, 
          uploadname, 
          type
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['checkid'] . "', 
          '" . $old['answer'] . "', 
          '" . $old['uploadname'] . "', 
          '" . $old['type'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoicecheck_contentlang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoicecheck_contentlang (
          id, 
          contentid, 
          content, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['contentid'] . "', 
          '" . $old['content'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoicecheck_questions
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoicecheck_questions (
          id, 
          checkid, 
          answer, 
          type
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['checkid'] . "', 
          '" . $old['answer'] . "', 
          '" . $old['type'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoicecheck_questionslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoicecheck_questionslang (
          id, 
          questionid, 
          question, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['questionid'] . "', 
          '" . $old['question'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_additionalcontent
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_additionalcontent (
          id, 
          styleid, 
          orgid, 
          type, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['type'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_additionalcontentanswers
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_additionalcontentanswers (
          id, 
          equipmentid, 
          additionalcontentid, 
          content, 
          uploadname, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['equipmentid'] . "', 
          '" . $old['additionalcontentid'] . "', 
          '" . $old['content'] . "', 
          '" . $old['uploadname'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_additionalcontentlang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_additionalcontentlang (
          id, 
          contentid, 
          title, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['contentid'] . "', 
          '" . $old['title'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_alerts
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_alerts (
          id, 
          styleid, 
          orgid, 
          checkfrequency, 
          levelone, 
          leveltwo, 
          levelthree, 
          creator, 
          creatorip, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['checkfrequency'] . "', 
          '" . $old['levelone'] . "', 
          '" . $old['leveltwo'] . "', 
          '" . $old['levelthree'] . "', 
          '" . $old['creator'] . "', 
          '" . $old['creatorip'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_checkquestions
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_checkquestions (
          id, 
          styleid, 
          orgid, 
          type, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['type'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_checkquestionslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_checkquestionslang (
          id, 
          questionid, 
          question, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['questionid'] . "', 
          '" . $old['question'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_styles
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_styles (
          id, 
          typeid, 
          orgid, 
          checkfrequency, 
          levelone, 
          leveltwo, 
          levelthree, 
          creator, 
          creatorip, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['checkfrequency'] . "', 
          '" . $old['levelone'] . "', 
          '" . $old['leveltwo'] . "', 
          '" . $old['levelthree'] . "', 
          '" . $old['creator'] . "', 
          '" . $old['creatorip'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_styleslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_styleslang (
          id, 
          styleid, 
          name, 
          `set`, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['styleid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['set'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_types (
          id, 
          orgid, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM firstvoice_typeslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.firstvoice_typeslang (
          id, 
          typeid, 
          name, 
          language
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['language'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM hazards
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.hazards (
          id, 
          typeid, 
          name, 
          location, 
          notes, 
          creation, 
          updated, 
          locationid, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['location'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM hazard_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.hazard_types (
          id, 
          orgid, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM hazard_typeslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.hazard_typeslang (
          id, 
          typeid, 
          name, 
          language, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['language'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM immunizations_personswithtypes
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.immunizations_personswithtypes (
          id, 
          personid, 
          typeid, 
          status, 
          date, 
          expiration, 
          employee, 
          supervisor, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['personid'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['status'] . "', 
          '" . $old['date'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['employee'] . "', 
          '" . $old['supervisor'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM immunizations_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.immunizations_types (
          id, 
          orgid, 
          nextimmunization, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['nextimmunization'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM immunizations_typesalerts
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.immunizations_typesalerts (
          id, 
          typeid, 
          days, 
          statusalert, 
          employee, 
          supervisor, 
          constantemail, 
          subject, 
          body, 
          attachmentonefile, 
          attachmenttwofile, 
          attachmentthreefile, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['days'] . "', 
          '" . $old['statusalert'] . "', 
          '" . $old['employee'] . "', 
          '" . $old['supervisor'] . "', 
          '" . $old['constantemail'] . "', 
          '" . $old['subject'] . "', 
          '" . $old['body'] . "', 
          '" . $old['attachmentonefile'] . "', 
          '" . $old['attachmenttwofile'] . "', 
          '" . $old['attachmentthreefile'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM immunizations_typeslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.immunizations_typeslang (
          id, 
          typeid, 
          name, 
          language, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['language'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM keycodes
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.keycodes (
          id, 
          keycode, 
          insertedby, 
          usedby, 
          orgid, 
          aedid, 
          type, 
          creator, 
          creatorip, 
          creation, 
          usedwhen, 
          expiration, 
          notes
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['keycode'] . "', 
          '" . $old['insertedby'] . "', 
          '" . $old['usedby'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['aedid'] . "', 
          '" . $old['type'] . "', 
          '" . $old['creator'] . "', 
          '" . $old['creatorip'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['usedwhen'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['notes'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM licenses_personswithtypes
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.licenses_personswithtypes (
          id, 
          personid, 
          typeid, 
          status, 
          agency, 
          number, 
          expiration, 
          renewaldate, 
          companyunits, 
          dateissued, 
          file, 
          notes, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['personid'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['status'] . "', 
          '" . $old['agency'] . "', 
          '" . $old['number'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['renewaldate'] . "', 
          '" . $old['companyunits'] . "', 
          '" . $old['dateissued'] . "', 
          '" . $old['file'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM licenses_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.licenses_types (
          id, 
          orgid, 
          ceu, 
          code, 
          safety, 
          discipline, 
          notes, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['ceu'] . "', 
          '" . $old['code'] . "', 
          '" . $old['safety'] . "', 
          '" . $old['discipline'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM licenses_typesalerts
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.licenses_typesalerts (
          id, 
          typeid, 
          days, 
          statusalert, 
          employee, 
          supervisor, 
          constantemail, 
          subject, 
          body, 
          attachmentonefile, 
          attachmenttwofile, 
          attachmentthreefile, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['days'] . "', 
          '" . $old['statusalert'] . "', 
          '" . $old['employee'] . "', 
          '" . $old['supervisor'] . "', 
          '" . $old['constantemail'] . "', 
          '" . $old['subject'] . "', 
          '" . $old['body'] . "', 
          '" . $old['attachmentonefile'] . "', 
          '" . $old['attachmenttwofile'] . "', 
          '" . $old['attachmentthreefile'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM licenses_typeslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.licenses_typeslang (
          id, 
          typeid, 
          name, 
          language, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['language'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM locations
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.locations (
          id, 
          orgid, 
          externalid, 
          internalid, 
          name, 
          status, 
          region, 
          address, 
          city, 
          province, 
          county, 
          country, 
          zip, 
          mailing_address, 
          mailing_city, 
          mailing_province, 
          mailing_country, 
          mailing_zip, 
          phone, 
          fax, 
          website, 
          safetydirector, 
          sitecontact, 
          main, 
          shippingcontact, 
          shippingcontacttitle, 
          shippingcontactphone, 
          shippingcontactemail, 
          shippingcontactfax, 
          billingcontact, 
          billingcontacttitle, 
          billingcontactphone, 
          billingcontactemail, 
          billingcontactfax, 
          shipping_contact_id, 
          billing_contact_id, 
          site_contact_id, 
          safety_director_contact_id, 
          plan_id, 
          latitude, 
          longitude, 
          monday_open, 
          monday_close, 
          sunday_close, 
          notes, 
          creation, 
          updated, 
          display, 
          sunday_open, 
          saturday_close, 
          saturday_open, 
          friday_close, 
          friday_open, 
          thursday_close, 
          thursday_open, 
          wednesday_close, 
          wednesday_open, 
          tuesday_close, 
          tuesday_open
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['externalid'] . "', 
          '" . $old['internalid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['status'] . "', 
          '" . $old['region'] . "', 
          '" . $old['address'] . "', 
          '" . $old['city'] . "', 
          '" . $old['province'] . "', 
          '" . $old['county'] . "', 
          '" . $old['country'] . "', 
          '" . $old['zip'] . "', 
          '" . $old['mailing_address'] . "', 
          '" . $old['mailing_city'] . "', 
          '" . $old['mailing_province'] . "', 
          '" . $old['mailing_country'] . "', 
          '" . $old['mailing_zip'] . "', 
          '" . $old['phone'] . "', 
          '" . $old['fax'] . "', 
          '" . $old['website'] . "', 
          '" . $old['safetydirector'] . "', 
          '" . $old['sitecontact'] . "', 
          '" . $old['main'] . "', 
          '" . $old['shippingcontact'] . "', 
          '" . $old['shippingcontacttitle'] . "', 
          '" . $old['shippingcontactphone'] . "', 
          '" . $old['shippingcontactemail'] . "', 
          '" . $old['shippingcontactfax'] . "', 
          '" . $old['billingcontact'] . "', 
          '" . $old['billingcontacttitle'] . "', 
          '" . $old['billingcontactphone'] . "', 
          '" . $old['billingcontactemail'] . "', 
          '" . $old['billingcontactfax'] . "', 
          '" . $old['shipping_contact_id'] . "', 
          '" . $old['billing_contact_id'] . "', 
          '" . $old['site_contact_id'] . "', 
          '" . $old['safety_director_contact_id'] . "', 
          '" . $old['plan_id'] . "', 
          '" . $old['latitude'] . "', 
          '" . $old['longitude'] . "', 
          '" . $old['monday_open'] . "', 
          '" . $old['monday_close'] . "', 
          '" . $old['sunday_close'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "', 
          '" . $old['sunday_open'] . "', 
          '" . $old['saturday_close'] . "', 
          '" . $old['saturday_open'] . "', 
          '" . $old['friday_close'] . "', 
          '" . $old['friday_open'] . "', 
          '" . $old['thursday_close'] . "', 
          '" . $old['thursday_open'] . "', 
          '" . $old['wednesday_close'] . "', 
          '" . $old['wednesday_open'] . "', 
          '" . $old['tuesday_close'] . "', 
          '" . $old['tuesday_open'] . "'
        )
    ");
}

//TODO location_contacts

//TODO location_hours

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM medicaldirection
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.medicaldirection (
          id, 
          name, 
          startdate, 
          enddate, 
          reviewdate, 
          nextreviewdate, 
          license, 
          company, 
          address, 
          city, 
          province, 
          country, 
          zip, 
          phone, 
          cell, 
          fax, 
          email, 
          status, 
          notes, 
          creation, 
          updated, 
          locationid, 
          current, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['name'] . "', 
          '" . $old['startdate'] . "', 
          '" . $old['enddate'] . "', 
          '" . $old['reviewdate'] . "', 
          '" . $old['nextreviewdate'] . "', 
          '" . $old['license'] . "', 
          '" . $old['company'] . "', 
          '" . $old['address'] . "', 
          '" . $old['city'] . "', 
          '" . $old['province'] . "', 
          '" . $old['country'] . "', 
          '" . $old['zip'] . "', 
          '" . $old['phone'] . "', 
          '" . $old['cell'] . "', 
          '" . $old['fax'] . "', 
          '" . $old['email'] . "', 
          '" . $old['status'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['current'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_accessories
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_accessories (
          aed_accessory_id, 
          location_id, 
          aed_id, 
          aed_accessory_type_id, 
          expiration, 
          lot, 
          spare, 
          display
        ) VALUES (
          '" . $old['aed_accessory_id'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['aed_accessory_type_id'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['spare'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_accessory_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_accessory_types (
          aed_accessory_type_id, 
          type, 
          partnum, 
          length, 
          buylink
        ) VALUES (
          '" . $old['aed_accessory_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['partnum'] . "', 
          '" . $old['length'] . "', 
          '" . $old['buylink'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_accessory_types_on_models
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_accessory_types_on_models (
          aed_accessory_types_on_model_id, 
          aed_accessory_type_id, 
          aed_model_id, 
          creation
        ) VALUES (
          '" . $old['aed_accessory_types_on_model_id'] . "', 
          '" . $old['aed_accessory_type_id'] . "', 
          '" . $old['aed_model_id'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_batteries
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_batteries (
          aed_battery_id, 
          location_id, 
          aed_id, 
          aed_battery_type_id, 
          expiration, 
          lot, 
          spare, 
          display
        ) VALUES (
          '" . $old['aed_battery_id'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['aed_battery_type_id'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['spare'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_battery_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_battery_types (
          aed_battery_type_id, 
          type, 
          partnum, 
          length
        ) VALUES (
          '" . $old['aed_battery_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['partnum'] . "', 
          '" . $old['length'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_battery_types_on_models
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_battery_types_on_models (
          aed_battery_types_on_model_id, 
          aed_battery_type_id, 
          aed_model_id, 
          creation
        ) VALUES (
          '" . $old['aed_batery_types_on_model_id'] . "', 
          '" . $old['aed_battery_type_id'] . "', 
          '" . $old['aed_model_id'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_pads
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_pads (
          aed_pad_id, 
          location_id, 
          aed_id, 
          aed_pad_type_id, 
          expiration, 
          lot, 
          spare, 
          display
        ) VALUES (
          '" . $old['aed_pad_id'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['aed_pad_type_id'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['spare'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_pad_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_pad_types (
          aed_pad_type_id, 
          type, 
          pediatric, 
          partnum, 
          length
        ) VALUES (
          '" . $old['aed_pad_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['pediatric'] . "', 
          '" . $old['partnum'] . "', 
          '" . $old['length'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_pad_types_on_models
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_pad_types_on_models (
          aed_pad_types_on_model_id, 
          aed_pad_type_id, 
          aed_model_id, 
          creation
        ) VALUES (
          '" . $old['aed_pad_types_on_model_id'] . "', 
          '" . $old['aed_pad_type_id'] . "', 
          '" . $old['aed_model_id'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_paks
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_paks (
          aed_pak_id, 
          location_id, 
          aed_id, 
          aed_pak_type_id, 
          expiration, 
          lot, 
          spare, 
          display
        ) VALUES (
          '" . $old['aed_pak_id'] . "', 
          '" . $old['location_id'] . "', 
          '" . $old['aed_id'] . "', 
          '" . $old['aed_pak_type_id'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lot'] . "', 
          '" . $old['spare'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_pak_types
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_pak_types (
          aed_pak_type_id, 
          type, 
          pediatric, 
          partnum, 
          length, 
          buylink
        ) VALUES (
          '" . $old['aed_pak_type_id'] . "', 
          '" . $old['type'] . "', 
          '" . $old['pediatric'] . "', 
          '" . $old['partnum'] . "', 
          '" . $old['length'] . "', 
          '" . $old['buylink'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM old_aed_pak_types_on_models
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.old_aed_pak_types_on_models (
          aed_pak_types_on_model_id, 
          aed_pak_type_id, 
          aed_model_id, 
          creation
        ) VALUES (
          '" . $old['aed_pak_types_on_model_id'] . "', 
          '" . $old['aed_pak_type_id'] . "', 
          '" . $old['aed_model_id'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM organizations
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.organizations (
          id, 
          name, 
          type, 
          status, 
          clientnumber, 
          streetaddress, 
          city, 
          state, 
          zip, 
          country, 
          mailingaddress, 
          mailingcity, 
          mailingstate, 
          mailingzip, 
          mailingcountry, 
          mainphone, 
          mainbillcontact, 
          mainbilltitle, 
          mainbillphone, 
          mainbillemail, 
          mainbillfax, 
          mainmailcontact, 
          mainmailtitle, 
          mainmailphone, 
          mainmailemail, 
          mainmailfax, 
          main_billing_contact_id, 
          main_mailing_contact_id, 
          master_region_code, 
          latitude, 
          longitude, 
          notes, 
          global_email, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['name'] . "', 
          '" . $old['type'] . "', 
          '" . $old['status'] . "', 
          '" . $old['clientnumber'] . "', 
          '" . $old['streetaddress'] . "', 
          '" . $old['city'] . "', 
          '" . $old['state'] . "', 
          '" . $old['zip'] . "', 
          '" . $old['country'] . "', 
          '" . $old['mailingaddress'] . "', 
          '" . $old['mailingcity'] . "', 
          '" . $old['mailingstate'] . "', 
          '" . $old['mailingzip'] . "', 
          '" . $old['mailingcountry'] . "', 
          '" . $old['mainphone'] . "', 
          '" . $old['mainbillcontact'] . "', 
          '" . $old['mainbilltitle'] . "', 
          '" . $old['mainbillphone'] . "', 
          '" . $old['mainbillemail'] . "', 
          '" . $old['mainbillfax'] . "', 
          '" . $old['mainmailcontact'] . "', 
          '" . $old['mainmailtitle'] . "', 
          '" . $old['mainmailphone'] . "', 
          '" . $old['mainmailemail'] . "', 
          '" . $old['mainmailfax'] . "', 
          '" . $old['main_billing_contact_id'] . "', 
          '" . $old['main_mailing_contact_id'] . "', 
          '" . $old['master_region_code'] . "', 
          '" . $old['latitude'] . "', 
          '" . $old['longitude'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['global_email'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM persons
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.persons (
          id, 
          employeeid, 
          firstname, 
          lastname, 
          address, 
          email, 
          phone, 
          city, 
          state, 
          zip, 
          firstvoicetraining_username, 
          notes, 
          `current`, 
          creation, 
          updated, 
          locationid, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['employeeid'] . "', 
          '" . $old['firstname'] . "', 
          '" . $old['lastname'] . "', 
          '" . $old['address'] . "', 
          '" . $old['email'] . "', 
          '" . $old['phone'] . "', 
          '" . $old['city'] . "', 
          '" . $old['state'] . "', 
          '" . $old['zip'] . "', 
          '" . $old['firstvoicetraining_username'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['current'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

//TODO plans

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM privileges
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.privileges (
          id, userid, showaed, newaed, editaed, editaedmodel, deleteaed, showerp, newerp, deleteerp, showmedicaldirection, newmedicaldirection, editmedicaldirection, deletemedicaldirection, showplan, newplan, editplan, deleteplan, showevents, newevents, deleteevents, showrecalls, newrecalls, editrecalls, deleterecalls, showstatelaws, showmap, showequipment, newequipment, editequipment, deleteequipment, newequipmentcheck, editequipmentcheck, showpersons, newpersons, editpersons, deletepersons, movepersons, moveaeds, showtraining, newtraining, edittraining, deletetraining, printcards, showtrainingprints, edittrainingprints, showdocuments, newdocuments, editdocuments, deletedocuments, showlicenses, newlicenses, editlicenses, deletelicenses, showimmunizations, newimmunizations, editimmunizations, deleteimmunizations, showhazard, newhazard, edithazard, deletehazard, showcontact, newcontact, editcontact, deletecontact, showaedcheck, newaedcheck, editaedcheck, aedcheckunlockconsumables, groupaedcheck, showaedservicing, newaedservicing, editaedservicing, showfirstvoice, newfirstvoice, editfirstvoice, deletefirstvoice, newfirstvoicecheck, editfirstvoicecheck, showadmin, showuser, newuser, edituser, deleteuser, showtracing, newtracing, edittracing, deletetracing, assigntracing, showsetup, showsetuphazards, newhazardtype, edithazardtype, deletehazardtype, newhazardalltype, edithazardalltype, deletehazardalltype, showsetupdocuments, newdocumenttype, editdocumenttype, deletedocumenttype, newdocumentalltype, editdocumentalltype, deletedocumentalltype, showsetupimmunizations, newimmunizationtype, editimmunizationtype, deleteimmunizationtype, newimmunizationalltype, editimmunizationalltype, deleteimmunizationalltype, showsetuplicenses, newlicensetype, editlicensetype, deletelicensetype, newlicensealltype, editlicensealltype, deletelicensealltype, showsetuptraining, newtrainingcourse, edittrainingcourse, deletetrainingcourse, newtrainingallcourse, edittrainingallcourse, deletetrainingallcourse, showsetupequipment, newequipmenttypes, editequipmenttypes, deleteequipmenttypes, newequipmentstyles, editequipmentstyles, deleteequipmentstyles, newequipmentalltypes, editequipmentalltypes, deleteequipmentalltypes, newequipmentallstyles, editequipmentallstyles, deleteequipmentallstyles, showsetupfirstvoice, newfirstvoicetypes, editfirstvoicetypes, deletefirstvoicetypes, newfirstvoicestyles, editfirstvoicestyles, deletefirstvoicestyles, newfirstvoicealltypes, editfirstvoicealltypes, deletefirstvoicealltypes, newfirstvoiceallstyles, editfirstvoiceallstyles, deletefirstvoiceallstyles, showorganization, neworganization, editorganization, deleteorganization, newlocation, editlocation, deletelocation, showkeycode, generatekeycode, showkeycodepool, assignkeycodepool, deletekeycodepool, newkeycode, showalert, newalert, editalert, deletealert, showscheduler, newscheduler, editscheduler, deletescheduler, showmasteraed, create_users, showcaresregistry) VALUES ()
    ");
}

//TODO psap

//TODO quotes

//TODO quote_items

$olds = $oldRR->query("
    SELECT *
    FROM recalls
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.recalls (
          id, 
          recalldate, 
          servicedate, 
          type, 
          modelid, 
          typeid, 
          `range`, 
          notes, 
          creator, 
          creatorip, 
          creation, 
          updated, 
          locationid, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['recalldate'] . "', 
          '" . $old['servicedate'] . "', 
          '" . $old['type'] . "', 
          '" . $old['modelid'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['range'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['creator'] . "', 
          '" . $old['creatorip'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM recalls_documents
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.recalls_documents (
          id, 
          locationid, 
          type, 
          name, 
          file, 
          display, 
          creation
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['type'] . "', 
          '" . $old['name'] . "', 
          '" . $old['file'] . "', 
          '" . $old['display'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM recalls_history
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.recalls_history (
          id, 
          brandid, 
          units, 
          recalldate, 
          title, 
          issue, 
          fix, 
          link, 
          document, 
          email
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['brandid'] . "', 
          '" . $old['units'] . "', 
          '" . $old['recalldate'] . "', 
          '" . $old['title'] . "', 
          '" . $old['issue'] . "', 
          '" . $old['fix'] . "', 
          '" . $old['link'] . "', 
          '" . $old['document'] . "', 
          '" . $old['email'] . "'
        )
    ");
}

//TODO recalls_service

$olds = $oldRR->query("
    SELECT *
    FROM region_codes
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.region_codes (
          region_code_id, 
          region_code, 
          organization, 
          name, 
          phone_aed, 
          phone_training, 
          phone_support, 
          email_aed, 
          email_training, 
          email_support, 
          website, 
          ecommerce, 
          display
        ) VALUES (
          '" . $old['region_code_id'] . "', 
          '" . $old['region_code'] . "', 
          '" . $old['organization'] . "', 
          '" . $old['name'] . "', 
          '" . $old['phone_aed'] . "', 
          '" . $old['phone_training'] . "', 
          '" . $old['phone_support'] . "', 
          '" . $old['email_aed'] . "', 
          '" . $old['email_training'] . "', 
          '" . $old['email_support'] . "', 
          '" . $old['website'] . "', 
          '" . $old['ecommerce'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

//TODO reports_schedule

$olds = $oldRR->query("
    SELECT *
    FROM rights_locations
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.rights_locations (
          id, 
          userid, 
          orgid, 
          locationid, 
          display, 
          creation, 
          updated
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['userid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['locationid'] . "', 
          '" . $old['display'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "'
        )
    ");
}

//TODO rights_locations_backup

$olds = $oldRR->query("
    SELECT *
    FROM rights_orgs
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.rights_orgs (
          id, 
          userid, 
          orgid, 
          level, 
          display, 
          creation, 
          updated
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['userid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['level'] . "', 
          '" . $old['display'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "'
        )
    ");
}

//TODO roles

$olds = $oldRR->query("
    SELECT *
    FROM scheduler
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.scheduler (
          id, 
          userid, 
          company, 
          contact, 
          phone, 
          address, 
          city, 
          province, 
          country, 
          uid, 
          subject, 
          location, 
          date, 
          timezone, 
          hour, 
          minute, 
          description, 
          creation, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['userid'] . "', 
          '" . $old['company'] . "', 
          '" . $old['contact'] . "', 
          '" . $old['phone'] . "', 
          '" . $old['address'] . "', 
          '" . $old['city'] . "', 
          '" . $old['province'] . "', 
          '" . $old['country'] . "', 
          '" . $old['uid'] . "', 
          '" . $old['subject'] . "', 
          '" . $old['location'] . "', 
          '" . $old['date'] . "', 
          '" . $old['timezone'] . "', 
          '" . $old['hour'] . "', 
          '" . $old['minute'] . "', 
          '" . $old['description'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM scheduler_sent
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.scheduler_sent (
          id, 
          eventid, 
          userid, 
          creation
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['eventid'] . "', 
          '" . $old['userid'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM statelaws
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.statelaws (
          id, 
          state, 
          medicaldirection, 
          training, 
          registration, 
          file, 
          aedmandates, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['state'] . "', 
          '" . $old['medicaldirection'] . "', 
          '" . $old['training'] . "', 
          '" . $old['registration'] . "', 
          '" . $old['file'] . "', 
          '" . $old['aedmandates'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM temp_union
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.temp_union (
          id, 
          firstname, 
          lastname, 
          middlename, 
          memberid, 
          licenseyear, 
          employeer, 
          address, 
          email, 
          phone, 
          city, 
          state, 
          zip, 
          creation
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['firstname'] . "', 
          '" . $old['lastname'] . "', 
          '" . $old['middlename'] . "', 
          '" . $old['memberid'] . "', 
          '" . $old['licenseyear'] . "', 
          '" . $old['employeer'] . "', 
          '" . $old['address'] . "', 
          '" . $old['email'] . "', 
          '" . $old['phone'] . "', 
          '" . $old['city'] . "', 
          '" . $old['state'] . "', 
          '" . $old['zip'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM tokens
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.tokens (
          token
        ) VALUES (
          '" . $old['token'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM tracing
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.tracing (
          id, 
          aedid, 
          model, 
          serialnumber, 
          invoicedate, 
          invoicenumber, 
          datereceived, 
          shipto, 
          display, 
          creation, 
          updated
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['aedid'] . "', 
          '" . $old['model'] . "', 
          '" . $old['serialnumber'] . "', 
          '" . $old['invoicedate'] . "', 
          '" . $old['invoicenumber'] . "', 
          '" . $old['datereceived'] . "', 
          '" . $old['shipto'] . "', 
          '" . $old['display'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM tracing_battery
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.tracing_battery (
          id, 
          batteryid, 
          tracingid, 
          typeid, 
          expiration, 
          lotnumber, 
          display, 
          creation, 
          updated
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['batteryid'] . "', 
          '" . $old['tracingid'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lotnumber'] . "', 
          '" . $old['display'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM tracing_pad
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.tracing_pad (
          id, 
          padid, 
          tracingid, 
          typeid, 
          expiration, 
          lotnumber, 
          display, 
          creation, 
          updated
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['padid'] . "', 
          '" . $old['tracingid'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lotnumber'] . "', 
          '" . $old['display'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM tracing_pak
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.tracing_pak (
          id, 
          pakid, 
          tracingid, 
          typeid, 
          expiration, 
          lotnumber, 
          display, 
          creation, 
          updated
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['pakid'] . "', 
          '" . $old['tracingid'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['lotnumber'] . "', 
          '" . $old['display'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM training_classprints
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.training_classprints (
          id, 
          typeid, 
          orgid, 
          prints
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['prints'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM training_classtypes
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.training_classtypes (
          id, 
          orgid, 
          ceu, 
          code, 
          safety, 
          discipline, 
          printable, 
          notes, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['orgid'] . "', 
          '" . $old['ceu'] . "', 
          '" . $old['code'] . "', 
          '" . $old['safety'] . "', 
          '" . $old['discipline'] . "', 
          '" . $old['printable'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM training_classtypeslang
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.training_classtypeslang (
          id, 
          typeid, 
          name, 
          cardname, 
          language, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['name'] . "', 
          '" . $old['cardname'] . "', 
          '" . $old['language'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM training_personswithtypes
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.training_personswithtypes (
          id, 
          personid, 
          typeid, 
          classnumber, 
          status, 
          prefix, 
          skillscheck, 
          class_type, 
          part_number, 
          trainername, 
          trainernumber, 
          completiondate, 
          expiration, 
          keycode, 
          certid, 
          creation, 
          updated, 
          display
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['personid'] . "', 
          '" . $old['typeid'] . "', 
          '" . $old['classnumber'] . "', 
          '" . $old['status'] . "', 
          '" . $old['prefix'] . "', 
          '" . $old['skillscheck'] . "', 
          '" . $old['class_type'] . "', 
          '" . $old['part_number'] . "', 
          '" . $old['trainername'] . "', 
          '" . $old['trainernumber'] . "', 
          '" . $old['completiondate'] . "', 
          '" . $old['expiration'] . "', 
          '" . $old['keycode'] . "', 
          '" . $old['certid'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM training_printlog
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.training_printlog (
          id, 
          swtid, 
          userid, 
          date
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['swtid'] . "', 
          '" . $old['userid'] . "', 
          '" . $old['date'] . "'
        )
    ");
}

/**EXTRAS**/
$olds = $oldRR->query("
    SELECT *
    FROM users
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.users (
          id, 
          username, 
          password, 
          type, 
          mobile_type, 
          ems_type, 
          firstname, 
          lastname, 
          phone, 
          company, 
          preferredlanguage, 
          address, 
          city, 
          province, 
          country, 
          zip, 
          firstlogin, 
          filterorg, 
          filterlocation, 
          creator, 
          creatorip, 
          sales_person_id, 
          creation, 
          updated, 
          display, 
          private_program_head, 
          pad_program, 
          email_alerts, 
          text_alerts, 
          receive_notifications, 
          notification_distance, 
          map_type, 
          program_details, 
          needs_assistance
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['username'] . "', 
          '" . $old['password'] . "', 
          '" . $old['type'] . "', 
          '" . $old['mobile_type'] . "', 
          '" . $old['ems_type'] . "', 
          '" . $old['firstname'] . "', 
          '" . $old['lastname'] . "', 
          '" . $old['phone'] . "', 
          '" . $old['company'] . "', 
          '" . $old['preferredlanguage'] . "', 
          '" . $old['address'] . "', 
          '" . $old['city'] . "', 
          '" . $old['province'] . "', 
          '" . $old['country'] . "', 
          '" . $old['zip'] . "', 
          '" . $old['firstlogin'] . "', 
          '" . $old['filterorg'] . "', 
          '" . $old['filterlocation'] . "', 
          '" . $old['creator'] . "', 
          '" . $old['creatorip'] . "', 
          '" . $old['sales_person_id'] . "', 
          '" . $old['creation'] . "', 
          '" . $old['updated'] . "', 
          '" . $old['display'] . "', 
          '" . $old['private_program_head'] . "', 
          '" . $old['pad_program'] . "', 
          '" . $old['email_alerts'] . "', 
          '" . $old['text_alerts'] . "', 
          '" . $old['receive_notifications'] . "', 
          '" . $old['notification_distance'] . "', 
          '" . $old['map_type'] . "', 
          '" . $old['program_details'] . "', 
          '" . $old['needs_assistance'] . "'
        )
    ");
}

$olds = $oldRR->query("
    SELECT *
    FROM users_activity
");
while ($old = $olds->fetch_assoc()) {
    $new = $newRR->query("
        INSERT INTO tsdemosc_rescue_ready.users_activity (
          id, 
          userid, 
          action, 
          notes, 
          creation
        ) VALUES (
          '" . $old['id'] . "', 
          '" . $old['userid'] . "', 
          '" . $old['action'] . "', 
          '" . $old['notes'] . "', 
          '" . $old['creation'] . "'
        )
    ");
}

//TODO user_roles

//TODO zip_codes