<?php

@ $crmdb = new mysqli('tsdemos.com', 'tsdemosc_scscrap', '1105firstvoice', 'tsdemosc_suitecrmnew');

$key = array('AL', 'new', 'Alexander City AL', '163 Alabama Street Alexander City AL 35010', '(555) 867-5309', 'www.google.com');

$accountHash = md5(date('Y-m-d H:i:s') . time());
$accountCity = str_replace(' ' . $key[0], '', $key[2]);
$accountAddressInfo = explode(' ', $key[3]);
$insertCrmAccount = $crmdb->query("
            INSERT INTO accounts
            (id, name, date_entered, date_modified, modified_user_id, created_by, description, assigned_user_id, account_type, industry, annual_revenue, phone_fax, billing_address_street, billing_address_city, billing_address_state, billing_address_postalcode, billing_address_country, rating, phone_office, phone_alternate, website, ownership, employees, ticker_symbol, shipping_address_street, shipping_address_city, shipping_address_state, shipping_address_postalcode, shipping_address_country, parent_id, sic_code, campaign_id)
            VALUES (
              '" . $accountHash . "', 
              'AnytimeFitness - " . $key[3] . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '1f8d0df1-cbe4-d290-f575-574f492e636f', 
              '1f8d0df1-cbe4-d290-f575-574f492e636f', 
              '',  
              '00532000004zEQQAA2', 
              '', 
              'Fitness', 
              '', 
              '', 
              '" . $key[3] . "', 
              '" . $accountCity . "', 
              '" . $key[0] . "', 
              '" . $accountAddressInfo[count($accountAddressInfo) - 1] . "', 
              '', 
              '', 
              '" . $key[4] . "', 
              '', 
              '" . $key[5] . "', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '8c412cc3-68aa-09c8-7c4b-574f78710e54'
            )
        ");

$insertCrmAccountCstm = $crmdb->query("
            INSERT INTO accounts_cstm
            (id_c, type_c, club_status_c)
            VALUES
            ('" . $accountHash . "', 'Prospect', '" . $key[1] . "')
        ");

$dueDate = new DateTime("now");
$dueDate->add(new DateInterval('P1D'));
if ($dueDate->format('l') == 'Saturday') {
    $dueDate->add(new DateInterval('P2D'));
} else if ($dueDate->format('l') == 'Sunday') {
    $dueDate->add(new DateInterval('P1D'));
}

//TODO: create a task for tobias on next business day
$taskHash = md5(date('Y-m-d H:i:s') . time());
$insertTask = $crmdb->query("
            INSERT INTO tasks
            (id, name, date_entered, date_modified, modified_user_id, created_by, description, deleted, assigned_user_id, status, date_due_flag, date_due, date_start_flag, date_start, parent_type, parent_id, contact_id, priority)
            VALUES (
              '" . $taskHash . "', 
              'New AnytimeFitness Club', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '1f8d0df1-cbe4-d290-f575-574f492e636f', 
              '1f8d0df1-cbe4-d290-f575-574f492e636f', 
              '', 
              '0', 
              '00532000004zEQQAA2', 
              '', 
              '', 
              '" . $dueDate->format('Y-m-d H:i:s') . "', 
              '', 
              '" . $dueDate->format('Y-m-d H:i:s') . "', 
              'Accounts', 
              '" . $accountHash . "', 
              '', 
              'High'
            )
        ");

?>