<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 1/11/2017
 * Time: 11:10
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $crmdb = new mysqli($host, 'tsdemosc', 'Hawkeye17!', 'tsdemosc_tcm_demo');

$accounts = $crmdb->query("
    SELECT *
    FROM accounts
");
while ($account = $accounts->fetch_assoc()) {
    $phoneDigits = preg_replace("/[^0-9]/", "", $account['phone']);
    $updateAccounts = $crmdb->query("
        UPDATE accounts
        SET phone = '" . $phoneDigits . "'
        WHERE account_id = '" . $account['account_id'] . "'
    ");
}

$contacts = $crmdb->query("
    SELECT *
    FROM contacts
");
while ($contact = $contacts->fetch_assoc()) {
    $phoneDigits = preg_replace("/[^0-9]/", "", $contact['phone']);
    $faxDigits = preg_replace("/[^0-9]/", "", $contact['fax']);
    $mobileDigits = preg_replace("/[^0-9]/", "", $contact['mobile']);
    $updateContacts = $crmdb->query("
        UPDATE contacts
        SET phone = '" . $phoneDigits . "', 
            fax = '" . $faxDigits . "', 
            mobile = '" . $mobileDigits . "'
        WHERE contact_id = '" . $contact['contact_id'] . "'
    ");
}