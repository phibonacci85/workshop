<?php
/**
 * Created by PhpStorm.
 * User: jbaker
 * Date: 8/3/2017
 * Time: 11:30 AM
 */

@ $rodb = new mysqli('fvdemos.com', 'fvdemos', 'Hawkeye17!', 'fvdemos_rescueone');

$duplicates = $rodb->query("
    SELECT location_id, contact_id, location_contact_id, count(*)
    FROM location_contacts
    GROUP BY location_id, contact_id
    HAVING count(*) > 1
");
while ($duplicate = $duplicates->fetch_assoc()) {
    $removeDuplicates = $rodb->query("
        DELETE FROM location_contacts
        WHERE location_contact_id = '" . $duplicate['location_contact_id'] . "'
    ");
}