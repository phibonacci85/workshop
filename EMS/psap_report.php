<?php
/**
 * Created by PhpStorm.
 * User: JBaker
 * Date: 7/20/2017
 * Time: 3:42 PM
 */
@ $emsdb = new mysqli('tsdemos.com', 'tsdemosc', 'Hawkeye17!', 'tsdemosc_ems');
@ $fvmdb = new mysqli('tsdemos.com', 'tsdemosc', 'Hawkeye17!', 'tsdemosc_fvmnew');
@ $rodb = new mysqli('fvdemos.com', 'fvdemos', 'Hawkeye17!', 'fvdemos_rescueone');

$fp = fopen('psap_report.csv', 'w');
fputcsv($fp, array('PSAP', 'Organization', 'Location', 'AED Serial Number'));

$aeds = $fvmdb->query("
    SELECT a.*, l.name AS location, o.name AS organization, l.zip
    FROM aeds a
    JOIN locations l ON a.location_id = l.id
    JOIN organizations o ON l.orgid = o.id
");
while ($aed = $aeds->fetch_assoc()) {
    $zips = $emsdb->query("
        SELECT *
        FROM zip_codes
        WHERE zip_code = '" . $aed['zip'] . "'
    ");
    if ($zip = $zips->fetch_assoc()) {
        $psapIds = "";
        $psaps = $emsdb->query("
            SELECT *
            FROM psap
            WHERE county LIKE '%" . $zip['county'] . "%'
            AND state = '" . $zip['state_abr'] . "'
        ");
        while ($psap = $psaps->fetch_assoc()) {
            if ($psaps->num_rows > 1) {
                $psapIds .= $psap['psap_id'] . ', ';
            } else {
                $psapIds .= $psap['psap_id'];
            }
        }
        fputcsv($fp, array($psapIds, $aed['organization'], $aed['location'], $aed['serialnumber']));
    }
}

fclose($fp);