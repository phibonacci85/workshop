<?php
	include('lang/fvm_english.php');
	$messageformat = new English();

	function isempty($var)
	{
		if($var == "")
			return true;
		else
			return false;
	}

	define("RODBHOST", "fvdemos.com");
	define("RODBUSER", "fvdemos");
	define("RODBPASS", "Hawkeye17!");
	define("RODB", "fvdemos_rescueone");
	@ $rodb = new mysqli( RODBHOST, RODBUSER, RODBPASS, RODB);

	$emails = $rodb->query("
		select *
		from emails
		where str_to_date(sent, '%Y-%m-%d %h:%i:%s') >= str_to_date('2016-02-26 10:44:11', '%Y-%m-%d %h:%i:%s')
		and str_to_date(sent, '%Y-%m-%d %h:%i:%s') <= str_to_date('2016-03-01 10:44:11', '%Y-%m-%d %h:%i:%s')
		and (`to` = 'lfreeland@rescue-one.com' or `to` = 'rcrawfor@rescue-one.com')
	");

	//get aedservicing info
				$aedservicing = $rodb->query("
					select s.*, a.*, a.location as aedlocation
					from aedservicing s
					join aeds a on s.aed_id = a.aed_id
					where aedservicing_id = '2286'
				");
				$aedlocations = $rodb->query("
					select l.name as lname, o.name as oname
					from locations l, organizations o, aeds a
					where a.location_id = l.id
					and l.orgid = o.id
					and a.aed_id = '1033'
				");
				$aedlocation = $aedlocations->fetch_assoc();
				//aedservicing 1st service
				$body = '
				<div class="subtab4" style="margin:0 2px;width:100%;display:none;clear: both;clear:both;">';
					if($aedservicinginfo = $aedservicing->fetch_assoc()) // servicing 1
					{	
						echo $messageformat->ask_organization().'<br />';
						$check_id = $aedservicinginfo['aedservicing_id'];
						$date = date('m/d/Y', strtotime($aedservicinginfo['date']));
						$body .=  '
						<table style="width:800px;table-layout:fixed;" border="1" cellspacing="0" cellpadding="10">
							<tr>
								<td>'.$messageformat->ask_organization().'</td>
								<td style="text-align:center;word-wrap:break-word;width:150px;">'.$aedlocation['oname'].'</td>
							</tr>
							<tr>
								<td>'.$messageformat->ask_location().'</td>
								<td style="text-align:center;word-wrap:break-word;width:150px;">'.$aedlocation['lname'].'</td>
							</tr>
							<tr>
								<td>'.$messageformat->ask_serial().'</td>
								<td style="text-align:center;word-wrap:break-word;width:150px;">'.$aedservicinginfo['serialnumber'].'</td>
							</tr>
							
							<tr>
								<td>'.$messageformat->ask_date().'</td>
								<td style="text-align:center;width:150px;">'.$date.'</td>
							</tr>
							
							<tr>
								<td>'.$messageformat->ask_placement().'</td>
								<td style="text-align:center;width:150px;">'.$aedservicinginfo['aedlocation'].'</td>
							</tr>
						</table>
						
						<h2 style="padding-top:30px;width:800px;">'.$messageformat->ask_generalinfo().'</h2>			
						<table style="width:800px;table-layout:fixed;" border="1" cellspacing="0" cellpadding="10">
							<tr>
								<td>'.$messageformat->ask_milesdriven().'</td>
								<td style="text-align:center;width:150px;">'.$aedservicinginfo['milesdriven'].'</td>
							</tr>
							
							<tr>
								<td>'.$messageformat->ask_programmanagementexp().'</td>
								<td style="text-align:center;width:150px;">'.$aedservicinginfo['programmanagementexp'].'</td>
							</tr>
							
							<tr>
								<td>'.$messageformat->ask_isaedlocation().'</td>
								<td style="text-align:center;width:150px;">
									'.$messageformat->ask_responses($aedservicinginfo['location']).'
								</td>
							</tr>
							
							<tr>
								<td>'.$messageformat->ask_isaedclean().'</td>
								<td style="text-align:center;">
									'.$messageformat->ask_responses($aedservicinginfo['condition']).'
								</td>
							</tr>
							
							<tr>
								<td>'.$messageformat->ask_isaedinalarm().'</td>
								<td style="text-align:center;">
									'.$messageformat->ask_responses($aedservicinginfo['cabinet']).'
								</td>
							</tr>';
								if($aedservicinginfo['cabinet'] == 'yes') {
									$body .=  '
									<tr dependant="cabinet">
										<td>'.$messageformat->ask_isalarmworking().'</td>
										<td style="text-align:center;">
											'.$messageformat->ask_responses($aedservicinginfo['alarm']).'
										</td>
									</tr>
									
									<tr dependant="cabinet">
										<td>'.$messageformat->ask_replacingalarmbattery().'</td>
										<td style="text-align:center;">
											'.$messageformat->ask_responses($aedservicinginfo['alarmbattery']).'
										</td>
									</tr>';
									
										if($aedservicinginfo['alarmbattery'] == 'yes') {
											$body .=  '
											<tr dependant="alarmbattery">
												<td style="padding-left:60px;">'.$messageformat->ask_alarmbatteryinsertiondate().':</td>
												<td style="text-align:center;">
													'.$date.'
												</td>
											</tr>
											
											<tr dependant="alarmbattery">
												<td style="padding-left:60px;">'.$messageformat->ask_alarmbatterychangedate().':</td>
												<td style="text-align:center;">
													'.date('m/d/Y', strtotime($date .'+1 year')).'
												</td>
											</tr>
											
											<tr dependant="alarmbattery">
												<td style="padding-left:60px;">'.$messageformat->ask_alarmbatteryexpirationdate().':</td>
												<td style="text-align:center;">
													'.$aedservicinginfo['alarmbatteryexpiration'].'
												</td>
											</tr>';
										}
								}
								
							$body .=  '
							<tr>
								<td>'.$messageformat->ask_kitpresent().'</td>
								<td style="text-align:center;">
									'.$messageformat->ask_responses($aedservicinginfo['rescuekit']).'
								</td>
							</tr>';
								
								if($aedservicinginfo['rescuekit'] == 'yes')					 {
									$body .=  '
									<tr dependant="rescuekit">
										<td>'.$messageformat->ask_kitsealed().'</td>
										<td style="text-align:center;">
											'.$messageformat->ask_responses($aedservicinginfo['rescuekitcond']).'
										</td>
									</tr>';
									
									if($aedservicinginfo['rescuekitcond'] == 'no') {	
										$body .=  '
										<tr ndependant="rescuekitcond">
											<td style="padding-left:60px;">'.$messageformat->ask_kitinclude().'</td>
											<td style="text-align:center;">
												'.$messageformat->ask_responses($aedservicinginfo['kitinclude']).'
											</td>
										</tr>';
									}
									if($aedservicinginfo['kitinclude'] == 'no') {	
										$body .=  '
										<tr ndependant="kitinclude">
											<td style="padding-left:60px;">'.$messageformat->ask_kitincludereplace().'</td>
											<td style="text-align:center;">
												'.$aedservicinginfo['kitincludereplace'].'
											</td>
										</tr>';
									}
								}
						$body .=  '
						</table>';
						
						//pad
						$pad_list = $rodb->query("
							select t.type as c_type, pt.type as type, t.expiration, t.lot, t.attached, t.`new`, npt.type as new_type, t.new_expiration, t.new_lot
							from aedservicing_pads t 
							left join aed_pad_types pt on pt.aed_pad_type_id = t.aed_pad_type_id
							left join aed_pad_types npt on npt.aed_pad_type_id = t.new_aed_pad_type_id
							where t.aedservicing_id = '$check_id'
						");  		
						while($raw = $pad_list->fetch_assoc()) {
							$pads[$raw['c_type']] = array('type'=>$raw['type'], 'expiration'=>$raw['expiration'], 'lot'=>$raw['lot'], 'attached'=>$raw['attached'], 'new'=>$raw['new'], 'new_type'=>$raw['new_type'], 'new_expiration'=>$raw['new_expiration'], 'new_lot'=>$raw['new_lot'],);
						}

						//pak
						$pak_list = $rodb->query("
							select t.type as c_type, pt.type as type, t.expiration, t.lot, t.attached, t.`new`, npt.type as new_type, t.new_expiration, t.new_lot
							from aedservicing_paks t 
							left join aed_pak_types pt on pt.aed_pak_type_id = t.aed_pak_type_id
							left join aed_pak_types npt on npt.aed_pak_type_id = t.new_aed_pak_type_id
							where t.aedservicing_id = '$check_id'
						");  		
						while($raw = $pak_list->fetch_assoc()) {
							$paks[$raw['c_type']] = array('type'=>$raw['type'], 'expiration'=>$raw['expiration'], 'lot'=>$raw['lot'], 'attached'=>$raw['attached'], 'new'=>$raw['new'], 'new_type'=>$raw['new_type'], 'new_expiration'=>$raw['new_expiration'], 'new_lot'=>$raw['new_lot'],);
						}

						//battery
						$battery_list = $rodb->query("
							select t.type as c_type, pt.type as type, t.expiration, t.lot, t.attached, t.`new`, npt.type as new_type, t.new_expiration, t.new_lot
							from aedservicing_batteries t 
							left join aed_battery_types pt on pt.aed_battery_type_id = t.aed_battery_type_id
							left join aed_battery_types npt on npt.aed_battery_type_id = t.new_aed_battery_type_id
							where t.aedservicing_id = '$check_id'
						");  		
						while($raw = $battery_list->fetch_assoc()) {
							$batteries[$raw['c_type']] = array('type'=>$raw['type'], 'expiration'=>$raw['expiration'], 'lot'=>$raw['lot'], 'attached'=>$raw['attached'], 'new'=>$raw['new'], 'new_type'=>$raw['new_type'], 'new_expiration'=>$raw['new_expiration'], 'new_lot'=>$raw['new_lot'],);
						}

						//accessory
						$accessory_list = $rodb->query("
							select t.type as c_type, pt.type as type, t.expiration, t.lot, t.attached, t.`new`, npt.type as new_type, t.new_expiration, t.new_lot
							from aedservicing_accessories t 
							left join aed_accessory_types pt on pt.aed_accessory_type_id = t.aed_accessory_type_id
							left join aed_accessory_types npt on npt.aed_accessory_type_id = t.new_aed_accessory_type_id
							where t.aedservicing_id = '$check_id'
						");  		
						while($raw = $accessory_list->fetch_assoc()) {
							$accessories[$raw['c_type']] = array('type'=>$raw['type'], 'expiration'=>$raw['expiration'], 'lot'=>$raw['lot'], 'attached'=>$raw['attached'], 'new'=>$raw['new'], 'new_type'=>$raw['new_type'], 'new_expiration'=>$raw['new_expiration'], 'new_lot'=>$raw['new_lot'],);
						}

						if(count($pads) > 0) {
							$body .=  '
							<h2 style="padding-top:30px;width:800px;">'.$messageformat->ack_pads().'</h2>
							<table style="width:800px;" border="1" cellspacing="0" cellpadding="10">
								<tr>
									<td>'.$messageformat->ack_question().'</td>
									<td style="text-align:center;width:150px;">
										<span style="margin-right:17px;">'.$messageformat->ack_responses('Yes').'</span>
										<span style="margin-left:17px;">'.$messageformat->ack_responses('No').'</span>
									</td>
								</tr>';

								if(!isempty($pads['primary_pad']['type']) || !isempty($pads['pediatric_primary_pad']['type'])) {
									$body .=  '
									<tr>
										<td'.(isset($_POST['submit']) && isempty($_POST['status']) ? ' style="color:red;"' : '').'>Are these electrodes(pads) attached to the unit?</td>
										<td style="text-align:center;">&nbsp;</td>
									</tr>';
								}

								if(!isempty($pads['primary_pad']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['primary_pad']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['primary_pad']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['primary_pad']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($pads['primary_pad']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($pads['primary_pad']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pad?</td>
											<td style="text-align:center;">'.($pads['primary_pad']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($pads['primary_pad']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div>
												<div style="width:400px;float:left;">'.$pads['primary_pad']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$pads['primary_pad']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$pads['primary_pad']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}
								if(isset($pads['pediatric_primary_pad']['type']) && !isempty($pads['pediatric_primary_pad']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($pads['pediatric_primary_pad']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($pads['pediatric_primary_pad']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pad?</td>
											<td style="text-align:center;">'.($pads['pediatric_primary_pad']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($pads['pediatric_primary_pad']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div>
												<div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$pads['pediatric_primary_pad']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}

								if(!isempty($pads['spare_1_pad']['type']) || !isempty($pads['spare_2_pad']['type']) || !isempty($pads['pediatric_spare_pad']['type'])) {
									$body .=  '
									<tr>
										<td'.(isset($_POST['submit']) && isempty($_POST['status']) ? ' style="color:red;"' : '').'>Are these spare electrodes(pads) attached to the unit?</td>
										<td style="text-align:center;">&nbsp;</td>
									</tr>';
								}

								
								if(!isempty($pads['spare_1_pad']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['spare_1_pad']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['spare_1_pad']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['spare_1_pad']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($pads['spare_1_pad']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($pads['spare_1_pad']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pad?</td>
											<td style="text-align:center;">'.($pads['spare_1_pad']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($pads['spare_1_pad']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div>
												<div style="width:400px;float:left;">'.$pads['spare_1_pad']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$pads['spare_1_pad']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$pads['spare_1_pad']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}
								if(isset($pads['spare_2_pad']['type']) && !isempty($pads['spare_2_pad']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['spare_2_pad']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['spare_2_pad']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['spare_2_pad']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($pads['spare_2_pad']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($pads['spare_2_pad']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pad?</td>
											<td style="text-align:center;">'.($pads['spare_2_pad']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($pads['spare_2_pad']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div>
												<div style="width:400px;float:left;">'.$pads['spare_2_pad']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$pads['spare_2_pad']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$pads['spare_2_pad']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}
								if(isset($pads['pediatric_spare_pad']['type']) && !isempty($pads['pediatric_spare_pad']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div><div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($pads['pediatric_spare_pad']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($pads['pediatric_spare_pad']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pad?</td>
											<td style="text-align:center;">'.($pads['pediatric_spare_pad']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($pads['pediatric_spare_pad']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pad().'</div>
												<div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$pads['pediatric_spare_pad']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}

							$body .=  '
							</table>';
						}

						if(isset($paks) && count($paks) > 0) {
							$body .=  '
							<h2 style="padding-top:30px;width:800px;">'.$messageformat->ack_paks().'</h2>
							<table style="width:800px;" border="1" cellspacing="0" cellpadding="10">
								<tr>
									<td>'.$messageformat->ack_question().'</td>
									<td style="text-align:center;width:150px;">
										<span style="margin-right:17px;">'.$messageformat->ack_responses('Yes').'</span>
										<span style="margin-left:17px;">'.$messageformat->ack_responses('No').'</span>
									</td>
								</tr>';

								if(!isempty($paks['primary_pak']['type']) || !isempty($paks['pediatric_primary_pak']['type'])) {
									$body .=  '
									<tr>
										<td'.(isset($_POST['submit']) && isempty($_POST['status']) ? ' style="color:red;"' : '').'>Are these electrodes(paks) attached to the unit?</td>
										<td style="text-align:center;">&nbsp;</td>
									</tr>';
								}

								if(!isempty($paks['primary_pak']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['primary_pak']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['primary_pak']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['primary_pak']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($paks['primary_pak']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($paks['primary_pak']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pak?</td>
											<td style="text-align:center;">'.($paks['primary_pak']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($paks['primary_pak']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div>
												<div style="width:400px;float:left;">'.$paks['primary_pak']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$paks['primary_pak']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$paks['primary_pak']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}
								if(!isempty($paks['pediatric_primary_pak']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($paks['pediatric_primary_pak']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($paks['pediatric_primary_pak']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pak?</td>
											<td style="text-align:center;">'.($paks['pediatric_primary_pak']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($paks['pediatric_primary_pak']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div>
												<div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$paks['pediatric_primary_pak']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}

								if(!isempty($paks['spare_1_pak']['type']) || !isempty($paks['spare_2_pak']['type']) || !isempty($paks['pediatric_spare_pak']['type'])) {
									$body .=  '
									<tr>
										<td'.(isset($_POST['submit']) && isempty($_POST['status']) ? ' style="color:red;"' : '').'>Are these spare electrodes(paks) attached to the unit?</td>
										<td style="text-align:center;">&nbsp;</td>
									</tr>';
								}

								
								if(!isempty($paks['spare_1_pak']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['spare_1_pak']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['spare_1_pak']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['spare_1_pak']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($paks['spare_1_pak']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($paks['spare_1_pak']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pak?</td>
											<td style="text-align:center;">'.($paks['spare_1_pak']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($paks['spare_1_pak']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div>
												<div style="width:400px;float:left;">'.$paks['spare_1_pak']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$paks['spare_1_pak']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$paks['spare_1_pak']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}
								if(!isempty($paks['spare_2_pak']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['spare_2_pak']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['spare_2_pak']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['spare_2_pak']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($paks['spare_2_pak']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($paks['spare_2_pak']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pak?</td>
											<td style="text-align:center;">'.($paks['spare_2_pak']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($paks['spare_2_pak']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div>
												<div style="width:400px;float:left;">'.$paks['spare_2_pak']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$paks['spare_2_pak']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$paks['spare_2_pak']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}
								if(!isempty($paks['pediatric_spare_pak']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div><div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($paks['pediatric_spare_pak']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($paks['pediatric_spare_pak']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pak?</td>
											<td style="text-align:center;">'.($paks['pediatric_spare_pak']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($paks['pediatric_spare_pak']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div>
												<div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$paks['pediatric_spare_pak']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}

							$body .=  '
							</table>';
						}


						if(count($batteries) > 0) {
							$body .=  '
							<h2 style="padding-top:30px;width:800px;">'.$messageformat->ack_batteries().'</h2>
							<table style="width:800px;" border="1" cellspacing="0" cellpadding="10">
								<tr>
									<td>'.$messageformat->ack_question().'</td>
									<td style="text-align:center;width:150px;">
										<span style="margin-right:17px;">'.$messageformat->ack_responses('Yes').'</span>
										<span style="margin-left:17px;">'.$messageformat->ack_responses('No').'</span>
									</td>
								</tr>';

								if(!isempty($batteries['primary_battery']['type'])) {
									$body .=  '
									<tr>
										<td'.(isset($_POST['submit']) && isempty($_POST['status']) ? ' style="color:red;"' : '').'>Are these batteries attached to the unit?</td>
										<td style="text-align:center;">&nbsp;</td>
									</tr>';
								}

								if(!isempty($batteries['primary_battery']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_battery().'</div><div style="width:400px;float:left;">'.$batteries['primary_battery']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$batteries['primary_battery']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$batteries['primary_battery']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($batteries['primary_battery']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($batteries['primary_battery']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pak?</td>
											<td style="text-align:center;">'.($batteries['primary_battery']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($batteries['primary_battery']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_battery().'</div>
												<div style="width:400px;float:left;">'.$batteries['primary_battery']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$batteries['primary_battery']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$batteries['primary_battery']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}

								if(isset($batteries['spare_battery']['type']) && !isempty($batteries['spare_battery']['type'])) {
									$body .=  '
									<tr>
										<td'.(isset($_POST['submit']) && isempty($_POST['status']) ? ' style="color:red;"' : '').'>Are these spare batteries attached to the unit?</td>
										<td style="text-align:center;">&nbsp;</td>
									</tr>';
								}

								
								if(isset($batteries['spare_battery']['type']) && !isempty($batteries['spare_battery']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div><div style="width:400px;float:left;">'.$batteries['spare_battery']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$batteries['spare_battery']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$batteries['spare_battery']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($batteries['spare_battery']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($batteries['spare_battery']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pak?</td>
											<td style="text-align:center;">'.($batteries['spare_battery']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($batteries['spare_battery']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_battery().'</div>
												<div style="width:400px;float:left;">'.$batteries['spare_battery']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$batteries['spare_battery']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$batteries['spare_battery']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}

							$body .=  '
							</table>';
						}


						if(isset($accessories) && count($accessories) > 0) {
							$body .=  '
							<h2 style="padding-top:30px;width:800px;">'.$messageformat->ack_accessories().'</h2>
							<table style="width:800px;" border="1" cellspacing="0" cellpadding="10">
								<tr>
									<td>'.$messageformat->ack_question().'</td>
									<td style="text-align:center;width:150px;">
										<span style="margin-right:17px;">'.$messageformat->ack_responses('Yes').'</span>
										<span style="margin-left:17px;">'.$messageformat->ack_responses('No').'</span>
									</td>
								</tr>';

								if(!isempty($accessories['primary_accessory']['type'])) {
									$body .=  '
									<tr>
										<td'.(isset($_POST['submit']) && isempty($_POST['status']) ? ' style="color:red;"' : '').'>Are these accessories attached to the unit?</td>
										<td style="text-align:center;">&nbsp;</td>
									</tr>';
								}

								if(!isempty($accessories['primary_accessory']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessory().'</div><div style="width:400px;float:left;">'.$accessories['primary_accessory']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$accessories['primary_accessory']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$accessories['primary_accessory']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($accessories['primary_accessory']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($accessories['primary_accessory']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pak?</td>
											<td style="text-align:center;">'.($accessories['primary_accessory']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($accessories['primary_accessory']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessory().'</div>
												<div style="width:400px;float:left;">'.$accessories['primary_accessory']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$accessories['primary_accessory']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$accessories['primary_accessory']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}

								if(!isempty($accessories['spare_accessory']['type'])) {
									$body .=  '
									<tr>
										<td'.(isset($_POST['submit']) && isempty($_POST['status']) ? ' style="color:red;"' : '').'>Are these spare accessories attached to the unit?</td>
										<td style="text-align:center;">&nbsp;</td>
									</tr>';
								}

								
								if(!isempty($accessories['spare_accessory']['type'])) {
									$body .=  '
									<tr>
										<td>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_pak().'</div><div style="width:400px;float:left;">'.$accessories['spare_accessory']['type'].'</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div><div style="width:400px;float:left;">'.$accessories['spare_accessory']['lot'].'&nbsp;</div>
											<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div><div style="width:400px;float:left;">'.$accessories['spare_accessory']['expiration'].'&nbsp;</div>
										</td>
										<td style="text-align:center;">'.($accessories['spare_accessory']['attached'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
									</tr>';

									if($accessories['spare_accessory']['attached'] == 0) {
										$body .=  '
										<tr>
											<td>Are you installing a new pak?</td>
											<td style="text-align:center;">'.($accessories['spare_accessory']['new'] ? $messageformat->ack_responses('yes') : $messageformat->ack_responses('no')).'</td>
										</tr>';
									}

									if($accessories['spare_accessory']['new'] == 1) {
										$body .=  '
										<tr>
											<td>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessory().'</div>
												<div style="width:400px;float:left;">'.$accessories['spare_accessory']['new_type'] .'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessorylot().'</div>
												<div style="width:400px;float:left;">'.$accessories['spare_accessory']['new_lot'].'&nbsp;</div>
												<div style="width:200px;float:left;font-weight:bold;">'.$messageformat->aed_accessoryexp().'</div>
												<div style="width:400px;float:left;">'.$accessories['spare_accessory']['new_expiration'] .'&nbsp;</div>
											</td>
											<td style="text-align:center;">&nbsp;</td>
										</tr>';
									}
								}

							$body .=  '
							</table>';
						}
						
						$body .=  '
						<h2 style="padding-top:30px;width:800px;">'.$messageformat->ask_servicingby().'</h2>
						<table style="width:800px;" border="1" cellspacing="0" cellpadding="10" id="servicertable">
							<tr>
								<td colspan="2">'.$messageformat->ask_servicerpromise().'</td>
							</tr>
							<tr>
								<td>'.$messageformat->ask_digitalsignature().'</td>
								<td>'.$aedservicinginfo['digitalsignature'].'</td>
							</tr>
							<tr>
								<td>'.$messageformat->ask_dateverified().'</td>	
								<td>'.$aedservicinginfo['date'].'</td>
							</tr>
							<tr>
								<td colspan="2">'.$messageformat->ask_servicerepverified().'</td>
							</tr>
							<tr>
								<td>'.$messageformat->ask_clientrepname().'</td>
								<td>'.$aedservicinginfo['clientrepname'].'</td>
							</tr>
							<tr>
								<td>'.$messageformat->ask_clientreptitle().'<span class="required">*</span></td>
								<td>'.$aedservicinginfo['clientreptitle'].'</td>
							</tr>
							<tr>
								<td>'.$messageformat->ask_clientrepemail().'</td>
								<td>'.$aedservicinginfo['clientrepemail'].'</td>
							</tr>
							<tr>
								<td>'.$messageformat->ask_clientrepphone().'</td>
								<td>'.$aedservicinginfo['clientrepphone'].'</td>
							</tr>
							<tr>
								<td>'.$messageformat->ask_dateverifiedclientlocation().'</td>	
								<td>'.$aedservicinginfo['date'].'</td>
							</tr>
						</table>
						
						<h2 style="padding-top:30px;width:800px;">'.$messageformat->ask_comments().'</h2>
						<p style="padding:10px;overflow:auto;width:780px;height:100px;border:1px solid black;margin-bottom:10px;" name="comments">'.nl2br($aedservicinginfo['comment']).'</p>';
					} else
						$body .=  $messageformat->ask_noservicing();
				$body .=  '	
				</div>';

	echo $body;
?>