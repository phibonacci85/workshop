<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Dashboard</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/morris.css" rel="stylesheet">
    <link href="../css/metisMenu.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <script src="../js/modernizr-2.6.2.min.js"></script>
</head>
<body>
    <?php include('../include/navbar.php'); ?>
    <?php include('../include/sidebar.php'); ?>

    <script src="http://code.jquery.com/jquery.js"></script>
    <script>
        window.jQuery || document.write('<script src="../js/jquery-1.8.2.min.js"><\/script>')
    </script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/Chart.min.js"></script>
    <script src="../js/morris.min.js"></script>
    <script src="../js/metisMenu.min.js"></script>
    <script src="../js/javascript.js"></script>
</body>
</html>