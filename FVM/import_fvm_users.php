<?php
require('../swiftmailer/swift_required.php');

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

date_default_timezone_set('America/Chicago');

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew');

$filename = 'uns_energy_users_import.csv';

$orgs = array();
$uniqueUsers = array();
if (($handle = fopen($filename, "r")) !== FALSE) {
    $line = 0;
    while (($data = fgetcsv($handle, 9000, ",")) !== FALSE) {
        if ($line == 0) {
            //echo $data[0].' '.$data[1].' '.$data[2].' '.$data[3].' '.$data[4].' '.$data[5].' '.$data[6].' '.$data[7].' '.$data[8].' '.$data[9].' '.$data[10].' '.$data[11].'<br />';
        } else {
            $users = $db->query("
                SELECT *
                FROM users
                WHERE username = '" . $data[6] . "'
            ");
            $user = $users->fetch_assoc();
            $userId = $user['id'];
            $uniqueUsers[$data[6]] = $userId;
            //$data[0] => Case #
            //$data[1] => Make / Model
            //$data[2] => Serial Number
            //$data[3] => Battery Date
            //$data[4] => Electrode Expiration
            //$data[5] => Location
            //$data[6] => Assigned Employee Email
            //$data[7] => Assigned Employee
            //$data[8] => 1st Level Notification
            //$data[9] => 2nd Level Notification
            //$data[10] => 2nd Level Notification
            //$data[11] => 3rd Level Notification
            //echo $data[0].' '.$data[1].' '.$data[2].' '.$data[3].' '.$data[4].' '.$data[5].' '.$data[6].' '.$data[7].' '.$data[8].' '.$data[9].' '.$data[10].' '.$data[11].'<br />';
            /*
                        $locations = $db->query("
                            SELECT l.*
                            FROM aeds a, locations l
                            WHERE a.location_id = l.id
                            AND a.serialnumber = '" . $data[2] . "'
                        ");
                        if (!$location = $locations->fetch_assoc()) {
                            echo 'Can not find location for serial number ' . $data[2] . '...skipping<br />';
                            continue;
                        }

                        $name = explode(' ', $data[7]);
                        if (!array_key_exists($data[6], $uniqueUsers)) {
                            $insertUser = $db->query("
                                INSERT INTO users
                                (username, password, firstname, lastname, phone, company, preferredlanguage, address, city, province, country, zip, firstlogin, creation, display)
                                VALUES(
                                  '" . $data[6] . "',
                                  PASSWORD('unsenergy'),
                                  '" . $name[0] . "',
                                  '" . $name[1] . "',
                                  '',
                                  'UNS Energy',
                                  'en',
                                  '',
                                  '',
                                  '',
                                  '',
                                  '',
                                  'yes',
                                  '" . date('Y-m-d H:i:s') . "',
                                  'yes'
                                )
                            ");
                            if ($insertUser) {
                                $userId = $db->insert_id;
                                $uniqueUsers[$data[6]] = $userId;
                                echo '<h3>Inserted User: ' . $data[6] . '</h3>';

                                $insertPrivileges = $db->query("
                                    INSERT INTO privileges
                                    (userid, showaed, showerp, showmedicaldirection, showevents, showrecalls, showstatelaws, showequipment, showpersons, showtraining, showdocuments, showhazard, showcontact, showaedcheck, newaedcheck, editaedcheck)
                                    VALUES
                                    ('" . $uniqueUsers[$data[6]] . "', 'yes', 'yes', 'yes', 'yes', 'yes', '1', 'yes', '1', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes')
                                ");
                                if ($insertPrivileges) {
                                    echo 'Inserted Privileges for User Id: ' . $userId . '<br />';
                                }
                            } else {
                                $users = $db->query("
                                    SELECT *
                                    FROM users
                                    WHERE username = '" . $data[6] . "'
                                ");
                                $user = $users->fetch_assoc();
                                $userId = $user['id'];
                                $uniqueUsers[$data[6]] = $userId;
                            }
                        }

                        $rightsOrgExists = $db->query("
                            SELECT *
                            FROM rights_orgs
                            WHERE userid = '" . $uniqueUsers[$data[6]] . "'
                            AND orgid = '" . $location['orgid'] . "'
                            AND LEVEL = '3'
                            AND display = '1'
                        ");
                        if ($rightsOrgExists->num_rows == 0) {
                            $insertRightsOrg = $db->query("
                                INSERT INTO rights_orgs
                                (userid, orgid, level, display, creation)
                                VALUES(
                                  '" . $uniqueUsers[$data[6]] . "',
                                  '" . $location['orgid'] . "',
                                  '3',
                                  '1',
                                  '" . date('Y-m-d H:i:s') . "'
                                )
                            ");
                            if ($insertRightsOrg) {
                                echo 'Inserted Rights Org<br />';
                            } else {
                                echo 'Error: adding rights org ' . $location['orgid'] . ' to user ' . $data[6] . '<br />';
                            }
                        }
                        $rightsLocationExists = $db->query("
                            SELECT *
                            FROM rights_locations
                            WHERE userid = '" . $uniqueUsers[$data[6]] . "'
                            AND orgid = '" . $location['orgid'] . "'
                            AND locationid = '" . $location['id'] . "'
                            AND display = '1'
                        ");
                        if ($rightsLocationExists->num_rows == 0) {
                            $insertRightsLoc = $db->query("
                                INSERT INTO rights_locations
                                (userid, orgid, locationid, display, creation)
                                VALUES(
                                  '" . $uniqueUsers[$data[6]] . "',
                                  '" . $location['orgid'] . "',
                                  '" . $location['id'] . "',
                                  '1',
                                  '" . date('Y-m-d H:i:s') . "'
                                )
                            ");
                            if ($insertRightsLoc) {
                                echo 'Inserted Rights Loc<br />';
                            }
                        }

                        //ALERTS
                        //$data[0] => Case #
                        //$data[1] => Make / Model
                        //$data[2] => Serial Number
                        //$data[3] => Battery Date
                        //$data[4] => Electrode Expiration
                        //$data[5] => Location
                        //$data[6] => Assigned Employee Email
                        //$data[7] => Assigned Employee
                        //$data[8] => 1st Level Notification
                        //$data[9] => 2nd Level Notification
                        //$data[10] => 2nd Level Notification
                        //$data[11] => 3rd Level Notification
                        if ($data[8] != '') {
                            $insertAlertAEDLevel1 = $db->query("
                            INSERT INTO alerts
                                (locationid, email, level, type, language, display)
                                VALUES (
                                  '" . $location['id'] . "',
                                  '" . $data[8] . "',
                                  '1',
                                  'aed',
                                  'en',
                                  'yes'
                                )
                            ");
                            $insertAlertAEDCheckLevel1 = $db->query("
                            INSERT INTO alerts
                            (locationid, email, level, type, language, display)
                                VALUES (
                                  '" . $location['id'] . "',
                                  '" . $data[8] . "',
                                  '1',
                                  'aedcheck',
                                  'en',
                                  'yes'
                                )
                            ");
                            if ($insertAlertAEDLevel1 && $insertAlertAEDCheckLevel1) {
                                echo 'Inserted AED and AED Check level 1 for: ' . $data[8] . '<br />';
                            }
                        }
                        if ($data[9] != '') {
                            $insertAlertAEDLevel2 = $db->query("
                                INSERT INTO alerts
                                (locationid, email, level, type, language, display)
                                VALUES (
                                  '" . $location['id'] . "',
                                  '" . $data[9] . "',
                                  '2',
                                  'aed',
                                  'en',
                                  'yes'
                                )
                            ");
                            $insertAlertAEDCheckLevel2 = $db->query("
                                INSERT INTO alerts
                                (locationid, email, level, type, language, display)
                                VALUES (
                                  '" . $location['id'] . "',
                                  '" . $data[9] . "',
                                  '2',
                                  'aedcheck',
                                  'en',
                                  'yes'
                                )
                            ");
                            if ($insertAlertAEDLevel2 && $insertAlertAEDCheckLevel2) {
                                echo 'Inserted AED and AED Check level 2 for: ' . $data[9] . '<br />';
                            }
                        }
                        if ($data[10] != '') {
                            $insertAlertAEDLevel22 = $db->query("
                                INSERT INTO alerts
                                (locationid, email, level, type, language, display)
                                VALUES (
                                  '" . $location['id'] . "',
                                  '" . $data[10] . "',
                                  '2',
                                  'aed',
                                  'en',
                                  'yes'
                                )
                            ");
                            $insertAlertAEDCheckLevel22 = $db->query("
                                INSERT INTO alerts
                                (locationid, email, level, type, language, display)
                                VALUES (
                                  '" . $location['id'] . "',
                                  '" . $data[10] . "',
                                  '2',
                                  'aedcheck',
                                  'en',
                                  'yes'
                                )
                            ");
                            if ($insertAlertAEDLevel22 && $insertAlertAEDCheckLevel22) {
                                echo 'Inserted AED and AED Check level 2 for: ' . $data[10] . '<br />';
                            }
                        }
                        if ($data[11] != '') {
                            $insertAlertAEDLevel3 = $db->query("
                                INSERT INTO alerts
                                (locationid, email, level, type, language, display)
                                VALUES (
                                  '" . $location['id'] . "',
                                  '" . $data[11] . "',
                                  '3',
                                  'aed',
                                  'en',
                                  'yes'
                                )
                            ");
                            $insertAlertAEDCheckLevel3 = $db->query("
                                INSERT INTO alerts
                                (locationid, email, level, type, language, display)
                                VALUES (
                                  '" . $location['id'] . "',
                                  '" . $data[11] . "',
                                  '3',
                                  'aedcheck',
                                  'en',
                                  'yes'
                                )
                            ");
                            if ($insertAlertAEDLevel3 && $insertAlertAEDCheckLevel3) {
                                echo 'Inserted AED and AED Check level 3 for: ' . $data[11] . '<br />';
                            }
                        }
            */
        }
        $line++;
    }
    fclose($handle);
} else {
    echo 'Can not open file';
}

foreach ($uniqueUsers as $email => $userId) {
    $welcomeEmail = '
        <html><head></head><body style="color:#000001;">
        <div style="margin:0 auto;width:970px;background-color:#C2E0F6;color:#000001;padding:10px 0">
            <div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid #0078C1;margin: 5px auto;">
                <h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
                    <img alt="Rescue One AED Manager" src="http://firstvoicemanager.com/images/FVMLogo.png">
                </h1>
                <div style="width:100%;height:30px;margin-bottom:5px;background-color:#0078C1;float:left; margin-top:5px;"></div>
                <h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
                    Welcome 
                </h2>
                <div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
                    You have recently been invited to join <a href="http://rescueoneaedmanageronline.com/">First Voice Manager</a>.  This is a program that allows you
                    to manage your AED equipment and perform monthly AED checks<br /><br />
                    <b>Logging into First Voice Manager is simple!</b>
                        <div style="margin-left:10px;font-weight:bold;">
                        Username: ' . $email . '<br />
                        Password: unsenergy
                        </div><br />
                    If you think you got this email by accident, please disregard.
                    <br />
                    If you have any questions, please feel free to contact us at <b>support@firstvoicemanager.com</b>.
                </div>
            </div>
        </div>
        <div style="width:970px;margin:0 auto;color:#000001;background-color:white;">Please do not reply to this email. We are unable to respond to inquiries sent to this address.</div>
        <br />
        <div style="width:970px;margin:0 auto;color:#000001;">Copyright &copy; 2013 Think Safe Inc.</div>
        </body></html>
    ';

    $transport = Swift_SmtpTransport::newInstance('server.tsdemos.com', 25)
        ->setUsername('donotreply@tsdemos.com')
        ->setPassword('1105firstvoice');
    $mailer = Swift_Mailer::newInstance($transport);
    $message = Swift_Message::newInstance()
        ->setSubject('Welcome to First Voice Manager')
        ->setFrom(array('donotreply@tsdemos.com' => 'First Voice Manager'))
        ->setTo($email)
        ->setBody($welcomeEmail, 'text/html');
    $result = $mailer->send($message);
    echo 'Sent to: ' . $email . '<br />';
}
?>