<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 10/17/2016
 * Time: 10:44
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $db = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew_demo');

$organizations = $db->query("
    SELECT *
    FROM organizations
    WHERE name NOT LIKE '%(demo)%'
");
while ($org = $organizations->fetch_assoc()) {
    $locations = $db->query("
        SELECT *
        FROM locations
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($location = $locations->fetch_assoc()) {
        $aeds = $db->query("
            SELECT *
            FROM aeds
            WHERE location_id = '" . $location['id'] . "'
        ");
        while ($aed = $aeds->fetch_assoc()) {
            $accessories = $db->query("
                DELETE FROM aed_accessories
                WHERE aed_id = '" . $aed['aed_id'] . "'
            ");
            $batteries = $db->query("
                DELETE FROM aed_batteries
                WHERE aed_id = '" . $aed['aed_id'] . "'
            ");
            $pads = $db->query("
                DELETE FROM aed_pads
                WHERE aed_id = '" . $aed['aed_id'] . "'
            ");
            $paks = $db->query("
                DELETE FROM aed_paks
                WHERE aed_id = '" . $aed['aed_id'] . "'
            ");

            $tracing = $db->query("
                SELECT *
                FROM tracing
                WHERE aedid = '" . $aed['aed_id'] . "'
            ");
            while ($trace = $tracing->fetch_assoc()) {
                $tracingBattery = $db->query("
                    DELETE FROM tracing_battery
                    WHERE tracingid = '" . $trace['id'] . "'
                ");
                $tracingPad = $db->query("
                    DELETE FROM tracing_battery
                    WHERE tracingid = '" . $trace['id'] . "'
                ");
                $tracingPak = $db->query("
                    DELETE FROM tracing_pak
                    WHERE tracingid = '" . $trace['id'] . "'
                ");
            }
            $tracing = $db->query("
                DELETE FROM tracing
                WHERE aedid = '" . $aed['aed_id'] . "'
            ");

            $checks = $db->query("
                SELECT *
                FROM aedcheck
                WHERE aed_id = '" . $aed['aed_id'] . "'
            ");
            while ($check = $checks->fetch_assoc()) {
                $checkAccessories = $db->query("
                    DELETE FROM aedcheck_accessories
                    WHERE aedcheck_id = '" . $check['aedcheck_id'] . "'
                ");
                $checkBatteries = $db->query("
                    DELETE FROM aedcheck_batteries
                    WHERE aedcheck_id = '" . $check['aedcheck_id'] . "'
                ");
                $checkChecker = $db->query("
                    DELETE FROM aedcheck_checker
                    WHERE aedcheck_id = '" . $check['aedcheck_id'] . "'
                ");
                $checkPads = $db->query("
                    DELETE FROM aedcheck_pads
                    WHERE aedcheck_id = '" . $check['aedcheck_id'] . "'
                ");
                $checkPaks = $db->query("
                    DELETE FROM aedcheck_paks
                    WHERE aedcheck_id = '" . $check['aedcheck_id'] . "'
                ");
            }
            $checks = $db->query("
                DELETE FROM aedcheck
                WHERE aed_id = '" . $aed['aed_id'] . "'
            ");
            $servicing = $db->query("
                SELECT *
                FROM aedservicing
                WHERE aed_id = '" . $aed['aed_id'] . "'
            ");
            while ($service = $servicing->fetch_assoc()) {
                $servicingAccessories = $db->query("
                    DELETE FROM aedservicing_accessories
                    WHERE aedservicing_id = '" . $check['aedservicing_id'] . "'
                ");
                $servicingBatteries = $db->query("
                    DELETE FROM aedservicing_batteries
                    WHERE aedservicing_id = '" . $check['aedservicing_id'] . "'
                ");
                $servicingPads = $db->query("
                    DELETE FROM aedservicing_pads
                    WHERE aedservicing_id = '" . $check['aedservicing_id'] . "'
                ");
                $servicingPaks = $db->query("
                    DELETE FROM aedservicing_paks
                    WHERE aedservicing_id = '" . $check['aedservicing_id'] . "'
                ");
            }
            $servicing = $db->query("
                DELETE FROM aedservicing
                WHERE aed_id = '" . $aed['aed_id'] . "'
            ");
        }
        $aeds = $db->query("
            DELETE FROM aeds
            WHERE location_id = '" . $location['id'] . "'
        ");

        $documents = $db->query("
            SELECT *
            FROM documents
            WHERE locationid = '" . $location['id'] . "'
        ");
        while ($document = $documents->fetch_assoc()) {
            $documentPersons = $db->query("
                DELETE FROM documents_persons
                WHERE documentid = '" . $document['id'] . "'
            ");
            $documentUploads = $db->query("
                DELETE FROM documents_uploads
                WHERE documentid = '" . $document['id'] . "'
            ");
        }
        $documents = $db->query("
            DELETE FROM documents
            WHERE locationid = '" . $location['id'] . "'
        ");

        $equipments = $db->query("
            SELECT *
            FROM equipment
            WHERE locationid = '" . $location['id'] . "'
        ");
        while ($equipment = $equipments->fetch_assoc()) {
            $equipmentChecks = $db->query("
                SELECT *
                FROM equipmentcheck
                WHERE equipmentid = '" . $equipment['id'] . "'
            ");
            while ($equipmentCheck = $equipmentChecks->fetch_assoc()) {
                $checkCheckers = $db->query("
                    DELETE FROM equipmentcheck_checker
                    WHERE checkid = '" . $equipmentCheck['id'] . "'
                ");

                $checkContents = $db->query("
                    SELECT *
                    FROM equipmentcheck_content
                    WHERE checkid = '" . $equipmentCheck['id'] . "'
                ");
                while ($checkContent = $checkContents->fetch_assoc()) {
                    $checkContentLangs = $db->query("
                        DELETE FROM equipmentcheck_contentlang
                        WHERE contentid = '" . $checkContent['id'] . "'
                    ");
                }
                $checkContents = $db->query("
                    DELETE FROM equipmentcheck_content
                    WHERE checkid = '" . $equipmentCheck['id'] . "'
                ");

                $checkQuestions = $db->query("
                    SELECT *
                    FROM equipmentcheck_questions
                    WHERE checkid = '" . $equipmentCheck['id'] . "'
                ");
                while ($checkQuestion = $checkQuestions->fetch_assoc()) {
                    $checkQuestionLangs = $db->query("
                        DELETE FROM equipmentcheck_questionslang
                        WHERE questionid = '" . $checkQuestion['id'] . "'
                    ");
                }
                $checkQuestions = $db->query("
                    DELETE FROM equipmentcheck_questions
                    WHERE checkid = '" . $equipmentCheck['id'] . "'
                ");
            }
            $equipmentChecks = $db->query("
                DELETE FROM equipmentcheck
                WHERE equipmentid = '" . $equipment['id'] . "'
            ");

            $fvChecks = $db->query("
                SELECT *
                FROM firstvoicecheck
                WHERE equipmentid = '" . $equipment['id'] . "'
            ");
            while ($fvCheck = $fvChecks->fetch_assoc()) {
                $fvCheckChecker = $db->query("
                    DELETE FROM firstvoicecheck_checker
                    WHERE checkid = '" . $fvCheck['id'] . "'
                ");

                $fvCheckContents = $db->query("
                    SELECT *
                    FROM firstvoicecheck_content
                    WHERE checkid = '" . $fvCheck['id'] . "'
                ");
                while ($fvCheckContent = $fvCheckContents->fetch_assoc()) {
                    $fvCheckContentLangs = $db->query("
                        DELETE FROM firstvoicecheck_contentlang
                        WHERE contentid = '" . $fvCheckContent['id'] . "'
                    ");
                }
                $fvCheckContents = $db->query("
                    DELETE FROM firstvoicecheck_content
                    WHERE checkid = '" . $fvCheck['id'] . "'
                ");

                $fvCheckQuestions = $db->query("
                    SELECT *
                    FROM firstvoicecheck_questions
                    WHERE checkid = '" . $fvCheck['id'] . "'
                ");
                while ($fvCheckQuestion = $fvCheckQuestions->fetch_assoc()) {
                    $fvCheckQuestionLangs = $db->query("
                        DELETE FROM firstvoicecheck_questionslang
                        WHERE questionid = '" . $fvCheckQuestion['id'] . "'
                    ");
                }
                $fvCheckQuestions = $db->query("
                    DELETE FROM firstvoicecheck_questions
                    WHERE checkid = '" . $fvCheck['id'] . "'
                ");
            }
            $fvChecks = $db->query("
                DELETE FROM firstvoicecheck
                WHERE equipmentid = '" . $equipment['id'] . "'
            ");
        }
        $equipments = $db->query("
            DELETE FROM equipment
            WHERE locationid = '" . $location['id'] . "'
        ");

        $persons = $db->query("
            SELECT *
            FROM persons
            WHERE locationid = '" . $location['id'] . "'
        ");
        while ($person = $persons->fetch_assoc()) {
            $documentPersons = $db->query("
                DELETE FROM documents_persons
                WHERE personid = '" . $person['id'] . "'
            ");
            $immPersonsWithTypes = $db->query("
                DELETE FROM immunizations_personswithtypes
                WHERE personid = '" . $person['id'] . "'
            ");
            $licensesPersonsWtihTypes = $db->query("
                DELETE FROM licenses_personswithtypes
                WHERE personid = '" . $person['id'] . "'
            ");
            $trainingPersons = $db->query("
                DELETE FROM training_personswithtypes
                WHERE personid = '" . $person['id'] . "'
            ");
        }
        $persons = $db->query("
            DELETE FROM persons
            WHERE locationid = '" . $location['id'] . "'
        ");

        $rightsLocations = $db->query("
            DELETE FROM rights_locations
            WHERE locationid = '" . $location['id'] . "'
        ");
        $reportsSchedule = $db->query("
            DELETE FROM reports_schedule
            WHERE location_id = '" . $location['id'] . "'
        ");
        $recalls = $db->query("
            DELETE FROM recalls
            WHERE locationid = '" . $location['id'] . "'
        ");
        $recallsDocuments = $db->query("
            DELETE FROM recalls_documents
            WHERE locationid = '" . $location['id'] . "'
        ");
        $recallsServices = $db->query("
            DELETE FROM recalls_service
            WHERE locationid = '" . $location['id'] . "'
        ");
        $medicalDirections = $db->query("
            DELETE FROM medicaldirection
            WHERE locationid = '" . $location['id'] . "'
        ");
        $firstVoices = $db->query("
            DELETE FROM firstvoice
            WHERE locationid = '" . $location['id'] . "'
        ");
        $events = $db->query("
            DELETE FROM events
            WHERE locationid = '" . $location['id'] . "'
        ");
        $erps = $db->query("
            DELETE FROM erp
            WHERE locationid = '" . $location['id'] . "'
        ");
        $alerts = $db->query("
            DELETE FROM alerts
            WHERE locationid = '" . $location['id'] . "'
        ");
        $contacts = $db->query("
            DELETE FROM contacts
            WHERE locationid = '" . $location['id'] . "'
        ");
        $hazards = $db->query("
            DELETE FROM hazards
            WHERE locationid = '" . $location['id'] . "'
        ");
    }
    $locations = $db->query("
        DELETE FROM locations
        WHERE orgid = '" . $org['id'] . "'
    ");

    $equipmentAddContents = $db->query("
        SELECT *
        FROM equipment_additionalcontent
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($equipmentAddContent = $equipmentAddContents->fetch_assoc()) {
        $contentAnswers = $db->query("
            DELETE FROM equipment_additionalcontentanswers
            WHERE additionalcontentid = '" . $equipmentAddContent['id'] . "'
        ");
        $contentLang = $db->query("
            DELETE FROM equipment_additionalcontentlang
            WHERE contentid = '" . $equipmentAddContent['id'] . "'
        ");
    }
    $equipmentAddContents = $db->query("
        DELETE FROM equipment_additionalcontent
        WHERE orgid = '" . $org['id'] . "'
    ");

    $equipmentCheckQuestions = $db->query("
        SELECT *
        FROM equipment_checkquestions
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($equipmentCheckQuestion = $equipmentCheckQuestions->fetch_assoc()) {
        $checkQuestionLangs = $db->query("
            DELETE FROM equipment_checkquestionslang
            WHERE questionid = '" . $equipmentCheckQuestion['id'] . "'
        ");
    }
    $equipmentCheckQuestions = $db->query("
        DELETE FROM equipment_checkquestions
        WHERE orgid = '" . $org['id'] . "'
    ");

    $equipmentStyles = $db->query("
        SELECT *
        FROM equipment_styles
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($equipmentStyle = $equipmentStyles->fetch_assoc()) {
        $stylesLangs = $db->query("
            DELETE FROM equipment_styleslang
            WHERE styleid = '" . $equipmentStyle['id'] . "'
        ");
    }
    $equipmentStyles = $db->query("
        DELETE FROM equipment_styles
        WHERE orgid = '" . $org['id'] . "'
    ");

    $equipmentTypes = $db->query("
        SELECT *
        FROM equipment_types
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($equipmentType = $equipmentTypes->fetch_assoc()) {
        $typesLangs = $db->query("
            DELETE FROM equipment_typeslang
            WHERE typeid = '" . $equipmentType['id'] . "'
        ");
    }
    $equipmentTypes = $db->query("
        DELETE FROM equipment_types
        WHERE orgid = '" . $org['id'] . "'
    ");

    $fvAddContents = $db->query("
        SELECT *
        FROM firstvoice_additionalcontent
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($fvAddContent = $fvAddContents->fetch_assoc()) {
        $fvAddContentAnswers = $db->query("
            DELETE FROM firstvoice_additionalcontentanswers
            WHERE additionalcontentid = '" . $fvAddContent['id'] . "'
        ");
        $fvAddContentLangs = $db->query("
            DELETE FROM firstvoice_additionalcontentlang
            WHERE contentid = '" . $fvAddContent['id'] . "'
        ");
    }
    $fvAddContents = $db->query("
        DELETE
        FROM firstvoice_additionalcontent
        WHERE orgid = '" . $org['id'] . "'
    ");

    $fvCheckQuestions = $db->query("
        SELECT *
        FROM firstvoice_checkquestions
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($fvCheckQuestion = $fvCheckQuestions->fetch_assoc()) {
        $fvCheckQuestionLangs = $db->query("
            DELETE FROM firstvoice_checkquestionslang
            WHERE questionid = '" . $fvCheckQuestion['id'] . "'
        ");
    }
    $fvCheckQuestions = $db->query("
        DELETE FROM firstvoice_checkquestions
        WHERE orgid = '" . $org['id'] . "'
    ");

    $fvStyles = $db->query("
        SELECT *
        FROM firstvoice_styles
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($fvStyle = $fvStyles->fetch_assoc()) {
        $fvStyleLangs = $db->query("
            DELETE FROM firstvoice_styleslang
            WHERE styleid = '" . $fvStyle['id'] . "'
        ");
    }
    $fvStyles = $db->query("
        DELETE FROM firstvoice_styles
        WHERE orgid = '" . $org['id'] . "'
    ");

    $fvTypes = $db->query("
        SELECT *
        FROM firstvoice_types
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($fvType = $fvTypes->fetch_assoc()) {
        $fvTypeLangs = $db->query("
            DELETE FROM firstvoice_typeslang
            WHERE typeid = '" . $fvType['id'] . "'
        ");
    }
    $fvTypes = $db->query("
        DELETE FROM firstvoice_types
        WHERE orgid = '" . $org['id'] . "'
    ");

    $hazardTypes = $db->query("
        SELECT *
        FROM hazard_types
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($hazardType = $hazardTypes->fetch_assoc()) {
        $hazardTypeLangs = $db->query("
            DELETE FROM hazard_typeslang
            WHERE typeid = '" . $hazardType['id'] . "'
        ");
    }
    $hazardTypes = $db->query("
        DELETE FROM hazard_types
        WHERE orgid = '" . $org['id'] . "'
    ");

    $immTypes = $db->query("
        SELECT *
        FROM immunizations_types
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($immType = $immTypes->fetch_assoc()) {
        $immTypeAlerts = $db->query("
            DELETE FROM immunizations_typesalerts
            WHERE typeid = '" . $immType['id'] . "'
        ");
        $immTypeLangs = $db->query("
            DELETE FROM immunizations_typeslang
            WHERE typeid = '" . $immType['id'] . "'
        ");
    }
    $immTypes = $db->query("
        DELETE FROM immunizations_types
        WHERE orgid = '" . $org['id'] . "'
    ");

    $trainingClassTypes = $db->query("
        SELECT *
        FROM training_classtypes
        WHERE orgid = '" . $org['id'] . "'
    ");
    while ($trainingClassType = $trainingClassTypes->fetch_assoc()) {
        $trainingClassTypeLangs = $db->query("
            DELETE FROM training_classtypeslang
            WHERE typeid = '" . $trainingClassType['id'] . "'
        ");
    }
    $trainingClassTypes = $db->query("
        DELETE FROM training_classtypes
        WHERE orgid = '" . $org['id'] . "'
    ");

    $trainingClassPrints = $db->query("
        DELETE FROM training_classtypes
        WHERE orgid = '" . $org['id'] . "'
    ");
    $rightsOrgs = $db->query("
        DELETE FROM rights_orgs
        WHERE orgid = '" . $org['id'] . "'
    ");
    $keycods = $db->query("
        DELETE FROM keycodes
        WHERE orgid = '" . $org['id'] . "'
    ");
    $fvAlerts = $db->query("
        DELETE FROM firstvoice_alerts
        WHERE orgid = '" . $org['id'] . "'
    ");
    $equipmentAlerts = $db->query("
        DELETE FROM equipment_alerts
        WHERE orgid = '" . $org['id'] . "'
    ");
}
$orgs = $db->query("
    DELETE FROM organizations
    WHERE name NOT LIKE '%(demo)%'
");