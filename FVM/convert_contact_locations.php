<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 12/9/2016
 * Time: 12:42
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

//@ $fvmdb = new mysqli($host, 'tsdemosc', 'Hawkeye17!', 'tsdemosc_fvmnew');
@ $fvmdb = new mysqli('fvdemos.com', 'fvdemos', 'Hawkeye17!', 'fvdemos_rescueone_2017_1_6_copy');

$organizations = $fvmdb->query("
    SELECT *
    FROM organizations
");
while ($organization = $organizations->fetch_assoc()) {
    $distinctContacts = $fvmdb->query("
        SELECT c.name, count(c.name)
        FROM contacts c
        JOIN locations l ON c.locationid = l.id
        WHERE l.orgid = '" . $organization['id'] . "'
        GROUP BY c.name
    ");
    echo $organization['name'] . ' - ' . $organization['id'] . '<br />';
    while ($distinctContact = $distinctContacts->fetch_assoc()) {
        $distinctName = str_replace("'", "''", $distinctContact['name']);
        $contacts = $fvmdb->query("
            SELECT c.*
            FROM contacts c
            JOIN locations l ON c.locationid = l.id
            WHERE l.orgid = '" . $organization['id'] . "'
            AND c.name = '" . $distinctName . "'
        ");
        if ($origContact = $contacts->fetch_assoc()) {
            $insertRelation = $fvmdb->query("
                INSERT INTO location_contacts (
                  location_id, 
                  contact_id
                ) VALUES (
                  '" . $origContact['locationid'] . "', 
                  '" . $origContact['id'] . "'
                )
            ");
            while ($contact = $contacts->fetch_assoc()) {
                echo $contact['name'] . ' - ' . $contact['locationid'] . '<br />';
                $existingRelations = $fvmdb->query("
                    SELECT *
                    FROM location_contacts
                    WHERE location_id = '" . $contact['locationid'] . "'
                    AND contact_id = '" . $origContact['id'] . "'
                ");
                if ($existingRelations->num_rows == 0) {
                    $insertRelation = $fvmdb->query("
                        INSERT INTO location_contacts (
                          location_id, 
                          contact_id
                        ) VALUES (
                          '" . $contact['locationid'] . "', 
                          '" . $origContact['id'] . "'
                        )
                    ");
                }
                $updateAedSiteCoordinators = $fvmdb->query("
                    UPDATE aeds
                    SET site_coordinator_contact_id = '" . $origContact['id'] . "'
                    WHERE site_coordinator_contact_id = '" . $contact['id'] . "'
                ");
                $updateOrgMainBilling = $fvmdb->query("
                    UPDATE organizations
                    SET main_billing_contact_id = '" . $origContact['id'] . "'
                    WHERE main_billing_contact_id = '" . $contact['id'] . "'
                ");
                $updateOrgMainMailing = $fvmdb->query("
                    UPDATE organizations
                    SET main_mailing_contact_id = '" . $origContact['id'] . "'
                    WHERE main_mailing_contact_id = '" . $contact['id'] . "'
                ");
                $updateLocShipping = $fvmdb->query("
                    UPDATE locations
                    SET shipping_contact_id = '" . $origContact['id'] . "'
                    WHERE shipping_contact_id = '" . $contact['id'] . "'
                ");
                $updateLocShipping = $fvmdb->query("
                    UPDATE locations
                    SET billing_contact_id = '" . $origContact['id'] . "'
                    WHERE billing_contact_id = '" . $contact['id'] . "'
                ");
                $updateLocShipping = $fvmdb->query("
                    UPDATE locations
                    SET site_contact_id = '" . $origContact['id'] . "'
                    WHERE site_contact_id = '" . $contact['id'] . "'
                ");
                $updateLocShipping = $fvmdb->query("
                    UPDATE locations
                    SET safety_director_contact_id = '" . $origContact['id'] . "'
                    WHERE safety_director_contact_id = '" . $contact['id'] . "'
                ");
                $deleteContact = $fvmdb->query("
                    DELETE FROM contacts
                    WHERE id = '" . $contact['id'] . "'
                ");
            }
        }
    }
}