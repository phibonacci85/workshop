<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 11/23/2016
 * Time: 08:41
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $fvmdb = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew_demo');
@ $crmdb = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicecrm');

$parts = $crmdb->query("
    SELECT *
    FROM vendor_parts
");
while ($part = $parts->fetch_assoc()) {
    if ($part['type'] == 'pad') {
        $types = $fvmdb->query("
            SELECT *
            FROM aed_pad_types
            WHERE partnum = '" . $part['part_number'] . "'
        ");
        if ($types->num_rows == 0) {
            echo $part['type'] . ' => ' . $part['part_number'] . '<br />';
        }
    } else if ($part['type'] == 'pak') {
        $types = $fvmdb->query("
            SELECT *
            FROM aed_pak_types
            WHERE partnum = '" . $part['part_number'] . "'
        ");
        if ($types->num_rows == 0) {
            echo $part['type'] . ' => ' . $part['part_number'] . '<br />';
        }
    } else if ($part['type'] == 'battery') {
        $types = $fvmdb->query("
            SELECT *
            FROM aed_battery_types
            WHERE partnum = '" . $part['part_number'] . "'
        ");
        if ($types->num_rows == 0) {
            echo $part['type'] . ' => ' . $part['part_number'] . '<br />';
        }
    } else if ($part['type'] == 'accessory') {
        $types = $fvmdb->query("
            SELECT *
            FROM aed_accessory_types
            WHERE partnum = '" . $part['part_number'] . "'
        ");
        if ($types->num_rows == 0) {
            echo $part['type'] . ' => ' . $part['part_number'] . '<br />';
        }
    }
}