<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 1/13/2017
 * Time: 11:30
 */

@ $db = new mysqli('fvdemos.com', 'fvdemos', 'Hawkeye17!', 'fvdemos_rescueone');


$organizations = $db->query("
    select *
    from organizations
");
while($organization = $organizations->fetch_assoc()) {
    $plans = $db->query("
        select *
        from plans p
        where org_id = '".$organization['id']."'
        order by str_to_date(p.expiration, '%m/%d/%Y') desc
        limit 1
    ");
    if($plan = $plans->fetch_assoc()) {
        echo $organization['id'] . ' - ' . $organization['name'] . ' => ' . $plan['plan_id'] . ' - ' . $plan['expiration'] . '<br />';
        $updateLocations = $db->query("
            update locations
            set plan_id = '".$plan['plan_id']."'
            where orgid = '".$organization['id']."'
        ");
    }
}
/*
$orgPlans = $db->query("
    SELECT p.plan_id, p.org_id, o.name AS organization, p.type, p.start_date, max(str_to_date(p.expiration, '%m/%d/%Y')), p.number_aeds, count(l.plan_id), p.display
    FROM plans p
      JOIN organizations o ON p.org_id = o.id
      JOIN locations l ON p.plan_id = l.plan_id
      WHERE p.org_id = '859'
    GROUP BY p.org_id
");
while ($orgPlan = $orgPlans->fetch_assoc()) {
    //echo '<pre>' . print_r($orgPlan, true) . '</pre>';

    $updateLocations = $db->query("
        update locations
        set plan_id = '".$orgPlan['plan_id']."'
        where orgid = '".$orgPlan['org_id']."'
    ");

}
*/