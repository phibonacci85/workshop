<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 2/2/2017
 * Time: 12:33
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $fvmdb = new mysqli($host, 'tsdemosc', 'Hawkeye17!', 'tsdemosc_fvmnew');

$fp = fopen('plans_report.csv', 'w');
fputcsv($fp, array('Organization', 'Plan', 'Start Date', 'Expiration Date', 'Part Number', 'Organization Mailing Email', 'Organization Billing Email', 'AED Check Alert Emails', 'Notes'));

$plans = $fvmdb->query("
    select o.id as org_id, o.name as organization, dp.display_name, str_to_date(p.start_date, '%m/%d/%Y') as start, max(str_to_date(p.expiration, '%m/%d/%Y')) as expiration, p.part_number, mailing.email as mailing_email, billing.email as billing_email, p.notes
    from plans p
      join organizations o on p.org_id = o.id
      join drop_plans dp on p.type = dp.name
      left join contacts mailing on o.main_mailing_contact_id = mailing.id
      left join contacts billing on o.main_billing_contact_id = billing.id
    where p.display = '1'
    group by p.org_id
");
while($plan = $plans->fetch_assoc()) {
    $alertEmails = '';
    $alerts = $fvmdb->query("
        select a.*
        from alerts a
        join locations l on a.locationid = l.id
        where l.orgid = '".$plan['org_id']."'
        and a.type = 'aedcheck'
        and a.display = 'yes'
    ");
    while($alert = $alerts->fetch_assoc()) {
        if($alert['email'] != '') {
            $alertEmails .= $alert['email'] . ', ';
        }
    }
    fputcsv($fp, array(
        $plan['organization'],
        $plan['display_name'],
        $plan['start'],
        $plan['expiration'],
        $plan['part_number'],
        $plan['mailing_email'],
        $plan['billing_email'],
        $alertEmails,
        $plan['notes']
    ));
}

fclose($fp);