<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 10/17/2016
 * Time: 10:44
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

//@ $fvmdb = new mysqli($host, 'tsdemosc', 'Hawkeye17!', 'tsdemosc_fvmnew');
@ $fvmdb = new mysqli('fvdemos.com', 'fvdemos', 'Hawkeye17!', 'fvdemos_rescueone_2017_1_6_copy');

$contacts = $fvmdb->query("
    SELECT *
    FROM contacts
");
while ($contact = $contacts->fetch_assoc()) {
    if (!empty($contact['s_name'])) {
        $insertContact = $fvmdb->query("
            INSERT INTO contacts (
              org_id, 
              name, 
              title, 
              company, 
              department, 
              contacttype, 
              preferredlanguage, 
              date, 
              phone, 
              fax, 
              cell, 
              receive_texts, 
              email, 
              address, 
              city, 
              province, 
              country, 
              zip, 
              notes, 
              sendaedcheck, 
              s_name, 
              s_title, 
              s_company, 
              s_department, 
              s_contacttype, 
              s_date, 
              s_phone, 
              s_fax, 
              s_cell, 
              s_email, 
              s_address, 
              s_city, 
              s_province, 
              s_country, 
              s_zip, 
              s_notes, 
              creation, 
              updated, 
              locationid, 
              display
            ) VALUES (
              '" . $contact['orgid'] . "', 
              '" . $contact['s_name'] . "', 
              '" . $contact['s_title'] . "', 
              '" . $contact['s_company'] . "', 
              '" . $contact['s_department'] . "', 
              '" . $contact['s_contacttype'] . "', 
              '" . $contact['preferredlanguage'] . "', 
              '" . $contact['s_date'] . "', 
              '" . $contact['s_phone'] . "', 
              '" . $contact['s_fax'] . "', 
              '" . $contact['s_cell'] . "', 
              '" . $contact['receive_texts'] . "', 
              '" . $contact['s_email'] . "', 
              '" . $contact['s_address'] . "', 
              '" . $contact['s_city'] . "', 
              '" . $contact['s_province'] . "', 
              '" . $contact['s_country'] . "', 
              '" . $contact['s_zip'] . "', 
              '" . $contact['s_notes'] . "', 
              '" . $contact['sendaedcheck'] . "',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '',
              '" . date('Y-m-d H:i:s') . "',
              '" . date('Y-m-d H:i:s') . "',
              '" . $contact['locationid'] . "',
              'yes'
            )
        ");
        $updateContact = $fvmdb->query("
            UPDATE contacts
            SET s_name = '',
              s_title = '',
              s_company = '',
              s_department = '',
              s_contacttype = '',
              s_date = '', 
              s_phone = '', 
              s_fax = '', 
              s_cell = '', 
              s_email = '', 
              s_address = '', 
              s_city = '', 
              s_province = '', 
              s_country = '', 
              s_zip = '', 
              s_notes = ''
            WHERE id = '" . $contact['id'] . "'
        ");
    }
}

$aeds = $fvmdb->query("
    SELECT a.*, l.orgid
    FROM aeds a
    JOIN locations l ON a.location_id = l.id
");
while ($aed = $aeds->fetch_assoc()) {
    if (!empty($aed['sitecoordinator'])) {
        $insertContact = $fvmdb->query("
            INSERT INTO contacts (
              org_id, 
              NAME, 
              title, 
              company, 
              department, 
              contacttype, 
              preferredlanguage, 
              date, 
              phone, 
              fax, 
              cell, 
              receive_texts, 
              email, 
              address, 
              city, 
              province, 
              country, 
              zip, 
              notes, 
              sendaedcheck, 
              s_name, 
              s_title, 
              s_company, 
              s_department, 
              s_contacttype, 
              s_date, 
              s_phone, 
              s_fax, 
              s_cell, 
              s_email, 
              s_address, 
              s_city, 
              s_province, 
              s_country, 
              s_zip, 
              s_notes, 
              creation, 
              updated, 
              locationid, 
              display
            ) VALUES (
              '" . $aed['orgid'] . "', 
              '" . $aed['sitecoordinator'] . "', 
              '', 
              '', 
              '', 
              'site_coordinator', 
              'en', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . $aed['location_id'] . "', 
              'yes'
            )
        ");
        $contactId = $fvmdb->insert_id;
        $updateAed = $fvmdb->query("
            UPDATE aeds
            SET site_coordinator_contact_id = '" . $contactId . "'
            WHERE aed_id = '" . $aed['aed_id'] . "'
        ");
    }
}

$organizations = $fvmdb->query("
    SELECT *
    FROM organizations
");
while ($organization = $organizations->fetch_assoc()) {
    if (!empty($organization['mainbillcontact'])) {
        $insertContact = $fvmdb->query("
            INSERT INTO contacts (
              org_id, 
              name, 
              title, 
              company, 
              department, 
              contacttype, 
              preferredlanguage, 
              date, 
              phone, 
              fax, 
              cell, 
              receive_texts, 
              email, 
              address, 
              city, 
              province, 
              country, 
              zip, 
              notes, 
              sendaedcheck, 
              s_name, 
              s_title, 
              s_company, 
              s_department, 
              s_contacttype, 
              s_date, 
              s_phone, 
              s_fax, 
              s_cell, 
              s_email, 
              s_address, 
              s_city, 
              s_province, 
              s_country, 
              s_zip, 
              s_notes, 
              creation, 
              updated, 
              locationid, 
              display
            ) VALUES (
              '" . $organization['id'] . "', 
              '" . $organization['mainbillcontact'] . "', 
              '" . $organization['mainbilltitle'] . "', 
              '', 
              '', 
              '', 
              'en', 
              '', 
              '" . $organization['mainbillphone'] . "', 
              '', 
              '', 
              '', 
              '" . $organization['mainbillemail'] . "', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '', 
              'yes'
            )
        ");
        $contactId = $fvmdb->insert_id;
        $updateOrg = $fvmdb->query("
            UPDATE organizations
            SET main_billing_contact_id = '" . $contactId . "'
            WHERE id = '" . $organization['id'] . "'
        ");
    }
    if (!empty($organization['mainmailcontact'])) {
        $insertContact = $fvmdb->query("
            INSERT INTO contacts (
              org_id, 
              name, 
              title, 
              company, 
              department, 
              contacttype, 
              preferredlanguage, 
              date, 
              phone, 
              fax, 
              cell, 
              receive_texts, 
              email, 
              address, 
              city, 
              province, 
              country, 
              zip, 
              notes, 
              sendaedcheck, 
              s_name, 
              s_title, 
              s_company, 
              s_department, 
              s_contacttype, 
              s_date, 
              s_phone, 
              s_fax, 
              s_cell, 
              s_email, 
              s_address, 
              s_city, 
              s_province, 
              s_country, 
              s_zip, 
              s_notes, 
              creation, 
              updated, 
              locationid, 
              display
            ) VALUES (
              '" . $organization['id'] . "', 
              '" . $organization['mainmailcontact'] . "', 
              '" . $organization['mainmailtitle'] . "', 
              '', 
              '', 
              '', 
              'en', 
              '', 
              '" . $organization['mainmailphone'] . "', 
              '', 
              '', 
              '', 
              '" . $organization['mainmailemail'] . "', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '', 
              'yes'
            )
        ");
        $contactId = $fvmdb->insert_id;
        $updateOrg = $fvmdb->query("
            UPDATE organizations
            SET main_mailing_contact_id = '" . $contactId . "'
            WHERE id = '" . $organization['id'] . "'
        ");
    }
}

$locations = $fvmdb->query("
    SELECT *
    FROM locations
");
while ($location = $locations->fetch_assoc()) {
    if (!empty($location['safetydirector'])) {
        $insertContact = $fvmdb->query("
            INSERT INTO contacts (
              org_id, 
              name, 
              title, 
              company, 
              department, 
              contacttype, 
              preferredlanguage, 
              date, 
              phone, 
              fax, 
              cell, 
              receive_texts, 
              email, 
              address, 
              city, 
              province, 
              country, 
              zip, 
              notes, 
              sendaedcheck, 
              s_name, 
              s_title, 
              s_company, 
              s_department, 
              s_contacttype, 
              s_date, 
              s_phone, 
              s_fax, 
              s_cell, 
              s_email, 
              s_address, 
              s_city, 
              s_province, 
              s_country, 
              s_zip, 
              s_notes, 
              creation, 
              updated, 
              locationid, 
              display
            ) VALUES (
              '" . $location['orgid'] . "', 
              '" . $location['safetydirector'] . "', 
              '', 
              '', 
              '', 
              '', 
              'en', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . $location['id'] . "', 
              'yes'
            )
        ");
        $contactId = $fvmdb->insert_id;
        $updateLocation = $fvmdb->query("
            UPDATE locations
            SET safety_director_contact_id = '" . $contactId . "'
            WHERE id = '" . $location['id'] . "'
        ");
    }
    if (!empty($location['sitecontact'])) {
        $insertContact = $fvmdb->query("
            INSERT INTO contacts (
              org_id, 
              name, 
              title, 
              company, 
              department, 
              contacttype, 
              preferredlanguage, 
              date, 
              phone, 
              fax, 
              cell, 
              receive_texts, 
              email, 
              address, 
              city, 
              province, 
              country, 
              zip, 
              notes, 
              sendaedcheck, 
              s_name, 
              s_title, 
              s_company, 
              s_department, 
              s_contacttype, 
              s_date, 
              s_phone, 
              s_fax, 
              s_cell, 
              s_email, 
              s_address, 
              s_city, 
              s_province, 
              s_country, 
              s_zip, 
              s_notes, 
              creation, 
              updated, 
              locationid, 
              display
            ) VALUES (
              '" . $location['orgid'] . "', 
              '" . $location['sitecontact'] . "', 
              '', 
              '', 
              '', 
              '', 
              'en', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . $location['id'] . "', 
              'yes'
            )
        ");
        $contactId = $fvmdb->insert_id;
        $updateLocation = $fvmdb->query("
            UPDATE locations
            SET site_contact_id = '" . $contactId . "'
            WHERE id = '" . $location['id'] . "'
        ");
    }
    if (!empty($location['shippingcontact'])) {
        $insertContact = $fvmdb->query("
            INSERT INTO contacts (
              org_id, 
              name, 
              title, 
              company, 
              department, 
              contacttype, 
              preferredlanguage, 
              date, 
              phone, 
              fax, 
              cell, 
              receive_texts, 
              email, 
              address, 
              city, 
              province, 
              country, 
              zip, 
              notes, 
              sendaedcheck, 
              s_name, 
              s_title, 
              s_company, 
              s_department, 
              s_contacttype, 
              s_date, 
              s_phone, 
              s_fax, 
              s_cell, 
              s_email, 
              s_address, 
              s_city, 
              s_province, 
              s_country, 
              s_zip, 
              s_notes, 
              creation, 
              updated, 
              locationid, 
              display
            ) VALUES (
              '" . $location['orgid'] . "', 
              '" . $location['shippingcontact'] . "', 
              '" . $location['shippingcontacttitle'] . "', 
              '', 
              '', 
              '', 
              'en', 
              '', 
              '" . $location['shippingcontactphone'] . "', 
              '" . $location['shippingcontactfax'] . "', 
              '', 
              '', 
              '" . $location['shippingcontactemail'] . "', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . $location['id'] . "', 
              'yes'
            )
        ");
        $contactId = $fvmdb->insert_id;
        $updateLocation = $fvmdb->query("
            UPDATE locations
            SET shipping_contact_id = '" . $contactId . "'
            WHERE id = '" . $location['id'] . "'
        ");
    }
    if (!empty($location['billingcontact'])) {
        $insertContact = $fvmdb->query("
            INSERT INTO contacts (
              org_id, 
              name, 
              title, 
              company, 
              department, 
              contacttype, 
              preferredlanguage, 
              date, 
              phone, 
              fax, 
              cell, 
              receive_texts, 
              email, 
              address, 
              city, 
              province, 
              country, 
              zip, 
              notes, 
              sendaedcheck, 
              s_name, 
              s_title, 
              s_company, 
              s_department, 
              s_contacttype, 
              s_date, 
              s_phone, 
              s_fax, 
              s_cell, 
              s_email, 
              s_address, 
              s_city, 
              s_province, 
              s_country, 
              s_zip, 
              s_notes, 
              creation, 
              updated, 
              locationid, 
              display
            ) VALUES (
              '" . $location['orgid'] . "', 
              '" . $location['billingcontact'] . "', 
              '" . $location['billingcontacttitle'] . "', 
              '', 
              '', 
              '', 
              'en', 
              '', 
              '" . $location['billingcontactphone'] . "', 
              '" . $location['billingcontactfax'] . "', 
              '', 
              '', 
              '" . $location['billingcontactemail'] . "', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . date('Y-m-d H:i:s') . "', 
              '" . $location['id'] . "', 
              'yes'
            )
        ");
        $contactId = $fvmdb->insert_id;
        $updateLocation = $fvmdb->query("
            UPDATE locations
            SET billing_contact_id = '" . $contactId . "'
            WHERE id = '" . $location['id'] . "'
        ");
    }
}