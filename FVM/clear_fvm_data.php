<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 5/10/2016
 * Time: 2:12 PM
 */

@ $db = new mysqli('tsdemos.com', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew');

$orgIds = [2976, 2979];
//$orgIds = [42];

foreach ($orgIds as $orgId) {
    $locations = $db->query("
        SELECT *
        FROM locations
        WHERE orgid = '" . $orgId . "'
    ");
    while ($location = $locations->fetch_assoc()) {
        $deleteAED = $db->query("
            DELETE FROM aeds
            WHERE location_id = '" . $location['id'] . "'
        ");
        if ($deleteAED) {
            echo 'Removed AEDs from Location: ' . $location['id'] . '<br />';
        }

        $deleteAlerts = $db->query("
            DELETE FROM alerts
            WHERE locationid = '" . $location['id'] . "'
        ");
        if ($deleteAlerts) {
            echo 'Removed Alerts from Location: ' . $location['id'] . '<br />';
        }

        $deleteContacts = $db->query("
            DELETE FROM contacts
            WHERE locationid = '" . $location['id'] . "'
        ");
        if ($deleteContacts) {
            echo 'Removed Contacts from Location: ' . $location['id'] . '<br />';
        }

        $deletePersons = $db->query("
            DELETE FROM persons
            WHERE locationid = '" . $location['id'] . "'
        ");
        if ($deletePersons) {
            echo 'Removed Persons from Location: ' . $location['id'] . '<br />';
        }

        $deleteTraining = $db->query("
            DELETE FROM training_personswithtypes
            WHERE locationid = '" . $location['id'] . "'
        ");
        if ($deleteTraining) {
            echo 'Removed Training from Location: ' . $location['id'] . '<br />';
        }
        echo '<br />';
    }
    $removeOrg = $db->query("
        DELETE FROM organizations
        WHERE id = '" . $orgId . "'
    ");
    $removeLocations = $db->query("
        DELETE FROM locations
        WHERE orgid = '" . $orgId . "'
    ");
    if ($removeOrg) {
        echo 'Removed Org: ' . $orgId . '<br />';
    }
    if ($removeLocations) {
        echo 'Removed Locations<br />';
    }

    $users = $db->query("
        SELECT *
        FROM rights_orgs
        WHERE orgid = '" . $orgId . "'
    ");
    while ($user = $users->fetch_assoc()) {
//        $deleteUser = $db->query("
//            DELETE FROM users
//            WHERE id = '" . $user['userid'] . "'
//        ");
//        if ($deleteUser) {
//            echo 'Removed User: ' . $user['userid'] . '<br />';
//        }

//        $deletePrivileges = $db->query("
//            DELETE FROM privileges
//            WHERE userid = '" . $user['userid'] . "'
//        ");
//        if ($deletePrivileges) {
//            echo 'Removed Privileges<br />';
//        }

        $deleteRightsLoc = $db->query("
            DELETE FROM rights_locations
            WHERE userid = '" . $user['userid'] . "'
        ");
        if ($deleteRightsLoc) {
            echo 'Removed Rights Location<br />';
        }
    }

    $deleteRightsOrg = $db->query("
        DELETE FROM rights_orgs
        WHERE orgid = '" . $orgId . "'
    ");
    if ($deleteRightsOrg) {
        echo 'Remeoved Rights Orgs<br />';
    }
    echo '<br />';
}
?>