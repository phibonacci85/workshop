<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 12/15/2016
 * Time: 11:20
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $fvmdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew_demo');

$start = 2835;
$end = $start;
for($orgId = $start; $orgId <= $end; $orgId++) {
    $organizations = $fvmdb->query("
    SELECT *
    FROM organizations
    WHERE id = '" . $orgId . "'
");
    if ($organization = $organizations->fetch_assoc()) {
        $locations = $fvmdb->query("
            SELECT *
            FROM locations
            WHERE orgid = '" . $organization['id'] . "'
        ");
        while ($location = $locations->fetch_assoc()) {
            $locationContacts = $fvmdb->query("
                SELECT *
                FROM location_contacts
                WHERE location_id = '" . $location['id'] . "'
            ");
            while ($locationContact = $locationContacts->fetch_assoc()) {
                $deleteContact = $fvmdb->query("
                    DELETE FROM contacts
                    WHERE id = '" . $locationContact['contact_id'] . "'
                ");
            }
            $deleteLocationContacts = $fvmdb->query("
                DELETE FROM location_contacts
                WHERE location_id = '" . $location['id'] . "'
            ");

            $aeds = $fvmdb->query("
                SELECT *
                FROM aeds
                WHERE location_id = '" . $location['id'] . "'
            ");
            while ($aed = $aeds->fetch_assoc()) {
                $deletePads = $fvmdb->query("
                    DELETE FROM aed_pads
                    WHERE aed_id = '" . $aed['aed_id'] . "'
                ");
                $deletePaks = $fvmdb->query("
                    DELETE FROM aed_paks
                    WHERE aed_id = '" . $aed['aed_id'] . "'
                ");
                $deleteAccessories = $fvmdb->query("
                    DELETE FROM aed_accessories
                    WHERE aed_id = '" . $aed['aed_id'] . "'
                ");
                $deleteBatteries = $fvmdb->query("
                    DELETE FROM aed_batteries
                    WHERE aed_id = '" . $aed['aed_id'] . "'
                ");
            }
            $deleteAeds = $fvmdb->query("
                DELETE FROM aeds
                WHERE location_id = '" . $location['id'] . "'
            ");
            $deleteAlerts = $fvmdb->query("
                DELETE FROM alerts
                WHERE locationid = '" . $location['id'] . "'
            ");
            $deleteLocation = $fvmdb->query("
                DELETE FROM locations
                WHERE id = '" . $location['id'] . "'
            ");
        }
        $deletePlan = $fvmdb->query("
            DELETE FROM plans
            WHERE org_id = '" . $organization['id'] . "'
        ");
        $deleteOrg = $fvmdb->query("
            DELETE FROM organizations
            WHERE id = '" . $organization['id'] . "'
        ");
    }
}