<?php
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

date_default_timezone_set('America/Chicago');

/** PHPExcel_IOFactory */
//require_once dirname(__FILE__) . './Classes/PHPExcel/IOFactory.php';
require_once('Classes/PHPExcel/IOFactory.php');

function geocode($address)
{
    $address = urlencode($address);
    $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";
    $resp_json = file_get_contents($url);
    $resp = json_decode($resp_json, true);
    if ($resp['status'] == 'OK') {
        $lat = $resp['results'][0]['geometry']['location']['lat'];
        $lon = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];
        if ($lat && $lon && $formatted_address) {
            $data_arr = array();
            array_push(
                $data_arr,
                $lat,
                $lon,
                $formatted_address,
                $resp['results'][0]['place_id']
            );
            return $data_arr;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

@ $db = new mysqli('tsdemos.com', 'tsdemosc', 'Hawkeye17!', 'tsdemosc_fvmnew');
//@ $db = new mysqli('tsdemos.com', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmbeta');

$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$xlxs = $objReader->load("./fvm_data.xlsx");

$orgs = array();
$uniqueUsers = array();

//Locations
$xlxs->setActiveSheetIndexByName('Locations');
$locationRows = $xlxs->getActiveSheet()->getRowIterator();
$xlxs->setActiveSheetIndexByName('Users');
$userRows = $xlxs->getActiveSheet()->getRowIterator();
$xlxs->setActiveSheetIndexByName('AED');
$aedRows = $xlxs->getActiveSheet()->getRowIterator();
$xlxs->setActiveSheetIndexByName('Alerts');
$alertRows = $xlxs->getActiveSheet()->getRowIterator();
$xlxs->setActiveSheetIndexByName('Contacts');
$contactRows = $xlxs->getActiveSheet()->getRowIterator();
$xlxs->setActiveSheetIndexByName('Training');
$trainingRows = $xlxs->getActiveSheet()->getRowIterator();

/***********************************************************************************************************************
 *                                                                                                                     *
 * INSERT ORGANIZATIONS AND LOCATIONS                                                                                  *
 *                                                                                                                     *
 **********************************************************************************************************************/
foreach ($locationRows as $location) {
    if ($location->getRowIndex() < 2) continue;
    $locInfo = array();
    foreach ($location->getCellIterator() as $cell) {
        if (!is_null($cell)) {
            $locCellVal = $cell->getCalculatedValue();
            if ($cell->getColumn() == 'A') {
                if (!array_key_exists($locCellVal, $orgs)) {
                    $insertOrg = $db->query("
                        INSERT INTO organizations
                        (name, type, status, creation, updated, display)
                        VALUES
                        ('" . $locCellVal . "', 'External', 'active', '" . date('Y-m-d H:i:s') . "', '" . date('Y-m-d H:i:s') . "', 'yes')
                    ");
                    if ($insertOrg) {
                        $orgId = $db->insert_id;
                        $orgs[$locCellVal] = array('orgid' => $orgId, 'locations' => array());
                        echo '<h3>Inserted Org: ' . $locCellVal . ' => ' . $orgId . '</h3><br />';
                    }
                }
                $locInfo['orgName'] = $locCellVal;
                $locInfo['orgId'] = $orgs[$locCellVal]['orgid'];
            } else if ($cell->getColumn() == 'B') {
                $locInfo['name'] = $locCellVal;
            } else if ($cell->getColumn() == 'C') {
                $locInfo['status'] = $locCellVal;
            } else if ($cell->getColumn() == 'D') {
                $locInfo['address'] = $locCellVal;
            } else if ($cell->getColumn() == 'E') {
                $locInfo['city'] = $locCellVal;
            } else if ($cell->getColumn() == 'F') {
                $locInfo['province'] = $locCellVal;
            } else if ($cell->getColumn() == 'G') {
                $locInfo['country'] = $locCellVal;
            } else if ($cell->getColumn() == 'H') {
                $locInfo['zip'] = $locCellVal;
            } else if ($cell->getColumn() == 'I') {
                $locInfo['phone'] = $locCellVal;
            } else if ($cell->getColumn() == 'J') {
                $locInfo['fax'] = $locCellVal;
            } else if ($cell->getColumn() == 'K') {
                $locInfo['website'] = $locCellVal;
            } else if ($cell->getColumn() == 'L') {
                $locInfo['safetydirector'] = $locCellVal;
            } else if ($cell->getColumn() == 'M') {
                $locInfo['sitecontact'] = $locCellVal;
            } else if ($cell->getColumn() == 'N') {
                $locInfo['main'] = $locCellVal;
            }
        }
    }
    $lat = "";
    $lng = "";
    $data_arr = geocode($locInfo['address'] . ', ' . $locInfo['city'] . ', ' . $locInfo['province'] . ' ' . $locInfo['zip']);
    if ($data_arr) {
        $lat = $data_arr[0];
        $lng = $data_arr[1];
    }

    $insertLocation = $db->query("
        INSERT INTO locations
        (orgid, name, status, address, city, province, country, zip, phone, fax, website, safetydirector, sitecontact, main, creation, latitude, longitude, display)
        VALUES(
          '" . $locInfo['orgId'] . "', 
          '" . $locInfo['name'] . "',
          '" . $locInfo['status'] . "',
          '" . $locInfo['address'] . "',
          '" . $locInfo['city'] . "',
          '" . $locInfo['province'] . "',
          '" . $locInfo['country'] . "',
          '" . $locInfo['zip'] . "',
          '" . $locInfo['phone'] . "',
          '" . $locInfo['fax'] . "',
          '" . $locInfo['website'] . "',
          '" . $locInfo['safetydirector'] . "',
          '" . $locInfo['sitecontact'] . "',
          '" . $locInfo['main'] . "', 
          '" . date('Y-m-d H:i:s') . "', 
          '" . $lat . "', 
          '" . $lng . "', 
          'yes'
        )
    ");
    $locId = $db->insert_id;
    if ($insertLocation) {
        echo 'Inserted Location ' . $locId . '<br />';
    }
    $orgs[$locInfo['orgName']]['locations'][$locInfo['name']]['id'] = $locId;
    $orgs[$locInfo['orgName']]['locations'][$locInfo['name']]['lat'] = $lat;
    $orgs[$locInfo['orgName']]['locations'][$locInfo['name']]['lng'] = $lng;

    /* Global Alerts - Adds an alert for each location being added */
    /*
    $insertGlobalAlert = $db->query("
        INSERT INTO alerts
        (locationid, email, level, type, language, display)
        VALUES (
          '" . $orgs[$locInfo['orgName']]['locations'][$locInfo['name']]['id'] . "',
          'rrapsey@rescue-one.com', 
          '3', 
          'aed', 
          'en', 
          'yes'
        )
    ");
    */
}
//echo '<pre>'.print_r($orgs, true).'</pre>';

/***********************************************************************************************************************
 *                                                                                                                     *
 * INSERT USERS                                                                                                        *
 *                                                                                                                     *
 **********************************************************************************************************************/
foreach ($userRows as $user) {
    if ($user->getRowIndex() < 2) continue;
    $userInfo = array();
    foreach ($user->getCellIterator() as $cell) {
        if (!is_null($cell)) {
            if ($cell->getColumn() == 'A') {
                $userInfo['orgName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'B') {
                $userInfo['locName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'C') {
                $userInfo['username'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'D') {
                $userInfo['firstname'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'E') {
                $userInfo['lastname'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'F') {
                $userInfo['phone'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'G') {
                $userInfo['company'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'H') {
                $userInfo['preferredlanguage'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'I') {
                $userInfo['address'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'J') {
                $userInfo['city'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'K') {
                $userInfo['province'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'L') {
                $userInfo['country'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'M') {
                $userInfo['zip'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'N') {
                $userInfo['firstlogin'] = $cell->getCalculatedValue();
            }
        }
    }

    if (!array_key_exists($userInfo['username'], $uniqueUsers)) {
        $insertUser = $db->query("
            INSERT INTO users
            (username, password, firstname, lastname, phone, company, preferredlanguage, address, city, province, country, zip, firstlogin, creation, display)
            VALUES(
              '" . $userInfo['username'] . "', 
              PASSWORD('occ1'), 
              '" . $userInfo['firstname'] . "', 
              '" . $userInfo['lastname'] . "', 
              '" . $userInfo['phone'] . "', 
              '" . $userInfo['company'] . "', 
              '" . $userInfo['preferredlanguage'] . "', 
              '" . $userInfo['address'] . "', 
              '" . $userInfo['city'] . "', 
              '" . $userInfo['province'] . "', 
              '" . $userInfo['country'] . "', 
              '" . $userInfo['zip'] . "', 
              '" . $userInfo['firstlogin'] . "', 
              '" . date('Y-m-d H:i:s') . "', 
              'yes'
            )
        ");
        if ($insertUser) {
            $userId = $db->insert_id;
            $uniqueUsers[$userInfo['username']] = $userId;
            echo '<h3>Inserted User: ' . $userInfo['username'] . '</h3>';

            $insertPrivileges = $db->query("
                INSERT INTO privileges
                (userid, showaed, showerp, showmedicaldirection, showevents, showrecalls, showstatelaws, showequipment, showpersons, showtraining, showdocuments, showhazard, showcontact, showaedcheck, newaedcheck, editaedcheck, showaedservicing)
                VALUES 
                ('" . $uniqueUsers[$userInfo['username']] . "', 'yes', 'yes', 'yes', 'yes', 'yes', '1', 'yes', '1', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes', '1')
            ");
            if ($insertPrivileges) {
                echo 'Inserted Privileges<br />';
            }

            $insertRightsOrg = $db->query("
                INSERT INTO rights_orgs
                (userid, orgid, level, display, creation)
                VALUES(
                  '" . $uniqueUsers[$userInfo['username']] . "', 
                  '" . $orgs[$userInfo['orgName']]['orgid'] . "', 
                  '3', 
                  '1', 
                  '" . date('Y-m-d H:i:s') . "'
                )
            ");
            if ($insertRightsOrg) {
                echo 'Inserted Rights Org<br />';
            }
        }
    }

    $insertRightsLoc = $db->query("
        INSERT INTO rights_locations
        (userid, orgid, locationid, display, creation)
        VALUES(
          '" . $uniqueUsers[$userInfo['username']] . "', 
          '" . $orgs[$userInfo['orgName']]['orgid'] . "', 
          '" . $orgs[$userInfo['orgName']]['locations'][$userInfo['locName']]['id'] . "', 
          '1', 
          '" . date('Y-m-d H:i:s') . "'
        )
    ");
    if ($insertRightsLoc) {
        echo 'Inserted Rights Loc<br />';
    }
}

/***********************************************************************************************************************
 *                                                                                                                     *
 * INSERT AEDs                                                                                                        *
 *                                                                                                                     *
 **********************************************************************************************************************/
foreach ($aedRows as $aed) {
    if ($aed->getRowIndex() < 3) continue;
    $aedInfo = array();
    foreach ($aed->getCellIterator() as $cell) {
        if (!is_null($cell)) {
            if ($cell->getColumn() == 'A') {
                $aedInfo['orgName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'B') {
                $aedInfo['locName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'C') {
                $aedInfo['updated'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'D') {
                $aedInfo['brand'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'E') {
                $aedInfo['model'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'F') {
                $aedInfo['serialnumber'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'G') {
                $aedInfo['installdate'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'H') {
                $aedInfo['sc_name'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'I') {
                $aedInfo['sc_email'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'J') {
                $aedInfo['purchase_type'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'K') {
                $aedInfo['purchase_date'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'L') {
                $aedInfo['warranty_expires'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'M') {
                $aedInfo['location_placement'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'N') {
                $aedInfo['plan_type'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'O') {
                $aedInfo['plan_date'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'P') {
                $aedInfo['plan_exp'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'Q') {
                $aedInfo['in_service'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'R') {
                $aedInfo['has_servicing_check'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'S') {
                $aedInfo['servicing_length'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'T') {
                $aedInfo['last_check'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'W') {
                $aedInfo['main_pad_model'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'X') {
                $aedInfo['main_pad_lot'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'Y') {
                $aedInfo['main_pad_expiration'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'Z') {
                $aedInfo['main_battery_model'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AA') {
                $aedInfo['main_battery_lot'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AB') {
                $aedInfo['main_battery_expiration'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AC') {
                $aedInfo['main_pak_model'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AD') {
                $aedInfo['main_pak_lot'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AE') {
                $aedInfo['main_pak_expiration'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AF') {
                $aedInfo['spare_pad_model'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AG') {
                $aedInfo['spare_pad_lot'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AH') {
                $aedInfo['spare_pad_expiration'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AI') {
                $aedInfo['ped_pad_model'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AJ') {
                $aedInfo['ped_pad_lot'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AK') {
                $aedInfo['ped_pad_expiration'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AL') {
                $aedInfo['spare_battery_model'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AM') {
                $aedInfo['spare_battery_lot'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AN') {
                $aedInfo['spare_battery_expiration'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AO') {
                $aedInfo['spare_pak_model'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AP') {
                $aedInfo['spare_pak_lot'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'AQ') {
                $aedInfo['spare_pak_expiration'] = $cell->getFormattedValue();
            }
        }
    }

    $modelId = '0';
    $brand_models = $db->query("
        SELECT m.*
        FROM aed_brands b, aed_models m
        WHERE b.aed_brand_id = m.aed_brand_id
        AND model = '" . $aedInfo['model'] . "'
        AND brand = '" . $aedInfo['brand'] . "'
    ");
    if ($brand_model = $brand_models->fetch_assoc()) {
        $modelId = $brand_model['aed_model_id'];
    }
    $inService = '0';
    if ($aedInfo['in_service'] == 'Y') $inService = '1';
    $hasServCheck = '0';
    if ($aedInfo['has_servicing_check'] == 'Y') $hasServCheck = '1';
    $plantype = 'medicaldirection';
    if ($aedInfo['plan_type'] == 'Type 3') $planType = 'medicaldirection';
    $insertAED = $db->query("
        INSERT INTO aeds
        (aed_model_id, serialnumber, installdate, sitecoordinator, purchasetype, purchasedate, warranty, location, plantype, plandate, planexpiration, current, hasservicecheck, lastcheck, location_id, latitude, longitude, creation, display)
        VALUES (
          '" . $modelId . "', 
          '" . $aedInfo['serialnumber'] . "', 
          '" . $aedInfo['installdate'] . "', 
          '" . $aedInfo['sc_name'] . "', 
          '" . $aedInfo['purchase_type'] . "', 
          '" . $aedInfo['purchase_date'] . "', 
          '" . $aedInfo['warranty_expires'] . "', 
          '" . $aedInfo['location_placement'] . "', 
          '" . $planType . "', 
          '" . $aedInfo['plan_date'] . "', 
          '" . $aedInfo['plan_exp'] . "', 
          '" . $inService . "', 
          '" . $hasServCheck . "', 
          '" . $aedInfo['last_check'] . "', 
          '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['id'] . "', 
          '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['lat'] . "', 
          '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['lng'] . "', 
          '" . date('Y-m-d H:i:s') . "', 
          '1'
        )
    ");
    if ($insertAED) {
        echo 'Inserted AED: ' . $aedInfo['serialnumber'] . '<br />';
        $aedId = $db->insert_id;
        if ($aedInfo['main_pad_model'] != '' || $aedInfo['main_pad_lot'] != '' || $aedInfo['main_pad_expiration'] != '') {
            if ($aedInfo['main_pad_model'] == '') {
                $types = $db->query("
                    SELECT *
                    FROM aed_pad_types_on_models tom
                    JOIN aed_pad_types t on tom.aed_pad_type_id = t.aed_pad_type_id
                    WHERE tom.aed_model_id = '" . $modelId . "'
                    AND t.pediatric = '0'
                ");
                if ($type = $types->fetch_assoc()) {
                    $aedInfo['main_pad_model'] = $type['aed_pad_type_id'];
                }
            }
            $insertPad = $db->query("
                INSERT INTO aed_pads (
                  location_id, 
                  aed_id, 
                  aed_pad_type_id, 
                  expiration, 
                  lot, 
                  udi, 
                  spare, 
                  display, 
                  updated
                ) VALUES (
                  '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['id'] . "', 
                  '" . $aedId . "', 
                  '" . $aedInfo['main_pad_model'] . "', 
                  '" . $aedInfo['main_pad_expiration'] . "', 
                  '" . $aedInfo['main_pad_lot'] . "', 
                  '', 
                  '0', 
                  '1', 
                  '" . date('Y-m-d H:i:s') . "'
                )
            ");
        }
        if ($aedInfo['main_battery_model'] != '' || $aedInfo['main_battery_lot'] != '' || $aedInfo['main_battery_expiration'] != '') {
            if ($aedInfo['main_battery_model'] == '') {
                $types = $db->query("
                    SELECT *
                    FROM aed_battery_types_on_models tom
                    WHERE tom.aed_model_id = '" . $modelId . "'
                ");
                if ($type = $types->fetch_assoc()) {
                    $aedInfo['main_battery_model'] = $type['aed_battery_type_id'];
                }
            }
            $insertBattery = $db->query("
                INSERT INTO aed_batteries (
                  location_id, 
                  aed_id, 
                  aed_battery_type_id, 
                  expiration, 
                  lot, 
                  udi, 
                  spare, 
                  display, 
                  updated
                ) VALUES (
                  '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['id'] . "', 
                  '" . $aedId . "', 
                  '" . $aedInfo['main_battery_model'] . "', 
                  '" . $aedInfo['main_battery_expiration'] . "', 
                  '" . $aedInfo['main_battery_lot'] . "', 
                  '', 
                  '0', 
                  '1', 
                  '" . date('Y-m-d H:i:s') . "'
                )
            ");
        }
        if ($aedInfo['main_pak_model'] != '' || $aedInfo['main_pak_lot'] != '' || $aedInfo['main_pak_expiration'] != '') {
            if ($aedInfo['main_pak_model'] == '') {
                $types = $db->query("
                    SELECT *
                    FROM aed_pak_types_on_models tom
                    JOIN aed_pak_types t on tom.aed_pak_type_id = t.aed_pak_type_id
                    WHERE tom.aed_model_id = '" . $modelId . "'
                    AND t.pediatric = '0'
                ");
                if ($type = $types->fetch_assoc()) {
                    $aedInfo['main_pak_model'] = $type['aed_pak_type_id'];
                }
            }
            $insertPak = $db->query("
                INSERT INTO aed_paks (
                  location_id, 
                  aed_id, 
                  aed_pak_type_id, 
                  expiration, 
                  lot, 
                  udi, 
                  spare, 
                  display, 
                  updated
                ) VALUES (
                  '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['id'] . "', 
                  '" . $aedId . "', 
                  '" . $aedInfo['main_pak_model'] . "', 
                  '" . $aedInfo['main_pak_expiration'] . "', 
                  '" . $aedInfo['main_pak_lot'] . "', 
                  '', 
                  '0', 
                  '1', 
                  '" . date('Y-m-d H:i:s') . "'
                )
            ");
        }
        if ($aedInfo['spare_pad_model'] != '' || $aedInfo['spare_pad_lot'] != '' || $aedInfo['spare_pad_expiration'] != '') {
            if ($aedInfo['spare_pad_model'] == '') {
                $types = $db->query("
                    SELECT *
                    FROM aed_pad_types_on_models tom
                    JOIN aed_pad_types t on tom.aed_pad_type_id = t.aed_pad_type_id
                    WHERE tom.aed_model_id = '" . $modelId . "'
                    AND t.pediatric = '0'
                ");
                if ($type = $types->fetch_assoc()) {
                    $aedInfo['spare_pad_model'] = $type['aed_pad_type_id'];
                }
            }
            $insertPad = $db->query("
                INSERT INTO aed_pads (
                  location_id, 
                  aed_id, 
                  aed_pad_type_id, 
                  expiration, 
                  lot, 
                  udi, 
                  spare, 
                  display, 
                  updated
                ) VALUES (
                  '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['id'] . "', 
                  '" . $aedId . "', 
                  '" . $aedInfo['spare_pad_model'] . "', 
                  '" . $aedInfo['spare_pad_expiration'] . "', 
                  '" . $aedInfo['spare_pad_lot'] . "', 
                  '', 
                  '1', 
                  '1', 
                  '" . date('Y-m-d H:i:s') . "'
                )
            ");
        }
        if ($aedInfo['ped_pad_model'] != '' || $aedInfo['ped_pad_lot'] != '' || $aedInfo['ped_pad_expiration'] != '') {
            if ($aedInfo['ped_pad_model'] == '') {
                $types = $db->query("
                    SELECT *
                    FROM aed_pad_types_on_models tom
                    JOIN aed_pad_types t on tom.aed_pad_type_id = t.aed_pad_type_id
                    WHERE tom.aed_model_id = '" . $modelId . "'
                    AND t.pediatric = '1'
                ");
                if ($type = $types->fetch_assoc()) {
                    $aedInfo['ped_pad_model'] = $type['aed_pad_type_id'];
                }
            }
            $insertPad = $db->query("
                INSERT INTO aed_pads (
                  location_id, 
                  aed_id, 
                  aed_pad_type_id, 
                  expiration, 
                  lot, 
                  udi, 
                  spare, 
                  display, 
                  updated
                ) VALUES (
                  '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['id'] . "', 
                  '" . $aedId . "', 
                  '" . $aedInfo['ped_pad_model'] . "', 
                  '" . $aedInfo['ped_pad_expiration'] . "', 
                  '" . $aedInfo['ped_pad_lot'] . "', 
                  '', 
                  '0', 
                  '1', 
                  '" . date('Y-m-d H:i:s') . "'
                )
            ");
        }
        if ($aedInfo['spare_battery_model'] != '' || $aedInfo['spare_battery_lot'] != '' || $aedInfo['spare_battery_expiration'] != '') {
            if ($aedInfo['spare_battery_model'] == '') {
                $types = $db->query("
                    SELECT *
                    FROM aed_battery_types_on_models tom
                    WHERE tom.aed_model_id = '" . $modelId . "'
                ");
                if ($type = $types->fetch_assoc()) {
                    $aedInfo['spare_battery_model'] = $type['aed_battery_type_id'];
                }
            }
            $insertBattery = $db->query("
                INSERT INTO aed_batteries (
                  location_id, 
                  aed_id, 
                  aed_battery_type_id, 
                  expiration, 
                  lot, 
                  udi, 
                  spare, 
                  display, 
                  updated
                ) VALUES (
                  '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['id'] . "', 
                  '" . $aedId . "', 
                  '" . $aedInfo['spare_battery_model'] . "', 
                  '" . $aedInfo['spare_battery_expiration'] . "', 
                  '" . $aedInfo['spare_battery_lot'] . "', 
                  '', 
                  '1', 
                  '1', 
                  '" . date('Y-m-d H:i:s') . "'
                )
            ");
        }
        if ($aedInfo['spare_pak_model'] != '' || $aedInfo['spare_pak_lot'] != '' || $aedInfo['spare_pak_expiration'] != '') {
            if ($aedInfo['spare_pak_model'] == '') {
                $types = $db->query("
                    SELECT *
                    FROM aed_pak_types_on_models tom
                    JOIN aed_pak_types t on tom.aed_pak_type_id = t.aed_pak_type_id
                    WHERE tom.aed_model_id = '" . $modelId . "'
                    AND t.pediatric = '0'
                ");
                if ($type = $types->fetch_assoc()) {
                    $aedInfo['spare_pak_model'] = $type['aed_pak_type_id'];
                }
            }
            $insertPak = $db->query("
                INSERT INTO aed_paks (
                  location_id, 
                  aed_id, 
                  aed_pak_type_id, 
                  expiration, 
                  lot, 
                  udi, 
                  spare, 
                  display, 
                  updated
                ) VALUES (
                  '" . $orgs[$aedInfo['orgName']]['locations'][$aedInfo['locName']]['id'] . "', 
                  '" . $aedId . "', 
                  '" . $aedInfo['spare_pak_model'] . "', 
                  '" . $aedInfo['spare_pak_expiration'] . "', 
                  '" . $aedInfo['spare_pak_lot'] . "', 
                  '', 
                  '1', 
                  '1', 
                  '" . date('Y-m-d H:i:s') . "'
                )
            ");
        }
    }
}

/***********************************************************************************************************************
 *                                                                                                                     *
 * INSERT Alerts                                                                                                       *
 *                                                                                                                     *
 **********************************************************************************************************************/
foreach ($alertRows as $alert) {
    if ($alert->getRowIndex() < 2) continue;
    $alertInfo = array();
    foreach ($alert->getCellIterator() as $cell) {
        if (!is_null($cell)) {
            if ($cell->getColumn() == 'A') {
                $alertInfo['orgName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'B') {
                $alertInfo['locName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'C') {
                $alertInfo['email'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'D') {
                $alertInfo['level'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'E') {
                $alertInfo['type'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'F') {
                $alertInfo['language'] = $cell->getCalculatedValue();
            }
        }
    }
    $testLocationId = $orgs[$alertInfo['orgName']]['locations'][$alertInfo['locName']]['id'];
    if ($testLocationId == 2193) {
        $DEBUG = true;
    }

    if (strpos($alertInfo['type'], 'AED Pads and Batteries') > 0) {
        $insertAlertAED = $db->query("
            INSERT INTO alerts
            (locationid, email, level, type, language, display)
            VALUES (
              '" . $orgs[$alertInfo['orgName']]['locations'][$alertInfo['locName']]['id'] . "', 
              '" . $alertInfo['email'] . "', 
              '" . $alertInfo['level'] . "', 
              'aed', 
              'en', 
              'yes'
            )
        ");
    }
    if (strpos($alertInfo['type'], 'Checks') > 0) {
        $insertAlertAEDCheck = $db->query("
            INSERT INTO alerts
            (locationid, email, level, type, language, display)
            VALUES (
              '" . $orgs[$alertInfo['orgName']]['locations'][$alertInfo['locName']]['id'] . "', 
              '" . $alertInfo['email'] . "', 
              '" . $alertInfo['level'] . "', 
              'aedcheck', 
              'en', 
              'yes'
            )
        ");
    }
    if (strpos($alertInfo['type'], 'Training') > 0) {
        $insertAlertTraining = $db->query("
            INSERT INTO alerts
            (locationid, email, level, type, language, display)
            VALUES (
              '" . $orgs[$alertInfo['orgName']]['locations'][$alertInfo['locName']]['id'] . "', 
              '" . $alertInfo['email'] . "', 
              '" . $alertInfo['level'] . "', 
              'training', 
              'en', 
              'yes'
            )
        ");
    }
    if ($insertAlertAED && $insertAlertAEDCheck && $insertAlertTraining) {
        echo 'Inserted Alerts, Location ID: ' . $orgs[$alertInfo['orgName']]['locations'][$alertInfo['locName']]['id'] . '<br />';
    }
}

/***********************************************************************************************************************
 *                                                                                                                     *
 * INSERT Contacts                                                                                                     *
 *                                                                                                                     *
 **********************************************************************************************************************/
foreach ($contactRows as $contact) {
    if ($contact->getRowIndex() < 2) continue;
    $contactInfo = array();
    foreach ($contact->getCellIterator() as $cell) {
        if (!is_null($cell)) {
            if ($cell->getColumn() == 'A') {
                $contactInfo['orgName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'B') {
                $contactInfo['locName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'C') {
                $contactInfo['name'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'D') {
                $contactInfo['title'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'E') {
                $contactInfo['company'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'F') {
                $contactInfo['department'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'G') {
                $contactInfo['contacttype'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'H') {
                $contactInfo['preferredlanguage'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'I') {
                $contactInfo['date'] = $cell->getFormattedValue();
            } else if ($cell->getColumn() == 'J') {
                $contactInfo['phone'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'K') {
                $contactInfo['fax'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'L') {
                $contactInfo['cell'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'M') {
                $contactInfo['email'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'N') {
                $contactInfo['address'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'O') {
                $contactInfo['city'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'P') {
                $contactInfo['province'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'Q') {
                $contactInfo['country'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'R') {
                $contactInfo['zip'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'S') {
                $contactInfo['notes'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'T') {
                $contactInfo['sendaedcheck'] = $cell->getCalculatedValue();
            }
        }
    }

    $sendAEDCheck = '0';
    if ($contactInfo['sendaedcheck'] == 'y') $sendAEDCheck = '1';
    $contactType = 'aedcoordinator';
    if ($contactInfo['contacttype'] == 'Site Coordinator') $contactType = 'aedcoordinator';
    $insertContact = $db->query("
        INSERT INTO contacts
        (name, title, company, department, contacttype, preferredlanguage, date, phone, fax, cell, email, address, city, province, country, zip, notes, sendaedcheck, creation, locationid, display)
        VALUES (
          '" . $contactInfo['name'] . "', 
          '" . $contactInfo['title'] . "', 
          '" . $contactInfo['company'] . "', 
          '" . $contactInfo['department'] . "', 
          '" . $contactType . "', 
          'en', 
          '" . $contactInfo['date'] . "', 
          '" . $contactInfo['phone'] . "', 
          '" . $contactInfo['fax'] . "', 
          '" . $contactInfo['cell'] . "', 
          '" . $contactInfo['email'] . "', 
          '" . $contactInfo['address'] . "', 
          '" . $contactInfo['city'] . "', 
          '" . $contactInfo['province'] . "', 
          '" . $contactInfo['country'] . "', 
          '" . $contactInfo['zip'] . "', 
          '" . $contactInfo['notes'] . "',
          '" . $sendAEDCheck . "', 
          '" . date('Y-m-d H:i:s') . "', 
          '" . $orgs[$contactInfo['orgName']]['locations'][$contactInfo['locName']]['id'] . "', 
          'yes'
        )
    ");
    if ($insertContact) {
        echo 'Contact Inserted: ' . $contactInfo['name'] . '<br />';
    }
}
/***********************************************************************************************************************
 *                                                                                                                     *
 * INSERT Training                                                                                                     *
 *                                                                                                                     *
 **********************************************************************************************************************/
foreach ($trainingRows as $training) {
    if ($training->getRowIndex() < 2) continue;
    $trainingInfo = array();
    foreach ($training->getCellIterator() as $cell) {
        if (!is_null($cell)) {
            if ($cell->getColumn() == 'A') {
                $trainingInfo['orgName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'B') {
                $trainingInfo['locName'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'C') {
                $trainingInfo['employeeid'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'D') {
                $trainingInfo['firstname'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'E') {
                $trainingInfo['lastname'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'F') {
                $trainingInfo['address'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'G') {
                $trainingInfo['email'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'H') {
                $trainingInfo['phone'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'I') {
                $trainingInfo['city'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'J') {
                $trainingInfo['state'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'K') {
                $trainingInfo['zip'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'L') {
                $trainingInfo['firstvoicetraining_username'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'W') {
                $trainingInfo['course'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'X') {
                $trainingInfo['classnumber'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'Y') {
                $trainingInfo['status'] = $cell->getCalculatedValue();
            } else if ($cell->getColumn() == 'AC') {
                $trainingInfo['completiondate'] = $cell->getFormattedValue();
            }
        }
    }

    $insertPerson = $db->query("
        INSERT INTO persons
        (firstname, lastname, address, email, phone, city, state, zip, firstvoicetraining_username, current, creation, locationid, display)
        VALUES (
          '" . $trainingInfo['firstname'] . "', 
          '" . $trainingInfo['lastname'] . "', 
          '" . $trainingInfo['address'] . "', 
          '" . $trainingInfo['email'] . "', 
          '" . $trainingInfo['phone'] . "', 
          '" . $trainingInfo['city'] . "', 
          '" . $trainingInfo['state'] . "', 
          '" . $trainingInfo['zip'] . "', 
          '" . $trainingInfo['firstvoicetraining_username'] . "', 
          'yes', 
          '" . date('Y-m-d H:i:s') . "',
          '" . $orgs[$trainingInfo['orgName']]['locations'][$trainingInfo['locName']]['id'] . "', 
          'yes'
        )
    ");

    $expiration = explode('/', $trainingInfo['completiondate']);
    $expiration[2] += 2;
    $expiration = $expiration[0] . '/' . $expiration[1] . '/' . $expiration[2];

    $typeId = '47';

    $personId = $db->insert_id;
    if ($insertPerson) {
        echo 'Inserted Person: ' . $personId . '<br />';

        $insertTraining = $db->query("
            INSERT INTO training_personswithtypes
            (personid, typeid, status, completiondate, expiration, creation, display)
            VALUES (
              '" . $personId . "', 
              '" . $typeId . "', 
              '" . $trainingInfo['status'] . "', 
              '" . $trainingInfo['completiondate'] . "', 
              '" . $expiration . "', 
              '" . date('Y-m-d H:i:s') . "', 
              'yes'
            )
        ");
        $trainingId = $db->insert_id;
        if ($insertTraining) {
            echo 'Inserted Training: ' . $trainingId . '<br />';
        }
    }
}
?>