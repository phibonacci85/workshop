<?php
require('../swiftmailer/swift_required.php');

if (($handle = fopen("emails.csv", "r")) !== FALSE) {
    $line = 0;
    while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
        $to = $data[0];
        echo $to.'<br />';
        $trainingExpirationsEmail = '
<html><head></head><body style="color:#000001;">
                <div style="margin:0 auto;width:970px;background-color:#C2E0F6;color:#000001;padding:10px 0">
                    <div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid #0078C1;margin: 5px auto;">
                        <h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
                            <img alt="First Voice Manager" src="http://firstvoicemanager.com/images/FVMLogo.png">
                        </h1>
                        <div style="width:100%;height:30px;margin-bottom:5px;background-color:#0078C1;float:left; margin-top:5px;"></div>
                        <h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
                            Training Expirations
                        </h2>
                        <br />
                        <div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
                            Sorry for the inconvenience. Please disregard the email you received on 2-9-17 about being 736766 days past due, this was caused by having a blank expiration date for your certification course. This problem has been fixed in the system and your expiration reminder was invalid.  Thank you for your understanding and patience in this matter.
                        </div>
                        <br /><br />
                    </div>
                </div>
                <div style="width:970px;margin:0 auto;color:#000001;background-color:white;">
                    Please do not reply to this email. We are unable to respond to inquiries sent to this address.
                </div>
                <br />
                <div style="width:970px;margin:0 auto;color:#000001;">
                    Copyright &copy; 2013 Think Safe Inc.
                </div>
            </body></html>
 ';

        $transport = Swift_SmtpTransport::newInstance('smtp.tsdemos.com', 25)
            ->setUsername('donotreply@firstvoicemanager.com')
            ->setPassword('1105firstvoice');
        $mailer = Swift_Mailer::newInstance($transport);
        $message = Swift_Message::newInstance()
            ->setSubject('Training Expirations')
            ->setFrom(array('donotreply@firstvoicemanager.com' => 'First Voice Manager'))
            ->setTo($to)
            ->setBody($trainingExpirationsEmail, 'text/html');
        $result = $mailer->send($message);
        $line++;
    }
    fclose($handle);
} else {
    echo 'Error opening file';
}
?>