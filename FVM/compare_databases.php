<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 12/30/2016
 * Time: 13:48
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

$fvmDBString = 'tsdemosc_tcm';
@ $fvmdb = new mysqli($host, 'tsdemosc', 'Hawkeye17!', $fvmDBString);
@ $comparedb = new mysqli('tsdemos.com', 'tsdemosc', 'Hawkeye17!', 'tsdemosc_tcm_demo');
//@ $comparedb = new mysqli($host, 'tsdemosc', 'Hawkeye17!', 'tsdemosc_fvmnew');

$tables = $fvmdb->query("
    show tables
");
while ($table = $tables->fetch_assoc()) {
    //echo '<pre>'.print_r($table, true).'</pre>';
    //echo $table['Tables_in_'.$fvmDBString].'<br />';
    $tableExists = $comparedb->query("
        show tables like '" . $table['Tables_in_' . $fvmDBString] . "'
    ");
    if ($tableExists->num_rows == 0) {
        echo 'MISSING TABLE: ' . $table['Tables_in_' . $fvmDBString] . '<br />';
    } else {
        $columns = $fvmdb->query("
            show columns from " . $table['Tables_in_' . $fvmDBString] . "
        ");
        while ($column = $columns->fetch_assoc()) {
            //echo '<pre>' . print_r($column, true) . '</pre>';
            //echo $column['Field'].'<br />';
            $columnExists = $comparedb->query("
                show columns from " . $table['Tables_in_' . $fvmDBString] . "
                where Field like '" . $column['Field'] . "'
            ");
            if ($columnExists->num_rows == 0) {
                echo 'MISSING COLUMN: ' . $table['Tables_in_' . $fvmDBString] . ' => ' . $column['Field'] . '<br />';
            }
        }
    }
}