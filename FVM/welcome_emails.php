<?php
require('../swiftmailer/swift_required.php');

function sendWelcomeEmail($username) {
    $welcomeEmail = '
        <html>
        <head></head>
        <body style="color:#000001;">
        <div style="margin:0 auto;width:970px;background-color:#C2E0F6;color:#000001;padding:10px 0">
            <div style="width:910px;background-color:#FFF;border-radius: 30px 30px 30px 30px;border: 5px solid #0078C1;margin: 5px auto;">
                <h1 style="font-size:38px;width:850px;margin-left:10px;margin-top:10px;margin-bottom:0;">
                    <img alt="First Voice Manager" src="http://firstvoicemanager.com/images/FVMLogo.png">
                </h1>
                <div style="width:100%;height:30px;margin-bottom:5px;background-color:#0078C1;float:left; margin-top:5px;"></div>
                <h2 style="width:850px;margin-left:10px;color:#000001;font-size:18pt;clear:both;">
                    Welcome
                </h2>
                <div style="width:850px;font-size:12pt;margin-bottom:10px; margin-left: 10px; ">
                    You have recently been invited to join <a href="http://firstvoicemanager.com/">First Voice Manager</a>. This
                    is a program that allows you
                    to manage your equipment such as AEDs and first aid kits.<br/>
                    <b>Logging into First Voice Manager is simple!</b>
                    <div style="margin-left:10px;font-weight:bold;">
                        Username: '.$username.'<br/>
                        Password: NYANG1!
                    </div>
                    If you think you got this email by accident, please disregard.
                    <br/>
                    If you have any questions, please feel free to contact us at <b>support@firstvoicemanager.com</b>.
                </div>
            </div>
        </div>
        <div style="width:970px;margin:0 auto;color:#000001;background-color:white;">Please do not reply to this email. We are
            unable to respond to inquiries sent to this address.
        </div>
        <br/>
        <div style="width:970px;margin:0 auto;color:#000001;">Copyright &copy; 2013 Think Safe Inc.</div>
        </body>
        </html>
    ';

    $transport = Swift_SmtpTransport::newInstance('server.tsdemos.com', 25)
        ->setUsername('donotreply@firstvoicemanager.com')
        ->setPassword('1105firstvoice');
    $mailer = Swift_Mailer::newInstance($transport);
    $message = Swift_Message::newInstance()
        ->setSubject('Welcome to First Voice Manager')
        ->setFrom(array('donotreply@firstvoicemanager.com' => 'First Voice Manager'))
        ->setTo($username)
        ->setBody($welcomeEmail, 'text/html');
    $result = $mailer->send($message);
}


if (($handle = fopen("welcome_users.csv", "r")) !== FALSE) {
    $line = 0;
    while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
        if($line == 0) {
            // Skip Header
        } else {
            echo 'Emailing \''.$data[0].'\' ... ';
            sendWelcomeEmail($data[0]);
            echo 'COMPLETE<br />';
        }
        $line++;
    }
    echo '<br />Emails Sent: '.($line-1).'<br />';
    fclose($handle);
} else {
    echo 'ERRROR: opening file';
}

    echo $user['username'] . '<br />';
?>