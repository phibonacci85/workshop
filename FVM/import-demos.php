<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 8/15/2016
 * Time: 16:22
 */

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $db = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew');
@ $r7db = new mysqli('fvdemos.com', 'fvdemos_rescue7', 'z3zX144EJx7z', 'fvdemos_rescue7manager');

$locations = $db->query("
    select *
    from locations
    where province = 'IA'
    and latitude != '0'
    and longitude != '0'
");
$count = 1;
while($location = $locations->fetch_assoc()) {
    $aeds = $db->query("
        select *
        from aeds
        where location_id = '".$location['id']."'
    ");
    while($aed = $aeds->fetch_assoc()) {
        $insertAED = $r7db->query("
            insert into aeds (
              aed_id, 
              aed_model_id, 
              serialnumber, 
              installdate, 
              sitecoordinator, 
              purchasetype, 
              purchasedate, 
              warranty, 
              location, 
              plantype, 
              plandate, 
              planexpiration, 
              current, 
              hasservicecheck, 
              servicechecklength, 
              lastcheck, 
              lastservice, 
              pad_program, 
              notes, 
              location_id, 
              creation, 
              updated, 
              display
            ) VALUES (
              '".$aed['aed_id']."', 
              '".$aed['aed_model_id']."', 
              '".$aed['serialnumber']."', 
              '".$aed['installdate']."', 
              'Site Coordinator', 
              '".$aed['purchasetype']."', 
              '".$aed['purchasedate']."', 
              '".$aed['warranty']."', 
              '".$aed['location']."', 
              '".$aed['plantype']."', 
              '".$aed['plandate']."', 
              '".$aed['planexpiration']."', 
              '".$aed['current']."', 
              '".$aed['hasservicecheck']."', 
              '".$aed['servicechecklength']."', 
              '".$aed['lastcheck']."', 
              '".$aed['lastservice']."', 
              '".$aed['pad_program']."', 
              '".$aed['notes']."', 
              '".$aed['location_id']."', 
              '".$aed['creation']."', 
              '".$aed['updated']."', 
              '".$aed['display']."'
            )
        ");
    }
}