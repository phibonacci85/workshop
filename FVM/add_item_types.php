<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 11/28/2016
 * Time: 10:26
 */
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}

@ $fvmdb = new mysqli('localhost', 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_fvmnew_demo');

$types = $fvmdb->query("
    SELECT *
    FROM aed_battery_types
");
while ($type = $types->fetch_assoc()) {
    $alternates = $fvmdb->query("
        SELECT *
        FROM aed_consumables_alternate_part_numbers
        WHERE alternate_part_number = '" . $type['partnum'] . "'
    ");
    if ($alternates->num_rows == 0) {
        $insertAlternate = $fvmdb->query("
            INSERT INTO aed_consumables_alternate_part_numbers (
              consumable_type, 
              consumable_id, 
              alternate_part_number
            ) VALUES (
              'battery', 
              '" . $type['aed_battery_type_id'] . "', 
              '" . $type['partnum'] . "'
            )
        ");
    }
}

$types = $fvmdb->query("
    SELECT *
    FROM aed_pad_types
");
while ($type = $types->fetch_assoc()) {
    $alternates = $fvmdb->query("
        SELECT *
        FROM aed_consumables_alternate_part_numbers
        WHERE alternate_part_number = '" . $type['partnum'] . "'
    ");
    if ($alternates->num_rows == 0) {
        $insertAlternate = $fvmdb->query("
            INSERT INTO aed_consumables_alternate_part_numbers (
              consumable_type, 
              consumable_id, 
              alternate_part_number
            ) VALUES (
              'pad', 
              '" . $type['aed_pad_type_id'] . "', 
              '" . $type['partnum'] . "'
            )
        ");
    }
}

$types = $fvmdb->query("
    SELECT *
    FROM aed_pak_types
");
while ($type = $types->fetch_assoc()) {
    $alternates = $fvmdb->query("
        SELECT *
        FROM aed_consumables_alternate_part_numbers
        WHERE alternate_part_number = '" . $type['partnum'] . "'
    ");
    if ($alternates->num_rows == 0) {
        $insertAlternate = $fvmdb->query("
            INSERT INTO aed_consumables_alternate_part_numbers (
              consumable_type, 
              consumable_id, 
              alternate_part_number
            ) VALUES (
              'pak', 
              '" . $type['aed_pak_type_id'] . "', 
              '" . $type['partnum'] . "'
            )
        ");
    }
}

$types = $fvmdb->query("
    SELECT *
    FROM aed_accessory_types
");
while ($type = $types->fetch_assoc()) {
    $alternates = $fvmdb->query("
        SELECT *
        FROM aed_consumables_alternate_part_numbers
        WHERE alternate_part_number = '" . $type['partnum'] . "'
    ");
    if ($alternates->num_rows == 0) {
        $insertAlternate = $fvmdb->query("
            INSERT INTO aed_consumables_alternate_part_numbers (
              consumable_type, 
              consumable_id, 
              alternate_part_number
            ) VALUES (
              'accessory', 
              '" . $type['aed_accessory_type_id'] . "', 
              '" . $type['partnum'] . "'
            )
        ");
    }
}