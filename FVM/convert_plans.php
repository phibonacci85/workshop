<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 12/9/2016
 * Time: 10:34
 */

//@ $fvmdb = new mysqli($host, 'tsdemosc', 'Hawkeye17!', 'tsdemosc_fvmnew');
@ $fvmdb = new mysqli('fvdemos.com', 'fvdemos', 'Hawkeye17!', 'fvdemos_rescueone_2017_1_6_copy');
/*
$aeds = $fvmdb->query("
    select a.*, l.id as lid, o.id as oid
    from aeds a
    join locations l on a.location_id = l.id
    join organizations o on l.orgid = o.id
");
while($aed = $aeds->fetch_assoc()) {
    $insertPlan = $fvmdb->query("
        insert into plans (
          org_id, 
          type, 
          start_date, 
          expiration, 
          number_aeds, 
          notes, 
          company_name, 
          license, 
          phone, 
          cell, 
          fax, 
          email, 
          street, 
          city, 
          state, 
          zip, 
          country, 
          last_review, 
          next_review, 
          display
        ) values (
          '".$aed['oid']."', 
          '".$aed['plantype']."', 
          '".$aed['plandate']."', 
          '".$aed['planexpiration']."', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          '', 
          ''
        )
    ");
}
*/

$organizations = $fvmdb->query("
    SELECT *
    FROM organizations
    WHERE display = 'yes'
");
while ($organization = $organizations->fetch_assoc()) {
    $locations = $fvmdb->query("
        SELECT *
        FROM locations
        WHERE display = 'yes'
        AND orgid = '" . $organization['id'] . "'
    ");
    while ($location = $locations->fetch_assoc()) {
        /*
        $medicalDirections = $fvmdb->query("
            SELECT *
            FROM medicaldirection
            WHERE display = 'yes'
            AND locationid = '" . $location['id'] . "'
            AND (
                NAME LIKE '%M.D.%'
                OR NAME LIKE '%medical%direction%'
                OR NAME LIKE '%oversight%'
                OR NAME LIKE '%md0%'
                OR NAME LIKE '% MD%'
            )
            ORDER BY str_to_date(enddate, '%m/%d/%Y') DESC
        ");
        $programManagements = $fvmdb->query("
            SELECT *
            FROM medicaldirection
            WHERE display = 'yes'
            AND locationid = '" . $location['id'] . "'
            AND (
                NAME NOT LIKE '%M.D.%'
                AND NAME NOT LIKE '%medical%direction%'
                AND NAME NOT LIKE '%oversight%'
                AND NAME NOT LIKE '%md0%'
                AND NAME NOT LIKE '% MD%'
            )
            ORDER BY str_to_date(enddate, '%m/%d/%Y') DESC
        ");
        */
        $plans = $fvmdb->query("
            SELECT *
            FROM (
              (
                SELECT *, 'medicaldirection' AS type
                FROM medicaldirection
                WHERE display = 'yes'
                AND locationid = '" . $location['id'] . "'
                AND (
                  NAME LIKE '%M.D.%'
                  OR NAME LIKE '%medical%direction%'
                  OR NAME LIKE '%oversight%'
                  OR NAME LIKE '%md0%'
                  OR NAME LIKE '% MD%'
                )
              )
              UNION ALL
              (
                SELECT *, 'programmanagement' AS type
                FROM medicaldirection
                WHERE display = 'yes'
                AND locationid = '" . $location['id'] . "'
                AND (
                  NAME NOT LIKE '%M.D.%'
                  AND NAME NOT LIKE '%medical%direction%'
                  AND NAME NOT LIKE '%oversight%'
                  AND NAME NOT LIKE '%md0%'
                  AND NAME NOT LIKE '% MD%'
                )
              )
            )results
            ORDER BY str_to_date(enddate, '%m/%d/%Y') DESC
        ");
        if ($plans->num_rows > 0) {
            $plan = $plans->fetch_assoc();
            $insertPlan = $fvmdb->query("
                INSERT INTO plans (
                  org_id, 
                  type, 
                  start_date, 
                  expiration, 
                  number_aeds, 
                  service_check_length, 
                  notes, 
                  company_name, 
                  license, 
                  phone, 
                  cell, 
                  fax, 
                  email, 
                  street, 
                  city, 
                  state, 
                  zip, 
                  country, 
                  last_review, 
                  next_review, 
                  display
                ) VALUES (
                  '" . $organization['id'] . "', 
                  '" . $plan['type'] . "', 
                  '" . $plan['startdate'] . "',
                  '" . $plan['enddate'] . "', 
                  '', 
                  '', 
                  '" . $plan['notes'] . "', 
                  '" . $plan['company'] . "', 
                  '" . $plan['license'] . "', 
                  '" . $plan['phone'] . "', 
                  '" . $plan['cell'] . "', 
                  '" . $plan['fax'] . "', 
                  '" . $plan['email'] . "', 
                  '" . $plan['address'] . "', 
                  '" . $plan['city'] . "', 
                  '" . $plan['province'] . "', 
                  '" . $plan['zip'] . "', 
                  '" . $plan['country'] . "', 
                  '" . $plan['reviewdate'] . "', 
                  '" . $plan['nextreviewdate'] . "', 
                  '1'
                )
            ");
            $planId = $fvmdb->insert_id;
            $updateLocation = $fvmdb->query("
                UPDATE locations
                SET plan_id = '" . $planId . "'
                WHERE id = '" . $location['id'] . "'
            ");
            while ($plan = $plans->fetch_assoc()) {
                $insertPlan = $fvmdb->query("
                    INSERT INTO plans (
                      org_id, 
                      type, 
                      start_date, 
                      expiration, 
                      number_aeds, 
                      service_check_length, 
                      notes, 
                      company_name, 
                      license, 
                      phone, 
                      cell, 
                      fax, 
                      email, 
                      street, 
                      city, 
                      state, 
                      zip, 
                      country, 
                      last_review, 
                      next_review, 
                      display
                    ) VALUES (
                      '" . $organization['id'] . "', 
                      '" . $plan['type'] . "', 
                      '" . $plan['startdate'] . "',
                      '" . $plan['enddate'] . "', 
                      '', 
                      '', 
                      '" . $plan['notes'] . "', 
                      '" . $plan['company'] . "', 
                      '" . $plan['license'] . "', 
                      '" . $plan['phone'] . "', 
                      '" . $plan['cell'] . "', 
                      '" . $plan['fax'] . "', 
                      '" . $plan['email'] . "', 
                      '" . $plan['address'] . "', 
                      '" . $plan['city'] . "', 
                      '" . $plan['province'] . "', 
                      '" . $plan['zip'] . "', 
                      '" . $plan['country'] . "', 
                      '" . $plan['reviewdate'] . "', 
                      '" . $plan['nextreviewdate'] . "', 
                      '1'
                    )
                ");
            }
        }/*
        if ($medicalDirections->num_rows > 0 || $programManagements->num_rows > 0) {
            if ($medicalDirection = $medicalDirections->fetch_assoc()) {
                $insertPlan = $fvmdb->query("
                INSERT INTO plans (
                  org_id, 
                  type, 
                  start_date, 
                  expiration, 
                  number_aeds, 
                  service_check_length, 
                  notes, 
                  company_name, 
                  license, 
                  phone, 
                  cell, 
                  fax, 
                  email, 
                  street, 
                  city, 
                  state, 
                  zip, 
                  country, 
                  last_review, 
                  next_review, 
                  display
                ) VALUES (
                  '" . $organization['id'] . "', 
                  'medicaldirection', 
                  '" . $medicalDirection['startdate'] . "',
                  '" . $medicalDirection['enddate'] . "', 
                  '', 
                  '', 
                  '" . $medicalDirection['notes'] . "', 
                  '" . $medicalDirection['company'] . "', 
                  '" . $medicalDirection['license'] . "', 
                  '" . $medicalDirection['phone'] . "', 
                  '" . $medicalDirection['cell'] . "', 
                  '" . $medicalDirection['fax'] . "', 
                  '" . $medicalDirection['email'] . "', 
                  '" . $medicalDirection['address'] . "', 
                  '" . $medicalDirection['city'] . "', 
                  '" . $medicalDirection['province'] . "', 
                  '" . $medicalDirection['zip'] . "', 
                  '" . $medicalDirection['country'] . "', 
                  '" . $medicalDirection['reviewdate'] . "', 
                  '" . $medicalDirection['nextreviewdate'] . "', 
                  '1'
                )
            ");
                $planId = $fvmdb->insert_id;
                $updateLocation = $fvmdb->query("
                UPDATE locations
                SET plan_id = '" . $planId . "'
                WHERE id = '" . $location['id'] . "'
            ");
            } else if ($programManagement = $programManagements->fetch_assoc()) {
                $insertPlan = $fvmdb->query("
                INSERT INTO plans (
                  org_id, 
                  type, 
                  start_date, 
                  expiration, 
                  number_aeds, 
                  service_check_length, 
                  notes, 
                  company_name, 
                  license, 
                  phone, 
                  cell, 
                  fax, 
                  email, 
                  street, 
                  city, 
                  state, 
                  zip, 
                  country, 
                  last_review, 
                  next_review, 
                  display
                ) VALUES (
                  '" . $organization['id'] . "', 
                  'programmanagement', 
                  '" . $programManagement['startdate'] . "',
                  '" . $programManagement['enddate'] . "', 
                  '', 
                  '', 
                  '" . $programManagement['notes'] . "', 
                  '" . $programManagement['company'] . "', 
                  '" . $programManagement['license'] . "', 
                  '" . $programManagement['phone'] . "', 
                  '" . $programManagement['cell'] . "', 
                  '" . $programManagement['fax'] . "', 
                  '" . $programManagement['email'] . "', 
                  '" . $programManagement['address'] . "', 
                  '" . $programManagement['city'] . "', 
                  '" . $programManagement['province'] . "', 
                  '" . $programManagement['zip'] . "', 
                  '" . $programManagement['country'] . "', 
                  '" . $programManagement['reviewdate'] . "', 
                  '" . $programManagement['nextreviewdate'] . "', 
                  '1'
                )
            ");
                $planId = $fvmdb->insert_id;
                $updateLocation = $fvmdb->query("
                UPDATE locations
                SET plan_id = '" . $planId . "'
                WHERE id = '" . $location['id'] . "'
            ");
            }
        }*/ else {
            $aeds = $fvmdb->query("
                SELECT *
                FROM aeds
                WHERE display = 1
                AND location_id = '" . $location['id'] . "'
            ");
            $plan = 'none';
            $startDate = DateTime::createFromFormat('m/d/Y', '01/01/1969');
            $expiration = DateTime::createFromFormat('m/d/Y', '01/01/1969');
            $aedCount = $aeds->num_rows;
            $serviceCheckLength = 0;
            while ($aed = $aeds->fetch_assoc()) {
                if ($aed['plantype'] != 'medicaldirection' && $plan == 'medicaldirection') continue;
                $nextStartDate = DateTime::createFromFormat('m/d/Y', $aed['startdate']);
                $nextExpiration = DateTime::createFromFormat('m/d/Y', $aed['expiration']);
                if ($nextExpiration > $expiration) {
                    $startDate = $nextStartDate;
                    $expiration = $nextExpiration;
                    $plan = $aed['plantype'];
                    $serviceCheckLength = $aed['servicechecklength'];
                }
            }
            if ($plan != 'none') {
                $insertPlan = $fvmdb->query("
                INSERT INTO plans (
                  org_id, 
                  type, 
                  start_date, 
                  expiration, 
                  number_aeds, 
                  service_check_length, 
                  notes, 
                  company_name, 
                  license, 
                  phone, 
                  cell, 
                  fax, 
                  email, 
                  street, 
                  city, 
                  state, 
                  zip, 
                  country, 
                  last_review, 
                  next_review, 
                  display
                ) VALUES (
                  '" . $organization['id'] . "', 
                  '" . $plan . "', 
                  '" . $startDate->format('m/d/Y') . "', 
                  '" . $expiration->format('m/d/Y') . "', 
                  '" . $aedCount . "', 
                  '" . $serviceCheckLength . "', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '', 
                  '1'
                )
            ");
                $planId = $fvmdb->insert_id;
                $updateLocation = $fvmdb->query("
                UPDATE locations
                SET plan_id = '" . $planId . "'
                WHERE id = '" . $location['id'] . "'
            ");
            }
        }
    }
}