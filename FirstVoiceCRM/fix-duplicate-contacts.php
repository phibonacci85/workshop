<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 8/15/2016
 * Time: 10:16
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $crmdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicecrm_testing');
$count = 0;
$uniqueContacts = array();
$contacts = $crmdb->query("
    SELECT *
    FROM contacts
    WHERE active = '1'
");
while ($contact = $contacts->fetch_assoc()) {
    $firstName = str_replace("'", "''", $contact['first_name']);
    $lastName = str_replace("'", "''", $contact['last_name']);

    $duplicates = $crmdb->query("
        SELECT *
        FROM contacts
        WHERE contact_id != '" . $contact['contact_id'] . "'
        AND first_name = '" . $firstName . "'
        AND last_name = '" . $lastName . "'
        AND account_id = '" . $contact['account_id'] . "'
        AND active = '1'
    ");
    while ($duplicate = $duplicates->fetch_assoc()) {
        if(in_array($duplicate['contact_id'], $uniqueContacts)) continue;

        echo '<pre>' . print_r($contact, true) . '</pre>';
        echo '<pre>' . print_r($duplicate, true) . '</pre>';
        echo '<br /><hr />';

        $updateCalls = $crmdb->query("
            UPDATE calls
            SET name_id = '" . $contact['contact_id'] . "'
            WHERE name_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateCase = $crmdb->query("
            UPDATE cases
            SET contact_id = '" . $contact['contact_id'] . "'
            WHERE contact_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateContactCourses = $crmdb->query("
            UPDATE contact_courses
            SET contact_id = '" . $contact['contact_id'] . "'
            WHERE contact_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateReportsTo = $crmdb->query("
            UPDATE contacts
            SET reports_to = '" . $contact['contact_id'] . "'
            WHERE reports_to = '" . $duplicate['contact_id'] . "'
        ");

        $updateCreditCards = $crmdb->query("
            UPDATE credit_cards
            SET contact_id = '" . $contact['contact_id'] . "'
            WHERE contact_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateEmails = $crmdb->query("
            UPDATE email_relations
            SET parent_id = '" . $contact['contact_id'] . "'
            WHERE parent_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateEvents = $crmdb->query("
            UPDATE events
            SET name_id = '" . $contact['contact_id'] . "'
            WHERE name_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateNotes = $crmdb->query("
            UPDATE notes
            SET parent_id = '" . $contact['contact_id'] . "'
            WHERE parent_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateOpts = $crmdb->query("
            UPDATE opportunities
            SET parent_id = '" . $contact['contact_id'] . "'
            WHERE parent_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateSupportTickets = $crmdb->query("
            UPDATE support_tickets
            SET contact_id = '" . $contact['contact_id'] . "'
            WHERE contact_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateTasks = $crmdb->query("
            UPDATE tasks
            SET name_id = '" . $contact['contact_id'] . "'
            WHERE name_id = '" . $duplicate['contact_id'] . "'
        ");

        $updateTrainerInfo = $crmdb->query("
            UPDATE trainer_info
            SET contact_id = '" . $contact['contact_id'] . "'
            WHERE contact_id = '" . $duplicate['contact_id'] . "'
        ");

        $deactivateContact = $crmdb->query("
            UPDATE contacts
            SET active = '0'
            WHERE contact_id = '" . $duplicate['contact_id'] . "'
        ");

        $uniqueContacts[] = $contact['contact_id'];
    }
}
echo 'Count: ' . $count;