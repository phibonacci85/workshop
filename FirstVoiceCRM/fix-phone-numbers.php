<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 1/11/2017
 * Time: 11:10
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $crmdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicecrm');

$accounts = $crmdb->query("
    SELECT *
    FROM accounts
");
while ($account = $accounts->fetch_assoc()) {
    $phoneDigits = preg_replace("/[^0-9]/", "", $account['phone']);
    $updateAccounts = $crmdb->query("
        UPDATE accounts
        SET phone = '" . $phoneDigits . "'
        WHERE account_id = '" . $account['account_id'] . "'
    ");
}

$contacts = $crmdb->query("
    SELECT *
    FROM contacts
");
while ($contact = $contacts->fetch_assoc()) {
    $phoneDigits = preg_replace("/[^0-9]/", "", $contact['phone']);
    $faxDigits = preg_replace("/[^0-9]/", "", $contact['fax']);
    $mobileDigits = preg_replace("/[^0-9]/", "", $contact['mobile']);
    $updateContacts = $crmdb->query("
        UPDATE contacts
        SET phone = '" . $phoneDigits . "', 
            fax = '" . $faxDigits . "', 
            mobile = '" . $mobileDigits . "'
        WHERE contact_id = '" . $contact['contact_id'] . "'
    ");
}

$leads = $crmdb->query("
    SELECT *
    FROM leads
");
while ($lead = $leads->fetch_assoc()) {
    $phoneDigits = preg_replace("/[^0-9]/", "", $lead['phone']);
    $mobileDigits = preg_replace("/[^0-9]/", "", $lead['mobile']);
    $updateLeads = $crmdb->query("
        UPDATE leads
        SET phone = '" . $phoneDigits . "', 
            mobile = '" . $mobileDigits . "'
        WHERE lead_id = '" . $lead['lead_id'] . "'
    ");
}

$medicalDirections = $crmdb->query("
    SELECT *
    FROM medical_direction_contacts
");
while ($medicalDirection = $medicalDirections->fetch_assoc()) {
    $phoneDigits = preg_replace("/[^0-9]/", "", $medicalDirection['phone']);
    $cellDigits = preg_replace("/[^0-9]/", "", $medicalDirection['cell']);
    $faxDigits = preg_replace("/[^0-9]/", "", $medicalDirection['fax']);
    $updateMDContacts = $crmdb->query("
        UPDATE medical_direction_contacts
        SET phone = '" . $phoneDigits . "', 
            fax = '" . $faxDigits . "', 
            cell = '" . $cellDigits . "'
        WHERE medical_direction_contact_id = '" . $medicalDirection['medical_direction_contact_id'] . "'
    ");
}