<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 8/2/2016
 * Time: 4:10 PM
 */
//Tobias Id: 00532000004zEQQAA2
//Mike Id: 00560000003YkFOAA0
$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $db = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_places_search');
@ $crmdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicecrm');
$table = '24hourfitness';
$campaignId = '57bf4943af630';
$ownerId = '00532000004zEQQAA2';
$emailTitle = '24 Hour Fitness';
$locations = $db->query("
    SELECT *
    FROM " . $table . "
    WHERE NAME LIKE '%" . $emailTitle . "%'
    OR NAME LIKE '%" . str_replace(' ', '', $emailTitle) . "%'
    OR website LIKE '%" . str_replace(' ', '', $emailTitle) . "%'
");
while ($location = $locations->fetch_assoc()) {
    $details_url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" . $location['place_id'] . "&key=AIzaSyB-4-rvJiGamu1_aPDrwGYlqtxbqp14Lfo";
    $details_json = file_get_contents($details_url);
    $details = json_decode($details_json, true);
    $name = $details['result']['name'];
    $address = $details['result']['formatted_address'];
    $phone = $details['result']['formatted_phone_number'];
    $website = $details['result']['website'];
    $icon = $details['result']['icon'];
    $latitude = $details['result']['geometry']['location']['lat'];
    $longitude = $details['result']['geometry']['location']['lng'];

    $streetNumber = '';
    $route = '';
    $city = '';
    $state = '';
    $zip = '';
    $country = '';
    foreach ($details['result']['address_components'] as $component) {
        if (in_array('street_number', $component['types'])) {
            $streetNumber = $component['long_name'];
        } else if (in_array('route', $component['types'])) {
            $route = $component['short_name'];
        } else if (in_array('locality', $component['types'])) {
            $city = $component['long_name'];
        } else if (in_array('administrative_area_level_1', $component['types'])) {
            $state = $component['short_name'];
        } else if (in_array('postal_code', $component['types'])) {
            $zip = $component['short_name'];
        } else if (in_array('country', $component['types'])) {
            $country = $component['short_name'];
        }
    }

    $street = $streetNumber . ' ' . $route;
    $uniqueLeadId = uniqid('');
    $insertLead = $crmdb->query("
        INSERT INTO leads (
          lead_id,
          salutation,
          first_name,
          last_name,
          middle_name,
          suffix,
          title,
          company,
          phone,
          extension,
          mobile,
          email,
          status,
          club_status,
          website,
          industry,
          employees,
          lead_source,
          rating,
          description,
          referred_by,
          owner_id,
          address,
          city,
          state,
          postal_code,
          country,
          converted,
          converted_date,
          converted_account_id,
          campaign_id,
          attempted_calls,
          last_call_date,
          sent_email_date,
          verified_email_date,
          open_date,
          created_by_id,
          created_date,
          modified_by_id,
          modified_date,
          active
        ) VALUES (
          '" . $uniqueLeadId . "',
          '',
          'No',
          'Contact',
          '',
          '',
          '',
          '" . $emailTitle . "',
          '" . $phone . "',
          '',
          '',
          '',
          'New',
          '',
          '" . $website . "',
          'Fitness',
          '',
          'Website',
          'Warm',
          '',
          '',
          '" . $ownerId . "',
          '" . $street . "',
          '" . $city . "',
          '" . $state . "',
          '" . $zip . "',
          '" . $country . "',
          '0',
          '',
          '',
          '" . $campaignId . "',
          '0',
          '',
          '',
          '',
          '',
          '1',
          '" . date('Y-m-d H:i:s') . "',
          '',
          '',
          '1'
        )
    ");
}