<?php
/**
 * Created by PhpStorm.
 * User: John Baker
 * Date: 1/11/2017
 * Time: 12:52
 */

$host = 'localhost';
if ($_SERVER['HTTP_HOST'] == 'localhost:3141') {
    $host = 'tsdemos.com';
}
@ $crmdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicecrm');
@ $crmTestingdb = new mysqli($host, 'tsdemosc_jbaker', 'Jbake1234', 'tsdemosc_firstvoicecrm_testing');
//$crmTestingdb = new mysqli('localhost', 'root', 'root', 'firstvoice_crm_testing');

$origs = $crmTestingdb->query("
    SELECT *
    FROM leads
");
while ($orig = $origs->fetch_assoc()) {
    $leads = $crmdb->query("
        SELECT *
        FROM leads
        WHERE lead_id = '" . $orig['lead_id'] . "'
    ");
    if ($lead = $leads->fetch_assoc()) {
        if ($lead['phone'] == '' && $orig['phone'] != '') {
            $updateLeads = $crmdb->query("
                UPDATE leads
                SET phone = '" . preg_replace("/[^0-9]/", "", $orig['phone']) . "'
                WHERE lead_id = '" . $orig['lead_id'] . "'
            ");
        }
    }
}