<?php	
	if(isset($_GET['content']))
	{
		if(isset($_COOKIE['username']) && $_COOKIE['username'] != "Visitor")
			if (file_exists('includes/'.$_GET['content'].'.php')) {
				$course = $_GET['content'];
			} else 
				$course = "404";
		else {
			if(($_GET['content'] == "login") || ($_GET['content'] == "register") || ($_GET['content'] == "forgot") || ($_GET['content'] == "firstregister") ||($_GET['content'] == "contact") || ($_GET['content'] == "curriculum") 
			// misc files that should be moved
			|| ($_GET['content'] == "googlealerts") 
			// calendar portion of the site
			|| ($_GET['content'] == "calendar") || ($_GET['content'] == "calendarevent") || ($_GET['content'] == "registerinperson") )
				$course = $_GET['content'];
			else 
				$course = 'calendar';
		}
	} else 
		$course = 'calendar';
		
	require_once('../scripts/library.php');	
	require_once('../includes/calculations.php');
	require_once('../lib/swift_required.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<title>
		<?php 
			if (isset($_GET['content'])) {
				echo 'FirstVoice - ' . $_GET['content'] . ' page';
			} else {
				echo 'FirstVoice';
			}
		?>
	</title>
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-27439370-1']);
		_gaq.push(['_trackPageview']);
		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
		
		function toggleMe(expand, arrow)
		{
			var exp = document.getElementById(expand);
			var arr = document.getElementById(arrow);
			if(!exp)return true;
			//document.write(arrow);
			if(arr.className == "unflipped" || arr.className == "unflippedcontact")
			{
				if(arrow == "contactarrow" || arrow == "reportarrow" || arrow == "insertarrow" || arrow == "editarrow" || arrow == "salesarrow" || arrow == "inpersonarrow")
				{
					arr.className = "flip-verticalcontact";
					//arrow.style.padding.top = "5px";
				} else
					arr.className = "flip-vertical";
			} else {
				if(arrow == "contactarrow" || arrow == "reportarrow" || arrow == "insertarrow" || arrow == "editarrow" || arrow == "salesarrow" || arrow == "inpersonarrow")
				{
					arr.className = "unflippedcontact";
				} else {
				arr.className = "unflipped";
				}
			}
			
			if(exp.style.display == "none")
			{
				exp.style.display = "inherit";
				createCookie(arrow, 'down', 10);
			} else {
				exp.style.display = "none";
				createCookie(arrow, 'up', 10);
			}
			return true;
		}
		// w3schools cookie functions
		function createCookie(name,value,days) {
			if (days) {
				var date = new Date();
				date.setTime(date.getTime()+(days*24*60*60*1000));
				var expires = "; expires="+date.toGMTString();
			}
			else var expires = "";
			document.cookie = name+"="+value+expires+"; path=/";
		}

		/*
		function setCookie(c_name,value,exdays)
		{
			var exdate=new Date();
			exdate.setDate(exdate.getDate() + exdays);
			var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
			document.cookie=c_name + "=" + c_value;
		}
		
		function checkCookie()
		{
			var username=getCookie("username");
			if (username!=null && username!="")
			{
				return true;
			} else {
				return false;
			}
		}
		*/
	</script>
<?php
	if($course == 'review' || $course == 'course')
	{
		echo '<script type="text/javascript" src="scripts/audio-player.js"></script>  
        <script type="text/javascript">  
            AudioPlayer.setup("/images/audioplayer.swf", {  
                width: 560,
				autostart: "yes",
				lefticon: "FFFFFF",
				animation: "no",
				bg: "0078C1",
				leftbg: "0078C1",
				rightbg: "0078C1",
				rightbghover: "C2E0F6",
				righticonhover: "0078C1",
				volslider: "C2E0F6",
				righticon: "C2E0F6",
				loader: "0078C1",
				tracker: "C2E0F6"
            });  
        </script>';
	}
	
	if($course == 'mailme')
	{
		if(isset($_COOKIE['username']) && isset($_GET['courseName']))
		{
			$real = jk_checkrealcourse($_COOKIE['username'], $_GET['courseName']);
			if($real == 'yes')
			{
				$completed = jk_checkcompleted($_COOKIE['username'], $_GET['courseName']);
				if($completed == 'yes')
				{
					if( (isset($_POST['name']) && !empty($_POST['name'])) &&
					(isset($_POST['address']) && !empty($_POST['address'])) &&
					(isset($_POST['phone']) && !empty($_POST['phone'])) &&
					(isset($_POST['city']) && !empty($_POST['city'])) &&
					(isset($_POST['state']) && !empty($_POST['state'])) &&
					(isset($_POST['zip']) && !empty($_POST['zip'])) )
					{
						echo "<script  id='googlecart-script' type='text/javascript' src='https://checkout.google.com/seller/gsc/v2_2/cart.js?mid=210914267962142' 
						integration='jscart-wizard' post-cart-to-sandbox='false' currency='USD' productWeightUnits='LB'></script>";
					}
				} else
					$course = 'calendar';
			} else
				$course = 'calendar';
		} else
			$course = 'calendar';
	}
?>
</head>

<body>
<div id="container">
	<?php
	
		include('includes/header.php');
		include('includes/nav.php');
		
		include('includes/'.$course.'.php'); // middle sheet, picked based on get statement
	
		
		include('includes/footer.php');
	?>
	
</div> <!-- end of container div -->
</html>
